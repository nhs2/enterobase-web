HierCC equivalents
==============================

*Salmonella* database
--------------------------
* **HC900 & HC2000:** HC900 is equivalent to eBG (eBurstGroup) in legacy 7-gene MLST, which is shown to be natural population that is consistent with serovars. Thus we have given HC900 a trivial name as "ceBG" (cgMLST eBurstGroup). We also exploited a further clustering of cgSTs at HC2000 level. salmonellae of different eBG groups (serovars) grouped together at this level. 

`A mapping table between HC900 and HC2000 versus serovars <https://bitbucket.org/enterobase/enterobase-web/raw/b7444ac566bfd59c60e79ec506f290c19e2f5eaa/docs/files/Salmonella_HC900_lookup.xlsx>`_

* **HC2850:** HC2850 in *Salmonella* corresponds to the separation of subspecies and species in the genus. 

`A mapping table between HC2850 and Salmonella species and Clades <https://bitbucket.org/enterobase/enterobase-web/raw/5fbd7da27e367cf7e4c2fe93a164687b1a77f73a/docs/files/Salmonella_HC2850_vs_subsp.xlsx>`_

*Escherichia* database
--------------------------
* **HC2350:** HC2350 in *Escherichia* corresponds to the separation of species and clades in *Escherichia* genus. 

`A mapping table between HC2350 and Escherichia species and Clades <https://bitbucket.org/enterobase/enterobase-web/raw/b7444ac566bfd59c60e79ec506f290c19e2f5eaa/docs/files/Escherichia_HC2350_lookup.xlsx>`_

*Yersinia* database
--------------------------
* **HC1490 & HC1520:** HC1520 in *Yersinia* corresponds to the separation of species and the Species Complexes defined in `Reuter et al. PNAS <https://www.pnas.org/content/111/18/6768>`_, and HC1490 represents subspecies. 

`A mapping table between HC1490 & HC1520 versus Yersinia species <https://bitbucket.org/enterobase/enterobase-web/raw/e037ef5e1812ccb1d62cae920efeace7171548b4/docs/files/Yersinia_HC1520_1490_vs_species.xlsx>`_

*Clostridioides* database
--------------------------

* **HC2500 & HC2000:** HC2500 & HC2000 corresponds to the separation of major clades in *Clostridioides*. 
* **HC150:** HC150 corresponds to the separation of *C. difficile* ribotypes. 
* **HC10:** HC10 corresponds to the separation of *C. difficile* global epidemic clones. 

`A mapping table between HierCC levels and *C. difficile* epidemics, ribotypes or clades <https://bitbucket.org/enterobase/enterobase-web/raw/742a1e436a6bfd20e767814169618ff8a0d35ece/docs/files/Clostridioides_correspondences.xlsx>`_
