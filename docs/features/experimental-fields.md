Experiment Fields Reference
===========================

#Achtman 7 Gene#
##ST##
##eBG##
##Serotype (Predicted)##
##Lineage##
##Subspecies##
##aroC##
##dnaN##
##hemD##
##hisD##
##purE##
##sucA##
##thrA##

#McNally 7 Gene#
##ST##

#Assembly stats#
##Status##
##Coverage##
##N50##
##Length##
##Species##
##Contig Number(>=200 bp)
##Low Quality Bases##
##Version##
##Assembly Barcode##
##Eye icon##

#CRISPOL#
##Name##
##CRISPOL Type##

#Annotation#
##GBK Format##
When available, there will be a clickable downwards arrow icon that downloads the annotation in gzipped GBK format.
##GFF Format##
When available, there will be a clickable downwards arrow icon that downloads the annotation in gzipped GFF format.

#wgMLST#
##Eye icon##
##ST##

#CRISPR#
##CRISPR type##

#Serotype Prediction (SISTR)#
##Serovar##
##Serogroup##
##H1##
##H2##

#rMLST#
##Eye icon##
##rST##
##RMLST eBG##
##Lineage##
##Serotype (Predicted)##

#cgMLST#
##Eye icon##
##ST##