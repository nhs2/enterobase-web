How to download annotations on the EnteroBase website
=====================================================
If a search has been carried out, it is possible to download annotations (where 
they exist - and they will not exist in the case of entries for strains from 
legacy MLST in the results).  Select Annotation from the Experimental Data drop
down menu (on the top right of the page).  Annotations can then be downloaded in 
gzipped GBK or GFF format byclicking on the icons with downward arrows in blue 
circles in the relevant column (for GBK or GFF) for the desired strain.  
Alternatively, multiple annotations may be downloaded by clicking in the box next 
to the desired strain in the column headed with a tick icon (just to the right 
of the "Uberstrain" column which is on the far left of the table).

  .. image:: https://bitbucket.org/repo/Xyayxn/images/3878669462-user_download_annotations_eg.png

