General Grid Manipulation
=========================
Creating a Subclass
-------------------
the sub class is created in the following code

.. code-block:: javascript
        
        SuperGrid.prototype = Object.create(BaseGrid.prototype);
        SuperGrid.prototype.constructor= SuperGrid;
        
        
        function SuperGrid(name,config,species,scheme,scheme_name){
                BaseGrid.call(this,arg1,arg2);
                //Do any extra initialization here
                this.extra_param=10
        
        };
        SuperGrid.prototype.method1=function(value){
                this.extra_param=value
        }

**Adding Options To the Context Menu**

Extra options can be added to the right click context menu with the following
code.The menu is constructed each time it is called thus you can have
conditional inclusion of menu items as well as disabling them

.. code-block:: javascript
        
        SuperGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){
            
            var self = this;
            var extramenu= BaseGrid.prototype.addToContextMenu.call(this,rowIndex,colIndex,target);
            extramenu.push ({
                     name: 'Modify Cell',
                     title: 'Modify Cell',
                     fun: function () {
                         //do stuff based on rowIndex,colIndex and target
                     },
                     disable:false
                 });
            
            return extramenu;
        };

**Adding Extra Cell Formatting**

This is done by adding an extra renderer to the grid in the form of a
function, which accepts the cell and the cell's value. The row Id, column
name and row index are also provided but rarely used. For example to color
any cells red in the column_1, whose value is greater than a thousand, the
following code would be used

.. code-block:: javascript
        
        grid.addExtraRenderer("column_1",function(cell,value,row_id,col_name_row_index){
                        if (value>1000){
                                $(cell).css("background-color","red");
                        }
        });

***Adding A Cell Handler**

If you want to add a function which is called when the cell is clicked (left
mouse button, then addCustomColumnHandler method should be used where you
pass he column's name and function whuch accepts the cell,row index and row
id. To add a handler which simply displays the cell's contents the following
code would be used


.. code-block:: javascript

        var self = this
        this.addCustomColumnHandler("status",function(cell,rowIndex,rowID){
                var col = self.getColumnIndex("status");
                var value = self.getValueAt(rowIndex,col);
                Enterobase.modalAlert(value)
            });

**Adding A custom Validator**

Validation is usually specified in a column' data, but a custom validator can
be added using the *addCustomCellValidator* method giving the name of the
column and the validation function. This function should call *updateErrors*
with the column name row id and either a blank string if the cell value is
valid or a short message explaining why the value is not. For example

.. code-block:: javascript
        
        var self = this
        this.addCustomCellValidator("column_1",function(colName,rowID,value){
                        msg="" 
                        if (value>1000){
                                        msg="Value too high"
                        }		
                        return self.updateErrors(colName,rowID,msg);
        }


Experiment Grids
----------------
Adding the Grid to the Scheme
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
By default the BaseExperimentGrid JavaScript class in
entero/static/js/table/base_experiment_grid.js is used to display the
scheme's data. If extra functionality is required, this class needs to be
subclassed and the name of the subclass and the file it is in needs to be
specified in the scheme's params


.. code-block:: javascript

        {...,"js_grid":"class name","js_grid_file":"class file"}


Creating the Grid
^^^^^^^^^^^^^^^^^
The file needs to be in entero/static/js/table and should be constructed as follows

.. code-block:: javascript

        MyGrid.prototype = Object.create(BaseExperimentGrid.prototype);
        MyGrid.prototype.constructor=MyGrid;
        
        function MyGrid(name,config,species,scheme,scheme_name){ 
                BaseExperimentGrid.call(this,name,config,species,scheme,scheme_name);
        }


The grid will contain the following variables

* species - the database name e.g Senterica
* species_name - the name of the spceies e.g. Salmonella
* scheme - the name(description) of the scheme
* strainGrid - the strain grid that this is associated with- the grids are kept in synch so row index and row id will be the same