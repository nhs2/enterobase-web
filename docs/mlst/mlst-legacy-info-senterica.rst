Protocols used for MLST of *Salmonella enterica*
==============================================
**Update information 03.01.2013:**  
The primers that we currently use for medium throughput sequencing are now on
this page. This procedure was described in `O'Farrell et al. 2012
Transforming microbial genotyping: A robotic pipeline for genotyping
bacterial strains. PLoS One. 7,
e48022. <http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0048022>`_

**Update information 26.07.2012:**  
A description of the MLST scheme and an overview of strains of S. enterica
subsp. enterica has now been published and can be downloaded as shown below.
This includes an overview of 500 serovars and 138 eBGs. `Achtman,M., Wain,J.,
Weill,F.-X., Nair,S., Zhou,Z., Sangal,V., Krauland,M.G., Hale,J.L.,
Harbottle,H., Uebeck,A., Dougan,J., Harrison,L.H., Brisse,S., S. enterica
MLST Study Group. 2012. Multilocus sequence typing as a replacement for
serotyping in Salmonella enterica. PLoS Pathogens. 8,
e1002776. <http://journals.plos.org/plospathogens/article?id=10.1371/journal.ppat.1002776>`_

**Update information 01.02.2012:**  
There has been a major change in the information content of the S. enterica
database. Two fields are now included, eBG and Lineage, which were previously
absent. eBG is an acronym for eBurstGroup, and is equivalent to ST Complex or
Clonal Complex in other bacterial MLST schemes. STs were assigned to a common
eBG if an ST contained 10 or more isolates or there were at least 2 STs
linked by identity at 6/7 alleles (so-called Single Locus Variants). Some
double locus variants STs were affiliated with an existing eBG if they
contained the same serovar. The field "Lineage" indicates which isolates are
in Lineage 3, which was defined by `Didelot et al., in PLoS Pathogens 7:
e1002191
(2011) <http://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1002191>`_.
Lineage 3 is equivalent to Clade B in prior publications by Didelot and
Gordon. An assignment to Lineage 3 was performed on the basis of a BAPS
analysis.

**Update information 27.05.2008:**  
The database has >2,300 entries currently and some mistaken information has
been corrected in the last few weeks. New primers are being listed today for
strains for which it is difficult to obtain PCR products or clean sequences.
These can be combined in various combinations with the pre-existing primers.
The database home is in the process of being moved to UCC, Cork and we hope
to finally begin soon on a long needed update of the interface and
functionality.

**Update information 24.08.2005:**  
Sylvain Brisse has developed new variants of the primers for amplification
which consist of the same primers listed below except that the 5' end
contains an added universal primer that is also used for sequencing.
Amplification is at the same temperature of 55°. These added universal
primers consist of Forwards: GTTTTCCCAGTCACGACGTTGTA and Reverse:
TTGTGAGCGGATAACAATTTC. These primers have the advantage that it is possible
to sequence more reactions with a common primer. Our experience has been
generally positive except that it is now easier to cross contaminate across
wells.

**Update information:**  
Major changes were made related to strains within the SARB collection as documented below:
18 January, 2005  
Reexamination of the Selander SARB and SARA collections have required a
number of changes. These changes are based on MLST of strains in the SARB
collections maintained by Ken Sanderson (Univ. of Calgary) after transmission
to the Danish Royal Veterinary Laboratory, Copenhagen, Howard Ochman (Univ.
of Arizona) and Fidelma Boyd (Cork Univ.). They also include the results from
reserotyping of several of the strains by Wolfgang Rabsch, RKI, Wernigerode.
The changes reflect several sequencing mistakes in the original analysis of
the Sanderson collection as well as strain mixups and contamination. The
following table lists changes made on the MLST WEB site. A separate table
lists all contradictions between current assignments and the supposed
serotypes published in Boyd et al., J. gen. Microbiol. 139: 1125-1132, 1993.

+-------+------------+-------------+-------------+--------------+ 
|Strain |Former Name |Former Number|Current Name |Current Number|
+-------+------------+-------------+-------------+--------------+
|SARB1  |   Agona    | 9           | Agona       |       13     |
+-------+------------+-------------+-------------+--------------+ 
|SARB19 | Enteritidis| 76          | Enteritidis |           77 |
+-------+------------+-------------+-------------+--------------+ 
|SARB20 | Emek       |          77 | Emek        | 76           |
+-------+------------+-------------+-------------+--------------+ 
|SARB35 | Manhattan  | 18          | Muenchen    | 18           |
+-------+------------+-------------+-------------+--------------+ 
|SARA72 | Manhattan  |          113| Muenchen    | 113          |
+-------+------------+-------------+-------------+--------------+ 
|SARB44 | Paratyphi B| 87          | Paratyphi B | 110          |
+-------+------------+-------------+-------------+--------------+ 
|SARB47 | Paratyphi B| 89          | Limete      | 89           |
+-------+------------+-------------+-------------+--------------+ 
|SARB49 | Limete     | 89          | Paratyphi C | 114          |
+-------+------------+-------------+-------------+--------------+ 
|SARB50 | Paratyphi C| 91          | Unknown     | 91           |
+-------+------------+-------------+-------------+--------------+ 
|SARB69 | Typhisuis  | 100         | 6,7:c:-     | 100          |
+-------+------------+-------------+-------------+--------------+ 
|SARB70 | Typhisuis  | 70          | Decatur     | 70           |
+-------+------------+-------------+-------------+--------------+ 

The following alleles have been deleted from the database: hemD5, hisD45  
The following STs have been deleted from the database: ST9, 87   


Genes
-----
The S. enterica MLST scheme uses internal fragments of the following seven house-keeping genes:

* *thrA* (aspartokinase+homoserine dehydrogenase)  
* *purE* (phosphoribosylaminoimidazole carboxylas)  
* *sucA* (alpha ketoglutarate dehydrogenase)  
* *hisD* (histidinol dehydrogenase)  
* *aroC* (chorismate synthase)  
* *hemD* (uroporphyrinogen III cosynthase)  
* *dnaN* (DNA polymerase III beta subunit)  


PCR Amplification
-----------------
The primer pairs we use for the PCR amplification of internal fragments of these genes are:

+-------+-----------------------------------------------------------+----------------+
|       |                                                           | Product length |
+-------+-----------------------------------------------------------+----------------+
|*thrA*:| F 5'-GTCACGGTGATCGATCCGGT-3' **recommended**              |  852 bp        |
+-------+-----------------------------------------------------------+----------------+
|*thrA*:| R 5'-CACGATATTGATATTAGCCCG-3' **recommended**             |                |
+-------+-----------------------------------------------------------+----------------+
|*thrA*:| R1 5'-GTGCGCATACCGTCGCCGAC-3' (also Seq)                  |                |
+-------+-----------------------------------------------------------+----------------+
|       |                                                           |                |
+-------+-----------------------------------------------------------+----------------+
|*purE*:| F 5'-ATGTCTTCCCGCAATAATCC-3'                              |510 bp          |
+-------+-----------------------------------------------------------+----------------+
|*purE*:| F1 5'-GACACCTCAAAAGCAGCGT'-3' **recommended**             |                |
+-------+-----------------------------------------------------------+----------------+
|*purE*:| R 5'-TCATAGCGTCCCCCGCGGATC-3'                             |                |
+-------+-----------------------------------------------------------+----------------+
|*purE*:| R1 5'-CGAGAACGCAAACTTGCTTC-3'                             |                |
+-------+-----------------------------------------------------------+----------------+
|*purE*:| R2 5'-AGACGGCGATACCCAGCGG-3' **recommended**              |                |
+-------+-----------------------------------------------------------+----------------+
|       |                                                           |                |
+-------+-----------------------------------------------------------+----------------+
|*sucA*:| F 5'-AGCACCGAAGAGAAACGCTG-3'                              |643 bp          |
+-------+-----------------------------------------------------------+----------------+
|*sucA*:| F1 5'-CGCGCTCAAACAGACCTAC-3' **recommended**              |                |
+-------+-----------------------------------------------------------+----------------+
|*sucA*:| R 5'-GGTTGTTGATAACGATACGTAC-3'                            |                |
+-------+-----------------------------------------------------------+----------------+
|*sucA*:| R1 5'-GACGTGGAAAATCGGCGCC-3' **recommended**              |                |
+-------+-----------------------------------------------------------+----------------+
|       |                                                           |                |
+-------+-----------------------------------------------------------+----------------+
|*hisD*:| F 5'-GAAACGTTCCATTCCGCGCAGAC-3'                           |894 bp          |  
+-------+-----------------------------------------------------------+----------------+
|*hisD*:| F1 5'-GAAACGTTCCATTCCGCGC-3' **recommended**              |                |
+-------+-----------------------------------------------------------+----------------+
|*hisD*:| R 5'-CTGAACGGTCATCCGTTTCTG-3'                             |                |
+-------+-----------------------------------------------------------+----------------+
|*hisD*:| R1 5-GCGGATTCCGGCGACCAG-3' **recommended**                |                |
+-------+-----------------------------------------------------------+----------------+
|       |                                                           |                |
+-------+-----------------------------------------------------------+----------------+
|*aroC*:| F 5'-CCTGGCACCTCGCGCTATAC-3' **recommended**              |826 bp          |
+-------+-----------------------------------------------------------+----------------+
|*aroC*:| R 5'-CCACACACGGATCGTGGCG-3' **recommended**               |                |  
+-------+-----------------------------------------------------------+----------------+
|       |                                                           |                |
+-------+-----------------------------------------------------------+----------------+
|*hemD*:| F 5'-ATGAGTATTCTGATCACCCG-3'                              |666 bp          |
+-------+-----------------------------------------------------------+----------------+
|*hemD*:| F1 5'-GAAGCGTTAGTGAGCCGTCTGCG-3' **recommended**          |                |
+-------+-----------------------------------------------------------+----------------+
|*hemD*:| R 5'-ATCAGCGACCTTAATATCTTGCCA-3' **recommended**          |                |
+-------+-----------------------------------------------------------+----------------+
|       |                                                           |                |
+-------+-----------------------------------------------------------+----------------+
|*dnaN*:| F 5'-ATGAAATTTACCGTTGAACGTGA-3'                           |833 bp          |
+-------+-----------------------------------------------------------+----------------+
|*dnaN*:| R 5'-AATTTCTCATTCGAGAGGATTGC-3' **recommended**           |                |
+-------+-----------------------------------------------------------+----------------+
|*dnaN*:| R1 5'-CCGCGGAATTTCTCATTCGAG-3' **recommended** (also Seq) |                |
+-------+-----------------------------------------------------------+----------------+

An annealing temperature of 55° C is fine for all genes.


Sequencing
----------
Together with the recommended PCR primers above, we recommend using the
following sequencing primers at 50C

* *aroC*: aroC_sF1 (GGCGTGACGACCGGCAC) and aroC_sR1 (AGCGCCATATGCGCCAC)  
* *dnaN*: dnaN_sF (CCGATTCTCGGTAACCTGCT) and dnaN_sR1 (ACGCGACGGTAATCCGGG)  
* *hemD*: hemD_sF2 (GCCTGGAGTTTTCCACTG) and hemd_sR (GACCAATAGCCGACAGCGTAG)  
* *hisD*: hisD_sF (GTCGGTCTGTATATTCCCGG) and hisD_sR (GGTAATCGCATCCACCAAATC)  
* *purE*: purE_sF1 (ACAGGAGTTTTAAGACGCATG) and purE_sR1 (GCAAACTTGCTTCATAGCG)  
* *sucA*: sucA_sF1 (CCGAAGAGAAACGCTGGATC) and sucA_sR (GGTTGTTGATAACGATACGTAC)  
* *thrA*: thrA_sF (ATCCCGGCCGATCACATGAT) and thrA_sR1 (ACCGCCAGCGGCTCCAGCA)  


Our previous recommendations were to use the PCR primers for sucA, thrAR1 and
dnaNR1 for sequencing as well as the other primers listed below:

*thrA*: sF 5'-ATCCCGGCCGATCACATGAT-3'  
*thrA*: sR 5'-CTCCAGCAGCCCCTCTTTCAG-3'  

*purE*: sF 5'-CGCATTATTCCGGCGCGTGT-3'  
*purE*: sF1 5'-CGCAATAATCCGGCGCGTGT-3'  
*purE*: sR 5'-CGCGGATCGGGATTTTCCAG-3'  
*purE*: sR1 5'-GAACGCAAACTTGCTTCAT-3'  

*sucA*: sF 5'-AGCACCGAAGAGAAACGCTG-3'  
*sucA*: sR 5'-GGTTGTTGATAACGATACGTAC-3'  

*hisD*: sF 5'-GTCGGTCTGTATATTCCCGG-3'  
*hisD*: sR 5'-GGTAATCGCATCCACCAAATC-3'  

*aroC*: sF 5'-GGCACCAGTATTGGCCTGCT-3'  
*aroC*: sR 5'-CATATGCGCCACAATGTGTTG-3'  

*hemD*: sF 5'-GTGGCCTGGAGTTTTCCACT-3'  
*hemD*: sF1 5'-ATTCTGATCACCCGCCCCTC-3'  
*hemD*: sR 5'-GACCAATAGCCGACAGCGTAG-3'  

*dnaN*: sF 5'-CCGATTCTCGGTAACCTGCT-3'  
*dnaN*: sR 5'-CCATCCACCAGCTTCGAGGT-3'  



Allele template
---------------
Allelic profile of S. typhi strain CT18.

*thrA* (501 bp):  

.. code-block:: html

    GTGCTGGGCCGTAATGGTTCCGACTATTCCGCCGCCGTGCTGGCCGCCTGTTTACGCGCTGACTGCTGTGAAATCTGGACTGACGT  
    CGATGGCGTGTATACCTGTGACCCGCGCCAGGTGCCGGACGCCAGGCTGTTGAAATCGATGTCCTACCAGGAAGCGATGGAGCTCT  
    CTTACTTCGGCGCTAAAGTCCTTCACCCTCGCACCATAACGCCTATCGCCCAGTTCCAGATCCCCTGTCTGATTAAAAATACCGGC  
    AATCCGCAGGCGCCAGGAACGCTGATCGGCGCGTCCAGCGACGATGATAATCTGCCGGTTAAAGGGATCTCTAACCTTAACAACAT  
    GGCGATGTTTAGCGTCTCCGGCCCGGGAATGAAAGGGATGATTGGGATGGCGGCGCGTGTTTTCGCCGCCATGTCTCGCGCCGGGA  
    TCTCGGTGGTGCTCATTACCCAGTCCTCCTCTGAGTACAGCATCAGCTTCTGTGTGCCGCAGAGTGACTGC  
    
*purE* (399 bp):  

.. code-block:: html

    AGCGACTGGGCTACCATGCAATTCGCCGCCGAAATTTTTGAAATTCTGGATGTCCCGCACCATGTAGAAGTGGTTTCCGCCCATCG  
    CACCCCCGATAAACTGTTCAGCTTCGCCGAAACGGCGGAAGAGAACGGATATCAAGTGATTATTGCCGGCGCGGGCGGCGCGGCAC  
    ACCTGCCGGGAATGATTGCGGCAAAAACGCTGGTCCCGGTACTCGGCGTGCCGGTACAAAGCGCTGCGCTCAGCGGCGTGGATAGC  
    CTCTACTCCATCGTGCAGATGCCGCGCGGCATTCCGGTGGGTACGCTGGCGATCGGTAAAGCCGGGGCGGCGAACGCCGCACTGCT  
    GGCAGCGCAAATTTTGGCTACGCATGATAGCGCGCTGCATCGGCGCATCGCCGAC  
    
*sucA* (501 bp):  

.. code-block:: html

    AAACGCTTCCTGAACGAACTGACCGCCGCTGAAGGGCTGGAACGTTATCTGGGCGCCAAATTCCCGGGTGCGAAACGTTTCTCGCT  
    CGAGGGGGGAGATGCGCTGATACCTATGCTGAAAGAGATGGTTCGCCATGCGGGTAACAGCGGCACTCGCGAAGTGGTGCTGGGGA  
    TGGCGCACCGCGGTCGTCTGAACGTGCTGATCAACGTACTGGGTAAAAAACCGCAGGATCTGTTCGACGAGTTTGCCGGTAAACAT  
    AAAGAACATCTGGGTACCGGCGACGTGAAGTATCACATGGGCTTCTCGTCAGATATCGAAACTGAAGGCGGTCTGGTTCACCTGGC  
    GCTGGCGTTTAACCCATCGCATCTGGAAATTGTGAGCCCGGTGGTGATGGGCTCCGTGCGCGCCCGTCTGGACCGACTGGACGAAC  
    CGAGCAGTAATAAAGTGCTGCCGATCACTATTCACGGCGACGCCGCGGTGACCGGCCAGGGCGTGGTTCAG  
    
*hisD* (501 bp):  

.. code-block:: html

    ATTGCGGGATGCCAGAAGGTGGTTCTGTGCTCGCCGCCACCCATCGCTGATGAAATCCTCTATGCGGCGCAACTGTGTGGCGTGCA  
    GGAAATCTTTAACGTCGGCGGCGCGCAGGCGATTGCCGCTCTGGCCTTCGGCAGCGAGTCCGTACCGAAAGTGGATAAAATTTTTG  
    GCCCCGGCAACGCCTTTGTAACCGAAGCCAAGCGTCAGGTCAGCCAGCGTCTCGACGGCGCGGCTATCGATATGCCAGCCGGGCCG  
    TCTGAAGTGCTGGTGATCGCCGACAGCGGCGCAACACCGGATTTCGTCGCTTCTGACCTGCTCTCCCAGGCTGAGCACGGCCCGGA  
    TTCCCAGGTGATCCTGCTGACGCCGGATGCTGACATTGCCCGCAAGGTGGCGGAGGCGGTAGAACGTCAACTGGCGGAACTGCCGC  
    GCGCGGGCACCGCCCGGCAGGCCCTGAGCGCCAGTCGTCTGATTGTGACCAAAGATTTAGCGCAGTGCGTC  
    
*aroC* (501 bp):  

.. code-block:: html

    GTTTTTCGCCCGGGACACGCGGATTACACCTATGAGCAGAAATACGGCCTGCGCGATTACCGCGGCGGTGGACGTTCTTCCGCGCG  
    TGAAACCGCGATGCGCGTAGCGGCAGGGGCGATCGCCAAGAAATACTTGGCGGAAAAGTTCGGCATCGAAATCCGCGGCTGCCTGA  
    CCCAGATGGGCGACATTCCGCTGGAGATTAAAGACTGGCGTCAGGTTGAGCTTAATCCGTTCTTTTGCCCCGATGCGGACAAACTT  
    GACGCGCTGGACGAACTGATGCGCGCGCTGAAAAAAGAGGGTGACTCCATCGGCGCGAAAGTGACGGTGATGGCGAGCGGCGTGCC  
    GGCAGGGCTTGGCGAACCGGTATTTGACCGACTGGATGCGGACATCGCCCATGCGCTGATGAGCATCAATGCGGTGAAAGGCGTGG  
    AGATCGGCGAAGGATTTAACGTGGTGGCGCTGCGCGGCAGCCAGAATCGCGATGAAATCACGGCGCAGGGT  
    
*hemD* (432 bp):  

.. code-block:: html

    GCAACGCTGACGGAAAACGATCTGGTTTTTGCCCTTTCACAGCACTCCGTCGCCTTTGCTCACGCCCAGCTCCAGCGGGATGGACG  
    AAACTGGCCTGCGTCGCCGCGCTATTTCTCGATTGGCCGCACCACGGCGCTCGCCCTTCATACCGTTAGCGGGTTCGATATTCGTT  
    ATCCATTGGATCGGGAAATCAGCGAAGCCTTGCTACAATTACCTGAATTACAAAATATTGCGGGCAAACGCGCGCTGATTTTGCGT  
    GGCAATGGCGGCCGCGAACTGCTGGGCGAAACCCTGACAGTTCGCGGAGCCGAAGTCAGTTTTTGTGAATGTTATCAACGATGTGC  
    GAAACATTACGATGGCGCGGAAGAAGCGATGCGCTGGCATACTCGCGGCGTAACAACGCTTGTTGTTACCAGCGGCGAGATGTTGC  
    AA  
    
*dnaN* (501 bp):  

.. code-block:: html

    ATGGAGATGGTCGCGCGCGTTACGCTTTCTCAGCCGCATGAGCCAGGCGCCACTACCGTGCCGGCGCGGAAATTCTTTGATATCTG  
    CCGCGGCCTGCCGGAGGGCGCGGAGATTGCCGTTCAGTTGGAAGGCGATCGGATGCTGGTGCGTTCTGGCCGTAGCCGCTTCTCGC  
    TGTCTACGCTGCCTGCCGCCGATTTCCCGAATCTTGACGACTGGCAAAGCGAAGTTGAATTTACGCTGCCGCAGGCCACGATGAAG  
    CGCCTGATTGAAGCGACCCAGTTTTCGATGGCCCATCAGGATGTGCGCTACTACTTAAACGGTATGCTGTTTGAAACGGAAGGTAG  
    CGAACTGCGCACTGTTGCGACCGACGGCCACCGTCTGGCGGTGTGCTCAATGCCGCTGGAGGCGTCTTTACCTAGCCACTCGGTGA  
    TTGTGCCGCGTAAAGGCGTGATTGAACTGATGCGTATGCTCGACGGTGGCGAAAACCCGCTGCGCGTGCAG