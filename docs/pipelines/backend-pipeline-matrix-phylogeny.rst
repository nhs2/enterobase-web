matrix_phylogeny
================
The matrix_phylogeny pipeline runs phylogeny_workflow.py with the phylogeny option only.  This in turn runs
[RAxML] in order to compute a maximum likelihood phylogenetic tree.

The matrix_phylogeny pipeline is normally run as
[part of a workflow (i.e. the end) to compute a SNP tree](EnteroBase%20Backend%20Pipeline#markdown-header-snp-trees).

The matrix_phylogeny is currently in version 1.0.

Summary
-------
The matrix_phylogeny pipeline runs phylogeny_workflow.py with the phylogeny option.  This takes a SNP
matrix file (from a previous run of the [refMapper_matrix] pipeline)
 as input.  (The SNP matrix file documents mutations found in genome assemblies compared with a reference
genome assembly from running [refMapper].)  The SNP matrix is read in and
parsed and a [PHYLIP] file is written for the genome assemblies in order to run [RAxML]. [PHYLIP] is an
alignment file format, originally used by the [PHYLIP] alignment program.  In this case the [PHYLIP] file
is used to represent the concatenated sequence from the contigs of the genome assemblies where variation
is present in at least one of the genome assemblies.  [RAxML] (version 8.2.4) is run to compute a maximum likelihood tree
for all of the genome assemblies represented in the [PHYLIP] file.  The tree determined by [RAxML] is rooted
using the [ETE] 3 toolkit so that the root node is split into two balanced branches in terms of node distances.
The final tree is output in [Newick] format (which will be downloadable by the user who initiated [computation
of a SNP tree while using EnteroBase](SNP%20Projects)).

  [RAxML]: http://sco.h-its.org/exelixis/web/software/raxml/ "external link"
  [PHYLIP]: http://evolution.genetics.washington.edu/phylip.html "external link"
  [Newick]: http://en.wikipedia.org/wiki/Newick_format "external link"
  [ETE]: http://en.wikipedia.org/wiki/Newick_format "external link"
  [refMapper_matrix]: EnteroBase%20Backend%20Pipeline%3A%20refMapper_matrix
  [refMapper]: EnteroBase%20Backend%20Pipeline%3A%20refMapper