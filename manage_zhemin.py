
#!flask/bin/python

import os, re, copy, numpy as np, datetime
import json, requests,ujson
from flask_script import Manager, Shell, Command
from entero import create_app, db, dbhandle, config, load_logger,app, get_database, rollback_close_connection, rollback_close_system_db_session, generate_database_conns
from entero.databases.system.models import UserUploads,User,UserPermissionTags,UserJobs,query_system_database,UserPreferences, BuddyPermissionTags
from entero.cell_app.tasks import process_job, import_sra, assemble_user_uploads
import psycopg2
from psycopg2.extras import RealDictCursor
from datetime import datetime, timedelta
from sqlalchemy import func, not_
import logging
import platform
import shutil
import unidecode


# Set enviro with ENTERO_CONFIG=<See config.py for options>
entero = create_app(os.getenv('ENTERO_CONFIG') or 'development')
manager = Manager(entero)

from entero.admin import ENAsubmission


@manager.command
@manager.option('--database', help='database')
@manager.option('--user_id', help='user that updates.')
@manager.option('--tabfile', help='tab delimited file of strain Barcode and UDFs.')
def import_UDFs(database='senterica', user_id=69, tabfile=None) :
    from entero.ExtraFuncs.user_define_fields import UserDefinedFields
    import pandas as pd, numpy as np
    udfs = pd.read_csv(tabfile, sep='\t', dtype=str)
    cols = udfs.columns
    barcode, cols = np.where(cols=='Barcode')[0][0], { i:c for i, c in enumerate(cols) if c != 'Barcode' }
    udfs = udfs.values
    toUpdate = {}
    for strain in udfs :
        strain_id = strain[barcode]
        toUpdate[strain_id] = { c:strain[i] for i, c in cols.items() }
    with UserDefinedFields('senterica', 69) as udf:
        udf.upsert(toUpdate)
    return

@manager.command
@manager.option('--database', help='database')
@manager.option('--readlist', help='lists of files to assembled genomes.')
@manager.option('--username', help='user name.')
def import_assemblies(database='senterica', readlist=None, username='zhemin'):
    from entero.admin.fetch_reads import insert_assembly_into_enterobase
    user_id = db.session.query(User.id).filter_by(username=username).first()[0]
    
    data = {}
    with open(readlist) as fin :
        for line in fin :
            part = line.strip().split('\t')
            data[part[0]] = {'accession':','.join(part[1:]), 'strain':part[0], 'seq_platform':'Complete genome'}
    insert_assembly_into_enterobase(database, list(data.values()), owner=user_id)



@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-b', '--barcodes', help='Tab-delimited file with one barcode per line (Collumn header: "Barcode"). Can also add addtional metadata in the file, such as "collected_by"')
@manager.option('-p', '--project', help='Name of a file of two lines. The first line contains the name of the project and the rests are the abstract. This will be used to replace the default project information. ')
@manager.option('-c', '--center', help='Overwrite the submission center information. ')
@manager.option('-r', '--release', help='Release the reads immediately. Overwrite the "release_date" field in metadata ', action='store_true')
def uploadToENA(database=None, barcodes='', project = None, center=None, release = False) :
    import pandas as pd
    db_desc = config.Config.DB_CODES[database][0].capitalize()
    
    barcodes = pd.read_csv(barcodes, sep='\t', na_filter=False).to_dict('records')
    barcodes = { barcode['Barcode']:barcode for barcode in barcodes }

    dbase = get_database(database)
    
    # fetch and convert metadata
    strains = dbase.models.Strains
    traces = dbase.models.Traces
    
    qry = dbase.session.query(strains, traces)\
        .filter(strains.barcode.in_(barcodes.keys()))\
        .filter(strains.best_assembly is not None)\
        .filter(strains.secondary_study_accession is None)\
        .filter(traces.strain_id == strains.id)\
        .filter(traces.seq_platform == 'ILLUMINA')\
        .filter(traces.accession.like('traces-%'))\
        .filter(traces.read_location is not None)

    strain_data = [ [data[0].as_dict(), data[1].as_dict()] for data in qry]
    if len(strain_data) == 0 :
        return []
    
    # get rMLST species prediction
    assembly_ids = [ data[0]['best_assembly'] for data in strain_data if data[0].get('species', None) is None ]
    if len(assembly_ids) :
        schemes = dbase.models.Schemes
        types = dbase.models.AssemblyLookup
        assembly_data = dbase.session.query(types.assembly_id, types.st_barcode).filter(types.assembly_id.in_(assembly_ids)).filter(types.scheme_id==schemes.id).filter(schemes.description.like('%rMLST%')).filter(types.st_barcode != None).all()
        assembly_data = {d[0]:d[1] for d in assembly_data }
        st_barcodes = list(set(assembly_data.values()))

        url = entero.config['NSERV_ADDRESS'] + 'retrieve.api'
        species_data = { d['barcode']:d['info']['hierCC']['d40'] for d in json.loads(requests.post(url, data={'barcode': ','.join(st_barcodes), "fieldnames":'barcode,info', 'sub_fieldnames':'index_id', 'simple':1, 'convert':0}, timeout=30, ).text) if isinstance(d['info'], dict)}
        assembly_data = {k:species_data[v] for k, v in assembly_data.items() if v in species_data}
        
        for strain, trace in strain_data :
            if strain.get('species', None) is None and strain.get('best_assembly', None) in assembly_data :
                strain['species'] = assembly_data[strain['best_assembly']]
    for strain, trace in strain_data :
        if strain.get('species', None) == None :
            strain['species'] = db_desc
    # convert to ENA format
    dataset = ENAsubmission.convertData(strain_data, barcodes)
    if center :
        nd = {}
        for (c, r), ds in dataset.items() :
            key = (center, r)
            if key not in nd :
                nd[key] = ds
            else :
                nd[key].extend(ds)
        dataset = nd
    if release == True :
        nd = {}
        for (c, r), ds in dataset.items() :
            key = (c, 'released')
            if key not in nd :
                nd[key] = ds
            else :
                nd[key].extend(ds)
        dataset = nd
        
    
    # submission
    for (center, release_date), ds in dataset.items() :
        local_folder = min([d['SAMPLE'] for d in ds])
        #refname = local_folder
        if project :
            with open(project) as fin :
                project_name = fin.readline().strip()
                project_desc = ' '.join([line.strip() for line in fin.readlines()])
        else :
            project_name = 'EnteroBase {0} - User Uploads'.format(db_desc)
            project_desc = 'This BioProject contains the whole genome sequence data uploaded by EnteroBase users. This data is part of a pre-publication release. For information on the proper use of data uploaded by EnteroBase users, please see https://enterobase.readthedocs.io/en/latest/enterobase-terms-of-use.html.'
        accessions = ENAsubmission.ENAsubmission(local_folder, center, release_date, project_name, project_desc, app.config['EBI_FTP_USER'], app.config['EBI_FTP_PASSWORD']).run(ds)
        strain_records = { data['SAMPLE']: [accessions.get(data['SAMPLE'], ''), accessions['study']] for data in ds }
        trace_records = { data['__TRACE__']: accessions.get(data['__TRACE__'], '') for data in ds }
        for strain in dbase.session.query(strains).filter(strains.barcode.in_(strain_records.keys())) :
            strain.secondary_study_accession = strain_records[strain.barcode][1]
            strain.secondary_sample_accession = strain_records[strain.barcode][0]
        for trace in dbase.session.query(traces).filter(traces.barcode.in_(trace_records.keys())) :
            trace.traces_comment = trace.accession
            trace.accession = trace_records[trace.barcode]
        dbase.session.flush()
        dbase.session.commit()
    return 
    


@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-s', '--scheme', help='scheme')
def select_all_profiles(database='clostridium', scheme='gMLST') :
    dbase = get_database(database)
    strains = dbase.models.Strains
    schemes = dbase.models.Schemes
    types = dbase.models.AssemblyLookup
    records = dbase.session.query(types.st_barcode).filter(types.assembly_id==strains.best_assembly).filter(types.scheme_id==schemes.id).filter(schemes.description.like('%'+scheme+'%')).filter(types.st_barcode != None).all()
    records = list({r[0] for r in records})
    
    url = entero.config['NSERV_ADDRESS'] + 'retrieve.api'
    type_indices, value_indices = {}, {}
    for rId in xrange(0, len(records), 300) :
        print '{0}/{1}'.format(rId, len(records))
        data = ','.join(records[rId:rId+300])
        data = json.loads(requests.post(url, data={'barcode': data, "fieldnames":'index_id,value_indices', 'sub_fieldnames':'index_id', 'simple':1, 'convert':0}, timeout=30, ).text)
        type_indices.update({d['index_id']:1 for d in data})
        value_indices.update({dd:1 for d in data for dd in d['value_indices']})
    np.save('type_indices', sorted(list(type_indices.keys())))
    np.save('value_indices', sorted(list(value_indices.keys())))

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-c', '--cc', help='an integer of the hierCC level', dtype=int)
def select_hierCC_representative(database=None, cc=None) :
    dbase = get_database(database)
    schemes = dbase.models.Schemes
    scheme_id = dbase.session.query(schemes.id).filter(schemes.description.like('%cgMLST%')).filter(not_(schemes.description.like('%egacy%'))).order_by(schemes.id.desc()).first()
    data = dbase.session.execute('''SELECT strains.barcode, assemblies.n50, assemblies.low_qualities, assembly_lookup.st_barcode 
    FROM strains, assemblies, assembly_lookup 
    WHERE strains.best_assembly=assemblies.id 
    AND assemblies.id = assembly_lookup.assembly_id
    AND scheme_id = {0} AND strains.uberstrain = strains.id AND strains.release_date <= now()
    '''.format(scheme_id[0])).fetchall()
    sts = {d[3]:1 for d in data if d[3]}
    # check rST to be positive
    sts = {d['barcode'] : str(d['info']['hierCC'].get('d{0}'.format(cc) , '')) for d in ujson.loads(requests.post('http://137.205.123.127/ET/NServ/retrieve.api', dict(convert=0, fieldnames='barcode,info', barcode='{0}'.format(','.join(sts.keys())))).content)}
    
    data = [ [d[0], (d[1] if d[1] else 3000000) - (d[2] if d[2] else 0), sts.get(d[3], '')] for d in data if sts.get(d[3], '') != '' ]
    # find best assembly for each rST
    data.sort(key=lambda x:(-x[1], x[0]))
    res = {}
    for d in data :
        if d[2] not in res :
            res[d[2]] = d + [1]
        else :
            res[d[2]][3] += 1
    data = sorted([r for r in res.values()], key=lambda x:-x[3])
    # report
    print 'Barcode\tScore\tHC{0}\tTotal_count'.format(cc)
    for d in data :
        print '{0}\t{1}\t{2}\t{3}'.format(*d)


@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-m', '--min_dist', help='min_dist', type=int)
def select_rST_representative(database=None, min_dist=0) :
    min_dist = int(min_dist)
    dbase = get_database(database)
    schemes = dbase.models.Schemes
    scheme_id = dbase.session.query(schemes.id).filter(schemes.description=='rMLST').first()
    data = dbase.session.execute('''SELECT strains.barcode, assemblies.n50, assemblies.low_qualities, assembly_lookup.st_barcode 
    FROM strains JOIN assemblies ON strains.best_assembly=assemblies.id 
    JOIN assembly_lookup ON assemblies.id = assembly_id
    WHERE scheme_id = {0} AND strains.uberstrain = strains.id AND strains.release_date <= now()
    '''.format(scheme_id[0])).fetchall()
    #WHERE scheme_id = {0} AND strains.uberstrain = strains.id
    #'''.format(scheme_id[0])).fetchall()
    sts = {d[3]:1 for d in data if d[3]}
    # check rST to be positive
    sts = {d['barcode'] : [max([ dd['flag'] for dd in d['fieldvalues'] ]), len(d['fieldvalues']), d['type_id']] for d in ujson.loads(requests.post('http://137.205.123.127/ET/NServ/retrieve.api', dict(convert=0, fieldnames='barcode,value_indices,type_id', sub_fieldnames='flag', barcode='{0}'.format(','.join(sts.keys())))).content)}
    nField = np.max([d[1] for d in sts.values()])
    data = [[d[0], (d[1] if d[1] else 4000000) - 2*(d[2] if d[2] else 0), d[3], sts.get(d[3], [0])[0], sts[d[3]][2]] for d in data if sts.get(d[3], [10])[0] <= 8 and sts.get(d[3], [10, 0])[1] >= nField]
    # find best assembly for each rST
    data.sort(key=lambda x:(-x[1], x[0]), reverse=True)
    res = {}
    for d in data :
        res[d[2]] = d
    data = sorted([r for r in res.values()], key=lambda x:(-x[1],x[0]))
    sts = {d['barcode'] : np.array([ dd['value_id'] for dd in d['fieldvalues'] ]) for d in ujson.loads(requests.post('http://137.205.123.127/ET/NServ/retrieve.api', dict(convert=0, fieldnames='barcode,value_indices', sub_fieldnames='value_id', barcode='{0}'.format(','.join([ d[2] for d in data ])))).content)}
    
    if min_dist > 0 :
        res = []
        for d in data :
            st = sts[d[2]]
            if len(res) :
                dist = np.sum(res != st, 1)
                if np.min(dist) > min_dist :
                    res = np.vstack([res, st])
                else :
                    d[2] = ''
            else :
                res = np.array(st)[np.newaxis, :]
        data = [d for d in data if d[2] != '']
    data2 = dbase.session.execute('''
SELECT strains.barcode, assemblies.file_pointer, alu.gff, tr.accession
FROM strains JOIN assemblies ON strains.best_assembly=assemblies.id 
LEFT OUTER JOIN (
    select assembly_lookup.assembly_id, assembly_lookup.other_data#>>'{{results,gff_file}}' as gff from assembly_lookup join schemes 
    on schemes.id = assembly_lookup.scheme_id
    where schemes.description like '%annotation%' and assembly_lookup.other_data#>>'{{results,gff_file}}' is not NULL
) alu ON assemblies.id = alu.assembly_id 
left outer join (
    select * from traces 
    where traces.accession not like '%RR%'
) tr on tr.strain_id = strains.id
WHERE strains.barcode in (VALUES {0}) and strains.uberstrain = strains.id 
;
    '''.format(','.join([ "('{0}')".format(d[0]) for d in data ]))).fetchall()
    data_map = { d[0]:d[1:] for d in data2 }
    # report
    for d in data :
        print '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}'.format(d[0], d[1], d[2], d[3], d[4], data_map[d[0]][0], data_map[d[0]][1], data_map[d[0]][2])

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-p', '--profile', help='database')
@manager.option('-c', '--clusters', help='database')
@manager.option('-v', '--view', help='database')
def importHierCC(database=None, profile=None, clusters=None, view=None) :
    import pandas as pd, numpy as np
    dbase = get_database(database)
    strains = dbase.models.Strains

    # get hierCC levels
    udfs = UserPreferences
    custom_view = db.session.query(udfs.data).filter(udfs.id == int(view)).one()
    fields = [ int(fld['id']) for fld in json.loads(custom_view[0])['custom_columns'] ]
    levels = []
    for fld in db.session.query(udfs).filter(udfs.id.in_(fields)) :
        hiercc = int(re.findall(r'\d+$', json.loads(fld.data)['label'])[0])
        levels.append([hiercc+1, fld.id])
    levels = np.array(levels)
    levels = levels[np.argsort(levels.T[0])]
    
    data = pd.read_csv(profile, sep='\t', header=0).values
    records, sts = {}, {}
    for s, t in data :
        records[s], sts[t] = t, None
    clusters = np.load(clusters)['hierCC']
    for cls in clusters :
        if cls[0] in sts :
            sts[cls[0]] = cls[levels.T[0]]
    records = { s:sts[records[s]] for s in records }
    
    cnt = 0
    for strain in dbase.session.query(strains) :
        groups = records.get(strain.barcode, None)
        if groups is not None :
            if strain.custom_columns is not None :
                #if str(levels[0, 1]) in strain.custom_columns :
                    #continue
                cc = copy.deepcopy(strain.custom_columns)
            else :
                cc = {}
            try :
                cc.update({str(lvl):str(grp) for lvl, grp in zip(levels.T[1], groups)})
                strain.custom_columns = cc
                cnt += 1
                if cnt % 3000 == 0 :
                    print cnt
                    dbase.session.commit()
            except :
                pass
    dbase.session.commit()

    
@manager.command
@manager.option('-d', '--databases', help='databases')
def callUber(databases=None) : 
    # an experimental method to remove duplicated records
    def st_cmp2(ts, ss) :
        try :
            t1, t2 = ts
            min_d = 99999
            for tt1 in t1 :
                s1 = ss[tt1]
                for tt2 in t2 :
                    s2 = ss[tt2]
                    d = len([1 for k,v in s2.iteritems() if s1.get(k, v) != v])
                    if d <= 0 :
                        return 1
                if d < min_d :
                    min_d = d
            return 0 if min_d < 3 else -1
        except :
            return -1
    def st_cmp(t1, t2) :
        x = 0
        for tt1, tt2 in zip(t1, t2) :
            if tt1 == tt2 :
                if tt1 :
                    x += 1
            elif tt1 and tt2 :
                x -= 1
        return min(x, 1)
    def uberTicket(database) :
        udfs = UserPreferences
        uberColId = db.session.query(udfs.id).filter(udfs.user_id==69).filter(udfs.type=='custom_column').filter(udfs.database==database).filter(udfs.name=='UberTicket').first()
        if not uberColId :
            newUberCol = udfs(name='UberTicket', database=database, user_id=69, data=ujson.dumps({"datatype":"text","label":"UberTicket"}), type='custom_column')
            x = db.session.add(newUberCol)
            db.session.commit()
            return str(newUberCol.id)
        else :  
            return str(uberColId[0])
    for database in databases.split(',') :
        
        dbase = get_database(database)
        uberCol = uberTicket(database)
        strains = dbase.models.Strains
        schemes = dbase.models.Schemes
        types = dbase.models.AssemblyLookup
        lowResSchemes = sorted([r[0] for r in dbase.session.query(schemes.id).filter(schemes.description.like('%MLST%')).filter(~schemes.description.like('%gMLST%')).all()])
        highResScheme = dbase.session.query(schemes.id).filter(schemes.description.like('%cgMLST%')).order_by(schemes.id).all()
        if not highResScheme :
            highResScheme = dbase.session.query(schemes.id).filter(schemes.description.like('%wgMLST%')).order_by(schemes.id).all()
        highResScheme = highResScheme[-1][0] if highResScheme else None
        res = dbase.session.execute('''SELECT strains.id, strains.uberstrain, strains.strain, scheme_id, st_barcode 
        FROM strains 
        LEFT JOIN assembly_lookup ON strains.best_assembly = assembly_lookup.assembly_id 
        WHERE uberstrain > 0 AND scheme_id in ({0})'''.format(','.join(map(str, lowResSchemes)))).fetchall()
        records = {}
        for r in res :
            if r[0] not in records :
                if r[2] :
                    n = re.sub(r'salmonella\|escherichia\| coli\|yersinia\|strain\| str\.*', '', r[2].split('-sc-')[0].strip().lower()).strip()
                else :
                    n = ''
                records[r[0]] = [r[0], r[1], \
                                 re.sub(r'[ _\-]', '', n), \
                                 {}]
            x = records[r[0]][3]
            x[r[3]] = r[4]
        records = sorted([ [r[0], r[1], r[2], [r[3].get(s, None) for s in lowResSchemes ]] for r in records.itervalues() if len(r[2]) >= 2 ], key=lambda r:r[2])
        import numpy as np
        nameFreq =np.unique([r[2] for r in records], return_counts=True)
        skipNames = nameFreq[0][nameFreq[1] >= 8]
        print 'Most frequent names will be ignored. They are: {0}'.format(skipNames)
        skipNames = { nn for n in skipNames for nn in (n, ''.join(n[::-1])) }
        
        nRec, ubers = len(records), {}
        for r in records :
            ubers[r[0]] = r[1]
        for ite in range(2) :
            for i1, r1 in enumerate(records) :
                if r1[2] in skipNames :
                    continue
                for i2 in xrange(i1+1, nRec) :
                    r2 = records[i2]
                    if r2[2] in skipNames :
                        continue
                    if ubers[r1[0]] == ubers[r2[0]] : continue              
                    l1, l2 = len(r1[2]), len(r2[2])
                    
                    if r1[2] == r2[2] :
                        if ite > 0 :
                            continue
                        name_support = 1
                    elif l2 > l1>3 and r2[2].find(r1[2]) == 0 :
                        if l1 < .5*l2 : continue
                        d = r2[2][len(r1[2]):]
                        if re.match(r'[0-9\.:,\-]', d) : continue
                        name_support = 0
                        
                    elif l1 > l2>3 and r1[2].find(r2[2]) == 0 :
                        if l2 < .5*l1 : continue
                        d = r1[2][len(r2[2]):]
                        if re.match(r'[0-9\.:,\-]', d) : continue
                        name_support = 0
                    else :
                        break
                    if name_support >= 0 :
                        type_support = st_cmp(r1[3], r2[3])
                        if type_support < 0 or name_support + type_support <= 0 : continue
                        
                        high_support = 0
                        if highResScheme :
                            res = dbase.session.execute('''SELECT strains.uberstrain, st_barcode 
                            FROM strains 
                            LEFT JOIN assembly_lookup ON strains.best_assembly = assembly_lookup.assembly_id 
                            WHERE uberstrain in ({0},{1}) AND scheme_id = {2}'''.format(ubers[r1[0]], ubers[r2[0]], highResScheme)).fetchall()
                            if len(res) > 1 :
                                st_map = {}
                                for r in res :
                                    if r[1] :
                                        if r[0] not in res :
                                            st_map[r[0]] = {r[1]}
                                        else :
                                            st_map[r[0]] |= {r[1]}
                                if len(st_map) > 1 :
                                    barcodes = {vv for v in st_map.values() for vv in v if vv is not None}
                                    if len(barcodes) :
                                        types = ujson.loads(requests.get('http://137.205.123.127/ET/NServ/retrieve.api?convert=0&fieldnames=barcode,fieldvalues&barcode={0}'.format(','.join(barcodes))).content)
                                        types = {t['barcode']:{allele['fieldname']:allele['value_id'] for allele in t['fieldvalues'] if allele['value_id']>0} for t in types }
                                        high_support = st_cmp2(st_map.values(), types)
                                        if high_support < 0 : continue
                                
                        types = high_support + type_support + name_support
                        if r1[2] in skipNames or r2[2] in skipNames :
                            types = 1
                        if types >= 1 :
                            res = dbase.session.execute('''SELECT strains.*, assemblies.status, assemblies.n50 - 2*assemblies.low_qualities as score
                            FROM strains LEFT JOIN assemblies ON strains.best_assembly = assemblies.id
                            WHERE uberstrain IN ({0},{1})'''.format(r1[1], r2[1])).fetchall()
                            res = sorted([dict(r) for r in res], key=lambda r:({'Assembled':0, 'legacy':1}.get(r['status'], 2), -r['score'] if r['score'] else 0, strains.id))
                            if not len(res) : continue
                            uberGroup = res[0]['uberstrain']
                            for strain in dbase.session.query(strains).filter(strains.id.in_([r['id'] for r in res])) :
                                if strain.custom_columns :
                                    if strain.custom_columns.get(uberCol, '').lower().find('reject') >=0 :
                                        continue
                                    cc = copy.deepcopy(strain.custom_columns)
                                else :
                                    cc = {}
                                if types >= 2 :
                                    if strain.id == uberGroup :
                                        cc[uberCol] = 'Group:{0:07d};Uber'.format(uberGroup)
                                    else :
                                        strain.uberstrain = uberGroup
                                        cc[uberCol] = 'Group:{0:07d}'.format(uberGroup)
                                else :
                                    if strain.id == uberGroup :
                                        cc[uberCol] = 'Proposed:{0:07d};Uber'.format(uberGroup)
                                    else :
                                        cc[uberCol] = 'Proposed:{0:07d}'.format(uberGroup)
                                strain.custom_columns = cc
                            dbase.session.commit()
                            print r1
                            print r2
                            ubers[r1[0]] = ubers[r2[0]] = r1[1] = r2[1] = uberGroup
                            print types
            if ite == 0 :
                for r in records :
                    r[2] = ''.join(r[2][::-1])
                records.sort(key=lambda r:r[2])
        print ''

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-b', '--barcodes', help='filename for strain barcodes')
def getAssembliesAndAnnotations(database=None, barcodes=None) :
    # Get file pointers for assemblies and gff files. Used for ETiKi ortho
    dbase = get_database(database)
    schemes = dbase.models.Schemes
    scheme_id = dbase.session.query(schemes.id).filter(schemes.description.like('%annotation%')).first()
    barcode = []
    with open(barcodes) as fin :
        for line in fin :
            b = line.strip().split()[0]
            barcode.append(b)
    asm = dbase.models.Assemblies
    if not scheme_id :
        data = dbase.session.execute("SELECT strains.barcode, assemblies.file_pointer FROM strains join assemblies on strains.best_assembly=assemblies.id WHERE strains.barcode in ('{0}')".format("','".join(barcode))).fetchall()
        for d in data :
            print d[0], d[1]
    else :
        data = dbase.session.execute("SELECT strains.barcode, assemblies.barcode, assemblies.file_pointer, assembly_lookup.* FROM strains join assemblies on strains.best_assembly=assemblies.id LEFT OUTER JOIN assembly_lookup on assemblies.id = assembly_id where scheme_id = {0} and strains.barcode in ('{1}')".format(scheme_id[0], "','".join(barcode))).fetchall()
        for d in data :
            print d[0], d[1], d[2], d[-1].get('results', {}).get('gff_file', '-')

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-w', '--workspace', help='workspace id')
def getWorkspaceAssembliesAndAnnotations(database=None, workspace=None) :
    # Get file pointers for assemblies and gff files. Used for ETiKi ortho
    dbase = get_database(database)

    sql = "SELECT * FROM user_preferences WHERE id={0}".format(workspace)
    results  = query_system_database(sql)
    ws = json.loads(results[0]['data'])['data']['strain_ids']
    data = dbase.session.execute("SELECT DISTINCT strains.id FROM strains WHERE strains.id in ({0}) AND strains.uberstrain > 0".format(",".join([str(w) for w in ws]))).fetchall()
    ids = [str(d[0]) for d in data]
    
    schemes = dbase.models.Schemes
    scheme_id = dbase.session.query(schemes.id).filter(schemes.description.like('%annotation%')).first()
    asm = dbase.models.Assemblies
    results = {}
    if not scheme_id :
        data = dbase.session.execute("SELECT strains.barcode, assemblies.file_pointer FROM strains join assemblies on strains.best_assembly=assemblies.id WHERE strains.id in ({0})".format(",".join(ids))).fetchall()
        for barcode, genome in data :
            if not os.path.isfile(genome) :
                genome = '-'
            if barcode not in results or results[barcode][1] == '-' :
                results[barcode] = [barcode, genome]

    else :
        data = dbase.session.execute("SELECT strains.barcode, assemblies.file_pointer, assembly_lookup.* FROM strains join assemblies on strains.best_assembly=assemblies.id LEFT OUTER JOIN assembly_lookup on assemblies.id = assembly_id where scheme_id = {0} and strains.id in ({1})".format(scheme_id[0], ",".join(ids))).fetchall()
        for d in data :
            d = dict(d)
            if d['other_data'] is None :
                d['other_data'] = {}
            barcode, genome, gff = d['barcode'], d['file_pointer'], d['other_data'].get('results', {}).get('gff_file', '-')
            if not os.path.isfile(genome) :
                genome = '-'
            if not os.path.isfile(gff) :
                gff = '-'
            if barcode not in results :
                results[barcode] = [barcode, genome, gff]
            else :
                if results[barcode][1] == '-' :
                    results[barcode][1] = genome
                if results[barcode][2] == '-' :
                    results[barcode][2] = gff
    for d in sorted(results.values()) :
        print ' '.join(d)

@manager.command
@manager.option('-d', '--database', help='database')
def getAllAssembliesAndAnnotations(database=None) :
    # Get file pointers for assemblies and gff files. Used for ETiKi ortho
    dbase = get_database(database)
    schemes = dbase.models.Schemes
    scheme_id = dbase.session.query(schemes.id).filter(schemes.description.like('%annotation%')).first()
    asm = dbase.models.Assemblies
    if not scheme_id :
        data = dbase.session.execute("SELECT strains.barcode, assemblies.file_pointer FROM strains join assemblies on strains.best_assembly=assemblies.id").fetchall()
        for d in data :
            print d['barcode'], d['file_pointer']
    else :
        data = dbase.session.execute("SELECT strains.barcode, assemblies.file_pointer, assembly_lookup.* FROM strains join assemblies on strains.best_assembly=assemblies.id LEFT OUTER JOIN assembly_lookup on assemblies.id = assembly_id where scheme_id = {0}".format(scheme_id[0])).fetchall()
        for d in data :
            try :
                print d['barcode'], d['file_pointer'], d['other_data'].get('results', {}).get('gff_file', '-'), d['other_data'].get('job_id', '-')
            except :
                print d['barcode'], d['file_pointer'], d['other_data']


@manager.command
@manager.option('-d', '--databases', help='databases')
@manager.option('-s', '--scheme', help='name for the scheme')
def populate_MLST_scheme(databases='helicobacter', scheme='wgMLST') :
    # automatic send nomenclature jobs for missing records
    # todo: check whether the job is in CRobot before sending it out
    from entero.jobs.jobs import NomenclatureJob
    
    for dbname in databases.split(',') :
        print '##{0}'.format(dbname)
        dbase = get_database(dbname)
        schemes = dbase.models.Schemes
        if scheme in ('cgMLST', 'wgMLST') :
            scheme_infos = dbase.session.query(schemes).filter(schemes.name.like('%'+scheme+'%')).order_by(schemes.id).all()
            if len(scheme_infos) == 0 :
                continue
            else :
                scheme_info = scheme_infos[-1]
        else :
            scheme_info = dbase.session.query(schemes).filter(schemes.name == scheme).first()
            
        if not scheme_info : continue
        
        data = dbase.session.execute("SELECT assemblies.file_pointer, assemblies.barcode FROM strains join assemblies on strains.best_assembly=assemblies.id left outer join (select * from assembly_lookup where scheme_id = {0}) alu on assemblies.id = alu.assembly_id where uberstrain=strains.id and file_pointer is not NULL and st_barcode is NULL and assemblies.status='Assembled';".format(scheme_info.id)).fetchall()        
        #data = dbase.session.execute("SELECT assemblies.file_pointer, assemblies.barcode FROM strains join assemblies on strains.best_assembly=assemblies.id left outer join (select * from assembly_lookup where scheme_id = {0}) alu on assemblies.id = alu.assembly_id where uberstrain=strains.id and file_pointer is not NULL and assemblies.status='Assembled';".format(scheme_info.id)).fetchall()        
        print '##{0}'.format(len(data))
        for d in data :
            if os.path.isfile(d[0]) :
                job = NomenclatureJob(scheme=scheme_info,
                                      assembly_filepointer=d[0],
                                      assembly_barcode=d[1],
                                      database=dbname,
                                      user_id=69)
                d = job.send_job()
    return

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-s', '--scheme', help='scheme')
@manager.option('-f', '--fieldname', help='metadata field')
def get_metadata(database=None, scheme='MLST_Achtman', fieldname='serotype'):
    dbase = get_database(database)
    if not dbase:     
        message="No database found for the provided species: %s"%database
        app.logger.error(message)
        return make_response(message)    
    
    Strains = dbase.models.Strains
    Assemblies = dbase.models.Assemblies 
    AssemblyLookup = dbase.models.AssemblyLookup
    Schemes = dbase.models.Schemes
    scheme_id, scheme_desc = dbase.session.query(Schemes.id, Schemes.description).filter(Schemes.description.like('%{0}%'.format(scheme))).order_by(Schemes.id.desc()).first()
    search = [dict(st_barcode= x[0], scheme=scheme_desc, metadata=x[2])\
              for x in dbase.session.query(AssemblyLookup.st_barcode, AssemblyLookup.scheme_id, Strains.__table__.c[fieldname])\
              .filter(AssemblyLookup.st_barcode != 'EGG_ST')\
              .filter(AssemblyLookup.scheme_id == scheme_id)\
              .join(Assemblies).join(Strains).all()]
    with open('/share_space/interact/metadata.csv','w') as f:
        f.write('scheme,metadata,st_barcode\n')
        for j in search:
            j['metadata'] = '' if not j['metadata'] else j['metadata'].encode('ascii', 'ignore')
            f.write('%s\n' %'\t'.join(j.values()))
        f.close()
    return send_file('/share_space/interact/metadata.csv')



@manager.command
@manager.option('-d', '--databases', help='databases')
@manager.option('-c', '--cgst', help='list of cgST barcodes')
def update_wg_cgMLST_types(databases='Salmonella', cgst=None) :
    from entero.jobs.jobs import NomenclatureJob
    
    cgSTs = [x for x in open(cgst).read().split('\n') if x.endswith('_ST')]
    for dbname in databases.split(',') :
        print '##{0}'.format(dbname)
        dbase = get_database(dbname)
        schemes = dbase.models.Schemes
        scheme_info = {}
        for scheme in ('cgMLST', 'wgMLST') :
            scheme_infos = dbase.session.query(schemes).filter(schemes.name.like('%'+scheme+'%')).order_by(schemes.id).all()
            assert len(scheme_infos) > 0, 'Error. No cg or wgMLST'
            scheme_info[scheme] = scheme_infos[-1]
        
        data = dbase.session.execute("SELECT assemblies.file_pointer, assemblies.barcode FROM strains join assemblies on strains.best_assembly=assemblies.id left outer join (select * from assembly_lookup where scheme_id = {0}) alu on assemblies.id = alu.assembly_id where uberstrain=strains.id and file_pointer is not NULL and st_barcode in ({1}) and assemblies.status='Assembled';".format(
            scheme_info['cgMLST'].id, ','.join(["'{0}'".format(s) for s in cgSTs]))).fetchall()
        print '##{0}'.format(len(data))
        for d in data :
            if os.path.isfile(d[0]) :
                job = NomenclatureJob(scheme=scheme_info['cgMLST'],
                                      assembly_filepointer=d[0],
                                      assembly_barcode=d[1],
                                      database=dbname,
                                      user_id=69)
                dd = job.send_job()
                job = NomenclatureJob(scheme=scheme_info['wgMLST'],
                                      assembly_filepointer=d[0],
                                      assembly_barcode=d[1],
                                      database=dbname,
                                      user_id=69)
                dd = job.send_job()
    return


@manager.command
def rMLST_reference() :
    # one-time method. Not used anymore
    databases = generate_database_conns('', '', '').keys()
    for dbname in databases :
        print '##{0}'.format(dbname)
        dbase = get_database(dbname)
        schemes = dbase.models.Schemes
        scheme = dbase.session.query(schemes).filter(schemes.name == 'rMLST').first()
        if not scheme : continue
        
        asm = dbase.models.Assemblies
        alu = dbase.models.AssemblyLookup
        sts = {}
        for barcode, st in dbase.session.query(asm.barcode, alu.st_barcode).filter(asm.id == alu.assembly_id).filter(alu.scheme_id == scheme.id) :
            if st not in sts or barcode > sts[st] :
                sts[st] = barcode
        for st, barcode in sorted(sts.iteritems()) :
            print '{0}\tEnterobase:{1}:{2}'.format(st, scheme.param['scheme'].rsplit('_', 1)[0], barcode)


@manager.command
@manager.option('-d', '--databases', help='databases')
@manager.option('-r', '--records', help='barcodes for records')
def assemble_record(databases=None, records=None) :
    # send assembly jobs across all databases
    from entero.jobs.jobs import AssemblyJob
    
    for dbname in databases.split(',') :
        print '##{0}'.format(dbname)
        dbase = get_database(dbname)
        strains = dbase.models.Strains
        traces = dbase.models.Traces
        
        # get assessions
        accessions = {}
        for id, accession in dbase.session.query(traces.id, traces.accession).filter(strains.barcode.in_(records.split(','))).filter(traces.strain_id == strains.id):
            print '#Load:', id, accession
            accessions[accession] = id
        
        if len(accessions) <=0 : continue
        # check whether is running
        try :
            accs = accessions.keys()
            for aId in xrange(0, len(accs), 50) :
                aa = accs[aId:aId+50]
                r = requests.get('http://137.205.123.127/ET/head/show_jobs', \
                                 params={'FILTER':"pipeline='QAssembly' AND status in (VALUES ('RUNNING'),('WAIT RESOURCE'),('QUEUE')) AND (inputs#>>'{{read,0}}' IN (VALUES {0}) OR reads#>>'{{read}}' IN (VALUES {0}))".format(\
                                         ','.join(["('{0}')".format(acc) for acc in aa ])\
                                     )} \
                                 ).content
                for record in json.loads(r) :
                    try:
                        key = record['reads']['read']
                    except :
                        key = record['inputs']['read'][0]
                    accessions.pop(key, None)
        except :
            continue
        # send to CRobot
        for acc, id in accessions.iteritems():
            print '#Send:', id, acc
            job =AssemblyJob(trace_ids=[id],
                             database=dbname,
                             workgroup="public",
                             priority=3,
                             user_id=69)
            job.send_job()
    return "Assemblies submitted"


@manager.command
@manager.option('-d', '--database', help='comma delimited names for databases')
def audit_assembly(database='yersinia') :
    # Useful method to clean up assembly table
    def fastaStatus(fname) :
        import re
        if not fname :
            return 0,0,0,0
        if fname.find('fastq') :
            fname = fname.replace('fastq', 'fasta')
            if not os.path.isfile(fname) :
                return 0,0,0,0
        name, seq = None, {}
        with open(fname) as fin :
            for line in fin :
                if line.startswith('>') :
                    name = line[1:].strip().split()[0]
                    seq[name] = []
                elif len(line.strip()) :
                    if name :
                        seq[name].extend(line.strip().split())
                    else :
                        return 0,0,0,0
        sizes = []
        low_qual = 0
        for n, s in seq.iteritems() :
            s = ''.join(s)
            sizes.append(len(s))
            low_qual += len(re.split(r'[^acgtACGT]', s)) - 1
        sizes.sort(reverse=True)
        total_len = sum(sizes)
        n_contig = len(sizes)
        n50 = reduce(lambda o, n:o if o[0]>=total_len/2 else [o[0]+n, n], sizes, [0, 0])[1]
        return total_len, n_contig, low_qual, n50
    for dbname in database.split(',') :
        print '##{0}'.format(dbname)
        dbase = get_database(dbname)
        assemblies = dbase.models.Assemblies
        for assembly in dbase.session.query(assemblies).filter(assemblies.status == 'Assembled').filter(assemblies.other_data.like('%reason_failed%')) :
            x = json.loads(assembly.other_data)
            x.pop('reason_failed', None)
            assembly.other_data = json.dumps(x)
        dbase.session.commit()
    
        again = True
        while again :
            again = False
            print '#ReviseOldAssemblies'
            for assembly in dbase.session.query(assemblies).filter(assemblies.status == 'Assembled').filter(assemblies.coverage == None).filter(assemblies.total_length != None).limit(500) :
                again = True
                nBase = dbase.session.query(dbase.models.Traces.total_bases).filter(dbase.models.Traces.id == dbase.models.TracesAssembly.c['trace_id']).filter(dbase.models.TracesAssembly.c['assembly_id'] == assembly.id).first()
                if nBase is not None and nBase[0] is not None :
                    assembly.coverage = round(float(nBase[0])/assembly.total_length+0.5, 0)
                    if assembly.coverage < 20 :
                        assembly.coverage = 0
                else :
                    assembly.coverage = 0
            dbase.session.commit()
    
        for assembly in dbase.session.query(assemblies).filter(assemblies.status == 'Assembled').filter(assemblies.coverage == None).filter(assemblies.total_length == None) :
            print assembly.file_pointer
            tag = fastaStatus(assembly.file_pointer)
            assembly.total_length, assembly.contig_number, assembly.low_qualities, assembly.n50 = tag
            assembly.coverage = 1
        dbase.session.commit()
        for aId, file_pointer in enumerate(dbase.session.query(assemblies.file_pointer, assemblies.barcode, dbase.models.Strains.barcode).filter(assemblies.status == 'Assembled').filter(assemblies.id == dbase.models.Strains.best_assembly)) :
            if aId % 5000 == 0 :
                print '#{0}'.format(aId)
            filename, barcode, barcode2 = file_pointer
            if not filename or not os.path.isfile(filename) or os.path.getsize(filename) < 1000 :
                print 'Missing_assembly\t{0}\t{1}\t{2}'.format(barcode2, barcode, filename)
    return

@manager.command
@manager.option('-d','--database')
def add_annotation_scheme(database=None):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("add_annotation_scheme falied, can not find %s database" % database)
        return

    DataParam =dbase.models.DataParam
    Schemes =dbase.models.Schemes
    param = {"input_type":"assembly","job_rate":50,"pipeline":"prokka_annotation","query_method":"process_generic_query","params":{"taxon":"Clostridioides","prefix":"{barcode}"},"display":"public","js_grid":"AnnotationGrid"}
    scheme = Schemes(description='prokka_annotation',name='Annotation',param=param)
    try:
        dbase.session.add(scheme)
        param1 =DataParam(tabname='prokka_annotation',name='gff_file',mlst_field='outputs,gff_file,1',display_order=1,nested_order=0,label='GFF File',datatype='text')
        param2= DataParam(tabname='prokka_annotation',name='gbk_file',mlst_field='outputs,gbk_file,1',display_order=1,nested_order=0,label='GBK File',datatype='text')
        dbase.session.add(param1)
        dbase.session.add(param2)
        dbase.session.commit()

    except Exception as e:
        app.logger.exception("add_annotation_scheme failed, error message: %s"%e.message)
        dbase.rollback_close_session()
    
@manager.command
@manager.option('-d','--database')
def add_rMLST_scheme(database=None):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("add_rMLST_scheme falied, can not find %s database" % database)
        return

    try:
        DataParam =dbase.models.DataParam
        Schemes =dbase.models.Schemes
        scheme = Schemes(description='rMLST',name='rMLST')
        param = {"input_type":"assembly","pipeline":"nomenclature","barcode":"ST,1", "js_grid":"MediumSchemeGrid","display":"public","summary":"st"}
        name = app.config['ACTIVE_DATABASES'][database][0]
        param["scheme"]=name+"_rMLST"
        scheme.param=param
        dbase.session.add(scheme)
        dbase.session.commit()

        tn='rMLST'
        nested_order=1
        indexes=[]
        for x in range(1,22):
            indexes.append(str(x))

        for x in range(30,54):
            indexes.append(str(x))
        for x in range(56,66):
            indexes.append(str(x))
        for x in indexes:
            prefix = "BACT000000"[0:-len(x)]
            name = prefix+x
            dp = DataParam(name=name,label=name,tabname='rMLST',display_order=10,nested_order=nested_order,datatype='integer',group_name='Locus')
            dbase.session.add(dp)
            nested_order+=1
        dp = DataParam(name="st",label="rST",tabname='rMLST',display_order=1,nested_order=0,datatype='integer',mlst_field='ST_id')
        dbase.session.add(dp)
        dbase.session.commit()
    except Exception as e:
        app.logger.exception("add_rMLST_scheme falied, error message: %s"%e.message)
        dbase.rollback_close_session


@manager.command 
@manager.option('-w', '--workspace', help='scheme')
def delete_workspace(workspace=None):
    from entero.ExtraFuncs.workspace import get_analysis_object
    ws = get_analysis_object(int(workspace))
    ws.delete()

def __audit_user_metadata(fout, dbase) :
    Strains = dbase.models.Strains
    from collections import defaultdict
    source_toUpdate = defaultdict(list)
    geo_toUpdate = defaultdict(list)
    for strain in dbase.session.query(Strains).filter(Strains.secondary_sample_accession == None) :
        if not strain.source_niche or not strain.source_type :
            details = unidecode.unidecode(u'{0}, {1}, {2}, '.format(strain.source_details, strain.source_niche, strain.source_type).replace(u'None, ', u''))
            if details :
                source_toUpdate[details[:-2]].append([strain.barcode, strain.source_niche, strain.source_type])
        if not strain.continent or not strain.country :
            details = unidecode.unidecode(u'{0}, {1}, {2}, '.format(strain.admin1, strain.admin2, strain.city).replace(u'None, ', u''))
            if details or strain.country :
                geo_toUpdate[(strain.country, details[:-2])].append([strain.barcode, strain.continent, strain.country])
    toUpdate = defaultdict(dict)
    if len(geo_toUpdate) :
        geo_api = app.config['DOWNLOAD_URI'] + 'api/geo_format'
        for (country, raw), data in geo_toUpdate.items() :
            try :
                if country :
                    r = ujson.loads(requests.get(geo_api, {'raw':raw, 'country':country}).text)
                else :
                    r = ujson.loads(requests.get(geo_api, {'raw':raw}).text)
                country, continent = r[0]['Country'], r[0]['Continent']
                for d in data :
                    if not d[1] :
                        toUpdate[d[0]]['continent'] = continent
                    if not d[2] :
                        toUpdate[d[0]]['country'] = country
            except :
                pass
    if len(source_toUpdate) :
        source_api = app.config['DOWNLOAD_URI'] + 'api/host_format'
        for raw, data in source_toUpdate.items() :
            try :
                r = ujson.loads(requests.get(source_api, {'raw':raw}).text)
                if r[0]['Source Probability'] >= 0.5 :
                    n, t = r[0]['Source Niche'], r[0]['Source Type']
                    for d in data :
                        if not d[1] :
                            toUpdate[d[0]]['source_niche'] = n
                        if not d[2] :
                            toUpdate[d[0]]['source_type'] = t
            except :
                pass
    cnt = 0
    SA = dbase.models.StrainsArchive
    for strain in dbase.session.query(Strains).filter(Strains.barcode.in_(toUpdate.keys())) :
        toChange = toUpdate[strain.barcode]
        for k, v in toChange.items():
            fout.write('"{0}"\t"{1}"\t"{2}"\t"{3}"\n'.format(strain.barcode, k, getattr(strain, k), v))
        
        if len(toChange) :
            sa = SA()
            for c in strain.__table__.columns :
                if not c.primary_key :
                    setattr(sa, c.name, getattr(strain, c.name))
            strain.version += 1
            for k, v in toChange.items() :
                setattr(strain, k, v)
            dbase.session.add(sa)
            cnt += 1
            if cnt % 1000 == 0 :
                dbase.session.commit()
    dbase.session.commit()
    return toUpdate
def __cleanUp_metadata(data, records) :
    for d in data :
        key = d['Sample']['Main Accession']
        metadata = d['Sample']['Metadata']
        metadata['Center'] = d['Sample']['Source']['Center']
        records[key] = metadata
        
def __update_record_metadata(fout, dbase, data) :
    cnt = 0
    def tryFloat(d, default=None) :
        try :
            return float(d)
        except :
            return default
    def tryInt(d, default=None) :
        try :
            return int(d)
        except :
            return default
    # get handler for strains and strain_archives
    Strains = dbase.models.Strains
    SA = dbase.models.StrainsArchive
    # get barcodes
    data2 = {}
    cur_records = {}
    for strain in dbase.session.query(Strains).filter(Strains.sample_accession.in_(data.keys())) :
        d = data[strain.sample_accession]
        data2[strain.barcode] = dict(strain=d.get('Strain', None), contact=d.get('Center', None), 
                                     city=d.get('City', None), admin1=d.get('admin1', None), admin2=d.get('admin2', None), 
                                     country=d.get('Country', None), continent=d.get('Continent', None) if 'Continent' in d else d.get('Continient', None),
                                     latitude=tryFloat(d.get('latitude', None)), longitude=tryFloat(d.get('longitude', None)), 
                                     collection_date=tryInt(d.get('Date', None)), collection_month=tryInt(d.get('Month', None)), collection_year=tryInt(d.get('Year', None)), 
                                     source_details=d.get('Source Details', None), source_niche=d.get('Source Niche', None), source_type=d.get('Source Type', None), 
                                     species=d.get('Species', None), )
        cur_records[strain.barcode] = strain.as_dict()
    for record in dbase.session.query(SA).filter(SA.barcode.in_(data2.keys())) :
        r1 = cur_records[record.barcode]
        r0 = record.as_dict()
        for k, v0 in r0.items() :
            v1 = r1.get(k, v0)
            if v1 != v0 and v1 != 'Air' :
                r1.pop(k, None)
    for strain in dbase.session.query(Strains).filter(Strains.barcode.in_(data2.keys())) :
        r1 = cur_records[strain.barcode]
        r2 = data2[strain.barcode]
        toChange = []
        for k, v in r2.items() :
            v1 = r1.get(k, v)
            try :
                v1 = unidecode.unidecode(v1)
            except :
                pass
            try :
                v = unidecode.unidecode(v)
                v = v[:200]
            except :
                pass
            if v1 != v and (v1 or v) and ( not isinstance(v, float) or not isinstance(v1, float) or abs(v1-v) > 0.00001 ) :
                fout.write( '"{0}"\t"{1}"\t"{2}"\t"{3}"\n'.format(strain.barcode, k, v1, v) )
                toChange.append([k, v])
        if len(toChange) :
            sa = SA()
            for c in strain.__table__.columns :
                if not c.primary_key :
                    setattr(sa, c.name, getattr(strain, c.name))
            strain.version += 1
            for k, v in toChange :
                setattr(strain, k, v)
            dbase.session.add(sa)
            cnt += 1
            if cnt % 1000 == 0 :
                dbase.session.commit()
    dbase.session.commit()
    return

def __audit_assembly_metadata(dbname, organism) :
    records = {}
    numcount = 1000
    last_batch = 0
    for offset_count in xrange(0, 1000000000, numcount) :
        URI = app.config['DOWNLOAD_URI'] + 'assembly'
        params = dict(
            term = organism,
            reldate = 99999,
            start = offset_count,
            num = numcount
        )
        try :
            resp = requests.get(url = URI, params = params, timeout=3000).text
            data = ujson.loads(resp)
            if isinstance(data,dict):
                if data.get("error"):
                    break
            app.logger.info('Getting {1} results from NCBI assembly for {0} from offset {2}.'.format(
                            organism,len(data), offset_count))
            if len (data) > 0 :
                last_batch = offset_count
                genome_id_list = __cleanUp_metadata(data, records)
            elif offset_count - last_batch > 20000 :
                break
        except :
            break
    return records

def __audit_sra_metadata(dbname, organism) :
    records = {}
    numcount = 2000
    last_batch = 0
    for offset_count in xrange(0, 1000000000, numcount) :
        URI = app.config['DOWNLOAD_URI'] + 'dump'
        params = dict(
            organism = organism,
            reldate = 99999,
            start = offset_count,
            num = numcount
        )
        try :
            resp = requests.get(url = URI, params = params, timeout=3000).text
            data = ujson.loads(resp)
            if isinstance(data,dict):
                if data.get("error"):
                    break
            app.logger.info('Getting {1} results from SRA for {0} from offset {2}.'.format(
                            organism,len(data), offset_count))
            if len (data) > 0 :
                last_batch = offset_count
                genome_id_list = __cleanUp_metadata(data, records)
            elif offset_count - last_batch > 20000 :
                break
        except :
            break
    return records

@manager.command
@manager.option('-d', '--db', help='Database name or all (only if no file)')
def audit_metadata_special_fromEBI(db=None) :
    for database in db.split(',') :
        dbase = get_database(database)
        Strains = dbase.models.Strains
        for id, strain in enumerate(dbase.session.query(Strains).filter(Strains.contact == 'EBI')) :
            acc = strain.secondary_sample_accession
            if acc :
                q = requests.get('http://137.205.123.127/ET/meta/metadata', {'sample': acc})
                try :
                    data = json.loads(q.text)
                    strain.contact = data[0]['Sample']['Source']['Center']
                    app.logger.info('{0} {1}'.format(acc, strain.contact))
                    if id % 100 == 99 :
                        dbase.session.commit()
                except :
                    pass
        dbase.session.commit()

@manager.command
@manager.option('-d', '--db', help='Database name or all (only if no file)')
def audit_metadata(db=None):
    """
    Imports public metadata from SRA
    """
    # 1. fetch all user data with empty space from database
    # 2. get corresponding metadata category
    # 3. get SRA metadata from MetaParser
    # 4. get assembly data from MetaParser
    # for each record :
    # 5. check whether it has been manually modified
    # 6. if not, update its metadata category
    for database in db.split(',') :
        dbase = get_database(database)
        if not dbase:     
            message="No database found for the provided species: %s"%database
            app.logger.error(message)
            return make_response(message)    
        

        names = dbhandle[database].DATABASE_DESC
        for name in names :
            records = __audit_sra_metadata(database, name)
            records.update(__audit_assembly_metadata(database, name))
        #records = ujson.load(open('hp.json'))
        with open('{0}.{1}.audit.log'.format(database, datetime.now().strftime("%Y%m%d_%H_%M")), 'a') as fout :
            __update_record_metadata(fout, dbase, records)
            # fill in empty user data
            __audit_user_metadata(fout, dbase)
    
@manager.command
@manager.option('-d', '--database', help='Database name')
@manager.option('-f', '--fname', help='file for assembled genomes')
@manager.option('-u', '--user_id', help='id for user')
def import_user_genome(database='yersinia',fname="", user_id=69):
    if not dbhandle.get(database):
        app.logger.error("import_user_genome failed, %s database could not found"%database)
        return
    # load file
    records = {}
    with open(fname) as fin :
        for line in fin :
            fn = line.strip().split()[0]
            assert os.path.isfile(fn), 'file not found'
            records[fn] = {'trace':None, 'strain':None, 'assembly':None}
    # check and insert traces
    # check and insert assemblies
    # check and insert strains
    curr_db = dbhandle[database]    
    Strains = curr_db.models.Strains
    Traces = curr_db.models.Traces
    Assemblies = curr_db.models.Assemblies
    existing_reads = {trace.read_location:trace for trace in curr_db.session.query(Traces).filter(Traces.read_location.in_(list(records.keys())))}
    existing_assemblies = {asm.file_pointer:asm for asm in curr_db.session.query(Assemblies).filter(Assemblies.file_pointer.in_(list(records.keys())))}
    strain_ids = {trace.strain_id:fn for fn, trace in existing_reads.items()}
    strain_asms = {asm.id:fn for fn, asm in existing_assemblies.items()}
    existing_strains = {}
    for strain in curr_db.session.query(Strains).filter(Strains.id.in_(strain_ids.keys())) :
        existing_strains[strain_ids[strain.id]] = strain
    for strain in curr_db.session.query(Strains).filter(Strains.best_assembly.in_(strain_asms.keys())) :
        existing_strains[strain_asms[strain.best_assembly]] = strain
    
    for rid, (fn, record) in enumerate(records.items()) :
        if fn not in existing_reads :
            record['trace'] = Traces(status='Uploaded', 
                                            seq_platform='Complete Genome', 
                                            seq_library='Single', 
                                            read_location=fn, 
                                            lastmodifiedby=user_id,
                                            created = datetime.now(),
                                            )
            curr_db.session.add(record['trace'])
        else :
            record['trace'] = existing_reads[fn]
        if fn not in existing_assemblies :
            record['assembly'] = Assemblies(file_pointer=fn, 
                                                status='Assembled', 
                                                other_data='',
                                                lastmodifiedby=user_id,
                                                created = datetime.now(),
                                                )
            curr_db.session.add(record['assembly'])
        else :
            record['assembly'] = existing_assemblies[fn]
        if fn not in existing_strains :
            record['strain'] = Strains(owner=user_id,
                                        strain=os.path.basename(fn),
                                        lastmodifiedby=user_id, 
                                        created = datetime.now(),
                                        release_date = datetime.now(),
                                       )
            curr_db.session.add(record['strain'])
        else :
            record['strain'] = existing_strains[fn]
        if rid % 500 == 0 :
            curr_db.session.commit()
    curr_db.session.commit()
    for rid, (fn, record) in enumerate(records.items()) :
        if record['strain'].uberstrain == None :
            record['strain'].uberstrain = record['strain'].id
        if not record['strain'].barcode :
            record['strain'].barcode = curr_db.encode(record['strain'].id, database, 'strains')
        record['strain'].best_assembly = record['assembly'].id
        record['trace'].strain_id = record['strain'].id
        record['assembly'].status = 'Assembled'
        if not record['trace'].barcode :
            record['trace'].barcode = curr_db.encode(record['trace'].id, database, 'traces')
        if not record['assembly'].barcode :
            record['assembly'].barcode = curr_db.encode(record['assembly'].id, database, 'assemblies')
        if not record['assembly'].other_data :
            record['assembly'].other_data = ujson.dumps({"strain_id":record['strain'].id,
                                                         "status":'Complete Genome'})
        if rid % 500 == 0 :
            curr_db.session.commit()
    curr_db.session.commit()
            
@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-f', '--filename', help="Filename with annotation")
def add_scheme_info(database=None, filename=None ):
    from entero.admin import admin_tasks
    if database in dbhandle.keys() and filename:
        if os.path.isfile(filename) : admin_tasks.add_scheme_info(database, filename)
    else:
        app.logger.error("File %s does not exists or bad database %s specified"%(filename,database))
    
@manager.command
def refresh_trees():
    from entero.cell_app.tasks import process_job
    z = db.session.query(UserPreferences).filter(UserPreferences.type.in_(['ms_sa_tree', 'ms_tree'])).all()
    for x in z:
        if x.data:
            data = json.loads(x.data)
            if not data.get('nwk') and data.get('job_id'):
                print data
                process_job(int(data.get('job_id')))


    
@manager.command
def run_app():
    from entero import app
    app.run(host='0.0.0.0',port=5000)


if __name__ == '__main__':
    manager.run()

