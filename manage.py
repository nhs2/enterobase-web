

#!flask/bin/phon

import os, re, copy
import json, requests,ujson
from flask_script import Manager, Shell, Command
from entero import create_app, db, dbhandle, config, load_logger,app, get_database, rollback_close_connection, rollback_close_system_db_session, generate_database_conns, get_database_label
from entero.databases.system.models import UserUploads,User,UserPermissionTags,UserJobs,query_system_database,UserPreferences, BuddyPermissionTags
from entero.cell_app.tasks import process_job, import_sra
import psycopg2
from psycopg2.extras import RealDictCursor
from datetime import datetime, timedelta
from sqlalchemy import func
import logging
import platform
import shutil
import csv
#from entero.ExtraFuncs.query_functions import combine_strain_and_exp_data,process_strain_query, query_function_handle
#from entero.ExtraFuncs.query_functions import query_function_handle,get_alleles_from_strain_ids


# Set enviro with ENTERO_CONFIG=<See config.py for options>
if platform.system() == 'Windows':
    entero = create_app(os.getenv('ENTERO_CONFIG') or 'windows_development')
else:
    entero = create_app(os.getenv('ENTERO_CONFIG') or 'development')
manager = Manager(entero)



def get_subject_id(all_arr):
    subject_id=None
    for arr in all_arr:
        if arr[0]=="SUBJECT_ID":
            if subject_id:
                print "It is duplicated ...............>>>>>>"
            subject_id=arr[1]
    return  subject_id




def create_custom_columns(labels, user_id=1943, database='helicobacter', datatype='text'):
    '''
    create custom view column
    :param labels: a list which contains the custom view labels
    :param user_id:
    :param database:
    :param datatype:
    :return:
    '''
    lables_ids={}
    for label in labels:
        column = UserPreferences(name=label, user_id=user_id, type="custom_column", database=database)
        column.data = ujson.dumps({"label": label, "datatype": datatype})
        db.session.add(column)
        db.session.commit()
        lables_ids[label]=column.id

    import pandas as pd
    df = pd.DataFrame.from_dict(lables_ids, orient="index")
    df.to_csv("/home/khaled/temp/prod_custom_data.csv")
    return lables_ids


def get_custom_columns_values(cust_values, lables_ids):
    '''
    get custom columns values
    :param cust_values:
    :param lables_ids:
    :return:
    '''
    custom_view={}
    for label, value in cust_values.items():
        if lables_ids.get(label):
            custom_view[lables_ids.get(label)]=value

    return custom_view

@manager.command
@manager.option('-d', '--database', help='database')
def insert_strains_in_database(database='helicobacter'):
    '''
    This was a fast solution to insert some legacy strains from csv file into enetrobase.
    THe csv file contained also some custom columns which needs to be created and insert their values
    The csv file contained ST for 7Gene scheme, so it is needed to get he barcode_st for each one using this number and inserted into assembly_lookup
    This method should be modified later to be genric.
    K.M. 11/02/2020
    :param database:
    :return:
    '''
    user_id=0
    import datetime
    import pandas as pd
    created_strains={}
    created_strains_file='/home/khaled/temp/created_strains_prod.csv'
    if os.path.exists(created_strains_file):
        temp= pd.read_csv(created_strains_file, sep=',', na_filter=False).to_dict('records')
        
        for ite in temp:
            created_strains[ite.get('key')]=ite.get('barcode')

    print created_strains
    created = datetime.datetime.now()
    dbase=get_database(database)
    if not dbase:
        print "database erro, could find databse for: %s"%database
        return
    params = dbase.models.DataParam
    Strains=dbase.models.Strains
    Traces=dbase.models.Traces
    Assemblies = dbase.models.Assemblies
    Assembly_lookup=dbase.models.AssemblyLookup



    strains_pars=dbase.session.query(params.name, params.label).filter(params.tabname=='strains').all()
    st_pars={}
    for st_par in strains_pars:
        st_pars[st_par[1]]=st_par[0]

    traces_pars=dbase.session.query(params.name, params.label).filter(params.tabname=='traces').all()
    tr_pars={}
    for ts_par in traces_pars:
        tr_pars[ts_par[1]]=ts_par[0]



    metadata_fields=['Name','Species','Comment','Source Niche',	'Source Type','Source Details','Host Sex','Host Ethnicity','Host Age','Continent','Country',	'Region',	'Area',	'Latitude',	'Longitude',	'Uploader',	'Contact','Citations', 'ST']
    custom_fields=['DNANum','HP_Number','Patient Name','Relationship',	'Language Family',	'Language high order subgroup',	'Language low order subgroup', 'Language']
    #lables_id=create_custom_columns(custom_fields)
    #print "==============================================="
    #print lables_id
    #print "==============================================="
    #return
    #the file which contains the custom cloumns (label and id)
    cust_files="/home/khaled/temp/prod_custom_data.csv"
    lables_id_=pd.read_csv(cust_files, sep=',', na_filter=False).to_dict('records')
    lables_ids={}
    for id_label in lables_id_:
        lables_ids[id_label['label']]=id_label['id']

    #csv file contains the data to insert the strains
    csv_file="/home/khaled/temp/Hpylori_M.csv"
    data = pd.read_csv(csv_file, sep=',', na_filter=False).to_dict('records')
    strains_to_insert = {}
    custom_views_to_insert={}
    for row in data:
        metedata={}
        cust_view={}
        strains_to_insert[row.get('Key')]=metedata
        custom_views_to_insert[row.get('Key')]=cust_view
        for item in metadata_fields:
            if row.get(item):
                metedata[item]=row.get(item)
        for cust in custom_fields:
            if row.get(cust):
                cust_view[cust]=row.get(cust)
    for key,strn in strains_to_insert.items():
        #if the data for the strain has been inserted before it will
        #exclude it
        if key in created_strains:
            continue
        custom_view_values=get_custom_columns_values(custom_views_to_insert[key], lables_ids)
        newStrain = {'custom_columns': custom_view_values}
        newStrain['owner'] = user_id
        newStrain['created'] = created
        for label, name in st_pars.items():
            if strn.get(label):
                newStrain[name]=strn.get(label)

        #dbase.session.add(newStrain)


        new_traces={"seq_platform": "7GeneMLST", "seq_library": "ND"}
        new_traces['status']='Legacy'
        for label, name in tr_pars.items():
            if strn.get(label):
                new_traces[name] = strn.get(label)

        new_assembly={}
        new_assembly['status']='legacy'
        new_assembly['lastmodified']=created
        new_assembly['lastmodifiedby']=user_id
        new_assembly['created'] = created

        new_traces['lastmodified'] = created
        new_traces['created'] = created
        new_traces['lastmodifiedby'] = user_id

        newStrain['lastmodified'] =  created
        newStrain['lastmodifiedby'] =user_id

        newStrain = Strains(**newStrain)
        new_traces = Traces(**new_traces)
        new_assembly=Assemblies(**new_assembly)

        dbase.session.add(newStrain)
        dbase.session.add(new_traces)
        dbase.session.add(new_assembly)
        dbase.session.commit()

        newStrain.barcode = dbase.encode(newStrain.id, database, 'strains')
        newStrain.uberstrain = newStrain.id

        new_traces.barcode = dbase.encode(new_traces.id, database, 'traces')

        new_assembly.barcode = dbase.encode(new_assembly.id, database, 'assemblies')

        newStrain.best_assembly = new_assembly.id

        new_traces.strain_id=newStrain.id

        dbase.session.commit()


        sql="insert into trace_assembly (trace_id, assembly_id) values (%s, %s)"%(new_traces.id, new_assembly.id)
        dbase.execute_query(sql, return_results=False)

        print newStrain.barcode
        print new_assembly.barcode
        print new_traces.barcode
        #check if ST is provided, if so it will create assemblylook row which contains the required data
        if strains_to_insert[key].get('ST'):
            #call nserv to get st_barcode for the provided st
            url = app.config['NSERV_ADDRESS'] + "/search.api/Helicobacter/7GeneMLST/types?type_id=%s" % \
                  strains_to_insert[key].get('ST')
            resp = requests.get(url=url, timeout=app.config['NSERV_TIMEOUT'])
            try:
                data = json.loads(resp.text)
                print data[0].get('barcode')
                print "type barcode in the server "

                other_data = {'results': {'st_id': strains_to_insert[key].get('ST')}}
                asslook = {'st_barcode': data[0].get('barcode'), 'assembly_id': new_assembly.id,
                           'status': 'COMPLETE', 'version': '1.0', 'scheme_id': 14, 'other_data': other_data}
                asslook = Assembly_lookup(**asslook)
                dbase.session.add(asslook)
                dbase.session.commit()

                # print type(data)

            except Exception as e:
                print e.error
                print "======================================>>>>>>>>>"


        created_strains[key] = newStrain.barcode
        df_2 = pd.DataFrame.from_dict(created_strains, orient="index")
        #write the finished strain into a file
        #it can be used to follow up the run and
        #if something wrong up, we will have a record for already inserted strains so,
        #if run the method again we will not insert them again
        df_2.to_csv(created_strains_file)

        # traces status is Legacy
        # assembly status is legacy




@manager.command
def change_ownership_fx_accesion_sanger(dbname='ecoli', filename='/home/khaled/temp/FX_IP_ERR_4Khaled.csv'):
    owner_id=259
    with open(filename) as f:
        acessons_code = f.read()
    #print type(acessons_code)
    acessons_code = acessons_code.split('\n')
    dbase=get_database(dbname)
    strains = dbase.models.Strains
    traces = dbase.models.Traces
    count=0
    miss_values=[]
    for accesion in acessons_code:
        accesion = accesion.strip()
        if not accesion:
            continue
        #print accesion


        #res = dbase.session.query(strains, traces).filter(traces.strain_id == strains.id).filter(traces.accession==accesion).all()



        sql="select strains.id, strains.owner from strains inner join traces on traces.strain_id=strains.id where traces.accession ='%s'"%accesion
        #print sql
        results=dbase.execute_query(sql)
        if len(results)>1:
            return
        if len(results)==0:
            #print  accesion
            miss_values.append(accesion)
            count+=1
            #print "==========================="
        else:
            print  results[0]['id'], results[0]['owner']
            #_change_strain_ownership(dbname, results[0]['id'], owner_id)

    print "missing values: ", count, len(miss_values)

    for acc in miss_values:
        acc=acc.strip()
        sql="select * from strains where secondary_sample_accession='%s'"%acc
        res = dbase.execute_query(sql)
        print  res[0]['id'], res[0]['owner']
        #_change_strain_ownership(dbname, res[0]['id'], owner_id)
        if len(res)==0:
            print "Wha is up ........."
        #if not acc in miss_values:
        #    print import_sra(dbname, accession=acc)

#FX_IP_ERR_4Khaled


def _change_strain_ownership(dbname, strain_id, owner_id):
    dbase = get_database(dbname)
    strains = dbase.models.Strains
    ed_strain=dbase.session.query(strains).filter(strains.id==strain_id).one()
    ed_strain.owner=owner_id
    result = dbase.update_record(0, ed_strain, 'strains', False)
    if result:
        print "Record is updated"
    else:
        print "Error: Failed to update the record"


#2795
#2795
@manager.command
def change_strains_ownership_jane():
    filename='/home/khaled/temp/jane_change_strains_owners.csv'
    dbase=get_database('ecoli')
    with open(filename) as f:
        strains_barcode= f.read()
    print type(strains_barcode)
    strains_barcode=strains_barcode.split('\n')
    print type(strains_barcode)
    print len(strains_barcode)
    Strains=dbase.models.Strains
    print strains_barcode    
    for barcode in strains_barcode:
        barcode=barcode.strip()
        if barcode=='Barcode':
            continue
        print "barcode:  ", barcode
        ed_strain=dbase.session.query(Strains).filter(Strains.barcode==barcode).one()
        ed_strain.owner=2795
        result =dbase.update_record(0, ed_strain, 'strains', False)        
        if result:
            print "Record is updated"
        else:
            print "Error: Failed to update the record"          


def change_strains_ownership():
    to_change_ownership={
    'vibrio': '/home/khaled/temp/vib_fx.csv',
    'ecoli': '/home/khaled/temp/ecloi_fx.csv',
    'senterica':'/home/khaled/temp/SAL_fx.csv' }
    
    for dbname, filename in to_change_ownership.items():
        print dbname
        dbase=get_database(dbname)
        if not dbase:
            continue
        Strains=dbase.models.Strains
        with open(filename) as f:
            strains_sra= f.read()  
            strains_sra=strains_sra.split('\n')
            for rw in strains_sra:
                rw=rw.split(',')
                if len (rw)!=4:
                    continue
                #print rw, len(rw)
                if dbname=='ecoli' or dbname=='senterica':
                    barcode=rw[1]
                else:
                    barcode=rw[2]
                ed_strain=dbase.session.query(Strains).filter(Strains.barcode==barcode).one()
                
                if ed_strain.id==int(rw[0]):
                    ed_strain.owner=0
                    result =dbase.update_record(0, ed_strain, 'strains', False)        
                    if result:
                        print "Record is updated"
                    else:
                        print "Error: Failed to update the record"                       
                    #print "OK"
                else:
                    print "Error"
            print len(strains_sra)
            print strains_sra[0]
        print "================================"
        

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-o', '--outputfolder', help='output folder')
def convert_GBK_to_faa(database='senterica', outputfolder='/share_space/assembly_faa_format'):
    processed_asemblies_file='/home/khaled/temp/SAL_GBK_To_faa.csv'
    processed_asemblies_file_r='/home/khaled/temp/SAL_GBK_To_faa_r.csv'
    
    if os.path.exists(processed_asemblies_file):
        with open(processed_asemblies_file) as f:
            processed_asemblies = f.read()   
    else:
        processed_asemblies=[]            
    
    database_folder=os.path.join(outputfolder, database)   
    
    if not os.path.exists(database_folder):
        os.makedirs(database_folder) 
        
    outputfolder=database_folder
    dbase=get_database(database)
    
    if not dbase:
        print "No Valid database for %s"%database
        return
    
    all_database_assemblies=get_all_database_strains(database)
    print "No of all datatbase assmeblies which are related to strains: ", len(all_database_assemblies)
    #print all_database_assemblies[0]
    #print all_database_assemblies[-1]
    tot=len(all_database_assemblies)-2
    for i in range (0, len(all_database_assemblies)):
    #for i in range (tot, 1, -1):    
        try:           
            #print "tot: ",tot
            #print "Counter: "
            sql='select other_data from assembly_lookup where scheme_id =10 and assembly_id=%s'%all_database_assemblies[i]['id']
            other_data=dbase.execute_query(sql)
            #print other_data
            gbk_file_name=other_data[0]['other_data']['results']['gbk_file']
            if not gbk_file_name or not os.path.exists(gbk_file_name) or gbk_file_name in processed_asemblies:
                print "File alerady handled for %s"%gbk_file_name
                continue
            GBK_faa_file_converter(gbk_file_name,outputfolder,processed_asemblies_file)
        except Exception as e:
            print "Error :%s"%e.message
            #return
        '''
   # None senterica /share_space/interact/outputs/4893/4893089/SAL_OB5542AA_AS.enterobase.gbk.gz 405542 None fasta
    gbk_files=["/share_space/interact/outputs/1396/1396164/SAL_AA9952AA_AS.enterobase.gbk.gz","/share_space/interact/outputs/1409/1409091/SAL_BA0004AA_AS.enterobase.gbk.gz",
      "/share_space/interact/outputs/1486/1486490/SAL_BA0854AA_AS.enterobase.gbk.gz","/share_space/interact/outputs/1486/1486488/SAL_BA0863AA_AS.enterobase.gbk.gz",
      "/share_space/interact/outputs/1486/1486486/SAL_BA0872AA_AS.enterobase.gbk.gz"]   
    for file_ in gbk_files:
        GBK_faa_file_converter(file_, outputfolder)
   '''

def GBK_faa_file_converter(filename,outputfolder, processed_asemblies_file):
    print "File:%s"%filename
    print "output foldere: %s"%outputfolder
    import gzip
    faa_contenets=[]
    faa_file_name=os.path.basename(filename).split('.')[0]+'.faa'
    faa_file_name=os.path.join(outputfolder,faa_file_name)    
    if os.path.exists(faa_file_name):
        return
    #try:
    with gzip.open(filename) as f:
        _data = f.read()
    data=_data.split('\n')        
    print type(data), data[0], data[-1]
    for i in range (0, len(data)):
        line=data[i]
        if line.lower().strip().startswith("/protein_id" ):            
            locus_tag='>'+line.strip().split(':')[1].replace('"','')
            returned=_get_translation(i, data)
            #print "locus_tag: ",locus_tag
            #print "Line: ", line
            #print"====================="
            #print translation
            if returned:
                faa_contenets.append(locus_tag)              
                translation=returned[1]
                faa_contenets.append(translation)
                i=returned[0]
            else:
                print returned
            #print "===================="
            #import sys
            #sys.exit()
            #continue
        #else:
            # pass#print line
        #with zipfile.ZipFile(filename, 'r') as zip_ref:
        #    zip_ref.extractall(outputfolder)    
    print "write the faa contenet to: %s"%faa_file_name
    ffa=[]
    ##print faa_contenets
    print len(faa_contenets)
    for i in range (0, len(faa_contenets)):
        #print "<><><><><><>",faa_contenets[i]
        if faa_contenets[i]:
            #print "=========",faa_contenets[i]
            ffa.append(faa_contenets[i])
        else:
            pass#print ">>>>>>>>", faa_contenets[i]
    #print ffa
    faa_contenets_='\n'.join(ffa)    
    f=open(faa_file_name, 'w')     
    f.write(faa_contenets_)  
    f.close()
    f=open(processed_asemblies_file, 'a')
    f.write("\n%s"%filename)
    f.close()     
        
    #except Exception as e:
    #    print "Error message: %s"%e.message

def _get_translation(i, data):
    found=False
    for j in range (i, len(data)):
        line=data[j]
        if line.lower().strip().startswith("/translation="):
            translation=line.strip().split('=')[1].replace('"','')
            found=True
            continue
        if (line.lower().strip().startswith('gene') or line.lower().strip().startswith('origin')) and found:
            return [j, translation]
        if found:
            if not translation:
                translation=line[j].strip().replace('"','')
            else:
                translation=translation+'\n'+line.strip().replace('"','')
        

@manager.command
@manager.option('-f', '--users_file', help='job_id')
#users_EBI_2.txt
def delete_user_reads(users_file='/home/khaled/users_EBI_2.txt'):
    #select owner,  contact  from strains where contact like '%Weil%' or contact like '%Pasteur%';
    #toz
    #694,606,882,683 ==>> keep them
    #11,952,989,181,085 ===>> delete
    #dbs=app.config['ACTIVE_DATABASES']
    '''
    1 : nabil Nabil Alikhan
2 : crobort Calculation Robot
3 : mskurnik Mikael Skurnik
4 : denise_morris Denise Morris
5 : zheminzhou Zhemin Zhou
6 : zhemin Zhemin Zhou
7 : wvntest William Nicholson
8 : Tsegaye Tsegaye Wondimu
9 : ced Anthony Smith
10 : Inga_Froding Inga Froding
11 : machtman Mark Achtman
12 : khaledk2 Khaled Mohamed
13 : khaled K Mohamed
14 : jchar Jane Charlesworth
15 : wangjing wang jing
76115
11863541548532
2488
7
#11870693145525
'''
    status_file='/home/khaled/temp/delete_read_files_status_count_12_08_2020_new.txt'
    import datetime

    start=datetime.datetime.now()


    print users_file
    import sys
    if users_file and os.path.isfile(users_file):
        with open(users_file) as f:
            usernames= f.read().splitlines()               
    else:
        print "file %s is not exist"%users_file
        return
    #sql="select * from users where username in %s"%','.join(usernames)
    for i in range (0, len(usernames)):
        usernames[i]=usernames[i].strip()
    users = db.session.query(User).filter(User.username.in_(usernames)).all()  
    counter=1
    users_ids=[]
    user_ids_=[]
    for user in users:
        print counter, user.firstname,user.lastname,', ', user.institution
        users_ids.append(str(user.id))
        user_ids_.append(user.id)
        counter+=1
    #return
    #UserUploads
    dbass=["senterica","vibrio","clostridium","ecoli","mcatarrhalis","yersinia","helicobacter"]

    reads_files=db.session.query(UserUploads).filter(UserUploads.species.in_(dbass)).filter(UserUploads.user_id.notin_(users_ids)).filter(UserUploads.status=='Assembled').all()
    total_s=0
    #for database_name, dbase in dbhandle.items():
    #    sql="select * from strains where owner not in (%s)"%",".join(users_ids)
    #    results=dbase.execute_query(sql)
    #    for res in results:
    #        sql_='select * from taces wehere strain_id=%s'%res['id']
    #        trscs=dbase.execute_query(sql_)
    #        #for trc in tr
    #    print len(results)
    
    #return
        
    total_size=0
    no_record=0
    not_exist=0
    users_uploads_sizes={}
    print ','.join(users_ids)
    #sys.exit()
    print "Removing read files, please wait ......"
    nt_included=0
    con=0
    for read_file in reads_files:
        con=con+1
        print "%s of %s"%(con, len(reads_files))
        if read_file.user_id in user_ids_:
            print "Erorr: user id is in ", read_file.user_id
            sys.exit()
        if read_file.file_name and read_file.file_location:
            p_file=os.path.join( read_file.file_location, read_file.file_name)
            if os.path.isfile(p_file):
            #if p_file.is_file():
                data=json.loads(read_file.data)
                #if data["seq_platform"] =="Complete Genome":
                #    continue
                if data["seq_platform"].strip().lower() !="ILLUMINA,ILLUMINA".lower() or read_file.status.strip().lower()!='Assembled'.lower() or data['seq_library'].strip().lower()!='Paired,Paired'.lower() or data['seq_library'].strip().lower()=='Single'.lower():
                    #print "not included ..........", p_file
                    nt_included+=1
                    #print data
                    #print data.keys()
                    continue #print data["seq_platform"]

                file_size=os.path.getsize(p_file)
                total_size+=file_size
                if read_file.user_id in users_uploads_sizes:
                    users_uploads_sizes[read_file.user_id]=users_uploads_sizes[read_file.user_id]+file_size
                else:
                    users_uploads_sizes[read_file.user_id]=file_size

                ####os.remove(p_file)
                f = open(status_file, 'a')
                f.write("\n%s" % p_file)
                f.close()

            else:
                not_exist+=1
                #print "%s is not a file"%p_file
                #print read_file.file_name, read_file.file_location
        else:
            no_record+=1
            #print "Missing file ..."
            #print read_file.file_name, read_file.file_location
                

    print  "Number of users to delete their reads: ",len (users_uploads_sizes)
    from collections import OrderedDict
    users_uploads_sizes_sorted_by_value = OrderedDict(sorted(users_uploads_sizes.items(), key=lambda x: x[1], reverse=True))   
    #print users_uploads_sizes_sorted_by_value.keys()[0] ,users_uploads_sizes_sorted_by_value[users_uploads_sizes_sorted_by_value.keys()[0]]
    lines=['First name,LastName,Institution, Read files size (GB)']
    for user_id, files_size in users_uploads_sizes_sorted_by_value.items():
        user = db.session.query(User).filter(User.id==user_id).one()
        lines.append((user.firstname+","+ user.lastname+","+user.institution+","+str(float(files_size)/1000000000)).encode('utf-8').strip())
        
    files_string='\n'.join(lines)    
    
    
    f=open('/home/khaled/temp/users_reads_to_be_deleted_acutal_12_08_2020.csv_new', 'w')
    f.write(files_string)
    f.close()
    print "=================================================================================="
    print len(reads_files)
    print "total_size:", total_size
    print "no_record", no_record
    print "not_exist:", not_exist
    print "nt_included: ", nt_included
    print "================================================================================"
    end = datetime.datetime.now()
    print "start:", start
    print "end time: ", end
    #import pandas as pd
    #data_files=pd.DataFrame(list(users_uploads_sizes_sorted_by_value.items()), columns=['User', 'Files_reads_size'])
   
#os.remove(p_file)
@manager.command
@manager.option('-j', '--job_id', help='job_id')
def kill_crobot_job(job_id=None):
    '''
    kill crobot job'''
    if not job_id:
        print "No job id is provided"
        return
    URI = app.config['CROBOT_URI']+"/head/kill_job/"+str(job_id)
    resp = requests.get(url = URI)   
    print resp
    

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-s', '--scheme', help='scheme')
@manager.option('-v', '--visible', help='visible')
def change_scheme_display(scheme='CRISPR', database='senterica', visible='private'):    
    '''
    change  the scheme Visuality'''
    #"display":"public"
    #"display": "private"
    dbase = get_database(database)       
    Schemes = dbase.models.Schemes
    ss = dbase.session.query(Schemes).filter(Schemes.description==scheme).first()
    print type(ss.param)
    pars=dict(ss.param)    
    pars['display']=visible
    ss.param=pars
    dbase.session.commit()    

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-s', '--scheme_id', help='scheme description')
def get_scheme_jobs_progress(scheme='SeroPred_2', database='senterica'):
    '''
    check scheme jobs progress '''
    dbase = get_database(database)       
    Schemes=dbase.models.Schemes   
    ss = dbase.session.query(Schemes).filter(Schemes.description==scheme).first()    
    #AssemblyLookup=dbase.models.AssemblyLookup     
    sql="select assembly_id, other_data from assembly_lookup where scheme_id=%s and status='%s'"%(ss.id,'COMPLETE')
    results=dbase.execute_query(sql)
    count=0
    failed_jobs=[]
    for res in results:        
        if res['other_data'].get("complete_count")>0:    
            count+=1
            failed_jobs.append(res['other_data']["job_id"])
    print len(failed_jobs)
    print count 
    print len(results)
    

@manager.command
def check_scheme_run_results():
    jobs_sent_file=r'/home/khaled/temp/jobs_sent_2.csv'
    if os.path.isfile(jobs_sent_file):
        with open(jobs_sent_file) as f:
            rn_records = f.read().splitlines()   
    else:
        rn_records=[]       
    database='senterica'
    dbase = get_database(database)    
    Schemes=dbase.models.Schemes    
    AssemblyLookup=dbase.models.AssemblyLookup 
    scheme_obj =dbase.session.query(Schemes).filter(Schemes.description=='SeroPred').one()
    sql="select assembly_id, other_data from assembly_lookup where scheme_id=12"
    results=dbase.execute_query(sql)
    count=0
    non_com_jobs_ids=[]
    comp_jobs_ass=[]
    from dateutil import parser
    dt = parser.parse('May 1 2019')
    print dt
    dit_ass={}
    for res in results:
        #print type(res['other_data'].get("results"))
        #print "==============================================="
        if res['other_data'].get("fail_count")==1:            
            non_com_jobs_ids.append(res['other_data'].get("job_id"))
            dit_ass[res['other_data'].get("job_id")]=res["assembly_id"]
                
                                
            count=count+1
                        
        #elif res['other_data'].get("complete_count")==1:
         #   comp_jobs_ass.append(res['assembly_id'])
            
        #if res['other_data'].get("results") and len(res['other_data'].get("results"))==0:
        
    jobs  = db.session.query(UserJobs).filter(UserJobs.id.in_(non_com_jobs_ids)).all()
    new_failed_jobs=[]
    ass_ids=[]
    for job in jobs:
        job_sent_date=job.date_sent   
    
        if job_sent_date>dt:
            new_failed_jobs.append(job.id)
            ass_ids.append(str(dit_ass[job.id]))
            print "Job id:", job.id
            print job_sent_date    
            print dit_ass[job.id]
            
    print "No of non failed records: %s"%count
    print len(non_com_jobs_ids)
    print "no of new failed josb: %s"%len(new_failed_jobs)
    print len(ass_ids)
    return
    sql="select file_pointer, id, status, barcode from assemblies where id in (%s)"%','.join(ass_ids)
    results=dbase.execute_query(sql)  
    counter=0
    for res in results:
            #check if the assembly is exist
            if not (res['file_pointer'])or not os.path.isfile(res['file_pointer']):          
                counter+=1  
                continue
            if res['barcode'] in rn_records:
                continue
            
            f=open(jobs_sent_file, 'a')
            f.write("\n%s"%res['barcode'])
            f.close()        
            
            job = GenericJob(database=database,
                                                scheme =scheme_obj,
                                                assembly_barcode =res['barcode'],
                                                assembly_filepointer=res['file_pointer'],
                                                user_id=0,
                                                priority=-3,
                                                workgroup="public")   
            job.send_job()
            print "Job is created for %s"%res['barcode']
    print len(results)
    print counter    
    print len(results)
    

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-s', '--scheme', help='scheme')
def send_scheme_jobs_for_database(database='senterica', scheme='SeroPred_2'):
    '''
    Send scheme jobs to croboot
    This is needed when creating a new scheme and want to run it using all the starins in the database
    '''
    from entero.ExtraFuncs.query_functions import combine_strain_and_exp_data,process_strain_query, query_function_handle
    from entero.jobs.jobs import GenericJob
    jobs_sent_file=r'jobs_sent.csv'
    if os.path.isfile(jobs_sent_file):
        with open(jobs_sent_file) as f:
            rn_records = f.read().splitlines()   
    else:
        rn_records=[]    
    dbase = get_database(database)    
    Schemes=dbase.models.Schemes
    results=get_all_database_strains()
    counter=0
    scheme_obj =dbase.session.query(Schemes).filter(Schemes.description==scheme).one()
    for res in results:
        #check if the assembly is exist
        if not (res['file_pointer'])or not os.path.isfile(res['file_pointer']):          
            counter+=1  
            continue
        if res['barcode'] in rn_records:
            continue
        
        f=open(jobs_sent_file, 'a')
        f.write("\n%s"%res['barcode'])
        f.close()        
        
        job = GenericJob(database=database,
                                            scheme =scheme_obj,
                                            assembly_barcode =res['barcode'],
                                            assembly_filepointer=res['file_pointer'],
                                            user_id=0,
                                            priority=-3,
                                            workgroup="public")   
        job.send_job()
        print "Job is created for %s"%res['barcode']
    print len(results)
    print counter
    '''
                job = GenericJob(database=database,
                                                scheme =scheme_obj,
                                                assembly_barcode =res['barcode'],
                                                assembly_filepointer=res['file_pointer'],
                                                user_id=0,
                                                priority=-9,
                                                workgroup="user_upload")
                job.send_job()

<class 'entero.jobs.jobs.GenericJob'>
senterica
SeroPred_2
<entero.databases.senterica.models.Schemes object at 0x7fe90bbf1f90>
scheme_obj =dbase.session.query(Schemes).filter(Schemes.description==scheme).one()


                '''       

@manager.command
def get_all_database_strains(database='senterica'):
    '''
       get all the assemblies which is related to strains (Assembled) inside a database

    '''
    from entero.ExtraFuncs.query_functions import combine_strain_and_exp_data,process_strain_query, query_function_handle
    #from entero.ExtraFuncs.workspace import CustomView
    
    #using the similar function which returns all the database strains    
    user_id=0
    #custom_view_id=26875
    strain_query='all'
    strain_query_type='query'
    show_substrains=None
    extra_data=None
    exp='assembly_stats'
    options={'only_editable': None, 'show_non_assemblies': None, 'no_legacy': 'true', 'only_with_data': None}
    #record_file=r'/home/khaled/temp/run_record.csv12'
    #failed_file=r'/home/khaled/temp/run_failed.csv12'
   #not_updated_list=r'/home/khaled/temp/run_not_updated.csv12'
    #only_to_be_run='/home/khaled/1_rn_record.csv'
    #if os.path.isfile(record_file):
       # with open(record_file) as f:
     #       rn_records = f.read().splitlines()   
    #else:
    #    rn_records=[]
        
    #if os.path.isfile(not_updated_list):
     #   with open(not_updated_list) as f:
      #      rn_no_udpate = f.read().splitlines()    
            
   # else:
     #   rn_no_udpate=[]   
        
   # with open(only_to_be_run) as f:
    #    only_to_be_run_list = f.read().splitlines()      
    
    print "process_strain_query"
    strain_data,aids = process_strain_query(database,strain_query,strain_query_type,user_id,show_substrains=show_substrains)
    asids=aids.split(',')
   
    if  not aids:
        exp_data={}
    else:
        print "query_function_handle"
        exp_data,aids = query_function_handle[database][exp](database,exp,aids,"assembly_ids",extra_data)
    return_data=combine_strain_and_exp_data(strain_data,exp_data,options)    
    return_data=json.loads(return_data)
   
    
    dbase = get_database(database)
    counter=0
    asids=[]
    strains_ass={}
    for strain in return_data['strains']:
        #check if it is has aleady run 
        #if so it will not be included
        #if str(strain.get('id')) in rn_records:
            #print "escape: ", strain.get('id')
        #    continue
            
        asid=strain.get('best_assembly')
        strains_ass[asid]=strain
        asids.append(str(asid))
    sql="select file_pointer, id, status, barcode from assemblies where id in (%s)"%','.join(asids)
    results=dbase.execute_query(sql)
    return results
'''
#this code had been used to run seqsero 2 and save the results inside 
# custome fields
    ids=[]
    for res in results:
        #check if the assembly is exist
        if not (res['file_pointer']) or not os.path.isfile(res['file_pointer']):          
            counter+=1
        #if exist run the code otherwise continue
        else:
            print strains_ass.get(res['id']).get('strain'), " assembly file: ",res['file_pointer']
          
            try:
             
                seq=_run_SeqSero(res['file_pointer'])
            except:
                print "failed : ", sid
                f=open(failed_file, 'a')
                line=("\n"+str(sid)+','+ strains_ass.get(res['id']).get('barcode') +','+ res['barcode'])
                f.write(line)
                f.close()    
                continue
                
            if len(res)>0:
                sid=strains_ass.get(res['id']).get('id')
                if not sid:                   
                    continue
                data={str(sid) :seq}               
             
                updated=CustomView.update_custom_data(database,data,custom_view_id=custom_view_id,user_id=user_id)
                if len(updated)>0:
                    print "saving the record: ", sid
                    f=open(record_file, 'a')
                    f.write("\n%s"%str(sid))
                    f.close()
                else:
                    print "Not updated"
                    f=open(not_updated_list, 'a')
                    f.write("\n%s"%str(sid))
                    f.close()                    
            
    print "counter: ", counter
    print len(return_data['strains'])
    print "ids: ", ids
'''
#SSO:26869
#SSH1

'''
 {26873: 'SSPS', 26870:'SSH1',26874 : 'SSPAP', 26869 : 'SSO', 26872:'SSserovar', 26871:'SSH2', 27057 : 'SSNote'}
------------------------------
 {'SSPS': 26873,'SSH1':26870, 'SSPAP':26874, 'SSO':26869, 'SSserovar': 26872, 'SSH2': 26871, 'SSNote':27057}
 
 custome_view
 name: id
 'SeqSero': 26875
  
'''


@manager.command
def _run_SeqSero(assembly_file=None):
    import subprocess
    '''
    Run seqSero2 using assembly file and return a dict to used to save the results inisde custome fields.
    '''
    ##########assembly_file=r'/share_space/assembly/51/51292/SAL_AA9952AA_AS.scaffold.fastq'
    ###########assembly_file=r'/home/khaled/SeqSero2/SAL_LB9537AA_AS.result.fasta'
    ext = os.path.splitext(assembly_file)[-1].lower()
    if ext == '.fastq':
        print "change the file ext ..."
        assembly_file =assembly_file[:-1]+"a"
    cmd=['/home/khaled/anaconda3/envs/SeqSero2/bin/python','/home/khaled/SeqSero2/SeqSero2_package.py','-m', 'k', '-t','4', '-i',assembly_file]
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, cwd=r'/home/khaled/temp/wd')
    stdout, stderr = process.communicate()
    seq={}
    #{'SSPS': 26873,'SSH1':26870, 'SSPAP':26874, 'SSO':26869, 'SSserovar': 26872, 'SSH2': 26871}
    if not stderr:
        result=stdout.split('\n')
        for item in result:
            line=item.split(':')
            if len(line)>=2:                
                lis_2=line[1:]                
                lis_2[0]=lis_2[0].replace('\t','')
                st=':'.join(lis_2)                
                if line[0]=='O antigen prediction':                    
                    seq['SSO']=st
                elif line[0]=='Note':
                    seq['SSNote']=st
                elif line[0]=='H1 antigen prediction(fliC)':                    
                    seq['SSH1']=st
                elif line[0]=='H2 antigen prediction(fljB)':                    
                    seq['SSH2']=st
                elif line[0]=='Predicted serotype':                    
                    seq['SSserovar']=st
                elif line[0]=='Predicted subspecies':                    
                    seq['SSPS']=st
                elif line[0]=='Predicted antigenic profile':                                        
                    seq['SSPAP']=st
                    
    return seq            
    


@manager.command
@manager.option('-a', '--api_token', help='api oken')
def get_user_from_api_token(api_token=None):
    user=User.decode_api_token(api_token)
    print user

@manager.command
@manager.option('-d', '--databases', help='databases')
@manager.option('-s', '--scheme', help='description for the scheme')
def populate_nserv_scheme_new(databases=None, scheme=None) :
    '''
    automatic send nomenclature jobs for all records that have assembly and have no info for a scheme    
    '''
    from entero.jobs.jobs import GenericJob
    
    for dbname in databases.split(',') :
        print '##{0}'.format(dbname)
        dbase = get_database(dbname)
        schemes = dbase.models.Schemes
        if scheme in ('cgMLST', 'wgMLST') :
            scheme_infos = dbase.session.query(schemes).filter(schemes.name.like('%'+scheme+'%')).order_by(schemes.id).all()
            if len(scheme_infos) == 0 :
                continue
            else :
                scheme_info = scheme_infos[-1]
        else :
            scheme_info = dbase.session.query(schemes).filter(schemes.description == scheme).first()            
        if not scheme_info : continue        
        assemblies = dbase.session.execute("SELECT assemblies.file_pointer, assemblies.barcode FROM strains join assemblies on strains.best_assembly=assemblies.id left outer join (select * from assembly_lookup where scheme_id = {0}) alu on assemblies.id = alu.assembly_id where uberstrain=strains.id and file_pointer is not NULL and st_barcode is NULL and assemblies.status='Assembled';".format(scheme_info.id)).fetchall()
        print "Number of assemblies: %s"%(len(assemblies))
        for assembly in assemblies:
            job = GenericJob(database=dbname,
                               scheme =scheme_info,
                               assembly_barcode =assembly[1],
                               assembly_filepointer=assembly[0],
                               user_id=0,
                               priority=-9,
                               workgroup="user_upload")
            job.send_job()                    

@manager.command
@manager.option('-d', '--databases', help='databases')
@manager.option('-s', '--scheme', help='name for the scheme')
def populate_nserv_schem(databases=None, scheme=None) :
    '''
    automatic send nomenclature jobs for all records that have assembly and have no info for a scheme
    # todo: check whether the job is already in CRobot before sending it out
    '''
    from entero.jobs.jobs import NomenclatureJob
    
    for dbname in databases.split(',') :
        print '##{0}'.format(dbname)
        dbase = get_database(dbname)
        schemes = dbase.models.Schemes
        if scheme in ('cgMLST', 'wgMLST') :
            scheme_infos = dbase.session.query(schemes).filter(schemes.name.like('%'+scheme+'%')).order_by(schemes.id).all()
            if len(scheme_infos) == 0 :
                continue
            else :
                scheme_info = scheme_infos[-1]
        else :
            scheme_info = dbase.session.query(schemes).filter(schemes.name == scheme).first()
            
        if not scheme_info : continue
        
        data = dbase.session.execute("SELECT assemblies.file_pointer, assemblies.barcode FROM strains join assemblies on strains.best_assembly=assemblies.id left outer join (select * from assembly_lookup where scheme_id = {0}) alu on assemblies.id = alu.assembly_id where uberstrain=strains.id and file_pointer is not NULL and st_barcode is NULL and assemblies.status='Assembled';".format(scheme_info.id)).fetchall()
        print '##{0}'.format(len(data))
        for d in data :
            if os.path.isfile(d[0]) :
                job = NomenclatureJob(scheme=scheme_info,
                                      assembly_filepointer=d[0],
                                      assembly_barcode=d[1],
                                      database=dbname,
                                      user_id=69)
                d = job.send_job()
    return

@manager.command
@manager.option('-d', '--databases', help='databases')
@manager.option('-s', '--scheme', help='name for the scheme')
def populate_alternatives(databases=None, scheme=None) :
    '''
    automatic send nomenclature jobs for all records that have assembly and have no info for a scheme
    # todo: check whether the job is already in CRobot before sending it out
    '''
    from entero.jobs.jobs import GenericJob
    
    for dbname in databases.split(',') :
        print '##{0}'.format(dbname)
        dbase = get_database(dbname)
        schemes = dbase.models.Schemes
        if scheme in ('cgMLST', 'wgMLST') :
            scheme_infos = dbase.session.query(schemes).filter(schemes.name.like('%'+scheme+'%')).order_by(schemes.id).all()
            if len(scheme_infos) == 0 :
                continue
            else :
                scheme_info = scheme_infos[-1]
        else :
            scheme_info = dbase.session.query(schemes).filter(schemes.name == scheme).first()
            
        if not scheme_info : continue
        
        data = dbase.session.execute("SELECT assemblies.file_pointer, assemblies.barcode FROM strains join assemblies on strains.best_assembly=assemblies.id left outer join (select * from assembly_lookup where scheme_id = {0}) alu on assemblies.id = alu.assembly_id where uberstrain=strains.id and file_pointer is not NULL and st_barcode is NULL and assemblies.status='Assembled';".format(scheme_info.id)).fetchall()
        print '##{0}'.format(len(data))
        for d in data :
            if os.path.isfile(d[0]) :
                job = GenericJob(scheme=scheme_info,
                                      assembly_filepointer=d[0],
                                      assembly_barcode=d[1],
                                      database=dbname,
                                      user_id=69)
                d = job.send_job()
    return


def write_dig_exit(rds, id_):
    for rd in rds:
        if rd[0]==id_:
            print "Docublicated records ...",  rd
    sys.exit()


@manager.command
@manager.option('-a', '--accession', help='accession')
def get_metadata(accession=None):    
    '''
    this method is used to fetch strain metadata from NCBI using accession no
    @accession: strain's accession number
    '''
    params = dict(run =accession)
    URI = app.config['DOWNLOAD_URI'] + '/metadata'      
    print "Calling the sever %s please wait ..."%URI
    resp = requests.post(url=URI, params=params, timeout=100)    
    if resp.status_code == 500:
        app.logger.error('Could not access metadata')
        
    elif resp.status_code == 404:
        app.logger.error('No response from NCBI')
        
    try:
        data = ujson.loads(resp.text) 
        return data
    except:
        print "Error while loading the server response: ", resp.text
    return None
 
 
@manager.command
def check_null_strain_names(): 
    "check the databases for the strains which do not have names"
    sql='select id, strain, created, lastmodified  from strains where strain is null order by lastmodified'
    for database_name, dbase in dbhandle.items():
        #if database_name!='ecoli':
        #    continue
        if database_name=='miu':
            continue
        results=dbase.execute_query(sql)
        if len(results)==0:
            continue     
        print "Database: %s"%database_name
        print "------------------"        
        in_2018=0
        in_2019=0
        in_2017=0
        in_2016=0        
        for res in results:
            #print res['created']
            if res['lastmodified'].year==2018:
                in_2018+=1
            elif res['lastmodified'].year==2019:
                in_2019+=1  
            elif res['lastmodified'].year==2017:
                in_2017+=1              
            elif res['lastmodified'].year==2016:
                in_2016+=1                  
                   
                #Traces = dbase.models.Traces    
                #records = dbase.session.query(Traces.strain_id,Traces.accession).filter(Traces.strain_id.in_([104466])).all()
                #allowedColumns = dbase.models.metadata.tables['strains'].c
                #print allowedColumns
                #call_ncbi_update_recordsudpare_records(dbase, records, allowedColumns, Traces)          
                #import sys
                #sys.exit()
                
                    
       
        print "Total no of strains: ", len(results)
        
        if in_2016>0:
            print "No of strains which are are added to EnteroBase  in 2016:  ", in_2016
        if in_2017>0:        
            print "No of strains which are are added to EnteroBase in 2017:  ", in_2017
        if in_2018>0:
            print "No of strains which are are added to EnteroBase in 2018:  ", in_2018
        if in_2019>0:        
            print "No of strains which are are added to EnteroBase in 2019: ", in_2019
        print "==================================================="

@manager.command
@manager.option('-j', '--job_id', help='files contains ids')
def call_job_from_Id(job_id=None)       :    
    '''
    Update job usinbg its job id
    it calls crobot to get the job string then use it to udpae the job
    Please note it uses the same methods which are used in Callback
    '''
    import datetime
    start=datetime.datetime.now()
    print "Start time: ", start
    try:
        URI = app.config['CROBOT_URI'] + "/head/show_job/" + str(job_id)
        resp = requests.get(URI)
        data = ujson.loads(resp.text)
        callback_info= data[0]        
        results= process_call_back(callback_info)        
        print results
        print start, datetime.datetime.now()
        #callback_info = request.form.get("CALLBACK")
    except Exception as e:      
        app.logger.exception("Error obtaining information for job id: %s\n URL: %s, error message: %s" % (job_id, URI, e.message))
        return ujson.dumps("Cannot get Job Information")
    

@manager.command
@manager.option('-f', '--job_file_name', help='files contains ids')
def call_job_from_file(job_file_name='/home/khaled/3721373.txt'):
    '''
    Update job from a file which conatins job json string
    Please note it uses the same methods which are used in Callback
    '''
    try:
        with open (job_file_name, "r") as job_file:
            callback_info=job_file.readlines()   
        print type(callback_info[0])
        callback_info=callback_info[0]
        if callback_info:
            data = json.loads(callback_info)
            data= data[0]        
            return process_call_back(callback_info)  
        else:
            app.logger.error("website_callback_debug failed, could not find CALLBACK, the form keys are: %s"%request.form)
            return "failed" 
        #return "Created"
    except Exception as e:        
        print (e.message)
        return "Failed"
    
def process_call_back(data):           
    from entero.jobs.jobs import LocusSearchJob,RefMaskerJob,get_crobot_job    
    jobNo = data.get("tag",0)    
    #app.logger.info("Received Call Back on Job %s" % jobNo)
    job = get_crobot_job(data=data)    
    #app.logger.info("Job %s has been gotten with object type %s" % (jobNo, type(job)))
    if job:
        print "Job is detremined %s, updating the job ....."%type(job)
        return job.update_job()
    else:
        app.logger.error("website_callback_debug failed, could not find Job, the callback_info is: %s" % callback_info)
        return "failed"

@manager.command
@manager.option('-f', '--file_name', help='files contains accession codes')
@manager.option('-d', '--dbname', help='database')
def get_missied_uberstrain(file_name=None, dbname='senterica'):
    '''
    search for missing strains database using their accession codes
    first it search for duplication records in the file
    then serach the database
    '''
    dbase=get_database(dbname) 
    Traces = dbase.models.Traces     
    if not dbase:
        return "database is not found for this name %s"%dbname    
    with open(file_name) as f:
        accession_codes = f.read().splitlines()
    checked_strains=[]
    repeated_acc=[]
    for i in range (0, len(accession_codes)):                   
        for j in range (0,len(accession_codes)):
            if i==j:
                continue            
            if accession_codes[i]==accession_codes[j]:
                if accession_codes[i] not in repeated_acc:
                    repeated_acc.append(accession_codes[i])  
        if accession_codes[i] not in checked_strains:
            checked_strains.append(accession_codes[i])
            
    print "dublicated field",repeated_acc, len(checked_strains), len(accession_codes)
   
    missed_strains=[]
    found_strains=[]
    duplicated_strains={}
    count=1    
    for acc in checked_strains:
        #print "record no: ",count
        count+=1
        records = dbase.session.query(Traces.strain_id,Traces.accession).filter(Traces.accession==acc).all()
        if len(records)==0:
            print acc ," is mssing"
            missed_strains.append(acc)
        else:
            if len(records)>1:
                print "no: ", len(records)
            else:
                if records[0][0] not in  found_strains:
                    found_strains.append(records[0][0])
                else:
                    duplicated_strains[acc]=records[0][0]
   
    print "Not found strains: ", missed_strains
    print "dublicated id: ", duplicated_strains
    
@manager.command
@manager.option('-f', '--file_name', help='files contains accession codes')
@manager.option('-d', '--dbname', help='database')
@manager.option('-r', '--reset_uberstrain', help='set True if to reset uberstrain')

def update_Metadata_from_NCBI_using_accession_codes(file_name=None, dbname='senterica', reset_uberstrain=False):   
    '''
    this method is used to update the strain metdata using reocrds fetched from NCBI
    @file_name: text file contains accession ids
    @dbname: database name which contains the strains
    @reset_uberstrain: reset uberstrain if its True
    '''
    with open(file_name) as f:
        accession_codes = f.read().splitlines()   
    
    dbase=get_database(dbname) 
    if not dbase:
        return "database is not found for this name %s"%dbname    
    Traces = dbase.models.Traces    
    records = dbase.session.query(Traces.strain_id,Traces.accession).filter(Traces.accession.in_(accession_codes)).all()
    allowedColumns = dbase.models.metadata.tables['strains'].c  
    print len(accession_codes)
    print len(records)
    call_ncbi_update_recordsudpare_records(dbase, records, allowedColumns, Traces, reset_uberstrain)
    
    
@manager.command
@manager.option('-f', '--file_name', help='files contains ids')
@manager.option('-d', '--dbname', help='database')
def update_Metadata_from_NCBI(file_name=None, dbname='senterica'):   
    '''
    this method is used to update the strain metdata using records fetched from NCBI
    @file_name: text file contains strain ids
    @dbname: database name which contains the strains
    '''
    with open(file_name) as f:
        strains_ids = f.read().splitlines()   
    
    dbase=get_database(dbname) 
    if not dbase:
        return "database is not found for this name %s"%dbname      
    Traces = dbase.models.Traces    
    records = dbase.session.query(Traces.strain_id,Traces.accession).filter(Traces.strain_id.in_(strains_ids)).all()
    allowedColumns = dbase.models.metadata.tables['strains'].c
    print allowedColumns
    call_ncbi_update_recordsudpare_records(dbase, records, allowedColumns, Traces)
    
def call_ncbi_update_recordsudpare_records(dbase, records, allowedColumns, Traces, change_uberstarins=False):
    accs={}
    counter=0
    Strains = dbase.models.Strains  
    
    for rd in records:
        counter+=1        
        print "Updating  recod no: ", counter, rd   
        #[u'Sample', u'Run', u'Creation Time', u'Project', u'Experiment', u'Sequencing']
        # [u'Source', u'SRA Accession', u'Errors in parsing NCBI attribute', u'Main Accession', u'Discarded NCBI attribute', u'Organism', u'Metadata']
        print "==================================="     
        metadata=get_metadata(rd[1])
        print ">>>>>>>>>>>>>>>>>>", type(metadata[0]),len(metadata)
        print "Sample<<<<<<<<<<<<>>>>>>>>>>>>",metadata[0]['Sample']
        #print "Run<<<<<<<<<<<<>>>>>>>>>>>>",metadata[0]['Run']
        #print "Creation Time<<<<<<<<<<<<>>>>>>>>>>>>",metadata[0]['Creation Time']
        #print "Experiment<<<<<<<<<<<<>>>>>>>>>>>>",metadata[0]['Experiment']
        #print "Sequencing<<<<<<<<<<<<>>>>>>>>>>>>",metadata[0]['Sequencing']
        #print "=====================================>>>>", metadata[0].keys()[2]
        return
        if not metadata:
            print "<><><><><><><><><><><><><><><><>><><><><><><><><><><>"
            print "Something wrong while getting metadata for accession: %s"%rd[1]
            print "<><><><><><><><><><><><><><><><>><><><><><><><><><><>"
            continue
        if not isinstance(metadata, list):
            print "<><><><><><><><><><><><><><><><>><><><><><><><><><><>"
            print "There is no metadata found for accession: %s"%rd[0]
            print "server respond: ", metadata
            print "<><><><><><><><><><><><><><><><>><><><><><><><><><><>"         
            continue
           
        print "prossesing the data"        
        editstrain=dbase.session.query(Strains).filter(Strains.id==rd[0]).one() 
        if change_uberstarins:
            editstrain.uberstrain=editstrain.id
        update_metadata(editstrain, metadata[0], allowedColumns,from_NCBI=True)
        #update the record and keep the current one temporary in strain archiev and they need to be deleted later
        result =dbase.update_record(0, editstrain, 'strains', False)        
        if result:
            print "Record is updated"
        else:
            print "Failed to update the record"
        

@manager.command
@manager.option('-f', '--file_name', help='files contains ids')
@manager.option('-d', '--dbname', help='database')    
def update_meta_Data_from_CSV(file_name=None, dbname='senterica'):
    '''
    update strins metadata from csv file
    the first column in the file is the key column, e.g. barcode
    Please note that the header need to have names as the ones in the starins table    
    '''
    dbase=get_database(dbname) 
    if not dbase:
        return "database is not found for this name %s"%dbname    
    import csv
    strains_list=[]
    with open(file_name, 'rb') as f:
        reader = csv.reader(f)
        strains_list = list(reader)  
    header=strains_list[0]
    new_meta_Dict={}
    for i in range (1, len(strains_list)):
        starin_meta_data={}
        new_meta_Dict[strains_list[i][0]]=starin_meta_data
        for j in range (1, len(header)):
            starin_meta_data[header[j]]=strains_list[i][j]

    Strains = dbase.models.Strains  
    allowedColumns = dbase.models.metadata.tables['strains'].c  
    if header[0]  not in allowedColumns:
        print "It is not found "
        return
    
    print header
    print allowedColumns
    counter=1
    for key, metadata in new_meta_Dict.items():
        print "%s: Updating row with this key % s: %s"%(counter,header[0], key)
        counter+=1
        editstrain=dbase.session.query(Strains).filter(Strains.sample_accession==key).one() 
        update_metadata(editstrain, metadata, allowedColumns)
        result =dbase.update_record(0, editstrain, 'strains', False)        
        if result:
            print "Record is updated"
        else:
            print "Error: Failed to update the record"        

    
        
        
        
def update_metadata(editstrain, metdata, allowedColumns, from_NCBI=False):
    ''''
    Updating strain metdata using a record fetched from NCBI or from csv file
    @editstrain: the strain's database record 
    @metdata: metada record 
    @allowedColumns: columns in strains table 
    '''
    sts={'creation_time':'created', 'center':'contact','main_accession':'sample_accession','sra_accession':'secondary_sample_accession','bioproject_id':'study_accession','year':'collection_year', 'month':'collection_month'}
    for key, value in metdata.items():
        if key=='Id':
            continue
        if isinstance(value, dict):
            update_metadata(editstrain, value, allowedColumns)
        else:
            new_key=key.replace(' ', '_').lower()  
            #print "Key: ", new_key,"Value: ", value
            
            if new_key in allowedColumns:
                if new_key=='contact' and from_NCBI:
                    print value[0]
                    #print "changing contyact to: ",(value[0][1]+' '+value[0][3])
                    print "changing email to : ", value[0][0]
                    #editstrain.contact=value[0][1]+' '+value[0][3]
                    print editstrain.email
                    editstrain.email=value[0][0]
                    #### I have commented this code as I think the strians ownership is not changed
                    #check user ownership
                    #owner = User.query.filter(func.lower(User.email)==func.lower(editstrain.email)).first()
                    #if not owner:
                    #    editstrain.owner=0
                    #else:
                    #    editstrain.owner=owner.id
                    #print "Onwer is : %s"%editstrain.owner
                    ####
                else:                   
                    if isinstance(value, list):
                        if len(value)==0:
                            value=None
                        else:
                            value=value[0]
                    elif new_key=='source_details':
                            value=value.split(';')[0]                        

                      
                    print "changing %s value to %s" %(new_key, value)
                             
                    #print getattr(editstrain, new_key, value)                    
                    setattr(editstrain, new_key, value)
            elif sts.get(new_key):
                print "changing %s value to %s" %(sts.get(new_key), value)
                #print getattr(editstrain, sts.get(new_key), value)
                setattr(editstrain, sts.get(new_key), value)                

           
@manager.command
@manager.option('-d', '--database', help='Database name')
@manager.option('-s', '--scheme', help='Description of the scheme to be linked')
@manager.option('-n', '--name', help='Name of the scheme to be linked. Ignore if name is the same as description', default=None)
@manager.option('-r', '--remote', help='Name of the scheme in NServ. Described as <dbname>_<scheme>')
#@manager.option('-u', '--users', help='comma delimited ids for the users to view the scheme. Default is 69 (zhemin)')
#I have changed to use usernames rather than ids as it more practical
@manager.option('-u', '--users', help='comma delimited names without white spaces for the users to view the scheme, Default (zhemin, khaledk2)')
def add_nserv_scheme(database=None, scheme=None, name=None, remote=None, users='zhemin,khaledk2'):
    '''
    Formal manage method to insert a scheme pre-defined in NServ into Enterobase. 
    The inserted scheme is still private and only visible to one user (as in parameter)
    You can set it to public manually in pgadmin. 
    Also need to restart the production server to make it run properly. 
    
    This is resistent to duplication. So you can also use this to grant access of a scheme to users. 
    
    Try to alter the URL address to default parameter if NServ has been moved out of hermes.warwick.ac.uk at the time of reading this. 
    
    Example 1:
        manage.py add_nserv_scheme -d helicobacter -s rMLST -r Helicobacter_rMLST
    Example 2:
        manage.py add_nserv_scheme -d helicobacter -s wgMLST -r HELwgMLST_wgMLST -u 69,73,123
    '''
    dbase = get_database(database)
    if not dbase:
        app.logger.error("create_scheme failed, %s database could not found" % database)
        return

    try:  
        nserv_dbname, nserv_scheme = remote.split('_', 1)
        if nserv_scheme == 'rMLST' :
            nserv_dbname = 'rMLST'

        #print nserv_dbname, nserv_scheme
        url = '{0}query.api/{1}/{2}/loci?fieldnames=fieldname'.format(app.config['NSERV_ADDRESS'], nserv_dbname,
                                                                        nserv_scheme)
        #print url
        loci = json.loads(requests.get(url='{0}search.api/{1}/{2}/loci?fieldnames=fieldname'.format(app.config['NSERV_ADDRESS'],nserv_dbname, nserv_scheme)).text)
        #print loci
        #url='..'.format(nserv_dbname, nserv_scheme)
        #loci =json.loads(requests.get(url=url).text)     
        loci = { locus['locus'] for locus in loci }
        # set up scheme tab
        schemes = dbase.models.Schemes
        s = dbase.session.query(schemes).filter(schemes.description==nserv_scheme).first()
        if not s :
            if not name :
                name = scheme
            param={"pipeline": "nomenclature", 
                   "input_type": "assembly", 
                   "barcode": "ST,1", 
                   "scheme": remote, 
                   "display": "private",
                   }
            if len(loci) > 10 :
                param.update({"query_method": "process_medium_scheme_query", 
                       "js_grid": "MediumSchemeGrid"})
            else :
                param.update({"query_method": "process_small_scheme_query_nserv",
                              "js_grid": "SchemeGrid"})
            
            s = schemes(description=nserv_scheme, name=name, param=param)
            dbase.session.add(s)
            dbase.session.commit()            
            s = dbase.session.query(schemes).filter(schemes.description==nserv_scheme).first()
        # set up ST column
        params = dbase.models.DataParam
        display_order = dbase.session.query(params.display_order).filter(params.tabname==s.description).order_by(params.display_order.desc()).first()
        display_order = display_order[0] if display_order else 0
        
        st_col = dbase.session.query(params).filter(params.tabname==s.description).filter(params.name=='st').first()
        if not st_col :
            display_order += 1
            st_col = params(tabname=s.description,name='st', display_order=display_order, nested_order=0, label='ST', datatype="integer", mlst_field='ST_id')
            dbase.session.add(st_col)
        # set up locus columns
        loci_cols = dbase.session.query(params).filter(params.tabname==s.description).filter(params.group_name=='Locus').all()
        if len(loci_cols) :
            display_order = loci_cols[0].display_order
            nested_order = max([ lc.nested_order for lc in loci_cols ])
            for lc in loci_cols :
                loci = loci - {lc.name}
        else :
            display_order += 1
            nested_order = 0
        for locus in sorted(loci):
            nested_order += 1
            print nested_order
            line = params(tabname=s.description,name=locus, display_order=display_order, nested_order=nested_order, label=locus, datatype="integer", group_name="Locus")
            dbase.session.add(line)
            
        dbase.session.commit()
        dbase.session.close()
        
        upt = UserPermissionTags
        for usr in users.split(',') :
            print usr
            print "======================================="
            user = db.session.query(User).filter(func.lower(User.username) == func.lower(usr)).first()
            if not user:
                continue
            uid = user.id
            ticket = db.session.query(upt).filter(upt.field=='allowed_schemes').filter(upt.value==nserv_scheme).filter(upt.species==database).filter(upt.user_id==uid).first()
            if not ticket :
                ticket = upt(field='allowed_schemes', value=nserv_scheme, species=database, user_id=uid)
                db.session.add(ticket)
        db.session.commit()
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("add_nserv_scheme failed, error message: %s"%e.message)


@manager.command
@manager.option('-d', '--database', help='database')
def add_anno(database='senterica'):
    dbase = get_database(database)
    dparam = dbase.models.DataParam
    dbase.session.execute("SELECT setval('data_param_id_seq', MAX(id)) FROM data_param;")    
    for x in dbase.session.query(dparam).filter(dparam.tabname == 'prokka_annotation').all():
        no_id = x.as_dict().copy()
        no_id.pop('id')
        new_row = dparam(**no_id)
        new_row.tabname = 'assembly_stats'
        dbase.session.add(new_row)
    dbase.session.commit()
    
    
    


@manager.command
@manager.option('--database', help='database')
@manager.option('--type', help='source of data [sanger, nextseq, ftp, standard]')
@manager.option('--samplesheet', help='SampleSheet.csv for nextseq run. Sample_ID needs to be itemtracker ID')
@manager.option('--readfolder', help='Actual folder to fastq reads.')
@manager.option('--username', help='user name.')
def import_new_reads(database='senterica', type=None, samplesheet=None, readfolder=None, username='zhemin'):
    user_id = db.session.query(User.id).filter_by(username=username).first()[0]
    from entero.admin import fetch_reads
    if type == 'sanger':
        print fetch_reads.fetch_from_sanger(database, os.getenv('sanger_password')) 
    elif type == 'sanger2':
        print fetch_reads.fetch_from_sanger2(database, readfolder) 
    elif type == 'nextseq':
        print fetch_reads.fetch_from_nextseq(database, samplesheet, readfolder)
    elif type == 'ftp':
        print fetch_reads.fetch_from_ftp(database)
    elif type == 'standard' :
        print fetch_reads.fetch_from_standard_sheet(database, samplesheet, readfolder, user_id)
    else: 
        print 'please specify data source'

@manager.command
def get_ass():
    dbname = 'ecoli'
    import tarfile
    dbase = get_database(dbname)
    Strains = dbase.models.Strains
    Assemblies =dbase.models.Assemblies
    files = ['/home/nabil/duy.txt'] # ['/home/nabil/bryan.txt']
    for file_pointer in files:
        with open(file_pointer) as f:
            x = f.read().split()
            #x = ['%s_AS' %z for z in x]
            print 'reading %s' %file_pointer
            locations = dbase.session.query(Assemblies.file_pointer, Strains.barcode)\
                .filter(Strains.barcode.in_(x))\
                .filter(Assemblies.file_pointer is not None)\
                .filter(Assemblies.file_pointer != 'missing')\
                .filter(Strains.best_assembly == Assemblies.id)\
                .all()
            print 'Fetched %d locations ' %len(locations)
            dir_path = '/share_space/interact/NServ_dump/%s' %os.path.basename(file_pointer.split('.')[0])
            if not os.path.isdir(dir_path):
                os.mkdir(dir_path)
            chunks = [locations[i:i + 1000] for i in xrange(0, len(locations), 1000)]
            count = 1
            for chunk in chunks:
                print ('writing chunk %d of %d' %(count, len(chunk)))
                file_path = os.path.join(dir_path, '%d.fasta.gz' %count)
                out = tarfile.open(file_path, mode='w')
                for x in chunk:
                    if x[0] and os.path.isfile(x[0][:-1] +'a'):
                        temp_path = '/home/nabil/' + x[1] + '.fasta'
                        shutil.copy(x[0][:-1] +'a', temp_path)
                        out.add(temp_path)
                        os.remove(temp_path)
                count += 1 
                out.close()    
    
@manager.command
@manager.option('-k', '--key', help='Decode api key')
def decode_api_token(key = None):
    if key:
        print User.decode_api_token(key)

@manager.command
def job_report():
    from entero.admin import admin_tasks
    admin_tasks.job_report()

@manager.command
@manager.option('-d', '--database', help='database')
def add_bases_length(database='senterica'):
    dbase = get_database(database)
    if not dbase:
        app.logger.error ("add_bases_length falied, can not find %s database" % database)
        return

    file_name="/share_space/interact/outputs/read_info.list"
    try:
        inFile = open(file_name);
        lines = inFile.readlines()
        Traces = dbase.models.Traces
        for line in lines:
            arr = line.split("\t")
            trace = dbase.session.query(Traces).filter_by(accession=arr[1]).first()
            if trace:
                trace.average_length=arr[3]
                trace.total_bases=arr[5]
                dbase.session.commit()

        inFile.close()
    except IOError:
        app.logger.exception("add_bases_length failed, could not read file: %s"%file_name)

    except Exception as e:
        app.logger.exception("add_bases_length failed, error message: %s"%e.message)
        dbase.rollback_close_session()

    
    

@manager.command
@manager.option('-u', '--user')
@manager.option('-f', '--folder')
@manager.option('-d', '--database')
@manager.option('-r', '--remote')
@manager.option("-m","--min_file_size")
def load_user_reads(user='martin',folder="/",database='senterica',remote="F",min_file_size=0):
    from entero.upload.views import load_reads_from_folder
    try:
        user_info = db.session.query(User).filter_by(username=user).first()
        ftp = remote
        if ftp in ["T","True","true"]:
            ftp_settings=True
        else:
            ftp_settings=None
        load_reads_from_folder(user_info.id, user, folder, database,ftp_settings,min_file_size)
    except Exception as e:
        app.logger.exception("load user reads failed, error message: %s"%e.message)
        #db.session.rollback()
        rollback_close_system_db_session()


#what is call_IS_positions?? I have commennted this function out till find the location of tis method
'''
@manager.command
def launch_is_job():
    from entero.ExtraFuncs.ISMapper import call_IS_positions
    from entero.ExtraFuncs.workspace import get_analysis_object
    ws = get_analysis_object(1888)
    aids = ws.get_assembly_ids()    
    call_IS_positions("yersinia", aids,"yersinia_IS1541")
'''
@manager.command
def move_s3_reads():
    from entero.admin import admin_tasks
    admin_tasks.move_s3_reads()

@manager.command
@manager.option('-d','--database')
def add_annotation_scheme(database=None):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("add_annotation_scheme falied, can not find %s database" % database)
        return

    DataParam =dbase.models.DataParam
    Schemes =dbase.models.Schemes
    param = {"input_type":"assembly","job_rate":50,"pipeline":"prokka_annotation","query_method":"process_generic_query","params":{"taxon":"Clostridioides","prefix":"{barcode}"},"display":"public","js_grid":"AnnotationGrid"}
    scheme = Schemes(description='prokka_annotation',name='Annotation',param=param)
    try:
        dbase.session.add(scheme)
        param1 =DataParam(tabname='prokka_annotation',name='gff_file',mlst_field='outputs,gff_file,1',display_order=1,nested_order=0,label='GFF File',datatype='text')
        param2= DataParam(tabname='prokka_annotation',name='gbk_file',mlst_field='outputs,gbk_file,1',display_order=1,nested_order=0,label='GBK File',datatype='text')
        dbase.session.add(param1)
        dbase.session.add(param2)
        dbase.session.commit()

    except Exception as e:
        app.logger.exception("add_annotation_scheme failed, error message: %s"%e.message)
        dbase.rollback_close_session()
    
@manager.command
@manager.option('-d','--database')
def add_rMLST_scheme(database=None):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("add_rMLST_scheme falied, can not find %s database" % database)
        return

    try:
        DataParam =dbase.models.DataParam
        Schemes =dbase.models.Schemes
        scheme = Schemes(description='rMLST',name='rMLST')
        param = {"input_type":"assembly","pipeline":"nomenclature","barcode":"ST,1", "js_grid":"MediumSchemeGrid","display":"public","summary":"st"}
        name = app.config['ACTIVE_DATABASES'][database][0]
        param["scheme"]=name+"_rMLST"
        scheme.param=param
        dbase.session.add(scheme)
        dbase.session.commit()

        tn='rMLST'
        nested_order=1
        indexes=[]
        for x in range(1,22):
            indexes.append(str(x))

        for x in range(30,54):
            indexes.append(str(x))
        for x in range(56,66):
            indexes.append(str(x))
        for x in indexes:
            prefix = "BACT000000"[0:-len(x)]
            name = prefix+x
            dp = DataParam(name=name,label=name,tabname='rMLST',display_order=10,nested_order=nested_order,datatype='integer',group_name='Locus')
            dbase.session.add(dp)
            nested_order+=1
        dp = DataParam(name="st",label="rST",tabname='rMLST',display_order=1,nested_order=0,datatype='integer',mlst_field='ST_id')
        dbase.session.add(dp)
        dbase.session.commit()
    except Exception as e:
        app.logger.exception("add_rMLST_scheme falied, error message: %s"%e.message)
        dbase.rollback_close_session()


@manager.command
@manager.option('-d','--database')
def update_assembly_coverage(database='clostridium'):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("update_assembly_coverage falied, can not find %s database" % database)
        return

    try:
        Assemblies = dbase.models.Assemblies
        asses =dbase.session.query(Assemblies).filter(Assemblies.coverage == None, Assemblies.status.in_(['Failed QC',"Assembled"])).order_by(Assemblies.id.desc()).all()
        job_to_ass ={}
        job_ids_list=[]
        sub_list=[]
        URI = app.config['CROBOT_URI']+"/head/show_jobs"
        for ass in asses:
            if not ass.job_id:
                continue
            job_to_ass[ass.job_id] = ass
            sub_list.append(str(ass.job_id))
            if len(sub_list)>50:
                job_ids_list.append(sub_list)
                sub_list=[]
        if len(sub_list)>0:
            job_ids_list.append(sub_list)

        for sub_list in job_ids_list:
            params = "tag IN (%s)" % (",".join(sub_list))
            resp = requests.post(URI,data={"FILTER":params})
            all_data = ujson.loads(resp.text)
            for data in all_data:
                try:
                    log = ujson.loads(data['log'])
                    bases = log['reads']['bases']
                    length = log['assembly']['Total_length']
                except :
                    continue
                coverage = bases/length
                coverage = float(format(coverage, '.1f'))
                ass=  job_to_ass[data['tag']]
                ass.coverage=coverage
            dbase.session.commit()

    except Exception as e:
        app.logger.exception ("update_assembly_coverage falied, error message: $s"%e.message)
        dbase.rollback_close_session()

 
@manager.command
def update_buddy_table():
    from entero.databases.system.models import BuddyPermissionTags

    sql = "SELECT * FROM buddy_permission_tags WHERE user_id <> 55 AND field = 'shared_workspaces'"
    results  = query_system_database(sql)
    if not results:
        return
    try:
        for item in results:
            buddy_id = item['buddy_id']
            user_id = item['user_id']
            w_spaces = item['value'].split(",")
            species= item['species']
            buddy_id=item['buddy_id']

            for ws in w_spaces:
                if not ws:
                    continue
                sql = "SELECT * FROM user_preferences WHERE user_id=%i AND database='%s' AND type= 'main_workspace' AND name ='%s'" % (user_id,species,ws)
                results  = query_system_database(sql)
                if len(results) ==0:
                    continue
                ws_id=results[0]['id']
                bpt =BuddyPermissionTags(user_id =user_id,buddy_id=buddy_id,field = 'main_workspace', value =str(ws_id),species=species)
                db.session.add(bpt)
            db.session.commit()

    except Exception as e:
        app.logger.exception ("update_buddy_table failed, error message: %s"%e.message)
        db.session.rollback()


@manager.command
def send_user_uploads():
    from entero.upload.views import file_upload_success
    
    try:
        uploads = db.session.query(UserUploads).filter_by(status='Uploaded')
        list_f = []
        for upload in uploads:
            list_f.append([upload.user_id,upload.file_name,upload.file_location,upload.species])
        for item in list_f:        
            file_upload_success(item[0],item[1],item[2],item[3])
    except Exception as e:
        app.logger.exception("Error in send_user_uploads, error message: %s"%e.message)
        db.session.close()

@manager.command
def chk_sra_import():
    dbase='ecoli'
    #acc='ERR832495'
    #acc='SRR9856453' #==>bad
    acc='SRR9845575' #==>should be good
    #acc='ERR832487'
    #acc='ERR592141'
    data=import_sra(dbase,accession=acc)
    print data
    
    '''
    
    #[u'Sample', u'Run', u'Creation Time', u'Project', u'Experiment', u'Sequencing']
    if data[0].get('Sample').get('Source'):
        print "BAD or not",data[0].get('Sample').get('Source').get('Center')
    print data[0]['Sample']['Metadata'].keys()
    parameters=['Country',
    'Year',{'source':[
    'Source Details',
    'Source Niche',
    'Source Type']}]
    counter=0
    source=0
    for item in parameters:
        print type (item)
        if isinstance (item, dict): 
            print item['source']
            for it in item['source']:
                print it
                if data[0]['Sample']['Metadata'].get(it):
                    source+=1
        elif data[0]['Sample']['Metadata'].get(item):
            counter=+1
    print counter, source
    if counter>=2 or (source>0 and counter >0):
        print "Go sample, good ahead"
    else:
        print "Bad sample, discard"
    
    
    for k, v in data[0]['Sample']['Metadata'].items():
        print "%s: %s"%(k,v)
    print data
    
    ''' 


@manager.command
@manager.option('-d', '--database')
@manager.option('-f', '--file_input')
@manager.option('-p', '--param')
def import_accessions_from_list(database="senterica",file_input=None,param='run'):
    if not get_database(database):
        app.logger.error("import_accessions_from_list falied, can not find %s database" % database)
        return
        
    input_handle = open(file_input,"r")
    accessions = input_handle.readlines()
    acc_list=[]
    for accession in accessions:
        acc_list.append(accession.strip())
    if param == 'sample':
        import_sra(database,sample=acc_list)
    else:
        import_sra(database,accession=acc_list)


@manager.command
@manager.option('-d', '--database')
@manager.option('-p', '--project')
@manager.option('-s','--sample')
@manager.option('-a','--accession')
def import_sra_data(database="senterica",project=None,sample=None,accession=None):
    if project:
        import_sra(database,project=project)
    elif sample:
        import_sra(database,sample=sample)
    elif accession:
        import_sra(database,accession=accession)    



###This method does not do anything rahter than quering databases and no results processed or returned
@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-s', '--scheme', help='scheme')
def replace_missing_sts(database='senterica',scheme='CRISPR'):
    from entero.cell_app.tasks import process_job
    dbase = get_database(database)
    if not dbase:
        app.logger.error("replace_missing_sts falied, can not find %s database" % database)
        return
    try:
        SQL ="SELECT strains.best_assembly,assemblies.barcode AS acc FROM strains inner join assemblies on assemblies.id = strains.best_assembly WHERE assemblies.status <> 'legacy' AND best_assembly NOT IN (SELECT assemblies.id FROM strains inner join assemblies on strains.best_assembly = assemblies.id inner join assembly_lookup ON assemblies.id = assembly_lookup.assembly_id WHERE assembly_lookup.scheme_id = 4)"
        results = dbase.execute_query(SQL)
        acc_list=[]
        for result in results:
            acc_list.append(result['acc'])
        jobs =db.session.query(UserJobs).filter(UserJobs.accession.in_(acc_list),UserJobs.pipeline=="CRISPer:Salmonella_CRISPR").order_by(UserJobs.id).all()
        for job in jobs:
            # I think something needs to be implemented rather than pass statment
            pass

    except Exception as e:
        app.logger.exception("replace_missing_sts falied, error message: %s" % e.message)
        db.session.rollback()
        db.session.close()

#Again I am not sure what this method is used for
#it is only quering databas and not processing the results or returing them
@manager.command
def send_snp_matrix():
    # the following method will failed as send_snp_matrix_job is not defined
    #so I have commneted out the folowing import sttament
    #from entero.cell_app.tasks import send_snp_matrix_job
    conn = None
    try:   
        in_handle= open("/share_space/snps/snp_numbers.txt")
        lines = in_handle.readlines()
        num_snps = {}
        for line in lines:
            arr = line.strip().split("\t")
            if arr[0] == 'YER_CA3196AA_AS':
                print arr[1]
            num_snps[arr[0]]=int(arr[1])
        conn = psycopg2.connect(app.config['ACTIVE_DATABASES']["yersinia"][1])
        cursor = conn.cursor(cursor_factory=RealDictCursor)
        SQL = "SELECT assembly_lookup.other_data,assemblies.barcode as bar,strains.strain AS s_name FROM assembly_lookup INNER JOIN assemblies on assembly_id = assemblies.id INNER JOIN strains ON strains.best_assembly= assembly_id WHERE scheme_id=5 AND st_barcode = 'YER_CA7729AA AS'"
        cursor.execute(SQL)
        results  = cursor.fetchall()
       
        
        ref = '/share_space/interact/outputs/664/664474/YER_CA7729AA_AS_genomic.fna'
        queries=[]
        for res in results:
            # what is rec ???????? I think it is res so I have chenged to res in the following line
            if res['bar']=='YER_CA7584AA_AS' or res['s_name']=='Justinian':
                continue            
            if num_snps[res['bar']]>2000 and res['bar']<>'YER_CA7750AA_AS':
                continue
            print res['bar']+"\t"+str(num_snps[res['bar']])
            data = json.loads(res['other_data'])
            queries.append(data['file_pointer'])
        print len(queries)
        #send_snp_matrix_job("yersinia", 0.5, queries, ref)
        conn.close()
            
    except Exception as e:
        app.logger.exception("send_snp_matrix failed, error message: %s"%e.message)
        if conn:
            rollback_close_connection(conn)
            #conn.rollback()
            #conn.close()

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-s', '--scheme', help='scheme')
def clean_barcodes(database='ecoli',scheme="rMLST"):
    conn = None
    try:   
        conn = psycopg2.connect(app.config['ACTIVE_DATABASES'][database][1])
        cursor = conn.cursor(cursor_factory=RealDictCursor)
        SQL = "SELECT * FROM schemes WHERE description='%s'" % scheme
        cursor.execute(SQL)
        scheme_info = cursor.fetchone()
        scheme_id  = int(scheme_info['id'])
        pipeline_name= scheme_info['pipeline_scheme']
        SQL = "SELECT st_barcode FROM assembly_lookup WHERE scheme_id=%i GROUP BY st_barcode" % (scheme_id)

        cursor.execute(SQL)
        results = cursor.fetchall()
        all_barcodes=[]
        for res in results:
            all_barcodes.append(res['st_barcode'])
        url = app.config['NSERV_ADDRESS']+"/retrieve.api"
        resp = requests.post(url=url,data={"barcode":",".join(all_barcodes)})
        data = json.loads(resp.text)
        
        for rec in data:
            all_barcodes.remove(rec['barcode'])
        for barcode in all_barcodes:
            SQL = "DELETE FROM assembly_lookup WHERE st_barcode='%s' AND scheme_id=%i" % (barcode,scheme_id)
            cursor.execute(SQL)
        conn.commit()
        conn.close()

    except Exception as e:
        if conn:
            rollback_close_connection(conn)
            #conn.rollback()
            #conn.close()
        app.logger.exception("clean_barcodes failed, error message: %s"%e.message)

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-l', '--limit', help='limit')
@manager.option('-f', '--force', help='force')
@manager.option('-f', '--queue', help='Queue')
@manager.option('-p', '--priority', help='priority')
def update_all_schemes(database='senterica',limit=100,force="false",queue='backend',priority=0):
    scheme_names=[]
    pipelines = []
    dbase = get_database(database)

    if not dbase:
        app.logger.error("update_all_schemes falied, can not find %s database" % database)
        return


    Schemes = dbase.models.Schemes
    app.logger.info('Updating all schemes')
    try:
        schemes = dbase.session.query(Schemes).filter(Schemes.param['pipeline']<>None). all()
        for scheme in schemes:
            param = scheme.param
            if param.get('alternate_scheme'):
                continue
            job_rate =param.get('job_rate')
            if job_rate == None:
                job_rate=limit
            update_scheme(database,job_rate,scheme.description,force=force,queue=queue,priority=priority)
        dbase.session.close()
    except Exception as e:
        app.logger.exception("can not update nomenclature jobs")
        dbase.rollback_close_session()

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-s', '--scheme', help='limit')
@manager.option('-f','--file_name',help='file')
@manager.option('-c','--category',help='file')
def add_alleles_to_scheme(database='senterica',scheme="",file_name=None,category=None):
    dbase = get_database(database)
    DataParam = dbase.models.DataParam
    Schemes = dbase.models.Schemes
    try:
        if file_name:
            if not os.path.exists(file_name):
                app.logger.exception("add_alleles_to_scheme, could not find %s file"%file)
                return
            in_handle = open(file_name,"r")
            lines = in_handle.readlines()
            header = lines[0].strip().split("\t")
            des_index = header.index("function")
            name_index = header.index("name")
            cat_index=header.index("category")

            count =1
            for line in lines[1:]:

                arr= line.strip().split("\t")
                if category and category<> arr[cat_index]:
                    continue
                locus =arr[0]
                description = arr[des_index]
                name = arr[name_index]
                label = locus+"("+name+")"
                dp = DataParam(tabname=scheme,name=locus,display_order=10,
                                           nested_order=count,label=label,description=description,
                                           datatype='integer',group_name='Locus')
                count+=1
                dbase.session.add(dp)
            dbase.session.commit()


        else:
            sch = dbase.session.query(Schemes).filter_by(description=scheme).first()

            arr=sch.pipeline_scheme.split("_")
            url="%s/search.api/%s/%s/loci" % ( app.config['NSERV_ADDRESS'],arr[0],arr[1])
            resp = requests.get(url=url)
            data = ujson.loads(resp.text)
            count=1
            for record in data:
                dp = DataParam(tabname=scheme,name=record['locus'],display_order=10,
                                nested_order=count,label=record['locus'],
                                datatype='integer',group_name='Locus')
                count+=1
                dbase.session.add(dp)
            dbase.session.commit()
    except Exception as e:
        app.logger.exception("add_alleles_to_scheme failed, error message: %s"%e.message)
        dbase.rollback_close_session()
        
        
@manager.command
def test2():
        url = app.config['NSERV_ADDRESS']+"/retrieve.api"
        resp = requests.post(url=url,data={"barcode":"ESW_HA3034AA_ST"})
        loci_set= set(['b2805','b2803'])
        data = ujson.loads(resp.text)     
        barcode_to_loci ={}
        for record in data:
            rec_dict={}
            for allele in record['alleles']:
                locus = allele['locus']
                if locus in loci_set:
                    rec_dict[locus]=allele['allele_id']
               
            barcode_to_loci[record['barcode']]=  rec_dict      
        return ujson.dumps(barcode_to_loci)
    


@manager.command
def test_job_method():
    from entero.jobs.jobs import  RefMaskerJob,CRISPRJob,AssemblyJob,get_crobot_job,send_all_jobs_for_assembly
    from entero.ExtraFuncs.workspace import get_analysis_object,get_analysis_objects
    from entero import app
    from entero.main.views import index
    #Schemes = dbhandle['senterica'].models.Schemes
    #scheme = dbhandle["senterica"].session.query(Schemes).filter_by(id=4).one()
    
    '''job = CRISPRJob(scheme=scheme,
                          assembly_filepointer="/share_space/interact/outputs/2686/2686203/SAL_YA3618AA_AS.scaffold.fastq",
                          assembly_barcode="SAL_YA3618AA_AS",
                          database='senterica',
                          user_id=55)
    #print "wwwww"
    job.send_job()
    
    job = AssemblyJob(database='miu',
                                      trace_ids=[244],
                                      user_id=55)
    print "hello"
    job.send_job()
    '''
    #client =app.test_client(use_cookies = True)
    #args={"email":"jack","password":"1234","remember_me":False}
    #client.post("/auth/login",data=args)
    #resp = client.get("/species/get_user_workspaces?database=senterica")
    #data = ujson.loads(resp.get_data())
    #check data
    #client.get("/auth/logout")
    '''dbase = dbhandle['clostridium']
    ass= dbase.session.query(dbase.models.Assemblies).filter_by(barcode="CLO_BA3700AA_AS").one()
    refmasker_scheme = dbase.session.query(dbase.models.Schemes).filter_by(description='ref_masker').first()
    job=RefMaskerJob(scheme=refmasker_scheme,
                                  assembly_barcode=ass.barcode,
                                  assembly_filepointer=ass.file_pointer,
                                  workgroup="user_upload",
                                  priority=-9,
                                  database="clostridium")
    job.send_job()
    '''
    #job = get_crobot_job(2745193)
    #job.update_job()
    #snp = get_crobot_job(268173)
    #snp.delete()
    #print snp
    searches = get_analysis_objects(name="__test__locus_search",user_id=55)
    for search in searches:
        search.delete()
    #o=get_analysis_object(11873)
   # o.send_ref_mapper_job()
    

@manager.command
def test_scheme_method():
    '''from entero.ExtraFuncs.workspace import JobTask
    from entero.cell_app.tasks import print_msg
    from entero.jobs.jobs import BaseJob,SuperJob
    #testing celery
    jt = JobTask()
    jt.apply_async(args=["hello from object"],queue='entero')
    print_msg.apply_async(args=["hello from method"],queue='entero')
    bj = BaseJob("pipeline1")
    #bj.apply_async(args=["data"],queue='entero')
    bj.process("data1")
    
    sj = SuperJob("pipeline2")
    sj.process("data2")
    '''
    #from entero.jobs.jobs import get_crobot_job
    #ls = get_crobot_job(2681713)
    #ls.process_job()
  
    from entero.ExtraFuncs.workspace import LocusSearch,MSTree
    from entero.jobs.jobs import get_crobot_job,MSTreeJob,NomenclatureJob,GenericJob,AssemblyJob
    from entero.ExtraFuncs.temp import UGSGenerator
    database="senterica"
    user_id=55
    params={"database":database,"user_id":user_id}    

    # WVN 23/2/18 TEMP uncommented
    
    '''#******test send Locus Search*********
    scheme= "cgMLST_v2"
    params['name']="__test__locus_search"
    fasta = open("/share_space/test_data/senterica.fasta").read()
    ls= LocusSearch.create_locus_search(params,scheme,fasta)
    ls.send_job()

    #******test send MS Tree*********
    scheme="cgMLST_v2"
    params['name']="__test_ms_tree"
    algorithm="MSTreeV2"
    data=ujson.loads(open("/share_space/test_data/ms_tree_data.json").read())
    parameters={"strain_number":15,"algorithm":"MSTreeV2","scheme":"test"}
    ms = MSTree.create_ms_tree(params,data,algorithm,parameters)
    ms.send_job()
    
    '''#******test send Nomenclature job********
    Schemes = dbhandle['senterica'].models.Schemes
    scheme = dbhandle["senterica"].session.query(Schemes).filter_by(id=1).one()
 
    job = NomenclatureJob(scheme=scheme,
                          assembly_filepointer="/share_space/interact/outputs/2686/2686203/SAL_YA3618AA_AS.scaffold.fastq",
                          assembly_barcode="SAL_YA3618AA_AS",
                          database='senterica',
                          user_id=55)
    job.send_job()
    '''
   
    #*********test send generic job**********
    Schemes = dbhandle['senterica'].models.Schemes
    scheme = dbhandle["senterica"].session.query(Schemes).filter_by(description="SeroPred").one()
    job = GenericJob(assembly_filepointer= "/share_space/interact/outputs/2687/2687727/SAL_YA3946AA_AS.scaffold.fastq",
                    assembly_barcode= "SAL_YA3946AA_AS",
                    database="senterica",
                    scheme=scheme,
                    user_id=55)
    job.send_job()

    
    
    #**************test send assembly job*******************
    job = AssemblyJob(database='miu',
                                  trace_ids=[244],
                                  user_id=55)
    job.send_job()
    
    job = AssemblyJob(database='senterica',
                                     trace_ids=[244],
                                     user_id=0)
    job.send_job()
    '''
   
    #job = get_crobot_job(2689223)
    #job.update_job()

    #job.update_job()
    #bj =BaseJob(params="help")
    #bj.send_job()
    #ms =MSTreeJob(user_id=10)
    #nj = get_crobot_job(2685729)
    #nj.process_job()
    #job =get_crobot_job(2689120)
    #job.update_job()    

    '''from entero.ExtraFuncs.temp import RemoteUGSJob
    Schemes = dbhandle['miu'].models.Schemes
    scheme = dbhandle["miu"].session.query(Schemes).filter_by(id=5).one()
    job =RemoteUGSJob(assembly_filepointer= "/share_space/interact/outputs/2689/2689223/MIU_AA0268AA_AS.scaffold.fastq",
                    assembly_barcode= "MIU_AA0268AA_AS",
                    database="miu",
                    scheme=scheme,
                    user_id=55)
    job.send_job()    '''

@manager.command 
@manager.option('-w', '--workspace', help='scheme')
def delete_workspace(workspace=None):
    from entero.ExtraFuncs.workspace import get_analysis_object
    ws = get_analysis_object(int(workspace))
    ws.delete()
    

@manager.command 
@manager.option('-d', '--dbname', help='database name')  
def check_strains_barcodes(dbname='senterica'):
    '''
    check strains barcode
    in case of more than one starins have the same barcode
    it will fix the one which has the wrong barcode by setting the correct barcode
    '''
    database=get_database(dbname)
    if not database:
        return "database is not found for this name %s"%dbname    
    try:
        sql='SELECT t.barcode FROM strains as t GROUP BY t.barcode HAVING COUNT(t.barcode)>1;'    
        Strains = database.models.Strains
        StrainsArchive = database.models.StrainsArchive
        results=database.execute_query(sql)
        fixed_strains=[]
        count=0
        correct=0
        for res in results:
            strains=database.session.query(Strains).filter(Strains.barcode==res.get('barcode')).all()
            for strain in strains:
                barcode_should_be=database.encode(strain.id, dbname, 'strains')                
                if barcode_should_be!=strain.barcode:                
                    #print "===>>>Strain Barcode is not correct, name: ", strain.strain, "owner:", strain.owner, "best assembly: ", strain.best_assembly, "created by: ", strain.created, "Modified by: ", strain.lastmodifiedby,"Version: ", strain.version, strain.uberstrain
                    strain.barcode=barcode_should_be
                    database.session.commit()
                else:      
                    print "This one is correct, name: ", strain.strain, "owner:", strain.owner, "best assembly: ", strain.best_assembly, "created by: ", strain.created, "Modified by: ", strain.lastmodifiedby,"Version: ", strain.version, strain.uberstrain
                    correct+=1                    
                
        print len(results), count, correct
    except Exception as e:
        print "error message: %s"%e.message
    
@manager.command
@manager.option('-d', '--dbname', help='database name')  
@manager.option('-e', '--email', help='Email')
def adjust_uberstarins_for_user(dbname='senterica', email=None):
    '''
    This method is used to check user's strain which have null uberstrain, 
    then update the returned record and set the uberstrain to be the strain id
    '''
    database=get_database(dbname)
    if not database:
        return "database is not found for this name %s"%dbname     
    user = db.session.query(User).filter(func.lower(User.email) == func.lower(email)).first()
    if not user:
        app.logger.info( "no user is found for this email: %s"%email)
        return    
    Strains = database.models.Strains
    strains=database.session.query(Strains).filter(Strains.owner==user.id).filter(Strains.uberstrain==None).all()
    print "length of null uberstrains: ", len(strains)
    for strain in strains:
        print strain.id, strain.owner, strain.uberstrain
        strain.uberstrain=strain.id
        result =database.update_record(0, strain, 'strains', False)  
        if result:
            print "Record is updated"
        else:
            print "Error: Failed to update the record"         
        
    
@manager.command 
@manager.option('-f', '--file_name', help='Email')
def delete_users_from_csv_file (file_name=None):
    '''
     Used to delete users using their emails which are saved in csv file (first column)
    '''
    import csv
    emails_list=[]
    with open(file_name, 'rb') as f:
        reader = csv.reader(f)
        emails_list = list(reader)
    print emails_list
    for email in emails_list:
        delete_user (email[0])

@manager.command 
@manager.option('-e', '--email', help='Email')
def delete_user (email=None):   
    '''
    Delete a user from database using his email'''

    if not email:
        app.logger.info("No user email is provided")
        return    
    try:        
        
        user = db.session.query(User).filter(func.lower(User.email) == func.lower(email)).first()
        if not user:
            app.logger.info( "no user is found for this email: %s"%email)
            return
        
        app.logger.info('deleteing user %s'%(user.firstname+' '+user.lastname))
        #change user starins ownership to crobot
        change_strains_ownership(user)
        #change user assembly ownership to crobot
        change_assembly_owneship(user)

        user_permission_tags=db.session.query(UserPermissionTags).filter(UserPermissionTags.user_id==user.id).all()
        
        app.logger.info('deleteing user permission tags')        
        
        for user_permission_tag in user_permission_tags:
            db.session.delete(user_permission_tag)
            
        budy_permission_tags=db.session.query(BuddyPermissionTags).filter((BuddyPermissionTags.buddy_id==user.id)|(BuddyPermissionTags.user_id==user.id)).all()
        app.logger.info('deleteing budy permission tags')
        
        for budy_permission_tag in budy_permission_tags:
            db.session.delete(budy_permission_tag)
            
        #user_uploads
        user_uploads=db.session.query(UserUploads).filter(UserUploads.user_id==user.id).all()
       
        app.logger.info('deleteing user uploads')
        for user_upload in user_uploads:
            db.session.delete(user_upload)
        db.session.commit()  
        #user_preferences, user_jobs and user_preferences will be deleted automaticaly
        db.session.delete(user)
        db.session.commit()    
    except Exception as e:
        app.logger.error("delete_user failed, error message: %s"%e.message)        
        rollback_close_system_db_session()
   
def change_strains_ownership(user):
    for database_name, dbase in dbhandle.items():
        app.logger.info("Looking for stains owned by user: %s in database: %s"%(user.firstname, database_name))
        Strains = dbase.models.Strains
        strains = dbase.session.query(Strains).filter(Strains.owner==user.id).all()
        if len(strains)>0:
            app.logger.info("Found %s strains owned by this user in database: %s "%(len(strains), database_name))     
            for starin in strains:
                starin.owner=0
            dbase.session.commit() 

def change_assembly_owneship(user):
    for database_name, dbase in dbhandle.items():
        app.logger.info("Looking for assembly owned by user: %s in database: %s"%(user.firstname, database_name))
        Assemblies = dbase.models.Assemblies
        assemblies = dbase.session.query(Assemblies).filter(Assemblies.user_id==user.id).all()
        if len(assemblies)>0:
            app.logger.info("Found %s assemblies owned by this user in database: %s "%(len(assemblies), database_name))     
            for assembly in assemblies:
                assembly.user_id=0
            dbase.session.commit() 

def check_dtabases_for_stains_owned_by_user  (user=None): 
    for database_name, dbase in dbhandle.items():
        app.logger.info("Looking for stains owned by user: %s in database: %s"%(user.firstname, database_name))
        Strains = dbase.models.Strains
        strains = dbase.session.query(Strains).filter(Strains.owner==user.id).all()
        if len(strains)>0:
            app.logger.info("Found %s strains owned by this user in database: %s "%(len(strains), database_name))     
            for stain in strains:
                starin.owner=0
            dbase.session.commit()           
            
            
@manager.command
def check_starisn_owners_change_no_valid_to_crobot():
    '''
    check assemblies and strains tables for all the database
    and report for each database and each strains and assemblies tables:
    - the total number of non valid owners (no row for them in users tables)
    - the total nubmer of null value in the table for the owner fiiedl
    - the total number owns y crobot
    - the total numbers owned by the users    
    and finally it change the ownership of non valid user to crobot(id=0)
    '''    
    for database_name, dbase in dbhandle.items():
        print("\nChecking %s database"%database_name)
        no_user=0
        none_user=0
        c_robot=0
        valid_users=0
        Strains = dbase.models.Strains
        Assemblies = dbase.models.Assemblies      
        strains = dbase.session.query(Strains).all()        
        print("%s starins in database"%len(strains))        
        for strain in strains: 
            #app.logger.info("checking strain %s with owner %s"%(strain.strain, strain.owner))
            if strain.owner==0:                
                c_robot+=1 
            elif not strain.owner:
                strain.owner=0
                none_user+=1                
            else:
                user = db.session.query(User).filter(User.id==strain.owner).first()                
                if not user:
                    no_user+=1
                    strain.owner=0                                      
                else:
                    valid_users+=1
        print("No valid user: %s , \n'Null' user: %s, \nc-robot owns: %s, \nowned by users: %s "%(no_user, none_user, c_robot, valid_users))
        dbase.session.commit()
        no_user=0
        none_user=0
        c_robot=0
        valid_users=0  
        assemblies=dbase.session.query(Assemblies).all()        
        print("%s assemblies in database"%len(assemblies))
        for assembly in assemblies:
            if assembly.user_id==0:
                c_robot+=1                        
            elif not assembly.user_id:
                none_user+=1
                assembly.user_id=0
            else:
                user = db.session.query(User).filter(User.id==assembly.user_id).first()
                if not user:
                    no_user+=1
                    assembly.user_id=0
                else:
                    valid_users+=1
        dbase.session.commit()
        print("No valid user: %s, \n'Null' user: %s, \nc-robot owns: %s, \nowned by users: %s "%(no_user, none_user, c_robot, valid_users))
        

@manager.command
@manager.option('-s', '--scheme', help='scheme')
@manager.option('-d', '--database', help='database')
def check_queued_jobs(scheme=None,database=None):
    dbase = get_database(database)

    if not dbase:
        app.logger.error("check_queued_jobs failed, %s database could not found"%database)
        return
    from entero.cell_app.tasks import process_job

    try:
        #for jobs returning alot of data e.g wgMLST this should be small
        chunk_size=2
        #get those jobs queued or complete without a barcode
        SQL = "SELECT assembly_lookup.other_data,assembly_lookup.id as alid FROM assembly_lookup INNER JOIN schemes on schemes.id = assembly_lookup.scheme_id   WHERE  ((status = 'COMPLETE' AND st_barcode IS NULL) OR status = 'QUEUED') AND schemes.description = '%s'"  % scheme
        results = dbase.execute_query(SQL)
        job_ids = []
        current_list=[]
        for result in results:
            job_id = result['other_data'].get("job_id")
            if job_id:
                current_list.append(str(job_id))
                if len (current_list) >chunk_size:
                    job_ids.append(current_list)
                    current_list=[]
            #No job id to recall - just delete and it will be called again
            else:
                SQL = 'DELETE  FROM assembly_lookup WHERE id = %i' % result['alid']
                dbase.execute_action(SQL)

        if len(current_list) <> 0 :
            job_ids.append(current_list)
        if len(job_ids)>0:
            for li in job_ids:
                process_job(",".join(li))
    except Exception as e:
        app.logger.exception("check_queued_jobs failed, error message: %s"%e.message)
        dbase.rollback_close_session()


@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-l', '--limit', help='limit')
@manager.option('-s', '--scheme', help='scheme')
@manager.option('-f', '--force', help='force')
@manager.option('-f', '--queue', help='Queue')
@manager.option('-p', '--priority', help='priority')
def update_scheme(database='senterica',limit=100,scheme='rMLST',force='False',queue='backend',priority=0):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("update_scheme failed, %s database could not found"%database)
        return
    from entero.jobs.jobs import network_job_classes
    conn = None
    limit= int(limit)

    try:   
        conn = psycopg2.connect(app.config['ACTIVE_DATABASES'][database][1])
        cursor = conn.cursor(cursor_factory=RealDictCursor)
        #get all the scheme info

        Schemes=dbase.models.Schemes
        scheme_obj = dbase.session.query(Schemes).filter_by(description=scheme).one()
        
        
        scheme_data = scheme_obj.param
        pipeline = scheme_data.get("pipeline") 
        max_fail=5
        if force in ['T',"True","true","t"]:
            max_fail=100

      
        SQL = "SELECT  assembly_lookup.other_data as info,assemblies.barcode as bar ,assemblies.file_pointer as file,assemblies.user_id as uid,assemblies.file_pointer as location,assembly_lookup.st_barcode AS st_barcode,assembly_lookup.status AS stat FROM strains INNER JOIN assemblies ON strains.best_assembly= assemblies.id AND assemblies.status = 'Assembled' LEFT JOIN assembly_lookup ON best_assembly=assembly_lookup.assembly_id AND assembly_lookup.scheme_id=%i WHERE assembly_lookup.status IS NULL OR assembly_lookup.status = 'FAILED'  ORDER BY assemblies.barcode DESC " % (scheme_obj.id)
        cursor.execute(SQL)
        results = cursor.fetchall()
        
        
        num_valid=0
        for res in results:
            if num_valid >= limit:
                break              
            #check whether job is worth resending
            other_data= res.get('info')
            if other_data:
                fails = other_data.get("fail_count")
                if fails and fails>max_fail:
                    continue
                #if other_data.get('invalid'):
                    #continue
            
            cur_priority = priority
            cur_workgroup= queue
            cur_user=0
            if res['uid']<>0:
                cur_user = res['uid']
                cur_workgroup='user_upload'
                cur_priority = -9
               
            job = network_job_classes[pipeline](scheme=scheme_obj,database=database, priority=cur_priority,
                      workgroup=cur_workgroup,
                      user_id=cur_user,assembly_barcode=res['bar'],assembly_filepointer=res['file'])
            job.send_job()
            num_valid=num_valid+1
        conn.close()
            
    except Exception as e:
        if conn:
            rollback_close_connection(conn)
            #conn.rollback()
            #conn.close()
        app.logger.exception("update_scheme failed, error message: %s"%e.message)


@manager.command
@manager.option('-d', '--databases', help='Database name')
@manager.option('-t', '--term', help='Database name')
@manager.option('-c', '--complete', help='Database name')
def import_whole_genome(databases='yersinia',term="",complete="T"):
    if complete== "T" or complete == "True" or complete =="true":
        complete=True
    else:
        complete=False

    for database in databases.split(',') :
        if not dbhandle.get(database):
            app.logger.error("import_whole_genome failed, %s database could not found"%database)
            return
        from entero.cell_app.tasks import import_assemblies
        if not term:
            import_assemblies(database,term,only_complete=complete)
        else:
            terms = term.split(",")
            for item in terms:
                import_assemblies(database,item,only_complete=complete)   


@manager.command
@manager.option('-d', '--database', help='Database name')
def create_scheme(database="senterica"):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("create_scheme failed, %s database could not found" % database)
        return

    try:   
        params = dbase.models.DataParam
        resp = requests.get(url = "http://147.188.173.178/NServ/search.api/Salmonella/rMLST/STs?ST_id=12")
        dat = json.loads(resp.text)
        pos=1
        for al in dat[0]['alleles']:
            line = params (tabname="rMLST",name=al['locus'],display_order=4,nested_order=pos,label=al['locus'],datatype="integer",group_name="Locus")
            pos+=1
            dbase.session.add(line)
            
        dbase.session.commit()
        dbase.session.close()
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("create_scheme failed, error message: %s"%e.message)

@manager.shell
def make_shell_context():
    """
    Creates a python REPL with several default imports
    in the context of the app
    """

    return dict(app=entero, db=db)

@manager.command
@manager.option('-q', '--queue', help='queue name')
@manager.option('-t', '--threads', help='threads')
def runcelery(queue="entero",threads=1,debug=True):
    from celery.bin.celery import main as celery_main
    #import any modules containing celery tasks or classes that have
    #not already been imported
    from entero.jobs  import jobs

    from entero import celery
    celery_args = ['celery', 'worker']
    celery_args.append('-Q%s'%queue)
    celery_args.append('--concurrency=%s'% str(threads))
    celery_main(celery_args)
    
@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-a', '--ass', help="Clean up assemblies")
def cleanup(database=None, ass=None):
    from entero.admin import admin_tasks
#    admin_tasks.sync_databases()
    keys = app.config['ACTIVE_DATABASES'].keys()
    if database: keys = [database]
#    admin_tasks.importcgMLST()
    ##admin_tasks.add_user_schemes(['/var/www/entero/schemes/diff_mlst.fas'],'Griffiths','clostridium','http://pubmlst.org/data/profiles/cdifficile.txt')
    ##admin_tasks.add_user_schemes(['/var/www/entero/schemes/list_mlst.fas'],'Pastuer','Listeria','http://pubmlst.org/data/profiles/lmonocytogenes.txt')
    ##admin_tasks.add_user_schemes(['/var/www/entero/schemes/effector.fna'],'Type3Effectors')
    ##admin_tasks.add_user_schemes(['/var/www/entero/schemes/STEC-VIR.fna','/var/www/entero/schemes/FLAG.fna'],'NFVir')
 #   admin_tasks.add_user_schemes(['/var/www/bigs/7genekleb.fna'],'pasteur7gene' ,'klebsiella', 'http://pubmlst.org/data/profiles/kpneumoniae.txt'  )
   # admin_tasks.add_user_schemes(['/var/www/bigs/betlackleb.fna'],'pasteurbetalac' ,'klebsiella' )
#    admin_tasks.add_user_schemes(['/var/www/bigs/cgmlstkleb.fna'],'pasteurcgmlst' ,'klebsiella' )
    #admin_tasks.add_user_schemes(['/var/www/bigs/cgmlst-list.fna'],'pasteurcgmlst' ,'Listeria' )
 #   admin_tasks.add_user_schemes(['/home/nabil/code/entero-web/entero/schemes/PA_ATCC43949_core_genes.fasta'],'cgmlstv1' ,'Photorhabdus' )
    
    
    for key in keys:
        #pass
        ###fix_legacy()
    ##    admin_tasks.sync_schemes(key)
        admin_tasks.refresh_dataparms(key)
        #if ass: 
            #admin_tasks.check_unassembled(key)
            #admin_tasks.clean_assemblies(key)
        #admin_tasks.fill_STs(key)

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-f', '--filename', help="Filename with annotation")
def add_scheme_info(database=None, filename=None ):
    from entero.admin import admin_tasks
    if database in dbhandle.keys() and filename:
        if os.path.isfile(filename) : admin_tasks.add_scheme_info(database, filename)
    else:
        app.logger.error("File %s does not exists or bad database %s specified"%(filename,database))
    

@manager.command
def update_mcnally(database=None, ass=None):
    from entero.admin import admin_tasks
    admin_tasks.update_mcnally()

@manager.command
@manager.option('-d', '--database', help='database')
def generate_strain_table_dump(database=None):
    from entero.admin import admin_tasks
    if database:     
        admin_tasks.strain_dump([database])
    else:
        admin_tasks.strain_dump(database)
        
    
@manager.command
@manager.option('-d', '--database', help='database')
def update_st_cache(database='senterica', ass=None):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("update_st_cache failed, could not find %s database"%database)
    dbase.update_st_cache(lowertime=10, uppertime=60)

    
@manager.command
@manager.option('-j', '--job', help='Test prefix')
def update_job(job=1):    
    process_job(job)

@manager.command
@manager.option('-j', '--job', help='Test prefix')
@manager.option('-p', '--priority')
@manager.option('-u', '--user')
def change_job_priority(job=0,priority =-9,user=None):
    try:
        arr = []
        URI_BASE = app.config['CROBOT_URI']+"/head/set_priority"
        if user:
            jobs = db.session.query(UserJobs).filter_by(user_id=int(user),status = 'QUEUE').all()
            for job in jobs:
                arr.append(str(job.id))
        else:
            arr = job.split(",")

        for job_id in arr:
            URI = URI_BASE + "?job_tag=%s&priority=%i"%(job_id,int(priority))
            resp = requests.get(url=URI)
    except Exception as e:
        app.logger.exception("Error in change_job_priority, error message: e%"%e.message)
        db.session.rollback()

@manager.command
def test_email():
    from entero.entero_email import send_email
    send_email("enterotest@gmail.com", "test", "mail/test_email",test="test")

@manager.command
def change_read_location():
    dbase = get_database('yersinia')
    try:
        Traces= dbase.models.Traces
        uploads = db.session.query(UserUploads).filter_by(species='yersinia').all()
        for upload in uploads:
            file_name = upload.file_name
            recs = dbase.session.query(Traces).filter(Traces.read_location.like("%"+file_name+"%"),Traces.lastmodifiedby==upload.user_id).all()
            for rec in recs:
                arr = rec.read_location.split(",")
                new_arr=[]
                for read in arr:
                    if file_name in read:
                        new_arr.append(os.path.join(upload.file_location,file_name))
                    else:
                        new_arr.append(read)
                rec.read_location = ",".join(new_arr)
        dbase.session.commit()
    except Exception as e:
        app.logger.exception("change_read_location failed, error message: %s"%e.message)
        dbase.rollback_close_session()
                    
        


    
@manager.command
def add_perms():
    from entero.databases.system.models import UserPermissionTags, User
    try:
        users = db.session.query(User).all()
        for user in users:
            dbs  = app.config['ACTIVE_DATABASES']
            for db_name in dbs:
                if dbs[db_name][2] == True:
                    perm = UserPermissionTags(species=db_name, field='view_species', user_id=user.id, value='True')
                    db.session.add(perm)
                    perm = UserPermissionTags(species=db_name, field='upload_reads', user_id=user.id, value='True')
                    db.session.add(perm)
        db.session.commit()
    except Exception as e:
        app.logger.exception("add_perms failed, error message: %s"%e.message)
        db.session.rollback()
    
@manager.command
def remove_duplicates():
    from entero.databases.system.models import UserJobs
    try:
        jobs = db.session.query(UserJobs).all()
        dict={}
        for job in jobs:
            if job.status.startswith('KILLED') or job.status.startswith('FAILED') or job.status == 'RECEIVE':
                continue
            if dict.get(job.accession):
                ojob = dict[job.accession]
                if job.status == 'COMPLETE' and ojob.status =='COMPLETE':
                    continue
                if job.status == 'COMPLETE' and ojob.status <> 'COMPLETE':
                    dict[job.accession] = job
                    URI = app.config['CROBOT_URI']+"/head/kill_job/"+str(ojob.id)

                    resp = requests.get(url = URI)

                    continue
                elif job.status <> 'COMPLETE' and ojob.status == 'RUNNING' or ojob.status == 'COMPLETE':
                    URI = app.config['CROBOT_URI']+"/head/kill_job/"+str(job.id)
                    resp = requests.get(url = URI)

                    continue
                else:
                    URI = app.config['CROBOT_URI']+"/head/kill_job/"+str(job.id)
                    resp = requests.get(url = URI)

            dict[job.accession] = job
    except Exception as e:
        app.logger.exception("remove_duplicates failed, error message: %s"%e.message)
        db.session.rollback()
                
@manager.command
@manager.option('-p', '--prefix', help='Test prefix')
def test(prefix='test'):
    """
    Run the unit tests.
    """
    # Setup logging for testing
    entero.config.from_object(config.configLookup['testing'])
    load_logger()
    
    entero.logger.info('Running Tests')

    from coverage import coverage
    import unittest
    # Do not run coverage profiler with WING debugging on
    if not entero.config['WINGDB_ACTIVE']:
        cov = coverage(branch = True, omit = ['tests/*'])
        cov.start()
    # Run tests
    test_loader = unittest.TestLoader()
    test_loader.testMethodPrefix = prefix
    tests = test_loader.discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)
    # Create coverage report
    if not entero.config['WINGDB_ACTIVE']:
        cov.save()
        cov.stop()
        print "\n\nCoverage Report:\n"
        cov.report()
        if not os.path.exists(os.path.join('tests', 'coverage')):
            os.mkdir(os.path.join('tests', 'coverage'))
        print "\nHTML version: " + os.path.join(os.getcwd(),
                                                "tests", "coverage", "index.html")
        print "\nSee testing.log for detailed debugging information"
        cov.html_report(directory='tests/coverage/')
        cov.erase()


@manager.command
@manager.option('-d','--database',help='name of database')
@manager.option('-c','--create_schemes',help='Set to T to create default schemes')
def create_new_database(database=None,create_schemes="T"):
    dbase=dbhandle[database]
    if not dbase:
        app.logger.error("create_new_database failed, could not find % database"%database)
        return
    schemes = True
    if (create_schemes=='F' or create_schemes=="False" or create_schemes=='false'):
        create_schemes=False
    dbase.create_db(create_schemes)


@manager.command

@manager.option('-f','--folder',help='folder to put the data')
@manager.option('-d','--database',help='database to backup (all by default)')
@manager.option('-s','--system',help='If True (default) system database will be backed up')
def backup_database(folder ="/share_space/database_backup",database='all',system="T"):
    backup_system=True
    if system in ["F","false","False","FALSE"]:
        backup_system=False
            
    from time import gmtime, strftime
    today = strftime("%Y_%m_%d", gmtime())
   
    full_path = os.path.join(folder,today)
    if not os.path.exists(full_path):
        os.makedirs(full_path)
    if backup_system:
        url = app.config['SQLALCHEMY_DATABASE_URI']
        out_file= os.path.join(full_path,"entero.dump")
        command = "pg_dump -F c -f %s %s " %(out_file,url)
        os.system(command)
    databases = app.config['ACTIVE_DATABASES']
    if database<>'all':
        databases = {database:True}
    for database in databases:
        out_file = os.path.join(full_path,database+".dump")
        url  = app.config['ACTIVE_DATABASES'][database][1]
        command = "pg_dump -F c -f %s %s " %(out_file,url)
        os.system(command)
    
 
 
  

@manager.command
@manager.option('-d','--database',help='all,species_only,system_only or name of database')
@manager.option('-r','--remove',help='Delete existing database')
def createdb(database='none', remove=None):
    """
    Drops and recreates defined databases, with no data
    """
    #Create User tables, which use default SQLAlchemy models
    if database=='system_only' or database == 'all':
        if remove: 
            db.drop_all()
        db.create_all()
        


    # Dynamically create all species databases, listed in ACTIVE_DATABASES
    # from Config.py
    os.chdir('entero/')

    if database == 'all' or database == 'species_only':
        for dbname in dbhandle.keys():
            entero.logger.info('Recreating database %s' %dbhandle[dbname].engine)
            if remove: 
                dbhandle[dbname].destroy_db()
            dbhandle[dbname].create_db()
        return
    
    entero.logger.info('Recreating database %s' %dbhandle[database].engine)
    if remove: 
        dbhandle[database].destroy_db()
    
    dbhandle[database].create_db()
#####################################################################
@manager.command
@manager.option('-d','--database',help='all,species_only,system_only or name of database')
def update_strain_meta(database='senterica'):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("update_strain_meta failed, could not find %s database"%database)
        return    
    try:
        Strains = dbase.models.Strains
        Traces = dbase.models.Traces
        missing_strains = dbase.session.query(Strains, Traces).filter(Strains.id==Traces.strain_id).filter(Traces.status != 'Legacy').all()
        run_accs = []
        for strain in missing_strains:
            run_accs.append(strain[1].accession)
            import_sra(database, accession=[strain[1].accession])
    except Exception as e:
        app.logger.exception("update_strain_meta failed, error message: %s"%e.message)
        dbase.rollback_close_session()

    

@manager.command
@manager.option('-d','--database',help='all,species_only,system_only or name of database')
def populatedb(database='all'):
    """
    Populates databases with intial data
    """
    os.chdir('entero/')
    # Populate system database
    if database=='system_only' or database == 'all':
        popfile = os.path.join('databases', \
                                    'system', 'populate.sql')
        if os.path.isfile(popfile):
            pop_handle = open(popfile, 'r')
            for insert in pop_handle:
                try:
                    if len(insert) > 0: db.engine.execute(insert.strip())
                except Exception:
                    print 'Didnt insert %s' %insert.strip()

    # Populate organism databases
    
    if database == 'all' or database == 'species_only':
        for dbname in dbhandle.keys():
            dbhandle[dbname].populate_db()
        return
    if dbhandle.get(database):
        dbhandle[database].populate_db()

def list_get (l, idx, default=''):
    try:
        return l[idx]
    except IndexError:
        return default


    
@manager.command
@manager.option('-d', '--db', help='Database name')
def clean_archive(db = 'senterica'):
    if dbhandle.get(db):
        dbhandle[db].clean_archives()

@manager.command
@manager.option('-d', '--db', help='Database name')
def wipe_archive_db(db='senterica'):
    if dbhandle.get(db):
        dbhandle[db].wipe_archive_db()

@manager.command
@manager.option('-d', '--db', help='Database name')
@manager.option('-t', '--table', help='Table name')
@manager.option('-i', '--index_id', help='Index id')
@manager.option('-v', '--version', help='Current version')
def get_versions(db='senterica',table='strains',index_id=None,current_version=None):
    if dbhandle.get(db):
        result = dbhandle[db].get_versions(table, index_id, current_version)
        import pprint
        for res in result :
            pprint.pprint(res)


  
@manager.command
def get_alleles():
    try:
        handle = open("/share_space/jbrowse/profiles.list")
        lines = handle.readlines()
        arr = lines[0].strip().split("\t")
        locus_list = arr[1:]
        alleles_to_get={}
        for locus in locus_list:
            alleles_to_get[locus]={}
        for line in lines[1:]:
            arr = line.strip().split("\t")
            for count,allele in enumerate(arr[1:]):
                alleles_to_get[locus_list[count]][allele]=""
        for locus in alleles_to_get:
            url= app.config['NSERV_ADDRESS']+"search.api/Escherichia/UoW/alleles?filter="
            allele_ids =[]
            for allele in alleles_to_get[locus]:
                allele_ids.append(str(allele))
            search = "allele_id IN (%s) AND locus = '%s'&fieldnames=allele_id,seq" % (",".join(allele_ids),locus)
            url=url+search
            resp = requests.get(url)
            data = ujson.loads(resp.text)
            for record in data:
                alleles_to_get[locus][str(record['allele_id'])]=record['seq']
        out = open("/share_space/jbrowse/concat.fasta","w")
        for line in lines[1:]:
            arr = line.strip().split("\t")
            out.write(">ST"+arr[0]+"\n")
            cat =""
            for count,allele in enumerate(arr[1:]):
                cat = cat + alleles_to_get[locus_list[count]][allele]
            out.write(cat+"\n")
        out.close()
        handle.close()
    except Exception as e:
        app.logger.exception("get_alleles failed, error message: %s"%e.message)
    
    

@manager.command
@manager.option('-d', '--database', help='Database name or all (only if no file)')
@manager.option('-f', '--fields', help='Database name or all (only if no file)')
def update_sra_fields(database='senterica',fields=None,):
    if dbhandle.get(db):
        from entero.cell_app.tasks import update_sra_fields
        name= app.config['ACTIVE_DATABASES'][database]
        update_sra_fields(database,name,fields)

@manager.command
@manager.option('-d', '--database', help='Database name or all (only if no file)')
@manager.option('-f', '--filename', help='Database name or all (only if no file)')
def change_barcode(database="ecoli",filename="none"):
    if not dbhandle.get(db):
        app.logger.error("change_barcode failed, %s database could not found"%database)
        return
    conn= None
    old_to_new={}
    try:
    
        out = open("/share_space/change.log",mode = "w")
        with open(filename) as f:
            lines = f.readlines()
            for line in lines:
                arr= line.strip().split("\t")
                old_to_new[arr[0]]=arr[1]

        
        conn = psycopg2.connect(app.config['ACTIVE_DATABASES'][database][1])
        cursor = conn.cursor(cursor_factory=RealDictCursor)
        for old in old_to_new:
            SQL= "UPDATE assembly_lookup SET st_barcode='%s' WHERE st_barcode='%s'" % (old,old_to_new[old])
            cursor.execute(SQL)
            out.write(SQL+"\n")
        out.close()
        conn.commit()
        conn.close()

    except IOError as e:
        app.logger.exception("change_barcode, error message: %s" % e.message)

    except Exception as e:
        if conn:
            rollback_close_connection(conn)
            #conn.rollback()
            #conn.close()
        app.logger.exception("change_barcode, error message: %s"%e.message)


@manager.command
@manager.option('-d', '--db', help='Database name')
@manager.option('-f', '--file_path', help='File path')
@manager.option('-c', '--column', help='column name ')
def update_custom_fields(db='senterica', file_path = 'home/nabil/code/Fig2-values.txt', column = "reBG & dominant serovar"):
    from entero.admin import admin_tasks
    if dbhandle.has_key(db):
        admin_tasks.update_custom_fields(db, file_path, column)
    else: 
        print 'Please specify valid database '


@manager.command
@manager.option('-d', '--database', help='Database name')
@manager.option('-u', '--user_name', help='User name')
@manager.option('-e', '--email', help='Email')
@manager.option('-t', '--tag', help='persmission tag')
@manager.option('-v', '--value', help='persmission tag value')
def add_user_permission_tag (database='ecoli', user_name='Khaled', email=None, tag=None, value=None):
    print "I am working ..", database, user_name, tag, value
    from entero.admin import admin_tasks
    admin_tasks.add_user_permission_tag(tag, database, user_name, email=email, value=value)

@manager.command
@manager.option('-d', '--db', help='Database name')
@manager.option('-u', '--user_name', help='User name')
@manager.option('-e', '--email', help='Email')
@manager.option('-a', '--api_only', help='Api only access')
def create_curator(user_name=None, db=None, api_only = False, email=None):
    from entero.admin import admin_tasks
    if api_only: 
        api_only = True

    if dbhandle.has_key(db) and (user_name or email):
        admin_tasks.create_curator(db, user_name, api_only, email= email)
    elif db == 'all' and (user_name or email):
        for db in ['senterica', 'ecoli', 'yersinia', 'mcatarrhalis']:
            admin_tasks.create_curator(db, user_name, api_only, email= email)
    else: 
        print 'Please specify valid database and user_name'

@manager.command
@manager.option('-d', '--db', help='Database name')
@manager.option('-m', '--module', help = 'Module to run, [all, uberstrain, representatives, autocorrect, sra_sync, serovar]')
def clean_metadata(user_name=None, db=None, api_only = False, email=None, module='all'):
    from entero.admin import admin_tasks
    if dbhandle.has_key(db) :
        if module in ['all', 'uberstrain', 'representatives', 'autocorrect', 'sra_sync', 'serovar']:
            admin_tasks.clean_metadata(db, module)
        else:
            print 'Invalid module choice'
    else: 
        print 'Please specify valid database'


@manager.command
def fix_data_param():
    database='helicobacter'
    dbase=get_database(database)
    if not dbase:
        print "database %s not found"%database
        return
    dataparm=dbase.models.DataParam
    results=dbase.session.query(dataparm).filter(dataparm.tabname=='strains').all()
    for res in results:
        
        if res.sra_field:
            print "'%s'"%res.label
            print "'%s'"%res.name
            print "'%s'"%res.sra_field            
            #res.sra_field=res.sra_field.strip()
            print "'%s'"%res.sra_field.strip()
            if res.sra_field=='Sample,Metadata, Host Ethinity':
                res.sra_field='Sample,Metadata,Host Ethinity'
                print "Found", res.sra_field
                dbase.session.commit()
                
            
        print "================="
    #dbase.session.commit()
    
@manager.command
@manager.option('-d', '--database', help='database name')
def get_st_barcodes_for_private_cgMLST_and_wgMLST(database='all'):
    '''
    get private cgMLST/wgMLST st_barcodes    
    '''
    s_list=['cgMLST V1 + HierCC', 'wgMLST', 'cgMLST V2 + HierCC']
    folder="/share_space/interact/inputs/private_sts/"    
    if database =='all':
        
        for dbname in dbhandle.keys():
            _st_barcodes(dbname, s_list,folder )
    else:
        _st_barcodes(database, s_list,folder )          
            
     
def _st_barcodes(dbname, s_list,folder ):
    dbase=get_database(dbname)
    if not dbase:
        return
    Schemes=dbase.models.Schemes
    s_ids=dbase.session.query(Schemes.id, Schemes.name).filter(Schemes.name.in_(s_list)).all()
    sids=''
    s_names=''
    if len(s_ids)>0:
        app.logger.info("checking database: %s"%dbname)

        for id_ in s_ids:
            if sids:
                sids=sids+','+str(id_[0])
                s_names=s_names+','+id_[1]
            else:
                sids=str(id_[0])
                s_names=id_[1]

        sids='(%s)'%sids
        query_date=str(datetime.now().date())
        sql="SELECT st_barcode FROM public.strains, assembly_lookup as alu where owner is not NULL and release_date > '%s'::timestamp and alu.assembly_id = strains.best_assembly and scheme_id in %s;"%(query_date,sids)
        results=dbase.execute_query(sql)
        file_contents=[]            
        for res in results:
            if res['st_barcode']:
                file_contents.append(res['st_barcode'])
        app.logger.info("no of records: %s for %s"%(len(file_contents),s_names))

        file_name=os.path.join(folder,dbname+'_st_barcode.txt');
        file_contents='\n'.join(file_contents)
        file_ = open(file_name, "w")
        file_.write(file_contents)            
        file_.close()        

@manager.command
def get_strains_with_release_period_more_than_year():
    '''
    this method is used to get strains with release period more than a year
    it can be used to  change the release date or save file which contents the strains names, owner, created and release date 
    the adminstrator can comment/un comment the lines for his use'''
    
    from dateutil.relativedelta import relativedelta    
    current_date=datetime.now()
    longest_current_date_ = current_date#+ relativedelta(years=1, days=1) 
    created_date=current_date+ relativedelta(years=-1, days=-1) 
    file_contents=[]
    file_contents.append('name, barcode,owner, creared, release date')    
    long_strains_release={}
    affected_users=[]
    total=0
    for dbname in dbhandle.keys(): 
        long_strains=[]
        long_strains_release[dbname]=long_strains
        dbase=get_database(dbname)
        Strains=dbase.models.Strains
        strains=dbase.session.query(Strains).filter(Strains.release_date>longest_current_date_).filter(Strains.created<created_date).all()
        for strain in strains:
            difference_in_years = relativedelta(strain.release_date, strain.created).years    
            if difference_in_years>1:
                long_strains.append(strain)                       
        
        if len(long_strains)>0:
            total=total+len(long_strains)
            
            file_contents.append(dbname)
            print "Long release date: ", len(long_strains)
            for strain in long_strains:
                if not strain.owner in  affected_users:
                    affected_users.append(strain.owner)                    
                #print strain.owner
                #new_date = strain.created + relativedelta(years=1)                
                #print "Name: %s, barcode: %s, Owner: %s, Created: %s, Release Date: %s"%(strain.strain, strain.barcode, strain.owner, strain.created, strain.release_date)
                file_contents.append("%s,%s,%s,%s,%s"%(strain.strain, strain.barcode, strain.owner, strain.created.date(), strain.release_date.date()))
                #strain.release_date=new_date                
                #result =dbase.update_record(0, strain, 'strains', False)
            #print len(results)
            print "====================================="
    #folder='/home/khaled'
    #file_name=os.path.join(folder,'strains_release_date.csv');
    #file_contents='\n'.join(file_contents)
    #file_ = open(file_name, "w")
    #file_.write(file_contents)            
    #file_.close()        
    print "total effected records", total
    print affected_users
             
@manager.command
@manager.option('-d', '--database', help='database name')
@manager.option('-t', '--tree_file', help='location of newick file')
@manager.option("-k","--key",help='key in nwk tree (assembly barcode by default')
@manager.option('-u',"--user_id" ,help='user_id (0 by default)')
@manager.option('-n',"--name" ,help='name of tree - nwk by default')
@manager.option('-s',"--scheme" ,help='scheme used to generate tree (none by default)')
@manager.option('-a',"--algorithm",help = "algorithm used to generate tree (unknown by default)")
def working_create_tree_from_newick(database=None,tree_file=None,key='assembly_barcodes',user_id=0,
                            name='nwk tree',scheme='"None',algorithm='unknown'):
    from Bio import Phylo
    from entero.ExtraFuncs.workspace import MSTree
    tree =  Phylo.read(tree_file,"newick")
    strains =  tree.get_terminals()
    data_={}
    for strain in strains:        
        data_[strain.name]=strain
        
    print "long ", len(data_)
    
    parameters={
        "strain_number":len(strains),
        "scheme":scheme,
        "algorithm":algorithm
    }
    tree_name='tree_%s'%datetime.now()
    print tree_name
    params={
        'name':tree_name,
        'user_id': user_id, 
        'database':database
    }    
    tree_id =MSTree.create_ms_tree(params, data_,algorithm,parameters,data_type=key,nwk_file=tree_file)
    print "Tree id: ", tree_id

@manager.command
@manager.option('-r', '--reldate', help='Relative date limit (default=7)')
@manager.option('-f', '--file_loc', help='Local file data')
@manager.option('-d', '--db', help='Database name or all (only if no file)')
@manager.option('-l', '--live', help="Run task directly without using celery (True/true)")
def importSRA(reldate=7, file_loc=None, db=None, live=None):
    """
    Imports public metadata from SRA
    """
    # Force SQLAlchemy message off
    entero.config['SQLALCHEMY_ECHO'] = False
    # Import data script and database connection
    app.logger.info('Importing from SRA')
    if file_loc != None and db != None:
        if not app.config['USE_CELERY'] or live:
            import_sra(db, file_loc=file_loc, backdate=reldate)
        else:
                import_sra.apply_async(args=[db], kwargs={'file_loc': file_loc, 'backdate': reldate},queue=db)
    else:
        if db == 'all':
            print "Working from all", dbhandle.keys()
            not_to_include=[]#'ecoli','yersinia', 'clostridium','miu']
            for dbname in dbhandle.keys():
                if dbname in not_to_include:
                    print dbname +", is excluded ..."
                    continue
                
                
                # Format genome name for Query
                print dbhandle[dbname].DATABASE_DESC
                names = dbhandle[dbname].DATABASE_DESC
                for name in names:          
                    if not app.config['USE_CELERY'] or live:
                        import_sra(dbname, organism_name=name,backdate=reldate)
                    else:                 
                        import_sra.apply_async(args=[dbname], kwargs={"organism_name":name, "backdate":reldate},queue=dbname)
        elif dbhandle.get(db):
         #   name = dbhandle[db].DATABASE_DESC
            names = dbhandle[db].DATABASE_DESC
            for name in names:
            # import data script
                if not app.config['USE_CELERY'] or live:
                    import_sra(db, organism_name=name,backdate=reldate)
                else:
                    import_sra.apply_async(args=[db], kwargs={"organism_name":name, "backdate":reldate},queue=db)
    

@manager.command
def change_CRISPR_barcodes():
    try:
        handle = open('/share_space/barcode.res')
        lines = handle.readlines()
        dbase = dbhandle['senterica']
        Lookup = dbase.models.AssemblyLookup
        Assemblies = dbase.models.Assemblies
        scheme_id = 5
        for line in lines:
            arr = line.strip().split(" ")
            assembly = dbase.session.query(Assemblies).filter_by(barcode=arr[0]).first()
            if not assembly:
                continue
            lookup =dbase.session.query(Lookup).filter(Lookup.assembly_id==assembly.id,Lookup.scheme_id==scheme_id).first()
            if not lookup:
                lookup = Lookup(assembly_id=assembly.id,scheme_id=scheme_id)
                lookup.other_data={}
                dbase.session.add(lookup)

            index = lookup.scheme_id-3
            if arr[index]=='None' or arr[index]=='ND':
                lookup.status = "FAILED"
                lookup.st_barcode=None
                lookup.other_data['invalid']='True'
                lookup.version=1.2
            else:
                lookup.status = "COMPLETE"
                lookup.st_barcode = arr[index]
                lookup.version=1.2
            dbase.session.commit()
        dbase.session.close()

    except IOError as e:
        app.logger.exception("change_CRISPR_barcodes failed, error message: %s" % e.message)

    except Exception as e:
        app.logger.exception("change_CRISPR_barcodes failed, error message: %s"%e.message)
        db.session.callback()
        db.rollback_close_session()




@manager.command
@manager.option('-f', '--fromNumber', help='populates from the given number(default 1)')
@manager.option('-t', '--toNumber', help='populates to the  given number(default all)')
@manager.option('-l', '--live', help="Run task directly without using celery (True/true)")
def load_jobs(fromNumber=1,toNumber=100000,live=None):
    from entero.cell_app.tasks import process_job
    num_list=[]
    f = int(fromNumber)
    t= int(toNumber)+1
    for num in range(f,t):
        num_list.append(str(num))
        if len(num_list)==200:
            to_send = ",".join(num_list)
            if live:
                process_job(to_send)                
            else:
                process_job.apply_async(args=[to_send],queue='job')
            num_list=[]
    if len(num_list) <> 0:
        to_send = ",".join(num_list)
        if live: 
            process_job(to_send)     
        else: 
            process_job.apply_async(args=[to_send],queue='job')      

@manager.command
def refresh_trees():
    from entero.cell_app.tasks import process_job
    z = db.session.query(UserPreferences).filter(UserPreferences.type.in_(['ms_sa_tree', 'ms_tree'])).all()
    for x in z:
        if x.data:
            data = json.loads(x.data)
            if not data.get('nwk') and data.get('job_id'):
                print data
                process_job(int(data.get('job_id')))


@manager.command
def update():
    for job in range (434260,440000):
        process_job(job)


@manager.command
@manager.option('-d', '--dbName', help='database name')
def attach_owner_to_strain(dbName = None):
    if not get_database(dbName):
        app.logger.error("attach_owner_to_strain failed, could not find database: %s"%dbName)
        return

    from entero.utilities import attach_owner_to_strain
    attach_owner_to_strain(dbName)
    

@manager.command
@manager.option('-d', '--dbName', help='database name')
def build_cgMLST(dbname='senterica'):
    if not get_database(dbname):
        app.logger.error("build_cgMLST failed, could not find database: %s"%dbname)
        return
    from entero.cell_app.tasks import build_cgmlst
    build_cgmlst(dbname)


   
    '''for ass in assemblies:
        for trace in ass.traces:
            trace.accession
            jobs =db.session.query(UserJobs).filter_by(accession = trace.accession).all()
            other =False
            for job in jobs:
                if not (job.status.startswith("KILLED") or job.status.startswith("FAILED")):
                    other = True
                    print str(ass.id) + ":" + job.status
            if not other:
                dbase.session.delete(ass)
                
    dbase.session.commit() '''     
    
@manager.command
@manager.option('-d', '--database', help='database name')
def check_queued_assemblies(database = 'senterica'):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("check_queued_assemblies failed, could not find database: %s"%database)
        return
    from entero.databases.system.models import UserJobs

    Ass= dbase.models.Assemblies
    try:
        assemblies = dbase.session.query(Ass).filter(Ass.status == 'Queued').all()
        nums = []
        for ass in (assemblies):
            if ass.job_id:
                nums.append(ass.job_id)
        for num in sorted(nums):
            process_job(num)
    except Exception as e:
        app.logger.exception("check_queued_assemblies failed, error message: %s"%e.message)
        dbase.rollback_close_session()


@manager.command
def CRISPR():
    db_conn=None
    try:
        db_conn = psycopg2.connect(app.config['SQLALCHEMY_DATABASE_URI'])
        db_cursor = db_conn.cursor(cursor_factory=RealDictCursor)
        dbase = dbhandle['senterica']
        results = dbase.execute_query("SELECT assemblies.barcode as bar FROM strains inner join assemblies on strains.best_assembly=assemblies.id AND assemblies.status='Assembled' left join assembly_lookup ON assembly_lookup.assembly_id = assemblies.id AND scheme_id=5  where assembly_lookup.id is null")
        li = []
        for record in results:
            li.append(record['bar'])
        txt = "'" +"','".join(li)+"'"
        SQL = "SELECT * FROM user_jobs WHERE  pipeline LIKE '%%CRISPR%%'  AND status='COMPLETE' AND accession IN (%s) order by id" % txt
        db_cursor.execute(SQL)
        results = db_cursor.fetchall()
        li=[]
        for record in results:
            if not float(record['version'])==1.2:
                continue
            li.append(str(record['id']))
            if len(li)==10:
                process_job(','.join(li))
                li=[]
        if len(li)>0:
            process_job(','.join(li))
            pass
        db_conn.close()
    except Exception as  e:
        app.logger.exception("CRISPR failed, errro message: %s"%e.message)
        if db_conn:
            db_conn.rollback()
            db_conn.close()

@manager.command
def killy():
    db_conn=None
    try:
        db_conn = psycopg2.connect(app.config['SQLALCHEMY_DATABASE_URI'])
        db_cursor = db_conn.cursor(cursor_factory=RealDictCursor)
        dbase = dbhandle['klebsiella']
        results = dbase.execute_query("SELECT assemblies.barcode as bar FROM assembly_lookup inner join assemblies on assemblies.id = assembly_lookup.assembly_id WHERE scheme_id=3 AND assembly_lookup.status= 'QUEUED'")
        li = []
        for record in results:
            li.append(record['bar'])
        txt = "'" +"','".join(li)+"'"
        SQL = "SELECT DISTINCT ON (accession) id,accession FROM user_jobs WHERE pipeline LIKE  '%%klebsiella_rMLST' AND status = 'COMPLETE'  AND accession IN (%s)" % txt
        db_cursor.execute(SQL)
        results = db_cursor.fetchall()
        li =[]
        for record in results:
            li.append(str(record['id']))
            if len(li)==10:
                process_job(','.join(li))
                li=[]
        if len(li)>0:
            process_job(','.join(li))
        db_conn.close()
    except Exception as e:
        app.logger.exception("killy failed, error message= %s"%e.message)
        if db_conn:
            db_conn.rollback()
            db_conn.close()


@manager.command 
@manager.option('-d', '--dbName', help='database name')
def kill_duplicate_assemblies(dbName = 'senterica'):
    if not get_database(dbName):
        app.logger.error("kill_duplicate_assemblies failed, could not find %s database"%dbName)
        return

    db_conn=None
    try:
        db_conn = psycopg2.connect(app.config['SQLALCHEMY_DATABASE_URI'])
        db_cursor = db_conn.cursor(cursor_factory=RealDictCursor)
        SQL = "SELECT id,accession,database,pipeline,status FROM (SELECT id,accession,database,pipeline,status, ROW_NUMBER() OVER (partition BY accession ORDER BY id) AS rnum FROM user_jobs WHERE database = '%s' AND pipeline = 'QAssembly' AND status NOT LIKE 'KILLED%%' AND status NOT LIKE 'FAILED%%' ) t WHERE t.rnum > 1 order by id" % (dbName)
        db_cursor.execute(SQL)
        results = db_cursor.fetchall()
        for record in results:
            URI = app.config['CROBOT_URI']+"/head/kill_job/"+str(record['id'])
            resp = requests.get(URI)
            reply = json.loads(resp.text)
            status= reply[0]['status'].split(":")[0]
            job  = db.session.query(UserJobs).filter_by(id=record['id'])
            job.status = status
        db_conn.close()
        db.session.commit()
    except Exception as e:
        app.logger.exception("kill_duplicate_assemblies failed, error message: %s"%e.message)
        if db_conn:
            db_conn.rollback()
            db_conn.close()
        db.session.rollback()
        db.session.close()
 
@manager.command 
def quick():
    dbase= dbhandle['clostridium']
    try:
        Lookup = dbase.models.AssemblyLookup
        lookups = dbase.session.query(Lookup).filter_by(scheme_id=4).all()
        for lookup in lookups:
            other_data= ujson.loads(lookup.other_data)

        #dbase.session.commit()
    except Exception as e:
        app.logger.exception("Quick failed, error message: %s"%e.message)
        dbase.rollback_close_session()
      
     
 
@manager.command 
@manager.option('-d', '--database', help='database name')
@manager.option('-s', '--scheme', help='database name')
@manager.option('-k', '--kill', help='database name')  
def kill_duplicate_jobs(database = 'senterica',scheme='rMLST',kill='T'):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("kill_duplicate_jobs failed, could not find %s database"%database)
    try:
            scheme_info = dbase.session.query(dbase.models.Schemes).filter_by(description=scheme).first()
            SQL ="SELECT * FROM assembly_lookup WHERE assembly_id IN (select assembly_id from (SELECT id,assembly_id,scheme_id,ROW_NUMBER() OVER(PARTITION BY assembly_id,scheme_id) AS Row FROM assembly_lookup WHERE scheme_id=%i) dups where dups.Row > 1 group by assembly_id) AND scheme_id=%i order by assembly_id" % (scheme_info.id,scheme_info.id)
            results = dbase.execute_query(SQL)
            complete_id=0
            ids=[]
            kill_jobs = False
            if kill in ['T','t','True','true']:
                kill_jobs=True
            lookup_ids =[]
            prev_assembly_id=0
            for count,record in enumerate(results):
                if (record['assembly_id'] <> prev_assembly_id and prev_assembly_id <> 0):
                    #delete list - all except last complete or first queued
                    if complete_id:
                        ids.remove(complete_id)
                    else:
                        ids.remove(ids[0])
                    #ids in assembly_lookup to delete
                    lookup_ids =[]
                    for num in ids:
                        if kill_jobs:
                            job_no =results[num]['other_data']['job_id']
                            URI = app.config['CROBOT_URI']+"/head/kill_job/"+str(job_no)
                            resp = requests.get(URI)
                        lookup_ids.append(str(results[num]['id']))
                    SQL = 'DELETE FROM assembly_lookup WHERE id in(%s)' % ",".join(lookup_ids)
                    dbase.execute_action(SQL)
                   
                    ids=[]
                    complete_id=0
                ids.append(count) 
                prev_assembly_id=record['assembly_id']
                if record['status'] == 'COMPLETE' and record['st_barcode']:
                    complete_id = count
            if len(ids)>0:         
                if complete_id:
                    ids.remove(complete_id)
                else:
                    ids.remove(ids[0])
                lookup_ids =[]            
                for num in ids:                   
                    lookup_ids.append(str(results[num]['id']))
                if len(lookup_ids)>0:           
                    SQL = 'DELETE FROM assembly_lookup WHERE id in(%s)' % ",".join(lookup_ids)
                    dbase.execute_action(SQL)
          
    except Exception as e:
        dbase.rollback_close_session()
        app.logger.exception("kill_duplicate_jobs failed, error message: %s"%e.message)

     
@manager.command
@manager.option('-u', '--user_id', help='database name')
def add_default_permissions(user_id=0):
    user_id = int(user_id)
    try:
        for database in ['senterica','ecoli','yersinia','mcatarrhalis']:
            tag =UserPermissionTags(field='upload_reads',value='True',species=database,user_id=user_id)
            db.session.add(tag)
            tag =UserPermissionTags(field='view_species',value='True',species=database,user_id=user_id)
            db.session.add(tag)
        db.session.commit()
    except Exception as e:
        app.logger.exception("add_default_permissions failed, error message: %s"%e.message)
        db.session.rollback()
        db.session.close()


@manager.command
@manager.option('-d', '--dbName', help='database name')
@manager.option('-l', '--limit', help='database name')
@manager.option('-f',"--force")
@manager.option("-q","--queue")
@manager.option("-p","--priority")
def update_assemblies(dbName = "senterica",limit=100,force=5,queue='backend',priority=0):

    if not get_database(dbName):
        app.logger.error("update_assemblies falied, can not find %s database" % dbName)
        return
    
    from entero.jobs.jobs import AssemblyJob
    force=int(force)
    conn = None
    db_conn = None
    app.logger.info('Running update assemblies')
    # a list of traces accession for assemblies which have failed > 5 times
    fail_list=""
    try:
        db_conn = psycopg2.connect(app.config['SQLALCHEMY_DATABASE_URI'])
        db_cursor = db_conn.cursor(cursor_factory=RealDictCursor)
        SQL = "SELECT accession,COUNT(id) AS num FROM user_jobs WHERE status LIKE 'FAILED%%' AND pipeline= 'QAssembly' GROUP BY accession HAVING count(id)>%i ORDER BY num DESC" % force
        db_cursor.execute(SQL) 
        results = db_cursor.fetchall()
        fails=[]
        for record in results:
            if record.get('accession'):
                fails.append(record['accession'])
        if len(fails)>0:
            fail_list =  "','".join(fails)
            fail_list = "AND traces.accession NOT IN ('"+fail_list+"')"
            app.logger.error('%d assemblies have Failed' %len(fails))
        db_conn.close()
    except Exception as e:
        if db_conn:
            db_conn.rollback()
            db_conn.close()
        app.logger.exception("update_assemblies falied, error message: %s"%e.message)
        return

    try:
        #no Assembly
        SQL = "SELECT traces.id as tid, traces.read_location as rl,strains.owner as uid FROM strains INNER JOIN traces on traces.strain_id = strains.id  AND traces.seq_platform = 'ILLUMINA' AND traces.seq_library='Paired' WHERE best_assembly IS NULL AND strains.uberstrain = strains.id %s ORDER BY strains.id DESC LIMIT %i " % (fail_list,int(limit))
        #no Scheme info
        #SQL ="SELECT traces.id as tid FROM strains INNER JOIN  assemblies on strains.best_assembly = assemblies.id  AND assemblies.status = 'Assembled' INNER JOIN trace_assembly ON trace_assembly.assembly_id = strains.best_assembly INNER JOIN traces on traces.id = trace_assembly.trace_id  LEFT  JOIN st_assembly ON st_assembly.assembly_id= best_assembly WHERE  st_assembly.st_id  IS NULL"
        #Reason failed is null
        #SQL ="SELECT strain,traces.id as tid,other_data,job_id FROM strains INNER JOIN  assemblies on strains.best_assembly = assemblies.id  AND assemblies.status = 'Failed QC' INNER JOIN trace_assembly ON trace_assembly.assembly_id = strains.best_assembly INNER JOIN traces on traces.id = trace_assembly.trace_id WHERE other_data ILIKE '%[null]%'  ORDER BY job_id DESC"
       
        conn = psycopg2.connect(app.config['ACTIVE_DATABASES'][dbName][1])
        cursor = conn.cursor(cursor_factory=RealDictCursor)    
        cursor.execute(SQL)
        results = cursor.fetchall()
        app.logger.info('Resending %d assemblies' %len(results))
        for res in results:
            send_priority=priority
            send_queue=queue
            user_id=0
            if res['uid']:
                user_id=res['uid']
                send_priority=-9
                send_queue="user_upload"
            
            job =AssemblyJob(trace_ids=[res['tid']], 
                                        database=dbName,
                                         workgroup=send_queue,
                                         priority=send_priority,
                                         user_id=user_id)
            job.send_job()
        conn.close()
            
    except Exception as e:
        if conn:
            rollback_close_connection(conn)
            #conn.rollback()
            #conn.close()
        app.logger.exception("update_assemblies falied, error message: %s" % e.message)


@manager.command
def format_snp_gff():
    from entero.cell_app.tasks import annotate_snp_gff
    annotate_snp_gff("YER_CA7729AA_AS", ["YER_CA6611AA_AS","YER_CA6613AA_AS","YER_CA6614AA_AS"], "yersinia", 5)

@manager.command
@manager.option('-b', '--barcode', help='Assembly barcode')
@manager.option('-d', '--database', help='Database name')
@manager.option('-f', '--force',help="Force overwrite of existing files")
def make_jbrowse_annotation(barcode=None,database='senterica',force=False):
    from entero.cell_app.tasks import make_jbrowse_from_genome
    if force in ["T","True","true"]:
        force=True
    make_jbrowse_from_genome(barcode, database,force)
   



@manager.command
@manager.option('-b', '--barcode', help='database name')
@manager.option('-d', '--database', help='database name')
def make_qual(barcode=None,database='senterica'):
    from Bio import SeqIO
    database = 'senterica'
    from entero.jbrowse.utilities import build_quality_file
    dbase = dbhandle[database]
    try:
        assembly =  dbase.session.query(dbase.models.Assemblies).filter_by(barcode=barcode).first()
        build_quality_file(assembly.file_pointer,barcode)
    except Exception as e:
        app.logger.exception("make_qual failed, error message: %s"%e.message)
        dbase.rollback_close_session()
    
@manager.command
@manager.option('-b', '--barcode', help='database name')
def make_names(barcode=None):
    from entero.jbrowse.utilities import build_names
    build_names(barcode)
    
    
    

@manager.command
@manager.option('-b', '--barcode', help='database name')
@manager.option('-d', '--database', help='database name')
def make_jbrowse_schemes(barcode=None,database='senterica'):

    from entero.jbrowse.utilities import build_gff_files,build_names
    dbase = get_database(database)
    if not dbase:
        app.logger.error("make_jbrowse_schemes failed, could not find %s sdatabase"%database)
        return
    try:
        assembly = dbase.session.query(dbase.models.Assemblies).filter_by(barcode=barcode).first()
        barcode = assembly.barcode
        path = os.path.split(assembly.file_pointer)[0]
        schemes =dbase.session.query(dbase.models.Schemes).all()
        id_to_name ={}
        for scheme in schemes:
            id_to_name[scheme.id]=[scheme.description,scheme.name]

        lookups = dbase.session.query(dbase.models.AssemblyLookup).filter_by(assembly_id=assembly.id).all()
        gff_files = {}
        for lookup in lookups:
            if lookup.annotation:
                scheme_name = id_to_name[lookup.scheme_id]
                file_name = scheme_name[0]+".gff"
                file_path = os.path.join(path,file_name)
                handle = open(file_path,"w")
                handle.write(lookup.annotation)
                gff_files[scheme_name[0]]=[file_path,scheme_name[1]]
                handle.close()
        build_gff_files(barcode,gff_files)
        build_names(barcode)
    except Exception as e:
        app.logger.exception("make_jbrowse_schemes falied, %s"%e.message)
        dbase.rollback_close_session()
    
   
@manager.command
@manager.option('-d', '--dbase', help='database name')
def update_genome_database(dbname = None):
    dbase = get_database(dbname)
    if not dbase:
        app.logger.error ("update_genome_database falied, can not find %s database" % dbname)
        return

    from entero.databases.system.models import UserJobs
    jobs= UserJobs.query.filter(UserJobs.status == "COMPLETE")
    
    try:

        Traces =  dbase.models.Traces
        for job in jobs:
            genome = dbase.session.query(Traces).filter(Traces.accession == job.accession).first()
            if not genome:
                continue
            genome.status = "Assembled"
            job.database = dbname
        dbase.session.commit()
        db.session.commit()
    except Exception as e:
        app.logger.exception("update_genome_database falied, error message: %s"%e.message)
        dbase.rollback_close_session()


@manager.command
@manager.option('-d', '--dbase', help='database name')
def find_orphan_assemblies(dbname='senterica'):
    dbase  = get_database(dbname)

    if not dbase:
        app.logger.error ("find_orphan_assemblies falied, can not find %s database" % dbname)
        return
    try:
        Assemblies  =  dbase.models.Assemblies
        Strains  =  dbase.models.Strains
        errors = []
        genpair = {}
        genomes = dbase.session.query(Assemblies).filter(Assemblies.status == 'Assembled').all()
        for genome in genomes:
            if len(genome.traces) < 1 :
                print 'No traces'
                errors.append(genome)
            elif len(genome.traces) > 1:
                print 'Duplicate assemblies'

            strains = dbase.session.query(Strains).filter(Strains.id  == genome.traces[0].strain_id).all()
            for strain in strains:
                if strain.best_assembly != genome.id:
                    print 'No best assembly'
                    #strain.best_assembly = genome.id
                    errors.append(genome)
                    bad_gen = dbase.session.query(Assemblies).filter(Assemblies.id  == genome.id).all()
                    pairs = []
                    bigjob = 0
                    for gen in bad_gen:
                        pairs.append(gen.id)
                        if gen.id > bigjob: bigjob =  gen.id
                    bad_gen = dbase.session.query(Assemblies).filter(Assemblies.id  == strain.best_assembly).all()
                    for gen in bad_gen:
                        pairs.append(gen.id)
                        if gen.id > bigjob: bigjob =  gen.id
                    strain.best_assembly =  bigjob
                    genpair[strain.strain] = pairs
                    dbase.session.add(strain)
        print errors
        print genpair
        dbase.session.commit()
    except Exception as e:
        app.logger.exception("find_orphan_assemblies falied, error message: %s"%e.message)
        dbase.rollback_close_session()
    
    
@manager.command
@manager.option("-d","--database")
@manager.option("-o","--output_file")
def get_all_assemblies(database='senterica',output_file=""):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("get_all_assemblies failed, could not find %s database" % database)
        return
    import tarfile
    sql = "SELECT strains.barcode AS bc ,assemblies.file_pointer AS fp FROM strains INNER JOIN assemblies ON assemblies.status = 'Assembled' AND strains.best_assembly=assemblies.id"

    try:
        results = dbase.execute_query(sql)
       # first_file = results[0]['fp']
        #file_dir = os.path.dirname(first_file)
        #file_name =  os.path.basename(first_file)
        #file_name=file_name[:-1]+"a"
        #os.chdir(file_dir)
        #command= 'tar -cvf %s %s' % (output_file,file_name)
        #os.system(command)
        count=1
        tar = tarfile.open(output_file,mode="w:gz")
        for line in results:
            first_file = line['fp']
            first_file = first_file[:-1]+"a"
            file_name =  os.path.basename(first_file)
            count+=1
            tar.add(first_file,arcname=file_name)
            if count % 100 ==0:
                print count

        tar.close()
    except Exception as e:
        app.logger.exception("get_all_assemblies failed, error message: %s"%e.message)
        dbase.rollback_close_session()

@manager.command
@manager.option('-d', '--database', help='database name')
@manager.option('-l', '--logfile', help='path of log file to write to')
@manager.option('-t',"--test_only" ,help='just write records to delete to file')
def delete_orphan_assemblies(database="senterica",logfile=None,test_only="F"):
    test= False
    if not dbhandle.get(database):
        app.logger.error("delete_orphan_assemblies failed, could not find %s database"%database)
        return
    if test_only in ["T","t","True","true"]:
        test=True

    sql = "SELECT * FROM assemblies LEFT JOIN strains ON assemblies.id = strains.best_assembly WHERE strains.id IS NULL ORDER BY file_pointer"
    try:
        results = dbhandle[database].exeute_query(sql)
        for res in results:
            if res['file_pointer'] and  res['file_pointer'] <> 'missing':
                pass
                #ass_file = os.path.split(p)
    except Exception as e:
        app.logger.exception("delete_orphan_assemblies failed, error message: %s"%e.message)
        dbhandle[database].rollback_close_session()
            
    
@manager.command
@manager.option('-d', '--database', help='database name')
def make_reference_alleles(database=None):
    dbase = get_database(database)
    if not dbase:
        app.logger.error("make_reference_alleles failed, could not find %s database"%database)
        return

    Schemes = dbase.models.Schemes
    try:
        schemes = dbase.session.query(Schemes).all()
        for scheme in schemes:
            scheme_name = scheme.param.get("scheme")

            if scheme_name and not "CRISP" in scheme_name:
                arr = scheme_name.split("_")
                out_file= ''
                uri = app.config['NSERV_ADDRESS']+"search.api/"+arr[0]+"/"+arr[1]+"/alleles?allele_id=1&fieldnames=fieldname,value"
                resp = requests.get(url = uri)
                data = ujson.loads(resp.text)

                file_name = "/share_space/interact/NServ_dump/%s.%s/%s_ref.fasta" % (arr[0],arr[1],scheme.description)
                file_handle = open(file_name,"w")
                for  rec in data:
                    file_handle.write(">"+rec['locus']+"\n")
                    file_handle.write(rec['seq']+"\n")
                file_handle.close()
    except IOError as e:
        app.logger.exception("make_reference_alleles failed, error message: %s" % e.message)

    except Exception as e:
        app.logger.exception("make_reference_alleles failed, error message: %s"%e.message)
        dbase.rollback_close_session()
                
@manager.command
def tag_proteins():
    file_handle= open("/share_space/workspace/ecoli/plasmid_finder.txt")
    lines = file_handle.readlines()
    loci={}
    previous_name =""
    max_score=0
    best_vog= ""
    '''for line in lines[3:]:
        if line.startswith("#"):
            continue
        arr = line.split()
        arr2= arr[2].split("_")
        score = float(arr[5])
        name = arr2[0]
        vog=arr[0]
        if len(arr2)==4:
            name =name+"_"+arr2[1]
        
        if name <> previous_name and previous_name:
            if max_score >=100:
                loci[name]=best_vog
            max_score=0
        if score>max_score:
            max_score=score
            best_vog=vog
        previous_name=name
    loci[name]=best_vog
    '''
    for line in lines[1:]:
        arr = line.split("\t")
        loci[arr[3]]=arr[0].replace("'","''")
    
    loci_list = "','".join(list(loci))
    loci_list ="('"+loci_list+"')"
    dbase=dbhandle['ecoli']
    try:
        sql = "SELECT * FROM data_param WHERE tabname='wgMLST' AND name IN %s" % loci_list
        results =dbase.execute_query(sql)
        for item in results:
            if not item['param']:
                item['param']={}
            item['param']['plasmid_gene']=loci[item['name']]
            val = ujson.dumps(item['param'])
            sql = "UPDATE data_param SET param='%s' WHERE id=%i" % (val,item['id'])
            dbase.execute_action(sql)

    except Exception as e:
        app.logger.exception("tag_proteins failed, error message: %s"%e.message)
        dbase.rollback_close_session()


@manager.command
@manager.option('-d', '--database', help='database name')
@manager.option('-t', '--tree_file', help='location of newick file')
@manager.option("-k","--key",help='key in nwk tree (assembly barcode by default')
@manager.option('-u',"--user_id" ,help='user_id (0 by default)')
@manager.option('-n',"--name" ,help='name of tree - nwk by default')
@manager.option('-s',"--scheme" ,help='scheme used to generate tree (none by default)')
@manager.option('-a',"--algorithm",help = "algorithm used to generate tree (unknown by default)")
def create_tree_from_newick(database=None,tree_file=None,key='assembly_barcode',user_id=0,
                            name='nwk tree',scheme='"None',algorithm='unknown'):
    '''
    use working_create_tree_from_newick instead
    '''
    from Bio import Phylo
    from entero.ExtraFuncs.workspace import MSTree
    tree =  Phylo.read(tree_file,"newick")
    strains =  tree.get_terminals()
    data = map(lambda x:x.name,strains)
    parameters={
        "strain_number":len(strains),
        "scheme":scheme,
        "algorithm":algorithm
    }
    tree_id =MSTree.create_ms_tree(dict(name=name, user_id=user_id, database=database),data,algorithm,parameters, 0,data_type=key,nwk_file=tree_file)

@manager.command
@manager.option("-w","--workspace_id")
def send_phylogeny_job(workspace_id=None) :
    from entero.ExtraFuncs.workspace import get_analysis_object
    
    ws = get_analysis_object(int(workspace_id))
    
    prefix= ws.name
    matrix =ws.data['matrix']
    URI = app.config['CROBOT_URI']+"/head/submit"
    params = {
        "source_ip":app.config['CALLBACK_ADDRESS'],
        "usr":"admin",
        "query_time":str(datetime.now()),
        "workgroup":"user_upload",
        "_priority":-9,
        "params":{
            "prefix":prefix .replace(" ","_").replace("/","_").replace(".","_"),      
            },
        "pipeline":"matrix_phylogeny"  ,
        "inputs":{"matrix":matrix
                  }
    }
    resp = requests.post(url = URI, data = {"SUBMIT":ujson.dumps(params)})
    if resp.status_code != 502:
        data = ujson.loads(resp.text)
        app.logger.info("Sending matrix phylogeny No:%i, database:%s" % (data['tag'], ws.database))

        job = UserJobs(id=data['tag'], pipeline="matrix_phylogeny", date_sent=data["query_time"], database=ws.database,
                       status=data["status"], accession=ws.name + ":" + str(ws.id), user_id=ws.user_id)
        try:
            db.session.add(job)
            db.session.commit()
        except Exception as e:
            app.logger.exception("send_phylogeny_job")
            db.session.rollback()

@manager.command
def add_infomation():
    from entero.ExtraFuncs.workspace import CustomView
    file_name = '/share_space/workspace/senterica/res_finder.txt'
    file_handle = open(file_name,"r")
    lines = file_handle.readlines()
    loci=[]
    for line in lines[1:]:
        arr= line.split("\t")
        #arr2 = arr[0].split("_")
        
        #if (arr[5]== 'Strict' or arr[5]=='Perfect') and arr[12]=='protein homolog model':
            #name = arr2[0]
            #if len(arr2)==4:
             #   name =name+"_"+arr2[1]
            #label = arr[8]
        name = arr[3]
        label=arr[0]
        category=arr[5]
        loci.append([name,label,category])
    categories={   
        "Aminoglycoside resistance":{"color":"#81D4FA"},
        "Beta-lactam resistance":{"color":"#80CBC4"},
        "Fluoroquinolone resistance":{"color":"#A5D6A7"},
        "Lincosamide resistance":{"color":"#DEB887"},
        "Fosfomycin resistance":{"color":"#E6EE9C"},
        "Macrolide resistance":{"color":"#FFF59D"},
        "Macrolide, Lincosamide and Streptogramin B resistance":{"color":"#FFB74D"},
        "Phenicol resistance":{"color":"#FFAB91"},
        "Quinolone resistance":{"color":"#BCAAA4"},
        "Rifampicin resistance":{"color":"#69F0AE"},
        "Sulphonamide resistance":{"color":"#B39DDB"},
        "Tetracycline resistance":{"color":"#90CAF9"},
        "Trimethoprim resistance":{"color":"#FBC02D"}
    }
    description="All the Loci in the wgMLST scheme that were detected by Res Finder (DTU) as AMR genes"
    CustomView.make_custom_view("wgMLST","senterica", loci, "AMR(Res Finder)",user_id=55,categories = categories,description=description)

#*********ebi stuff****************
@manager.command
def add_accession_to_ebi():
    inhandle =None
    try:
        inhandle = open("/share_space/user_reads/ebi_upload/receipts/receipt_2.tsv")
    except Exception as e:
        app.logger.exception("add_accession_to_ebi is failed. error message: %s"%e.message)

    lines = inhandle.readlines()
    sample_count=0
    experiment_count=0
    run_count=0
    info_dict={}
    
    for line  in lines:
        arr = line.strip().split("\t")
        if arr[0] == 'Sample':
            sample = arr[1].split()
            
            info_dict[sample_count]={"name":arr[2],"sample_1":sample[0],"sample_2":sample[1][1:-1]}
            sample_count+=1
        elif arr[0]=='Experiment':
            info_dict[experiment_count]['experiment']=arr[1]
            experiment_count+=1
        elif arr[0]=='Run':
            info_dict[run_count]['run']=arr[1]
            run_count+=1
    dbase = dbhandle['senterica']
    try:
        for key in info_dict:
            record= info_dict[key]
            if not record.get("run"):
                continue
            if record['name']== '05_1994':
                continue


            Strains = dbase.models.Strains
            Traces = dbase.models.Traces
            strain = dbase.session.query(Strains).filter_by(strain=record['name'],owner=55).one()
            strain.sample_accession=record['sample_2']
            strain.secondary_sample_accession=record['sample_1']
            strain.study_accesion='PRJEB19916'
            strain.secondary_study_accession='ERP021993'
            traces=dbase.session.query(Traces).filter_by(strain_id=strain.id).order_by(Traces.id).all()

            trace=traces[-1]
            trace.accession=record['run']
            trace.experiment_accession=record['experiment']
        dbase.session.commit()
    except Exception as e:
        app.logger.exception("add_accession_to_ebi failed, error message: %s"%e.message)
        dbase.rollback_close_session()

@manager.command
@manager.option('-d', '--database', help='database name')
@manager.option('-e', '--experiment', help='experiment name')
def filter_clustering(database = 'senterica', exp = 'cluster_hierarchy'):
    import numpy as np
    from sqlalchemy.orm.attributes import flag_modified        
    # Get information about existing hierachical scheme
    dbase = dbhandle[database]
    Schemes=dbase.models.Schemes
    scheme = dbase.session.query(Schemes).filter_by(description=exp).first()
    base_scheme_id = scheme.param['base_scheme_id']
    DataParam = dbase.models.DataParam    
    dp_dict={}
    # Load numpy array/
    mm = np.memmap(scheme.param['numpy_file'],dtype='int')
    size= mm.size
    cols = scheme.param['table_width']
    rows = size/cols
    mm=mm.reshape(rows,cols)
    dps= dbase.session.query(DataParam).filter_by(tabname=exp).all()
    # Save filtered copy of array. 
    levels = [0]
    level_names=['rST']
    ret_dict={}
    ret_list=[]
    for item in dps:
        # the distances are strings like;  g5, g2600, g2000
        level = int(item.name[1:]) + 1
        level_names.append(item.name)
        levels.append(level)
    levels.sort()
    filtered_mm = mm[:,levels]
    filtered_file_path = os.path.join(os.path.dirname(scheme.param['numpy_file']), 
                                      'filtered_' + os.path.basename(scheme.param['numpy_file']))
    filtered_mm.tofile(filtered_file_path)
    # Check if scheme present and valid.
    filtered_exp = 'filtered_%s' %exp
    dbase.session.commit()
    dbase.session.execute("SELECT setval('schemes_id_seq', MAX(id)) FROM schemes;")
    dbase.session.execute("SELECT setval('data_param_id_seq', MAX(id)) FROM data_param;")
    dbase.session.commit()
    filtered_scheme = dbase.session.query(Schemes).filter_by(description=filtered_exp).first()
    # Create scheme  if needed
    if not filtered_scheme:
        new_scheme = scheme.as_dict()
        new_scheme.pop('id', None)
        filtered_scheme = Schemes(**new_scheme)
    filtered_scheme.description = filtered_exp
    filtered_scheme.name = 'HierCC'
    filtered_scheme.param['display'] = 'public'
    filtered_scheme.param['table_width'] = len(levels)
    filtered_scheme.param['numpy_file'] = filtered_file_path
    dbase.session.add(filtered_scheme)
    flag_modified(filtered_scheme, 'param')
    dbase.session.commit()
    # Create data Params if needed
    filtered_level_names = [ ('g{0}'.format(id), cId) for id, cId in list(enumerate(levels[1:]))]
    for  paired_filtered in filtered_level_names :
        filtered_dp = dbase.session.query(DataParam).filter_by(tabname=filtered_exp,
                                                 name = paired_filtered[0]).first()
        if not filtered_dp:
            filtered_dp = DataParam(datatype = 'integer', 
                      display_order = int(paired_filtered[0][1:]),
                      name = paired_filtered[0],
                      tabname = filtered_exp)
        filtered_dp.label = 'd_%s' %(paired_filtered[1] - 1)
        if paired_filtered[1] == 1:
            filtered_dp.label += ' (indistinguishable)'
        if database == 'senterica' and paired_filtered[1] == 901:
            filtered_dp.label += ' (cEBG)'
        if database == 'senterica' and paired_filtered[1] == 2001:
            filtered_dp.label += ' (Super Lineage)'
        if database == 'senterica' and paired_filtered[1] == 2851:
            filtered_dp.label += ' (Subspecies)'
        if (database == 'ecoli' and paired_filtered[1] == 1101) or (database == 'yersinia' and paired_filtered[1] == 601):
            filtered_dp.label += ' (cST Cplx)'
        dbase.session.add(filtered_dp)
    dbase.session.commit()


def update_records_from_EBI_recipt(receipt_xml, dbase, bio_project_id=None, secondary_study_accession=None):
    '''
    K.M. 01/07/2020
    Update traces and strains tables using  he extracted attributes from the  rereceived recipt after submitting
    the date to EBI.
    #Traces _table
    RUN==>>Acession code
    EXpresm==>>experiment_accession
    Srains:
    sample_1==>>secondary_sample_accession
    BIO Project ID  : study_accession
    secondary_study_accession: secondary_study_accession
    '''
    import xml.etree.cElementTree as ET
    print receipt_xml

    tree = ET.ElementTree(file=receipt_xml)
    receipt = tree.getroot()
    if not bio_project_id:
        bio_project_id = receipt.find('STUDY').find('EXT_ID').get('accession')

    if not secondary_study_accession:
        secondary_study_accession = receipt.find('STUDY').attrib['accession']
    sample_acc = receipt.findall('SAMPLE')  # .find('EXT_ID').get('accession')
    runs_ = receipt.findall('RUN')
    expres_ = receipt.findall('EXPERIMENT')
    samples_1 = {}
    samples_2 = {}
    runs = {}
    exprems = {}
    strains_barcodes = []
    traces_barcodes = []
    for sample in sample_acc:
        strains_barcodes.append(sample.attrib["alias"])
        samples_1[sample.attrib["alias"]] = sample.attrib["accession"]
        samples_2[sample.attrib["alias"]] = sample.find('EXT_ID').get('accession')

    for run in runs_:
        traces_barcodes.append(run.attrib["alias"])
        runs[run.attrib["alias"]] = run.attrib["accession"]

    for exp in expres_:
        exprems[exp.attrib['alias'].replace('-EXP', '')] = exp.attrib['accession']

    Strains = dbase.models.Strains
    Traces = dbase.models.Traces
    strains = dbase.session.query(Strains).filter(Strains.barcode.in_(strains_barcodes)).all()
    traces = dbase.session.query(Traces).filter(Traces.barcode.in_(traces_barcodes)).all()
    for trace in traces:
        if trace.accession == runs[trace.barcode]:
            continue

        trace.accession = runs[trace.barcode]
        trace.experiment_accession = exprems[trace.barcode]
        result = dbase.update_record(0, trace, 'traces', False)

        if result:
            print "Trace :%s has been updated " % trace.barcode
        else:
            print "FAILED to update trace %s" % trace.barcode
    for strain in strains:
        if strain.secondary_study_accession == secondary_study_accession:
            continue
        strain.secondary_study_accession = secondary_study_accession
        strain.study_accession = bio_project_id
        strain.sample_accession = samples_2[strain.barcode]
        strain.secondary_sample_accession = samples_1[strain.barcode]
        result = dbase.update_record(0, strain, 'strains', False)
        if result:
            print "Strain: %s has been updated " % strain.strain, strain.barcode
        else:
            print "failed to update strain: %s" % strain.strain, strain.barcode

    print "FINSIHDED ...."
    print len(samples_1)
    print len(samples_2)
    print len(runs)
    print len(exprems)
    print len(strains_barcodes)
    print len(traces_barcodes)
    return len(traces), len(strains)

def check_fix_file(read_file, local_fix_foldr ,test=True):
    file_content=None
    from subprocess import Popen, PIPE
    import gzip
    import datetime
    st=datetime.datetime.now()
    try:
        if not test:
            raise Exception("Error")

        with gzip.open(read_file, 'rb') as f:
            f.read()

        nd_ = datetime.datetime.now()
        print "Time for the correct one is: ", (nd_-st).total_seconds()

        return True
    except:
        nd=datetime.datetime.now()
        from shutil import copy2, copyfile

        import os

        print "copy file to the local folder ...."
        base_name = os.path.basename(read_file)
        local_file_name=os.path.join(local_fix_foldr, base_name)
        copyfile(read_file, local_file_name)
        print "decompress the file ..."
        process = Popen(['gzip', '-d', local_file_name], stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
        print stdout
        print stderr
        print "compress the file ...."
        process = Popen(['gzip', local_file_name.replace('.gz', '')], stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
        print stdout
        print stderr
        print "copy the fixed file to the orginal folder ...."
        copyfile(local_file_name, read_file)
        os.remove(local_file_name)
        rd = datetime.datetime.now()
        print ("1st chck: ", (nd - st).total_seconds())
        print ("ndt chck: ", (rd - nd).total_seconds())

        return False


def get_public_databases():
    ''''
    Return EnteroBase public databases
    '''
    db_info = {}
    dbs = app.config['ACTIVE_DATABASES']
    # a call to main enterobase to get the aviable databases
    # this nees to be addded here
    for db_name in dbs:
        if dbs[db_name][2] == True:
            db_info[db_name]=dbs[db_name][0]
    return db_info;

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-m', '--mode', help='run mode')
def create_database_EBI_Project(database=None, mode='test'):
    '''
    Create an EBI project for EnteroBase Database
    :param database:
    :param mode:
    :return:
    '''
    from entero.admin.ENAsubmission import create_project
    #Get list of public databass
    dbase = get_database(database)
    if not dbase:
        print "No database is found for %s" % database
        return None, None

    public_dbs = get_public_databases()
    if not public_dbs.get(database):
        print "Only public databases are allowed, database %s is private"%database
        return None, None

    local_fix_foldr = app.config["EBI_TEMP_FOLDER"]
    # file contains json string for th created databases
    ebi_projet_infor_file = os.path.join(local_fix_foldr, "broker_ebi_projects.json")
    if os.path.isfile(ebi_projet_infor_file):
        with open(ebi_projet_infor_file) as json_file:
            ebi_data = json.load(json_file)
            #check if the database is alrady crateed
            if database in ebi_data:
                print 'Database %s is alrady created' % database
                return  None, None

    else:
        ebi_data = {}

    database_label=get_database_label(database)
    project_desc ='''This project contains Illumina genomic sequences uploaded to EnteroBase (http://enterobase.warwick.ac.uk).\n 
                    These sequences have been assembled and genotyped in EnteroBase and are uploaded for public access as a proxy for its users.\n 
                    This data is part of a pre-publication release. For information on the proper use of data uploaded by EnteroBase users, please see https://enterobase.readthedocs.io/en/latest/enterobase-terms-of-use.html.\n 
                    EnteroBase development was funded by the BBSRC (BB/L020319/1).'''

    project_title="Proxy upload by EnteroBase (from database %s)"%database_label
    project_name=database_label
    bio_project_id, secondary_study_accession = create_project(project_name, project_title,project_desc, mode, app.config['BROKER_EBI_FTP_USER'], app.config['BROKER_EBI_FTP_PASSWORD'])
    #if the project is cratd correctley, it will return bio project id and secondary study accession
    #otherwise it will return none for both of them
    if  bio_project_id and  secondary_study_accession:
        #toDO set a statorge for each databases to be used later in user reads submission
        #it is a simple json string saved in a file, it should be modified later
        ebi_data[database]={'bio_project_id':bio_project_id, 'secondary_study_accession':secondary_study_accession}
        with open(ebi_projet_infor_file, 'w') as outfile:
            json.dump(ebi_data, outfile)
        return bio_project_id, secondary_study_accession

@manager.command
@manager.option('-m', '--mode', help='run mode')
@manager.option('-u', '--users_file', help='file conatins usr who snt permission to submit their reads to EBI')
def upload_reads(mode='test', users_file=None):
    if not users_file:
        local_fix_foldr = app.config["EBI_TEMP_FOLDER"]
        users_file=os.path.join(local_fix_foldr, "users_EBI_perm.txt")

    if users_file and os.path.isfile(users_file):
        with open(users_file) as f:
            # users who gave us permission to submit their reads to EBI
            users = f.read().splitlines()
    else:
        print "file %s is not exist" % users_file
        return

    dbs = get_public_databases()
    for user in users:
        for db in dbs.keys():
            print "Checking user %s for database %s"%(user, db)
            upload_reads_to_EBI_project(database=db, user_name=user, mode=mode)

@manager.option('-d', '--database', help='database')
@manager.option('-u', '--user_name', help='database')
@manager.option('-m', '--mode', help='run mode')
def upload_reads_to_EBI_project(database=None, user_name=None, mode='test'):
    '''
    upload user reads to EBI
    :param database:
    :param user_name:
    :param mode:
    :return:
    '''
 # the contenets of the following files needs to be added to one file in group acces location
    import os
    from entero.admin import ENAsubmission

    #EBI folder which is used to save EBI databases projects information
    #and other related to EBI, e.g. file contains list of fixed users reads files

    local_fix_foldr=app.config["EBI_TEMP_FOLDER"]

    fixed_files=os.path.join(local_fix_foldr,"fixed_files.txt")

    with open(fixed_files) as f:
        lines_ = f.read()
        already_fixed_files = lines_.split("\n")


    if not database or not user_name:
        return


    dbase = get_database(database)
    if not dbase:
        print "No database is found for %s"%database
        return
    database_label=get_database_label(database)
    user = db.session.query(User).filter(User.username==user_name).first()

    if not user:
        print "no record in he database for user: %s"%user_name
        return


    collectedby=user.firstname[0]+". "+user.lastname+", "+user.institution
    strains = dbase.models.Strains
    traces = dbase.models.Traces
    assemeblies=dbase.models.Assemblies
    traces_assembly = dbase.models.TracesAssembly

    qry = dbase.session.query(strains, traces, traces_assembly,assemeblies) \
        .filter(strains.owner==user.id) \
        .filter(strains.best_assembly is not None) \
        .filter(traces.strain_id == strains.id) \
        .filter(traces.seq_platform == 'ILLUMINA') \
        .filter(traces.read_location is not None) \
        .filter(traces.accession.notlike('ERR%')) \
        .filter(traces.accession.like('traces-%%')) \
        .filter(strains.best_assembly==assemeblies.id)\
        .filter(assemeblies.status=='Assembled') \
        .filter(strains.uberstrain > 0)\
        .filter(traces_assembly.columns['trace_id'] == traces.id) \
        .filter(traces_assembly.columns['assembly_id'] == strains.best_assembly)\

    strain_data = [[data[0].as_dict(), data[1].as_dict()] for data in qry]
    if len(strain_data) == 0:
        print  "nothing is found"
        return []
    # get rMLST species prediction
    barcodes={}
    assembly_ids = [data[0]['best_assembly'] for data in strain_data if data[0].get('species', None) is None]
    for data in strain_data:
        if data[0].get('barcode'):
            barcodes[data[0].get('barcode')]={}


    if len(assembly_ids):
        schemes = dbase.models.Schemes
        types = dbase.models.AssemblyLookup
        assembly_data = dbase.session.query(types.assembly_id, types.st_barcode).filter(
            types.assembly_id.in_(assembly_ids)).filter(types.scheme_id == schemes.id).filter(
            schemes.description.like('%rMLST%')).filter(types.st_barcode != None).all()
        assembly_data = {d[0]: d[1] for d in assembly_data}
        st_barcodes = list(set(assembly_data.values()))

        url = entero.config['NSERV_ADDRESS'] + 'retrieve.api'
        resp = (requests.post(url, data={'barcode': ','.join(st_barcodes), "fieldnames": 'barcode,info',
                                                   'sub_fieldnames': 'index_id', 'simple': 1, 'convert': 0},
                                        timeout=30, ).text)
        resp=json.loads(resp)

        species_data = {d['barcode']: d['info']['hierCC']['d40'] for d in resp if isinstance(d['info'], dict) and len(
            d['info']) > 0}
        assembly_data = {k: species_data[v] for k, v in assembly_data.items() if v in species_data}

        # for strain in results:
        for strain, trace in strain_data:
            if strain.get('species', None) is None and strain.get('best_assembly', None) in assembly_data:
                strain['species'] = assembly_data[strain['best_assembly']]


    # convert to ENA format
    print "Total numbr of strains=: ",len(strain_data)
    dataset = ENAsubmission.convertData(strain_data, barcodes, database, collectedby)

    ebi_projet_infor_file =os.path.join(local_fix_foldr,"broker_ebi_projects.json")
    if os.path.isfile(ebi_projet_infor_file):
        with open(ebi_projet_infor_file) as json_file:
            ebi_data = json.load(json_file)
    else:
        ebi_data = {}
    if ebi_data.get(database):
        project_data = ebi_data[database]
        bio_project_id = project_data.get('bio_project_id')
        secondary_study_accession = project_data.get('secondary_study_accession')
        print bio_project_id, secondary_study_accession
    else:
        print "Could not find databas %s in created EBI projects, please check that."
        return
    #local folder whihc is used as working folder to fix users read files
    local_file_fix_folder = os.path.join(os.path.expanduser('~'), 'temp')
    if not os.path.exists(local_file_fix_folder):
        os.mkdir(local_file_fix_folder)

    for (center, release_date), ds in sorted(dataset.items()) :
        print "------------------------------->>>>>>>"
        local_folder = min([d['SAMPLE'] for d in ds])
        print "Release Date:", release_date
        print  "Center, ", center
        print "no of strains:",len(ds)
        print local_folder
        print collectedby
        print "Mode is: ", mode
        co=0
        # submitt only release strains as the EBI project can not have public and private samples
        if release_date != "released":
            continue

        #check read files to fix the users read files to remove "trailing garbage" and replaced the original ones,
        for data in ds:
            for fname in data['READ_FILES']:
                co+=1
                #chck if the file has been fixed before
                if fname in already_fixed_files:
                    print "%s / %s: %s is already fixed ...Good news!"%(co,(len(ds)*2),fname)
                    continue
                print co,"/",len(ds)*2, fname
                check_fix_file(fname, local_file_fix_folder)

                #write the fixed file name to the file which contains fixed files list
                f = open(fixed_files, 'a')
                f.write("\n%s" % fname)
                f.close()
                already_fixed_files.append(fname)

        receipt_xml=ENAsubmission.ENAsubmission(local_folder, "Proxy upload by EnteroBase (from database %s)"%database_label, center, release_date,  app.config['BROKER_EBI_FTP_USER'], app.config['BROKER_EBI_FTP_PASSWORD']).run(ds, local_fix_foldr, mode)
         
        if receipt_xml:
            update_records_from_EBI_recipt(receipt_xml, dbase, bio_project_id=bio_project_id, secondary_study_accession=secondary_study_accession)

@manager.command
def list_routes():
    import urllib
    from flask import Flask, url_for    
    output = []
    for rule in app.url_map.iter_rules():

        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        methods = ','.join(rule.methods)
        url = url_for(rule.endpoint, **options)
        line = urllib.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, url))
        output.append(line)

    for line in sorted(output):
        print line

@manager.command
def run_app():
    from entero import app
    app.run(host='0.0.0.0',port=5569, ssl_context=("/home/khaled/temp/temp_crt/cert.pem","/home/khaled/temp/temp_crt/key.pem"))

#runserver -h 0.0.0.0 -p 5567 --cert=cert.pem --key=key.pem
if __name__ == '__main__':
#    Command.capture_all_args = True
    manager.run()

