import os
import ujson
import socket
import random

basedir = os.path.abspath(os.path.dirname(__file__))
projectdir = os.path.join(os.getcwd(), 'entero')

class Config:
    '''Global configuration parameters for Enterobase. Instance specific settings 
       (i.e. server by server can be found in ENTEROBASE_INSTANCE_CONFIG)
    '''
    #********* CELERY SETUP *********************
    POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER = 3 * [None]
    BROKER_URL = 'amqp://guest:guest@localhost:5672//'
    #CELERY_IMPORTS = ('entero.jobs.jobs', )
    CELERY_RESULT_BACKEND = 'amqp'
    CELERY_ACCEPT_CONTENT = ['pickle']
    USE_CELERY=False
    ENTEROBASE_INSTANCE_CONFIG = os.path.join(os.path.expanduser('~'), '.enterobase_instance_config.yml')
    WIKI_BASE = 'http://enterobase.readthedocs.io/en/latest/'
    DOWNLOAD_URI = 'http://137.205.123.127/ET/meta/' #http://137.205.52.10/meta/'  # http://10.70.13.74:7353/Location of download server    
    CROBOT_URI = "http://137.205.123.127/ET/"
    FILE_UPLOAD_DIR = '/share_space/user_reads'
    JBROWSE_URI="http://137.205.123.127/entero_jbrowse"
    CALLBACK_ADDRESS = "10.160.108.2" 
    SERVER_ADDRESS =  '137.205.66.6'
    NSERV_ADDRESS = "http://137.205.123.127/ET/NServ/" #"http://olixo.lnx.warwick.ac.uk:6234"
    SERVER_BASE_NAME='enterobase.warwick.ac.uk/'
    
    #**********LOCAL ENTEROBASE UPLOAD AND ASSEMBLY TEST FILE LOCATIONS**********
    LOCAL_ENTEROBASE_UPLOAD_TEST_FILES = "test_files.json"
    LOCAL_ENTEROBASE_UPLOAD_TEST_FILES_EXPECTED_RESULTS = "test_files_expected_results.json"

    #**********DATABASE SETTINGS***********************
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    DATABASE_QUERY_TIMEOUT = 1

    #the name is used when sending assemblies
    DB_CODES = {'senterica' : ['salmonella','SAL'],
                'ecoli' : ['escherichia','ESC'],
                'mcatarrhalis' : ['moraxella','MOR'],
                'yersinia': ['yersinia','YER'],
                'listeria': ['listeria','LIS'],
                'neisseria': ['neisseria','NEI'],
                'mycobacterium': ['mycobacterium','MYC'],
                'klebsiella' : ['klebsiella','KLE'],
                'clostridium' : ['clostridioides','CLO'],
                'pathogen':['pathogen',"PAT"],
                'miu':['miu',"MIU"],
                "vibrio":['vibrio','VIB'],
                "klebsiella":['klebsiella','KLE'],
                "helicobacter":['helicobacter','HEL'],
                "photorhabdus":['photorhabdus','PHO'],
                "wtmetabase":["wtmetabase","WTM"],
                "cronobacter":["cronobacter","CRO"],
                "bacillus":["bacillus","BAC"],
                "virus":["virus","VIR"],
                "treponema":['treponema','TRE'],
                "tannerella":['tannerella','TAN'],
                "porphyromonas":['porphyromonas','POR'],
                "streptococcus": ['streptococcus', 'STR'],
                "coronavirus" : ['coronavirus', 'CRO'],
                "ancientdna": ['ancientdna', 'ANC']
                }

    TABLE_CODE = dict(strains='SS', datadefs='DE',schemes='SC',
                       assemblies='AS', alleles='AL',sts='ST',
                       refsets='RE',traces='TR',loci='LO', archive='AR',
                       taxondef='TA', snps='SN')

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    
    PLATFORM_TO_PARAM={

        "ILLUMINA":"pe",
        "LS454":"454",
        "PACBIO_SMRT":"pacbio",
        "ION_TORRENT":"iontorrent",
        "NANOPORE":"nanopore"
    }
    
    CONTIG_STATS_MAP= {
        "Species":"top_species",
        "Low_qualities":"low_qualities",
        "max contig number":"contig_number",
        "N50":"n50",
        "Total_length":"total_length" 
    } 

    ENTERO_MAIL_SUBJECT_PREFIX = '[Enterobase]'
    MAIL_SUPPRESS_SEND = False
    MAIL_FAIL_SILENTLY = False
    MAIL_DEBUG = True
    WTF_CSRF_ENABLED = False
    
    #***********MISC*****************************
    JAVASCRIPT_VERSION = random.uniform(0,1)
    ENTEROBASE_VERSION = '1.1.2'
    NSERV_TIMEOUT = 120
    
    #**********FILE UPLOADS/DIRECTORIES**********************************
    LOCAL_UPLOAD_DIR = '/share_space/user_reads/'
    SNP_DIRECTORY = "/share_space/snps"
    BASE_WORKSPACE_DIRECTORY = '/share_space/workspace/'
    WEB_CONTENT_DIRECTORY = '/share_space/web_content/'
    
    #********Analysis Plugins********
    ANALYSIS_TYPES={
        "snp_sa_project":{
                       "label":"SNP Project",
                       "icon":"/static/img/icons/snp_tree.png", 
                       "shareable":True,
                       "create_from_search":True,
                       "url":"species/<database>/snp_project/<id>",
                       "job_required":True,
                       "Class":"SNPProject",
                       "parameters":{
                            "strain_number":"Strains",
                            "reference":"Reference Genome",
                            "matrix":"Predefined Matrix",
                            "missing_sites":"Min % Sites Present",
                            "variant_sites":"No. Variant Sites",
                            "snp_list":"SNP List"
                        },
                       "max_snp_number":200,
                       "inform_administrator_on_fail":True
                       
                                      
        },
        "snp_project":{
                            "label":"SNP Project",
                            "icon":"/static/img/icons/snp_tree.png", 
                            "shareable":True,
                            "Class":"SNPProject",
                            "job_required":True,
                            "parameters":{
                                "strain_number":"Strains",
                                "reference":"Reference Genome",
                                "matrix":"Predefined Matrix",
                                "missing_sites":"% Sites Present",
                                "snp_list":"SNP List",
                                "variant_sites":"No. Variant Sites"
                            },
                            "max_snp_number":200,
                            "inform_administrator_on_fail":True
        },        
        "ms_sa_tree":{
                        "label":"MS Tree",
                        "icon":"/static/img/icons/ms_tree.png", 
                        "shareable":True,
                        "create_from_search":True,
                        "url":"ms_tree/<id>",
                        "Class":"MSTree",
                        "job_required":True,
                        "parameters":{
                            "strain_number":"Strains",
                            "scheme":"Scheme",
                            "algorithm":"Algorithm"  
                        }
        
        },
        "ms_tree":{
                              "label":"MS Tree",
                              "icon":"/static/img/icons/ms_tree.png", 
                              "shareable":True,
                              "Class":"MSTree",
                              "job_required":True,
                              "parameters":{
                                  "strain_number":"Strains",
                                  "scheme":"Scheme",
                                  "algorithm":"Algorithm"  
                              }
              
              },        
       
        
        
        "main_workspace":{
                        "label":"Workspace",
                        "icon":"/static/img/icons/list-square-button.png", 
                        "shareable":True,
                        "create_from_search":True,
                        "Class":"WorkSpace",                        
                        "url":"species/<database>/search_strains?query=workspace:<id>"
        },
        "custom_view":{
                        "label":"Custom View",
                        "icon":"/static/img/icons/custom_view.png", 
                        "shareable":True,
                        "Class":"CustomView"
        },        
        "custom_column":{
                        "label":"Custom Column",
                        "icon":"/static/img/icons/custom_column.gif", 
                        "shareable":True
        
        },
        "folder_icon24":{
                        "label":"Folder Icon",
                        "icon":"/static/img/icons/folder_icon24.png", 
                        "shareable":True
        },        
        "locus_search":{
                             "label":"Locus Tagger",
                             "icon":"/static/img/icons/locus_search.gif", 
                             "shareable":True,
                             "job_required":True,
                             "Class":"LocusSearch"
             
             },
        "snp_list":{
            "label":"SNP list",
            "icon":"/static/img/icons/custom_column.gif",
            "shareable":True,
            "Class":"SNPList",
            "parameters":{
                "snp_number":"SNP Number",
                "reference_strain":"Reference Strain",
                "snp_list":"Defined SNPs"
            
            }
        
        }
        
    }
    
    #********Analysis Plugins********
    GENE_CATEGORIES={
        "ecoli":{
            "amr_gene":{
                "label":"AMR Genes(Res Finder)",
                "description":"",
                "color":"red"
            },
            "phage_gene":{
                "label":"Phage Genes (VOGs)",
                "description":"",
                "color":"purple"
            },
            "plasmid_gene":{
                "label":"Plasmid Genes (Plasmid Finder)",
                "description":"",
                "color":"#7FFF00"                   
            }
        }
    }
    

    @staticmethod
    def init_app(app):
        pass
    

class DevelopmentConfig(Config):

    import wingdbstub
    WINGDB_ACTIVE = True
    PROFILE=False
    APP_DB_ECHO = True
    DEBUG = False

class ProductionConfig(Config):
    ''' Only to be used on remote server

    '''
    # Logging settings
    SQLALCHEMY_ECHO = False
    PROFILE = False
    APP_DB_ECHO = True
    WINGDB_ACTIVE = False

class WindowsDevelopmentConfig(Config):
    ''' Only to be used on Windows development machines

    '''

    FILE_UPLOAD_DIR = 'C:\\enterobase_dev'
    # Logging settings
    DEBUG = False
    SQLALCHEMY_ECHO = False
    #SERVER_ADDRESS =  'http://127.0.0.1/'
    USE_CELERY=False
    CALLBACK_ADDRESS="137.205.123.127"
    CACHE_TYPE = 'null'
    PROFILE = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    # This allows us to test the forms from WTForm
    APP_DB_ECHO = True


class TestingConfig(Config):
    
    # Logging settings
    TESTING = True
    DEBUG = False
    SQLALCHEMY_ECHO = False
    PROFILE = False

    WINGDB_ACTIVE = False
    APP_DB_ECHO = False
    
    #TEST EMAIL SERVER
    MAIL_SUPPRESS_SEND = False
    MAIL_FAIL_SILENTLY = False
    MAIL_DEBUG=True
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_USE_SSL = False #(with 587)
    MAIL_PORT = 587 #465 SSL or 587 TLS
    MAIL_USE_TLS = True
    MAIL_USERNAME = "enterotest@gmail.com"
    MAIL_PASSWORD= "bacteria123"

configLookup = {
    'windows_development': WindowsDevelopmentConfig,
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': ProductionConfig
}
