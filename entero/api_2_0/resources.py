from flask_apispec import use_kwargs, marshal_with, Ref,doc, MethodResource
from entero import app, db, dbhandle, get_database, rollback_close_system_db_session
from flask import request, make_response, jsonify, url_for, send_file
from entero.databases.system.models import User, DatabaseInfo, UserPermissionTags, UserPreferences
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from marshmallow import fields
from schemas import StrainSchema, LookupSchema, TracesSchema, AssembliesSchema, AssemblyLookupSchema
from schemas import StrainDataSchema, SchemeSchema, NServSchema, AllelesSchema, StsSchema, LociSchema, TopSchema, LoginSchema
import requests
from sqlalchemy.orm.attributes import flag_modified
import json
import time
from sqlalchemy.orm import Load, defer, load_only
import urlparse
from sqlalchemy import or_, DateTime, cast,desc
import re, urllib

API_VERSION ='2.0'
'''

def check_api_auth(request, database=None):
    #print "I am checking the token ..", request.authorization.username
    if request.headers.has_key('authorization') and request.authorization !=None:
        data = User.decode_api_token(request.authorization.username)
        is_authorized=False
        if data is not None:   
            #adding this check to be sure that the api_aceess is not revoked
            user=db.session.query(User).filter(User.username==data['username']).first() 
            if not user :
                is_authorized=False
            elif user.administrator==1:
                is_authorized=True
            elif not database:
                is_authorized=True
            elif database != None:
                results=db.session.query(UserPermissionTags).filter(UserPermissionTags.user_id==user.id).all()
                for res in results:                                                                                        
                    if res.species==database and str(res.field)=='api_access':                      
                        is_authorized=True
                        break                  
                
        if data is not None and is_authorized: 
            if data.has_key('api_access_%s' %database) or database == None or data.get('administrator') == 1:
                if ('rMLST' in request.url or request.url.startswith('user')) and data.get('administrator') != 1:
                    return  'You do not have access to these data.'
                return data
            else:
                return 'You are not authorised to access this database, please request access from us <enterobase@warwick.ac.uk>.'
        return 'Invalid token, please confirm your token from http://enterobase.warwick.ac.uk/'
    else:
        return 'Please authenticate all requests using Basic-Auth and a valid token. Register at http://enterobase.warwick.ac.uk/ and request an API Token from us <enterobase@warwick.ac.uk>'
'''
def create_pages(offset, limit, total_records, records):
    links = {}
    out = {}
    out['total____records'] =  total_records
    out['records'] = records
    url_part = list(urlparse.urlparse(request.url))
    update_params = urlparse.parse_qs(url_part[4])
    for key in update_params:
        if isinstance(update_params[key],list):
            update_params[key] = update_params[key][0]
    update_params.update(dict(offset = str(offset), limit=str(limit) ) )
    url_part[4] = urllib.urlencode(update_params)
    links['previous']  = urlparse.urlunparse(url_part)
    
    update_params.update(dict(offset = str(offset+limit), limit=str(limit) ) )
    url_part[4] = urllib.urlencode(update_params)    
    links['next']  = urlparse.urlunparse(url_part)
    
    # Remove next link if required
    if (offset + limit) >= total_records:
        links.pop('next')
    if offset == 0 :
        links.pop('previous')
    out['paging'] = links
    return out        

@marshal_with(Ref('schema'))
class LoginResource(MethodResource):
    schema = LoginSchema()
    table_name = ''

    def __init__(self):
        self.schema = LoginSchema()
        self.SERVER = app.config['NSERV_ADDRESS']
        self.table_name = 'login_info'
        self.name = 'login'
        self.description = 'Login endpoint, refresh your API token'       
        
    def format_output(self, res):
        out = res.as_dict()
        return out 
    
    @marshal_with(schema)    
    @use_kwargs(Ref('schema'),locations=['query'])    
    def get(self, **kwargs):
        try: 
            current_user = db.session.query(User).filter(User.username == kwargs.get('username')).one()
        except NoResultFound:
            #db.session.rollback()
            rollback_close_system_db_session()
            return make_response('This user %s does not exist' %kwargs.get('username'), 401)
        if len(kwargs.get('password', '' )) == 0:
            return make_response('No password supplied', 401)
        if not current_user.verify_password(kwargs.get('password', '')):
            return make_response('Incorrect password', 401)
        dat = {}
        api_valid = db.session.query(UserPermissionTags).filter(UserPermissionTags.user_id == current_user.id).filter(UserPermissionTags.field.like('api_access%')).first()
        if api_valid or  current_user.administrator == 1:
            dat['api_token'] = current_user.generate_api_token(expiration=15768000)
        if len(dat) <1 : 
            return make_response('You are not authorised to access the API, please request access from us <enterobase@warwick.ac.uk>.', 403)
        return jsonify(dat)


    def update_doc(self, docs):

        doc_string = docs.spec._paths['/api/v2.0/login']
        for method in doc_string.iterkeys():
            doc_string.get(method)['tags'] = [self.name.title()]
            doc_string.get(method)['description'] = self.description
            doc_string.get(method)['responses'] = self.__format_responses(method)
            for param in doc_string.get(method).get('parameters'):
                param['description'] = self.schema.get_description(param['name'])

        return docs
    
    def __format_responses(self, method):
        responses = {403 : dict(description = 'Unauthorised access for this specific resource or data')} 
        if method == 'get' or method == 'post':
            responses[200] = dict(description = 'A %s object' %self.name ) 
        return responses

@marshal_with(Ref('schema'))
class TopResource(MethodResource):
    schema = TopSchema()
    table_name = ''

    def __init__(self):
        self.schema = TopSchema()
        self.SERVER = app.config['NSERV_ADDRESS']
        self.table_name = 'database_info'
        self.name = 'info'
        self.description = 'Top level information about EnteroBase databases'       
        
        
    def format_output(self, res):
        out = res.as_dict()
        out.pop('id')
        out.pop('val')
        links = {} 
        links['current'] =url_for('infotopresource', _external=True)
        links['strains'] =url_for('strainslistresource' ,database=out.get('name'), _external=True)
        links['schemes'] =url_for('schemeslistresource' ,database=out.get('name'), _external=True)
        links['assemblies'] =url_for('assemblieslistresource' , database=out.get('name'),_external=True)
        links['sts']=url_for('stslistresource', database=out.get('name'), scheme='rMLST',_external=True)
        links['straindata']=url_for('straindatalistresource' , database=out.get('name'),_external=True)
        links['traces']=url_for('traceslistresource', database=out.get('name'),_external=True)
        out['links'] = links
        out['tables'] = json.loads(res.val)
        return out
    
    @marshal_with(schema)    
    @use_kwargs(Ref('schema'),locations=['query'])    
    def get(self, **kwargs):
        auth = User.check_api_auth(request)
        if isinstance(auth,dict):
            dat = [] 
            for res in DatabaseInfo.query.filter_by(**kwargs).all():
                if auth.has_key('api_access_%s' %res.name) or auth.get('administrator') == 1 :
                    dat.append(self.format_output(res))
            if len(dat) < 1:
                return bad_response('You are not authorised to access these databases, please request access from us <enterobase@warwick.ac.uk>.')
            return jsonify(dat)
        
        else:
            return bad_response(auth)

    def update_doc(self, docs):

        doc_string = docs.spec._paths['/api/v2.0']
        for method in doc_string.iterkeys():
            doc_string.get(method)['tags'] = [self.name.title()]
            doc_string.get(method)['description'] = self.description
            doc_string.get(method)['responses'] = self.__format_responses(method)
            for param in doc_string.get(method).get('parameters'):
                param['description'] = self.schema.get_description(param['name'])

        return docs
    
    def __format_responses(self, method):
        responses = {403 : dict(description = 'Unauthorised access for this specific resource or data')} 
        if method == 'get' or method == 'post':
            responses[200] = dict(description = 'A %s object' %self.name ) 
        return responses
    

@marshal_with(Ref('schema'))
class AbstractResource(MethodResource):

    schema = None 
    table_name = None

    @use_kwargs(Ref('schema'))
    def get(self, database, barcode, **kwargs):
        curr_db = get_database(database)
        if not curr_db:
            return bad_response('Can not find % database, please check your request' % database, 400)

        Table = getattr(curr_db.models, self.table_name)
        clean_search = kwargs 
        limit = clean_search.pop('limit',None)
        offset = clean_search.pop('offset',None)
        orderby = clean_search.pop('orderby',None)
        sortorder = clean_search.pop('sortorder',None)
        clean_search["barcode"] = barcode # WVN 12/5/17 Attempt at bug fix for barcode - need to actually filter after the fix below
        try:
            query = curr_db.session.query(Table).filter(Table.barcode is not None).filter_by(**clean_search)
            total_records = query.count()
            data = query.order_by('%s %s' %(orderby, sortorder)).limit(limit).offset(offset).all()
            result = []
            # WVN 12/5/17 Next 2 lines of code borrowed from the post method in bug fix attempt
            #for i in range(offset, (offset + limit)):
            #    if i >= len(data):
            #        continue
            #    dat=data[i]
            for dat in data:
                result.append(self.schema.dump(dat).data)
            out = {self.table_name: result}
            out['paging'] = create_pages(offset, limit, total_records, len(result))
            return jsonify(out)
        except Exception as e:
            curr_db.rollback_close_session()
            app.logger.exception("Error in AbstractResource for %s, error message: %s"%(database, e.message))
            return bad_response ('Error while access the %s database.'%database,503)

    @use_kwargs(Ref('schema'))
    def put(self, database, barcode, **kwargs):
        clean_search = kwargs 
        limit = clean_search.pop('limit',None)
        offset = clean_search.pop('offset',None)
        orderby = clean_search.pop('orderby',None)
        sortorder = clean_search.pop('sortorder',None)
        curr_db = get_database(database)
        if not curr_db:
            return make_response('No such database %s' %database, 404)
        auth = User.check_api_auth(request, database)
        if isinstance(auth, dict):
            if auth['administrator'] == 1 :
                Table = getattr(curr_db.models, self.table_name)
                # Handle custom fields put
                customfield_data = {}
                try:
                    for missed_key, missed_value in request.form.iteritems():
                        if missed_key not in clean_search.keys():
                            custom_id = db.session.query(UserPreferences.id,UserPreferences.name).filter(UserPreferences.name == missed_key).first()
                            if custom_id:
                                customfield_data[custom_id[0]] = missed_value

                    record = curr_db.session.query(Table).filter(Table.barcode == barcode).order_by(desc(Table.lastmodified)).first()
                    if record:
                        for key, vals in clean_search.iteritems():
                            setattr(record, key, vals)
                        if customfield_data:
                            for custom_key, custom_value in customfield_data.iteritems():
                                if not record.custom_columns:
                                    record.custom_columns = {}
                                record.custom_columns[custom_key] = custom_value
                        curr_db.session.add(record)
                        curr_db.session.commit()
                        return make_response(jsonify(self.schema.dump(record).data), 200)
                    else:
                        return make_response('Record %s could not be found in %s database'%(barcode, database), 404)
                except Exception as e:
                    curr_db.rollback_close_session()
                    rollback_close_system_db_session()
                    app.logger.exception("Error in putfor %s, error message: %s"%(database, e.message))
                    return make_response('Error while access the %s database.'%database,503)
            else:
                return make_response('Unauthorised access for this resource' , 403)
        else:
            return make_response(auth, 401)

    @use_kwargs(Ref('schema'))
    def post(self, database, barcode, **kwargs):
        curr_db = get_database(database)
        if not curr_db:
            return bad_response('No such database %s' %database, 404)

        Table = getattr(curr_db.models, self.table_name)
        clean_search = kwargs
        limit = clean_search.pop('limit',None)
        offset = clean_search.pop('offset',None)
        orderby = clean_search.pop('orderby',None)
        sortorder = clean_search.pop('sortorder',None)
        clean_search["barcode"] = barcode # WVN 12/5/17 Attempt at fix for barcode URL POST request case
        try:
            query = curr_db.session.query(Table).filter(Table.barcode is not None).filter_by(**clean_search)
            total_records = query.count()
            data = query.order_by('%s %s' %(orderby, sortorder)).limit(limit).offset(offset).all()
            result = []
            for dat in data:
            #for i in range(offset, (offset + limit)):
            #    if i >= len(data):
            #       continue
            #   dat = data[i]
                result.append(self.schema.dump(dat).data)
            out = {self.table_name: result}
            out['links'] = create_pages(offset, limit, total_records, len(result))
            return jsonify(out)
        except Exception as e:
            curr_db.rollback_close_session()
            app.logger.exception("Error in postfor %s, error message: %s"%(database, e.message))
            return bad_response('Error while access the %s database.'%database,503)


    def update_doc(self, docs):
        doc_string = docs.spec._paths['/api/v2.0/{database}/%s/{barcode}' %self.name]
        for method in doc_string.iterkeys():
            doc_string.get(method)['tags'] = [self.name.title()]
            doc_string.get(method)['description'] = self.description
            doc_string.get(method)['responses'] = self.__format_responses(method)
            for param in doc_string.get(method).get('parameters'):
                param['description'] = self.schema.get_description(param['name'])

        return docs

    def __format_responses(self, method):
        responses = {403 : dict(description = 'Unauthorised access for this specific resource')} 
        if method == 'get' or method == 'post':
            responses[200] = dict(description = 'List of %s objects' %self.name ) 
        elif  method == 'delete':
            pass
        elif  method == 'put':
            pass
        else: 
            responses[200] = dict(description = 'Valid response')
        return responses


import datetime

@marshal_with(Ref('schema'))
class AbstractListResource(MethodResource):

    schema = None 
    table_name = None

    @use_kwargs(Ref('schema'), locations=['query'])
    def get(self, database, **kwargs):
        auth = User.check_api_auth(request, database)
        if isinstance(auth, dict):
            curr_db = get_database(database)
            if not curr_db:
                return bad_response('No such database %s' %database, 404)

            Table = getattr(curr_db.models, self.table_name)
            clean_search = kwargs 
            limit = clean_search.pop('limit', None)
            offset = clean_search.pop('offset', None)
            orderby = clean_search.pop('orderby', None)
            sortorder = clean_search.pop('sortorder', None)
            only_fields = clean_search.pop('only_fields', None)
            my_strains = clean_search.pop('my_strains', None)
            substrains = clean_search.pop('substrains', None)
            return_all = clean_search.pop('return_all', None)
            reldate = clean_search.pop('reldate', None)
            try:
                query = curr_db.session.query(Table).filter(Table.barcode is not None)
                if reldate:
                    cutoff = datetime.datetime.now() - datetime.timedelta(days=reldate)
                    if hasattr(Table,'lastmodified'):
                        query = query.filter(Table.lastmodified > cutoff)
                    elif hasattr(Table,'created'):
                        query = query.filter(Table.created > cutoff)
                if only_fields:
                    clean_only_fields = []
                    for field in only_fields:
                        for table_field in field.split(','):
                            if self.schema.declared_fields.get(table_field, None):
                                clean_only_fields.append(table_field)
                    self.schema = self.schema.__class__(only=tuple(clean_only_fields))
                for field in self.schema.fields:
                        if self.schema.fields[field].attribute:
                            field_name = self.schema.fields[field].attribute
                        else:
                            field_name = field
                        if hasattr(Table, field_name):
                            query = query.options(Load(Table).load_only(field_name))
                if my_strains:
                    query = query.filter(Table.owner == auth['id'])
                if not substrains and self.table_name == 'Strains':
                    query = query.filter(Table.uberstrain == Table.id)
                for arg in clean_search.keys():
                    if isinstance(clean_search[arg], list):
                        query = query.filter(getattr(Table, arg).in_(clean_search[arg]))
                        clean_search.pop(arg)
                query = query.filter_by(**clean_search)
                total_records = query.count()
                if return_all:
                    data = query.order_by('%s %s' %(orderby, sortorder)).offset(offset).all()
                else:
                    data = query.order_by('%s %s' %(orderby, sortorder)).limit(limit).offset(offset).all()
                result = []
                #for i in range(offset, (offset + limit)):
                #    if i >= len(data):
                #        continue
                #    dat = data[i]
                for dat in data:
                    row = self.schema.dump(dat).data
                    #K.M. 07/09/2018
                    # I have added this lie to prevent cgMLST to be included fro senterica database
                    # in the results as we only support only cgMLST_V1

                    if row.get('scheme_name')=='cgMLST' and database=='senterica':
                        continue

                    if row.get('assembly_barcode') and str(row['assembly_barcode']).isdigit():
                        row['assembly_barcode'] = curr_db.encode(int(row['assembly_barcode']), database, self.name)
                    if row.get('uberstrain'):
                        row['uberstrain'] = curr_db.encode(int(row['uberstrain']), database, self.name)
                    if hasattr(self, 'format_links'):
                        row = self.format_links(dat)
                    result.append(row)
                out = {self.table_name: result}
                out['links'] = create_pages(offset, limit, total_records, len(result))
                return jsonify(out)
            except Exception as e:
                curr_db.rollback_close_session()
                app.logger.exception("Error in get in AbstractListResource for %s, error message: %s"%(database, e.message))
                return bad_response('Error while access the %s database.'%database,503)
        else:
            return bad_response(auth, 401)



    def update_doc(self, docs):

        doc_string = docs.spec._paths['/api/v2.0/{database}/%s' %self.name]
        for method in doc_string.iterkeys():
            doc_string.get(method)['tags'] = [self.name.title()]
            doc_string.get(method)['description'] = self.description
            doc_string.get(method)['responses'] = self.__format_responses(method)
            for param in doc_string.get(method).get('parameters'):
                param['description'] = self.schema.get_description(param['name'])

        return docs

    def __format_responses(self, method):
        responses = {403 : dict(description = 'Unauthorised access for this specific resource')} 
        if method == 'get' or method == 'post':
            responses[200] = dict(description = 'List of %s objects' %self.name ) 
        elif  method == 'delete':
            pass
        elif  method == 'put':
            pass
        else: 
            responses[200] = dict(description = 'Valid response')
        return responses




class LookupResource(MethodResource):

    schema = LookupSchema()
    table_name = 'lookup'

    def __init__(self):
        self.schema = LookupSchema()
        self.SERVER = app.config['NSERV_ADDRESS']
        self.table_name = 'lookup'
        self.name = 'lookup'
        self.description = 'Generic endpoint for lookup of barcodes'

    @marshal_with(schema)
    def get(self, barcode):
        curr_db = dbhandle['senterica']
        Strains = curr_db.models.Strains
        #This code is broken as there is no kwargs
        # it is used to set value for two attributes which are never used
        #so I have commented the next three lines
        #clean_search = kwargs
        #limit = clean_search.pop('limit',None)
        #offset = clean_search.pop('offset',None)
        try:
            data = curr_db.session.query(Strains).filter_by(barcode = barcode).all()
            result = []
            for dat in data:
                result.append(self.schema.dump(dat).data)
            return jsonify({"strains": result})
        except Exception as e:
            curr_db.rollback_close_session()
            app.logger.exception("Error in LookupResource 'get' for senterica database, error message: %s"%(e.message))
            return bad_response('Error while access the senterica database.',503)

    @marshal_with(schema)
    @use_kwargs(schema)
    def post(self, barcode, **kwargs):
        curr_db = dbhandle['senterica']
        Strains = curr_db.models.Strains
        clean_search = kwargs
        limit = clean_search.pop('limit',None)
        offset = clean_search.pop('offset',None)
        try:
            #ref to barcode attibutes is missing
            #I have added it as methods arg
            data = curr_db.session.query(Strains).filter_by(barcode = barcode).all()
            result = []
            for dat in data:
                result.append(self.schema.dump(dat).data)
            return jsonify({"strains": result})
        except Exception as e:
            curr_db.rollback_close_session()
            app.logger.exception("Error in LookupResource 'post' for senterica database, error message: %s"%(e.message))
            return bad_response('Error while access the senterica database.',503)

    def update_doc(self, docs):

        doc_string = docs.spec._paths['/api/v2.0/%s/{barcode}' %self.name]
        for method in doc_string.iterkeys():
            doc_string.get(method)['tags'] = [self.name.title()]
            doc_string.get(method)['description'] = self.description
            doc_string.get(method)['responses'] = self.__format_responses(method)
            for param in doc_string.get(method).get('parameters'):
                param['description'] = self.schema.get_description(param['name'])

        return docs

    def __format_responses(self, method):
        responses = {403 : dict(description = 'Unauthorised access for this specific resource or data')} 
        if method == 'get' or method == 'post':
            responses[200] = dict(description = 'A %s object' %self.name ) 
        return responses

class LookupListResource(MethodResource):

    schema = LookupSchema()
    table_name = 'lookup'

    def __init__(self):
        self.schema = LookupSchema()
        self.SERVER = app.config['NSERV_ADDRESS']
        self.table_name = 'lookup'
        self.name = 'lookup'
        self.description = 'Generic endpoint for lookup list of barcodes'    

    @marshal_with(schema)
    @use_kwargs(schema, locations=['query'])
    def get(self, **kwargs):
        auth = User.check_api_auth(request)
        if isinstance(auth,dict):
            dat = []
            data_keys =  kwargs
            clause_list = [] 
            lookup = dict(SAL='senterica',ESC='ecoli',YER='yersinia',MOR='moraxella',LIS='listeria',MIU='miu',
                          MYC='mycobacterium', NEI='neisseria',CLO='clostridium')
            if auth.get('administrator') == 1:
                clean_code = data_keys['barcode'].split(',')
            else:
                clean_code = [] 
                for bar in data_keys['barcode'].split(','):
                    if auth.has_key('api_access_%s' %lookup.get(bar[:3], 'Nabil')):
                        clean_code.append(bar)
            if len(clean_code) < 1: 
                return bad_response('You do not have access to these records')
            # WVN 11/5/17 Syntax of get_string is wrong - scheme is required as a parameter
            # in addition to barcode.  (This is also true of the alternative syntax where barcode is put
            # into the path instead going in as a parameter, and then history is required as a parameter as
            # well.)  Currently I appear to get an empty string (without an exception) if a scheme is included
            # so something else may be wrong.
            get_string = self.SERVER + '/retrieve.api?barcode=%s' % ','.join(clean_code)
            try:
                response= requests.get(get_string,timeout=app.config['NSERV_TIMEOUT'])
                dat =  json.loads(response.text)
                new_dat = list(dat)
                for rec in dat:
                    if rec.get('scheme') == 'rMLST' and rec.has_key('allele_id') and auth.get('administrator') != 1 :
                        new_dat.remove(rec)                    
                out = self.get_output_template(len(new_dat))
                out['results'] = new_dat
                return jsonify(out)                
            except requests.exceptions.ReadTimeout:
                return bad_response('Read timeout, please try again later.', 408)                
            except requests.exceptions.ConnectTimeout:
                return bad_response('Connection timeout, please try again later.', 408)
        else:
            return bad_response(auth)

    def get_output_template(self, records):
        links = {}
        out = {}
        out['records'] = records
        return out    


    def update_doc(self, docs):

        doc_string = docs.spec._paths['/api/v2.0/%s' %self.name]
        for method in doc_string.iterkeys():
            doc_string.get(method)['tags'] = [self.name.title()]
            doc_string.get(method)['description'] = self.description
            doc_string.get(method)['responses'] = self.__format_responses(method)
            for param in doc_string.get(method).get('parameters'):
                param['description'] = self.schema.get_description(param['name'])
        return docs

    def __format_responses(self, method):
        responses = {403 : dict(description = 'Unauthorised access for this specific resource or data')} 
        if method == 'get' or method == 'post':
            responses[200] = dict(description = 'List of %s objects' %self.name ) 
            responses[408] = dict(description = 'Connection timeout, please try again later.')
        return responses

@marshal_with(Ref('schema'))
class NServResource(MethodResource):

    schema = None
    table_name = None


    @marshal_with(Ref('schema'))
    @use_kwargs(Ref('schema'), locations=['query'])
    def get(self, database, scheme,  **kwargs):
        nserv_scheme = None
        nserv_db_name = None
        curr_db = get_database(database)
        if not curr_db:
            return make_response('No such database %s' %database, 404)

        Schemes = getattr(curr_db.models, 'Schemes')
        try:
            pipeline_scheme = curr_db.session.query(Schemes.param['scheme']).filter(Schemes.description == scheme).first()
            if not pipeline_scheme:
                return make_response('No such database or scheme: %s in %s' %(scheme,database), 404)
            pipeline_scheme = pipeline_scheme[0]
            nserv_scheme = pipeline_scheme.split('_')[1]
            nserv_db_name = pipeline_scheme.split('_')[0]
            if not nserv_scheme or not nserv_db_name:
                return make_response('No such database or scheme: %s in %s' %(scheme,database), 404)
            auth = User.check_api_auth(request, database)
            if isinstance(auth,dict):
                data_keys = kwargs
                scalar = data_keys.pop('only_fields', None)
                if isinstance(scalar, list):
                    scalar = scalar[0]
                show_alleles = data_keys.pop('show_alleles',None)
                temp_scalar = []
                if show_alleles:
                    temp_scalar.append('value_indices')
                if not scalar:
                    for field in self.schema.fields:
                        if self.schema.fields[field].attribute and field != 'alleles':
                            temp_scalar.append(self.schema.fields[field].attribute)
                    scalar = ','.join(temp_scalar)
                limit = data_keys.pop('limit',None)
                offset = data_keys.pop('offset',None)
                barcode_list = data_keys.pop('barcode',None)
                type_id_list = data_keys.pop('type_id',None)
                reldate = data_keys.pop('reldate',None)
                if self.table_name =='alleles':
                    if data_keys['locus'] == 'all':
                        data_keys.pop('locus',None)
                data = []
                clause_list = []
                for key in data_keys:
                    if data_keys[key]:
                        array = []
                        if isinstance(data_keys[key], unicode) or isinstance(data_keys[key], str) :
                            data_keys[key] = [data_keys[key]]
                        for value in data_keys.get(key):
                            delimited = value.split(',')
                            for delim in delimited:
                                if isinstance(delim, int):
                                    clean_value = delim
                                else:
                                    clean_value = "'%s'" % delim
                                array.append(clean_value)
                        if len(array) > 0:
                            clause_list.append( ' %s in (%s) ' % (key, ','.join(array) ))
                filter = ''
                index_field = 'type_id'
                if self.table_name =='loci':
                    index_field = 'index_id'
                elif self.table_name =='alleles':
                    index_field = 'allele_id'
                elif self.table_name =='STs':
                    index_field = 'ST_id'
                if len(clause_list) > 0:
                    filter = ' AND '.join(clause_list)
                    filter = ' AND ' + filter
                params = {}
                if type_id_list:
                    query = ' %s in (%s)  AND flag %% 2=1 ' %(index_field, type_id_list)
                else :
                    query = ' %s >= %d AND flag %% 2=1 ' %(index_field, offset)
                    params['sort_by']  = index_field
                    #params['limit'] = limit
                    if reldate:
                        date_cutoff = datetime.datetime.today() - datetime.timedelta(days=reldate)
                        query += ' AND create_time > \'%s\' ' %datetime.datetime.strftime(date_cutoff, '%Y-%m-%d')

                post_string = self.SERVER + '/search.api/%s/%s/%s' % (nserv_db_name, nserv_scheme, self.table_name)

            #  get_string = self.SERVER + '/search.api/%s/%s/%s?filter=%s > %d AND accepted= %% 2=1 AND limit = %d AND %s' % (self.database_name, self.scheme_name, self.table_name, iterate[self.table_name], offset, limit, ' AND '.join(clause_list))
                params['filter']  = str(query + filter)
                if barcode_list:
                    params['barcode'] = ','.join(barcode_list)
                params['fieldnames'] = scalar
                #params = dict(filter = 'accepted %%%% 2=1 AND %s limit %d' %(' AND '.join(clause_list),limit))
                response = None
                try:
                    response = requests.post(post_string, data=params,timeout=app.config['NSERV_TIMEOUT'])
                    nserv_db =  json.loads(response.text)

                except Exception as e:
                    error = 'Nothing'
                    if response:
                        error = response.text
                    return make_response('No connection to back end database, please try again later. '\
                                         'If problem persists, please inform us <<enterobase@warwick.ac.uk>>'\
                                         'with this error message %s gives %s' %(post_string, error), 503)

                # http://137.205.52.26//NServ/search.api/Salmonella/UoW/alleles?filter=index_id > 0&accepted=%% 2=1&limit=50
                #response= requests.get(self.SERVER + '/search.api/%s/%s/%s?filter=%s >= %d&accepted=%%%% 2=1&limit=%d' % (self.database_name, self.scheme_name, self.table_name, 'index_id', offset, limit))
                #nserv_db = json.loads(response.text)
                if not isinstance(nserv_db,  list):
                    return make_response('Improper request', 400)
                for res in nserv_db:
                    data.append(self.clean_row(res))
    #            params = dict(filter = '%s %s' %(query, filter), fieldnames='count( %s )'  %index_field)
                params['fieldnames'] = 'count( %s )' %index_field
                params.pop('sort_by', None)
                try:
                    response = requests.post(post_string , data=params, timeout=60)
                    nserv_db =  json.loads(response.text)
                except Exception as e:
                    error = 'Nothing'
                    if response:
                        error = response.text
                    return make_response('No connection to back end database, please try again later. '\
                                         'If problem persists, please inform us  <<enterobase@warwick.ac.uk>>'\
                                         'with this error message %s gives %s' %(post_string, error), 503)

                total_records = int(nserv_db[0]['count'])
                #response= requests.get(self.SERVER + '/search.api/%s/%s/%s?behave=max' %(self.database_name, self.scheme_name, self.table_name))
                result = []
                #for i in range(offset, (offset+limit)):
                if type_id_list:
                    for dat in data:
                        dat['database'] = nserv_db_name
                        result.append(self.schema.dump(dat).data)
                else:
                    for i in range(0, limit):
                        if i>=len(data):
                            continue
                        dat=data[i]
                        dat['database'] = nserv_db_name
                        result.append(self.schema.dump(dat).data)
                out = {self.table_name: result}
                #"format of the url:  http://hercules.warwick.ac.uk:5555/api/v2.0/senterica/MLST_Achtman/sts?limit=20&offset=40&show_alleles=false&scheme=MLST_Achtman&reldate=20"
                out['links'] = create_pages(offset, limit, total_records, len(result))
                return jsonify(out)
            else:
                return make_response(auth, 403)
        except Exception as e:
            curr_db.rollback_close_session()
            app.logger.exception("Error in NServResourcefor %s, error message: %s"%(database, e.message))
            return make_response('Error while access the %s database.'%database,503)


    def update_doc(self, docs):
        doc_string = docs.spec._paths['/api/v2.0/{database}/{scheme}/%s' %self.name]

        for method in doc_string.iterkeys():
            doc_string.get(method)['tags'] = [self.name.title()]
            doc_string.get(method)['description'] = self.description
            doc_string.get(method)['responses'] = self.__format_responses(method)
            for param in doc_string.get(method).get('parameters'):
                param['description'] = self.schema.get_description(param['name'])
        return docs  

    def clean_row(self, row):
        if row.has_key('lastmodified'): row['lastmodified'] =   str(row['lastmodified'])
        if row.has_key('created'): row['created'] =   str(row['created'])
        if row.has_key('date_sent'): row['date_sent'] =   str(row['date_sent'])
        if row.has_key('date_uploaded'): row['date_uploaded'] =   str(row['date_uploaded'])
        if row.has_key('barcode') and row.get('barcode') != None:
            row['barcode_link'] = url_for('api.nserv.lookup',  _external=True) + '?barcode=%s' % row['barcode']
        if row.has_key('job_id') and row.get('job_id') != None:
            row['job_id_link'] = url_for('api.jobs',  _external=True) + '?id=%s' % row['job_id']
        if row.has_key('user_id') and row.get('user_id') != None:
            row['user_id_link'] = url_for('api.users',  _external=True) + '?id=%s' % row['user_id']
    #  row.pop('index_id', None)
        return row    

    def __format_responses(self, method):
        responses = {403 : dict(description = 'Unauthorised access for this specific resource')} 
        responses[400] = dict(description='Malformed request, contains an error')
        if method == 'get' or method == 'post':
            responses[200] = dict(description = 'List of %s objects' %self.name ) 
        elif  method == 'delete':
            pass
        elif  method == 'put':
            pass
        else: 
            responses[200] = dict(description = 'Valid response')
        return responses


class StrainDataResource(MethodResource):

    schema = StrainDataSchema()
    table_name = 'straindata'

    def __init__(self):
        self.schema = StrainDataSchema()
        self.SERVER = app.config['NSERV_ADDRESS']
        self.table_name = 'straindata'    
        self.description = 'Strain data'
        self.name = 'straindata'


    @marshal_with(schema)
    @use_kwargs(Ref('schema'), locations=['query'])
    def get(self, database, **kwargs):
        start_time = time.time()
        auth = User.check_api_auth(request, database)
        if isinstance(auth, dict):
            curr_db = get_database(database)
            if not curr_db:
                return make_response('No such database %s' % database, 404)
            try:
                print 'auth %f' %(time.time() - start_time)
                start_time = time.time()
                Strains  = curr_db.models.Strains
                Assemblies = curr_db.models.Assemblies
                AssemblyLookup = curr_db.models.AssemblyLookup
                SCHEMES = curr_db.models.Schemes        
                self.scheme_key = {}
                for res in curr_db.session.query(SCHEMES).filter(SCHEMES.param['pipeline'].astext == 'nomenclature').all():
                    self.scheme_key[res.id] = res.as_dict()
    
                clean_search = kwargs 
                limit = clean_search.pop('limit', 50)
                offset = clean_search.pop('offset', 0)
                orderby = clean_search.pop('orderby', 'barcode')
                sortorder = clean_search.pop('sortorder', 'asc')
                my_strains = clean_search.pop('my_strains', None)
                substrains = clean_search.pop('substrains', None)
                return_all = clean_search.pop('return_all', None)
                only_fields = clean_search.pop('only_fields', None)
                reldate = clean_search.pop('reldate', None)
                custom_fields = clean_search.pop('custom_fields', None)
                table_choice = None
                altered = False
                ass_lookup_schema = AssemblyLookupSchema()
                query = curr_db.session.query(AssemblyLookup, Assemblies, Strains)\
                    .filter(AssemblyLookup.scheme_id.in_(self.scheme_key.keys()))\
                    .join(Assemblies)\
                    .join(Strains)\
                    .filter(AssemblyLookup.st_barcode != 'EGG_ST')\
                    .filter(AssemblyLookup.st_barcode is not None)\
                    .filter(Strains.barcode is not None)
                if my_strains:
                    query = query.filter(Strains.owner == auth['id'])
                if reldate:
                    cutoff = datetime.datetime.now() - datetime.timedelta(days=reldate)
                    expr = AssemblyLookup.other_data[('results', 'timestamp')].astext.cast(DateTime) 
                    query = query.filter(or_(Strains.lastmodified > cutoff, expr > cutoff))
                if not substrains:
                    query = query.filter(Strains.uberstrain == Strains.id)
                query = query.options(Load(AssemblyLookup).load_only('assembly_id', 'st_barcode', 'scheme_id','version','other_data'))
                customfield_ids = []
                if custom_fields:
                    # Get ids for custom fields 
                    customfield_ids = db.session.query(UserPreferences.id,UserPreferences.name).filter(UserPreferences.name.in_(custom_fields.split(','))). all()
                    query.options(Load(Strains).load_only('custom_columns'))
                if only_fields:
                    clean_only_fields = ['strain_barcode'] 
                    for field in only_fields:
                        for comma_field in  field.split(','):
                            if comma_field in self.schema.fields.keys():
                                clean_only_fields.append(comma_field)                    
                    self.schema = self.schema.__class__(only=tuple(clean_only_fields))
                    print self.schema.fields
                for field in self.schema.fields:
                    for table_obj in [Strains , Assemblies]:
                        if self.schema.fields[field].attribute:
                            field_name = self.schema.fields[field].attribute
                        else: 
                            field_name = field
                        if hasattr(table_obj, field_name):
                            query = query.options(Load(table_obj).load_only(field_name))
                # Handle assembly fields
                # Perform IN on fields if its a list 
                print 'init  %f' %(time.time() - start_time)
                start_time = time.time()
                for arg in clean_search.keys():
                    table_choice = Strains
                    if (hasattr(Assemblies, arg) and arg is not 'barcode'):
                        table_choice = Assemblies
                    if isinstance(clean_search[arg], list):
                        all_vals = [] 
                        for arg_val in clean_search[arg]:
                            all_vals += arg_val.split(',')
                        query = query.filter(getattr(table_choice, arg).in_(all_vals))
                        #clean_search.pop(arg)
                    else:
                        arg_vals = clean_search[arg].split(',')
                        if len(arg_vals) == 1 : 
                            query = query.filter(getattr(table_choice, arg) == clean_search[arg] )
                        else: 
                            query = query.filter(getattr(table_choice, arg).in_(arg_vals) )
                print 'format args %f' %(time.time() - start_time)
                start_time = time.time()
                total_records = query.count() 
                print 'count %f' %(time.time() - start_time)
                start_time = time.time()
                if return_all:
                    data = query.order_by('%s.%s %s' %(table_choice.__table__.name, orderby, sortorder)).offset(offset).all()
            #      elif table_choice:
            #        data = query.order_by('%s.%s %s' %(table_choice.__table__.name, orderby, sortorder)).limit(limit).offset(offset).all()
                else:
                    data = query.distinct(Strains.barcode).limit(limit).offset(offset).all()

                result = {}
                print 'query %f' %(time.time() - start_time) 
                start_time = time.time()
                for dat in data:
                    #print dat[2].__dict__['barcode']
                #for i in range(offset, (offset + limit)):
                #    if i >= len(data):
                #        continue
                #    dat = data[i]
                    # Fetch strain name to collapse st data 
                    row = self.schema.dump(dat[2]).data
                    for cid, cname in customfield_ids:
                        if unicode(cid) in dat[2].custom_columns.keys():
                            row[cname] = dat[2].custom_columns[unicode(cid)]
                    strain_barcode = row['strain_barcode']
                    if not result.get(strain_barcode):           
                        row.update(self.schema.dump(dat[1]).data)
                        if isinstance(row['strain_barcode'],list):
                            row['assembly_barcode'] = row['strain_barcode'][0]
                        else:
                            row['assembly_barcode'] = row['strain_barcode']
                        row['strain_barcode'] = strain_barcode
                        if row.has_key('uberstrain'):
                            row['uberstrain'] = curr_db.encode(int(row['uberstrain']), database, 'strains')                               
                        result[strain_barcode] = row                             
                    # ST Info                     
                    st = dat[0] 
                    st_dat = ass_lookup_schema.dump(st).data
                    if st.other_data:                       
                        st_dat.update(ass_lookup_schema.dump(st.other_data.get('results')).data)                 
                    st_dat['scheme_name'] = self.scheme_key[st.scheme_id]['description']
             
                    if not result.get(strain_barcode).get('sts'):
                        result[strain_barcode]['sts'] = []
                    result[strain_barcode]['sts'].append(st_dat)
                print 'nested loop %f' %(time.time() - start_time)
                start_time = time.time()                
                out = {self.table_name: result}
                out['links'] = create_pages(offset , limit , total_records, len(result))
                return jsonify(out)
            except Exception as e:
                print 'uhoh'
                curr_db.rollback_close_session()
                rollback_close_system_db_session()
                app.logger.exception("Error in Straindata API request for %s database, error message: %s"%(database, e.message))
                return jsonify("Error in Straindata API request")
                
        else:
            return jsonify(auth)

    def update_doc(self, docs):
        doc_string = docs.spec._paths['/api/v2.0/{database}/%s' %self.name]
        for method in doc_string.iterkeys():
            doc_string.get(method)['tags'] = [self.name.title()]
            doc_string.get(method)['description'] = self.description
            doc_string.get(method)['responses'] = self.__format_responses(method)
            for param in doc_string.get(method).get('parameters'):
                param['description'] = self.schema.get_description(param['name'])

        return docs    
    def __format_responses(self, method):
        responses = {403 : dict(description = 'Unauthorised access for this specific resource')} 
        if method == 'get' or method == 'post':
            responses[200] = dict(description = 'List of %s objects' %self.name ) 
        elif  method == 'delete':
            pass
        elif  method == 'put':
            pass
        else: 
            responses[200] = dict(description = 'Valid response')
        return responses


class StrainsResource(AbstractListResource):

    schema = StrainSchema()
    table_name = 'Strains'    

    def __init__(self):
        self.table_name = 'Strains'
        self.schema = StrainSchema()
        self.name = 'strains'
        self.description = 'Strain metadata'

class StrainResource(AbstractResource):

    schema = StrainSchema()
    table_name = 'Strains'    

    def __init__(self):
        self.table_name = 'Strains'
        self.schema = StrainSchema()
        self.name = 'strains'
        self.description = 'Strain metadata'        

class StrainsVersionResource(AbstractListResource):

    schema = StrainSchema()
    # WVN 11/5/17 Fixed to have correct name of table (or rather module attribute from curr_db.models)
    # table_name = 'Strainsversion'    
    table_name = 'StrainsArchive'

    def __init__(self):
        # WVN 11/5/17
        # self.table_name = 'Strainsversion'
        self.table_name = 'StrainsArchive'
        self.schema = StrainSchema()
        # WVN 11/5/17 Attempted a kludgy fix that did not work - changed from 'strainsversion' to 'strains'.  Possibly should be 'archive'
        # or code elsewhere, mostly likely in encode, needs changes.  (Problem is that it ends up re-using the earlier strains endpoint, I think.)
        # Changed back here.
        self.name = 'strainsversion'
        #self.name = 'strains'
        self.description = 'Strain previous metadata'        

class TracesResource(AbstractListResource):

    schema = TracesSchema()
    table_name = 'Traces'    

    def __init__(self):
        self.table_name = 'Traces'
        self.schema = TracesSchema()
        self.name = 'traces'
        self.description = 'Traces (sequence-reads) metadata'

class TraceResource(AbstractResource):

    schema = TracesSchema()
    table_name = 'Traces'    

    def __init__(self):
        self.table_name = 'Traces'
        self.schema = TracesSchema()
        self.name = 'traces'
        self.description = 'Traces (sequence-reads) metadata'

class AssembliesResource(AbstractListResource):

    schema = AssembliesSchema()
    table_name = 'Assemblies'    

    def __init__(self):
        self.table_name = 'Assemblies'
        self.schema = AssembliesSchema()
        self.name = 'assemblies'
        self.description = 'Genome assemblies'
        

class AssemblyResource(AbstractResource):

    schema = AssembliesSchema()
    table_name = 'Assemblies'    

    def __init__(self):
        self.table_name = 'Assemblies'
        self.schema = AssembliesSchema()
        self.name = 'assemblies'
        self.description = 'Genome assemblies'
        

class SchemeResource(AbstractResource):

    schema = SchemeSchema()
    table_name = 'Schemes'    

    def __init__(self):
        self.table_name = 'Schemes'
        self.schema = SchemeSchema()
        self.name = 'schemes'
        self.description = 'Genotyping schemes'

class SchemesResource(AbstractListResource):

    schema = SchemeSchema()
    table_name = 'Schemes'    

    def __init__(self):
        self.table_name = 'Schemes'
        self.schema = SchemeSchema()
        self.name = 'schemes'
        self.description = 'Genotyping schemes'


class LociResource(NServResource):

    schema = LociSchema()
    table_name = 'loci'    

    def __init__(self):
        self.table_name = 'loci'
        self.schema = LociSchema()
        self.name = 'loci'
        self.description = 'Loci '
        self.SERVER = app.config['NSERV_ADDRESS']


class AllelesResource(NServResource):

    schema = AllelesSchema()
    table_name = 'alleles'    

    def __init__(self):
        self.table_name = 'alleles'
        self.schema = AllelesSchema()
        self.name = 'alleles'
        self.description = 'Alleles  data '
        self.SERVER = app.config['NSERV_ADDRESS']



class StsResource(NServResource):

    schema = StsSchema()
    table_name = 'STs'    

    def __init__(self):
        self.table_name = 'STs'
        self.schema = StsSchema()
        self.name = 'sts'
        self.description = 'ST profile data'
        self.SERVER = app.config['NSERV_ADDRESS']

# WVN 25/10/17 Bug fix - need a working implementation of bad_response
# used by several methods above.
# bad_response function borrowed from api_1_0 since api_1_0 is deprecated
# and it is not really desirable to import functions and classes from it.
def bad_response(message,status_code = 401):
    response = jsonify({ 'code': status_code, 'message' : message})
    response.status_code = status_code
    return response
#

