from flask import Blueprint, request, session, url_for
from flask import render_template, redirect, jsonify
from werkzeug.security import gen_salt
from authlib.flask.oauth2 import current_token
from authlib.oauth2 import OAuth2Error
from entero.databases.system.models import (
    User,
    UserGrantAuthorizationCode,
    LocalEnterobaseClient,
    check_permission
    )
from . import oauth
#from utils import save_user_confirmation
from .. import app
from flask_login import login_required, fresh_login_required, current_user
from .utils import check_user_confirmation, save_user_confirmation
from authlib.specs.rfc7009 import RevocationEndpoint
import os
from .utils import authorization, require_oauth


@oauth.route('/authorize', methods=['GET', 'POST'])
@fresh_login_required
def authorize():
    """
    This function is part of the authorisation workflow, here the user authorises the Local Enterobase instance to access the required resources

    Returns:
        Response: authorisation response 
    """

    user = current_user
    grant_user = None
    already_confirmed_flag=False
    request.base_url=request.base_url.replace("http://","https://")
    if request.method == 'GET':
        try:
            grant = authorization.validate_consent_request(end_user=user)
            already_confirmed_flag = check_user_confirmation(request.args.get('client_id'), user.id)
        except OAuth2Error as error:
            app.logger.exception("Authorize error message: %s" % error.error)
            return redirect(request.args.get("redirect_uri"),302)
        if not already_confirmed_flag:
            return render_template('oauth/authorize.html', user=user, grant=grant)
        else:
            grant_user = user

    if request.method == 'POST':
        if request.form.get('confirm'):
            if not already_confirmed_flag:
                save_user_confirmation(request.args.get('client_id'), user.id)
            grant_user = user

    auth_response = authorization.create_authorization_response(grant_user=grant_user)
    return auth_response


@oauth.route('/token', methods=['GET','POST'])
def issue_token():
    """
    This function generates the access token required in the OAuth2 authorisation code flow

    Returns:
        Response: response containing the access token
    """

    entero_config_env = os.getenv("ENTERO_CONFIG")
    if entero_config_env in {None, "development", "windows_development"}:
        # REQUIRED FOR TESTING/DEVELOPMENT PURPOSES
        os.environ['AUTHLIB_INSECURE_TRANSPORT'] = '1'
   
    response = authorization.create_token_response()
    return response


@oauth.route('/revoke', methods=['POST'])
def revoke_token():
    """
    This function revokes a users access token

    Returns:
        Response: OAuth2 revocation response
    """
    return authorization.create_endpoint_response('revocation')


@oauth.route('/oauth_config', methods=['GET'])
def oauth_config():
    """
    This function returns the basic endpoints of the OAuth2 server

    Returns:
        Response: JSON response containing the relevant OAuth2 server information
    """
    return jsonify({
        "issuer": url_for("main.index", _external=True),
        "authorization_endpoint": url_for("oauth.authorize", _external=True),
        "token_endpoint": url_for("oauth.issue_token", _external=True),
        "revocation_endpoint": url_for("oauth.revoke_token", _external=True),
        "userinfo_endpoint": url_for("oauth.user_info", _external=True),
        "clientinfo_endpoint": url_for("oauth.local_enterobase_client_info", _external=True)
    })


@oauth.route('/user_info')
@require_oauth("local_enterobase")
def user_info():
    """
    This function provides the basic information about a user to a Local Enterobase instance

    Returns:
        Response: JSON response containing a user's basic information
    """
    user = current_token.user
    local_enterobase_client = LocalEnterobaseClient.query.filter_by(client_id=current_token.client_id).first()

    is_administrator = False
    if user.id == local_enterobase_client.admin_id:
        is_administrator = True

    return jsonify(id=user.id, username=user.username, email=user.email, firstname=user.firstname, is_administrator=is_administrator)


@oauth.route('/local_enterobase_client_info')
@require_oauth("local_enterobase")
def local_enterobase_client_info():
    """
    This function provides the basic information about a Local Enterobase instance

    Returns:
        Response: JSON response containing the instance's basic information
    """
    local_enterobase_client = LocalEnterobaseClient.query.filter_by(client_id=current_token.client_id).first()
    admin = User.query.filter_by(id=local_enterobase_client.admin_id).first()

    return jsonify(
        name=local_enterobase_client.client_name,
        description=local_enterobase_client.description,
        admin_name="%s %s" % (admin.firstname, admin.lastname),
        contact_email=admin.email
    )


@oauth.route('/lastname')
@require_oauth("local_enterobase")
def test():
    """
    TEST
    """
    user = current_token.user
    return jsonify(lastname=user.lastname)


@oauth.route("/upload_strain",methods = ['POST'])
@require_oauth("local_enterobase")
def upload_strain():
    """
    This is a dummy endpoint for uploading strains, it just checks that the request is in the correct format

    Returns:
        Response: JSON success or error message
    """

    try:
        data = request.form
        files = request.files
        database = data['database']
        strain_metadata = data['metadata']
    except:
        return jsonify({"error":"The format of the request is incorrect"}), 400

    return jsonify({"success":"Successful strain upload"}), 200


@oauth.route('/check_user_authorised_for_database', methods=["GET"])
@require_oauth("local_enterobase")
def check_user_authorised_for_database():
    """
    Checks if a user is allowed to access a database

    Returns:
        Response: success if they are allowed or an error (401) if they are not
    """

    active_databases  = app.config['ACTIVE_DATABASES']
    user = current_token.user

    database_name = request.args.get("database_name")

    if active_databases[database_name][2] == True or check_permission(user.id, 'view_species', database_name):
        return jsonify({"success": "User is authorised to access this database"}), 200

    return jsonify({"error": "User is not authorised to access this database"}), 401
