from authlib.flask.oauth2 import (
    AuthorizationServer, 
    ResourceProtector
)
from authlib.flask.oauth2.sqla import (
    create_query_client_func,
    create_save_token_func,
    create_revocation_endpoint,
    create_bearer_token_validator,
)
from authlib.oauth2.rfc6749 import grants
from werkzeug.security import gen_salt
from entero import db
from entero.databases.system.models import (
    User,
    UserGrantAuthorizationCode,
    LocalEnterobaseClient,
    UsersToken, 
    User, 
    LocalEnterobaseClientUsersConfirmation
)
from flask_login import current_app
from flask.app import Flask

class AuthorizationCodeGrant(grants.AuthorizationCodeGrant):
    def create_authorization_code(self, client, grant_user, request):
        code = gen_salt(48)
        item = UserGrantAuthorizationCode(
            code=code,
            client_id=client.client_id,
            redirect_uri=request.redirect_uri,
            scope=request.scope,
            user_id=grant_user.id,
        )
        db.session.add(item)
        db.session.commit()
        return code

    def parse_authorization_code(self, code, client):
        item = UserGrantAuthorizationCode.query.filter_by(
            code=code, client_id=client.client_id).first()
        if item and not item.is_expired():
            return item

    def delete_authorization_code(self, authorization_code):
        db.session.delete(authorization_code)
        db.session.commit()

    def authenticate_user(self, authorization_code):
        return User.query.get(authorization_code.user_id)


class RefreshTokenGrant(grants.RefreshTokenGrant):
    def authenticate_refresh_token(self, refresh_token):
        token = UsersToken.query.filter_by(refresh_token=refresh_token).first()
        if token and token.is_refresh_token_active():
            return token

    def authenticate_user(self, credential):
        return User.query.get(credential.user_id)

    def revoke_old_credential(self, credential):
        credential.revoked = True
        db.session.add(credential)
        db.session.commit()


query_client = create_query_client_func(db.session, LocalEnterobaseClient)
save_token = create_save_token_func(db.session, UsersToken)

current_app.config["OAUTH2_REFRESH_TOKEN_GENERATOR"] = True
current_app.config["OAUTH2_TOKEN_EXPIRES_IN"] = {
     'authorization_code': 86400,
     'refresh': 31*86400
}

def query_client(client_id):
    client = LocalEnterobaseClient.query.filter_by(client_id=client_id).first()
    if client is None:
        return None
    if client.current_status == "Approved" or client.current_status == "Reinstated":
        return client
    return None


authorization = AuthorizationServer(
    current_app,
    query_client=query_client,
    save_token=save_token,
)
require_oauth = ResourceProtector()


def config_oauth(app):
    """
    This function configures the authorisation server of the OAuth2 workflow

    Args:
        app (Flask): the flask Enterobase application instance
    """
    authorization.init_app(app)

    authorization.register_grant(grants.ClientCredentialsGrant)
    authorization.register_grant(AuthorizationCodeGrant)
    authorization.register_grant(RefreshTokenGrant)

    revocation_cls = create_revocation_endpoint(db.session, UsersToken)
    authorization.register_endpoint(revocation_cls)

    bearer_cls = create_bearer_token_validator(db.session, UsersToken)
    require_oauth.register_token_validator(bearer_cls())

def save_user_confirmation(client_id, user_id):
    """
    Saves the user's acception of the Local Enterobase instance

    Args:
        client_id (str):  id of the Local Enterobase client
        user_id (str): id of the user
    """
    local_enterobase_client_pk = db.session.query(LocalEnterobaseClient).filter(LocalEnterobaseClient.client_id==client_id).first().id
    con_users=db.session.query(LocalEnterobaseClientUsersConfirmation).filter(LocalEnterobaseClientUsersConfirmation.user_id==user_id, LocalEnterobaseClientUsersConfirmation.client_id==local_enterobase_client_pk).all()
    if len(con_users)>0:
       con_users[0].active=True
    else:
        con_user=LocalEnterobaseClientUsersConfirmation()
        con_user.user_id=user_id
        con_user.client_id = local_enterobase_client_pk
        db.session.add(con_user)
    db.session.commit()


def check_user_confirmation(client_id, user_id):
    """
    Check if the user has authorized the Local Enterobase instance

    Args:
        client_id (str): id of the Local Enterobase client
        user_id (str): id of the user

    Returns:
        bool: boolean value indicating whether the user is active and has approved the Local Enterobase instance
    """
    local_enterobase_client_pk = db.session.query(LocalEnterobaseClient).filter(LocalEnterobaseClient.client_id==client_id).first().id
    con_users=db.session.query(LocalEnterobaseClientUsersConfirmation).filter(LocalEnterobaseClientUsersConfirmation.user_id==user_id, LocalEnterobaseClientUsersConfirmation.client_id==local_enterobase_client_pk).all()
    if len(con_users)==0:
        return False
    return con_users[0].active


def check_client(client_id, user_id=None):
    if not client_id:
        return "False"
    client = db.session.query(LocalEnterobaseClient).filter(LocalEnterobaseClient.client_id == client_id).all()
    print len(client)
    if len(client) > 0:
        if user_id:
            #when user id is provided, it will check if the user is local adminstrator or nor
            if client[0].admin_id==admin_id:
                return True
            else:
                return False
        return "True"
    else:
        return "False"

