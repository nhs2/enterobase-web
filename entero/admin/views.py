from flask_admin.contrib.sqla import ModelView
from flask_admin.actions import action
from entero.databases.system import models
from ..decorators import admin_required
from entero import db, app
from entero import dbhandle
from entero.decorators import admin_required
from entero.admin import adminbp
from flask_admin import BaseView,expose
from flask_login import current_user
from flask import render_template,request, flash, redirect,url_for
from entero.databases.system.models import UserJobs, User, UserPermissionTags,LocalEnterobaseClient, LocalEnterobaseClientRequest
from entero.cell_app.tasks import update_user_jobs,process_job
import forms
from ..auth.views import RegistrationForm
from flask_admin.base import MenuLink, Admin, BaseView, expose
from threading import Thread
import psycopg2
from entero.entero_email import sendmail
import requests
import ujson,json
from markupsafe import Markup
from entero.entero_email import send_email
from flask import request

import os


class RestrictedView(ModelView):

    column_exclude_list = ['password_hash']
    column_hide_backrefs = False
    column_display_pk = True
    list_template  =  'admin/admin_list.html'
    edit_template  =  'admin/admin_edit.html'
    create_template = 'admin/admin_create.html'

    def is_accessible(self):
        if hasattr(current_user, 'administrator'):
            return current_user.administrator
        else:
            return False

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            return redirect(url_for('login', next=request.url))

class StrainView(RestrictedView):
    column_searchable_list = ['serotype']

class UserPermissionsView(RestrictedView):
    column_display_pk = False
    column_hide_backrefs = False
    column_searchable_list = ['field', 'user.username']
    column_filters = ['species', 'user.username']
    page_size = 100
    create_modal = True
    form = forms.EditUserPermissionsForm
    
class BuddyPermissionsView(RestrictedView):
    column_display_pk = False
    column_hide_backrefs = False
    column_searchable_list = ['field', 'user.username']
    column_filters = ['species', 'user.username']
    page_size = 100
    create_modal = True
    form = forms.EditBuddyPermissionsForm
    
class UserView(RestrictedView):
    column_display_pk = False
    column_hide_backrefs = False
    column_searchable_list = ['email', 'username']
    column_filters = ['confirmed', ]
    create_modal = True
    can_create = False
    
    
class AssemblyView(RestrictedView):
    column_display_pk = False
    column_hide_backrefs = False
    column_searchable_list = ['barcode',]
    column_filters = ['status', ]
    create_modal = True
    can_create = False

class JobsView(RestrictedView):
    column_display_pk = True
    column_hide_backrefs = False
    #column_searchable_list = ['id',]
    column_filters = ['status', 'pipeline', 'database']
    create_modal = True
    can_create = False
    
class LocalEnterobaseClientView(RestrictedView):
    """H.F. 28/12/2020
    This class allows an Enterobase admin to view Local Enterobase clients
    """
    column_display_pk = False
    column_hide_backrefs = False
    column_list = ('current_status', 'Dashboard', 'admin', 'OAuth Client ID', 'client_uri', 'client_name', 'description')
    def _dashboard(view, context, model, name):
        _html  = '<a href="{}">{} Dashboard</a>'.format(url_for("enteroadmin.local_enterobase_client_dashboard", local_enterobase_client_name=model.client_name), model.client_name)
        return Markup(_html)
    def _client_id(v,c,m,n):
        return m.client_id
    column_formatters = {'Dashboard':_dashboard, 'OAuth Client ID':_client_id}
    column_searchable_list = ['client_name', 'admin.username', 'current_status']
    column_filters = ['current_status']
    page_size = 100
    create_modal = True
    can_create = False
    can_edit = False
    can_delete = False

class LocalEnterobaseClientRequestView(RestrictedView):
    """H.F. 28/12/2020
    This class allows an Enterobase admin to view Local Enterobase client requests
    """
    column_display_pk = False
    column_hide_backrefs = False
    column_list = ('Resend Test Token', 'current_status', 'test_status', 'Dashboard', 'admin', 'client_name', 'client_uri', 'description')
    def _dashboard(view, context, model, name):
        _html  = '<a href="{}">{} Dashboard</a>'.format(url_for("enteroadmin.local_enterobase_client_dashboard", local_enterobase_client_name=model.client_name), model.client_name)
        return Markup(_html)
    def _resend_token(view, context, model, name):
        _html  = '<a href="{}">Resend Test Token</a>'.format(url_for("enteroadmin.resend_test_token_email", local_enterobase_client_name=model.client_name))
        return Markup(_html)
    column_formatters = {'Dashboard':_dashboard, 'Resend Test Token':_resend_token}
    column_searchable_list = ['client_name', 'admin.username', 'current_status']
    column_filters = ['current_status', 'test_status']
    page_size = 100
    create_modal = True
    can_create = False
    can_edit = False
    can_delete = False

def load(admin):
    admin.add_view(LocalEnterobaseClientView(models.LocalEnterobaseClient, db.session, category='Enterobase Clients'))
    admin.add_view(LocalEnterobaseClientRequestView(models.LocalEnterobaseClientRequest, db.session, category='Enterobase Clients'))
    
    admin.add_view(UserView(models.User, db.session, category='System'))
    admin.add_view(RestrictedView(models.UserBuddies, db.session, category='System'))
    admin.add_view(RestrictedView(models.UserUploads, db.session, category='System'))
    admin.add_view(JobsView(models.UserJobs, db.session, category='System'))
    admin.add_view(RestrictedView(models.UserAutoCorrect, db.session, name = 'User Autocorrect', category='System'))
    admin.add_view(RestrictedView(models.UserPermissionTags, db.session, name='User Permissions', category='System'))
    admin.add_view(BuddyPermissionsView(models.BuddyPermissionTags, db.session, name='Buddy Permissions', category='System'))        
    #for dbname in dbhandle.keys():
        #name =  app.config['ACTIVE_DATABASES'][dbname][0]
        #table =  dbhandle[dbname].models.DataParam
        #admin.add_view(RestrictedView(table, dbhandle[dbname].session, category=name, endpoint='%s-%s' % (dbname, table.__tablename__)))
        #table =  dbhandle[dbname].models.Strains
        #admin.add_view(RestrictedView(table, dbhandle[dbname].session, category=name, endpoint='%s-%s' % (dbname, table.__tablename__)))
        #table =  dbhandle[dbname].models.Assemblies
        #admin.add_view(AssemblyView(table, dbhandle[dbname].session, category=name, endpoint='%s-%s' % (dbname, table.__tablename__)))
        
   # admin.add_link(MenuLink(name='Google', category='Links', url='http://www.google.com/'))

@adminbp.route("/<species>/download_genome", methods = ['GET','POST'])
def download_genome(species):
    return render_template("admin/download_genome.html",species=species)


@adminbp.route("/<species>/jobs_monitor", methods = ['GET','POST'])
def jobs_monitor(species):
    return render_template("admin/jobs_view.html",species=species,data="admin",help = app.config['WIKI_BASE']+'/features/jobs.html')



@adminbp.route("/<species>/email_users", methods = ['GET','POST'])
def email_users(species):
    form = forms.EmailUserForm(request.form)
    if request.method == 'POST':
        dbs = request.form.getlist('database')
        usergroups = request.form.getlist('usergroups')
        subject = request.form.get('subject')
        body = request.form.get('body')
        extra = request.form.get('extra_recip')
        blacklist = request.form.get('blacklist')
        dryrun = request.form.get('dryrun')
        res = db.session.query(models.User.email,models.User.firstname).filter(models.UserPermissionTags.species.in_(dbs))\
            .filter(models.UserPermissionTags.field.in_(usergroups))\
            .filter(models.UserPermissionTags.user_id == models.User.id).all()
        flash('Email sent.')
        email_list = [] 
        if 'legacy_mlst' in usergroups:
            email_list = get_mlst_emails(subject, body, dryrun)
        for r in res:
            if r[0] not in email_list:
                email_list.append(r[0])
        black_list = [] 
        if len(blacklist) > 0:
            for y in blacklist.split():
                for x in y.split(','):
                    if x not in email_list:
                        black_list.append(x.strip())
        if len(extra) > 0:
            for y in extra.split():
                for x in y.split(','):
                    if x not in email_list:
                        email_list.append(x.strip())
        temp_list = [] 
        for x in email_list:
            if x not in black_list:
                temp_list.append(x)
        email_list = temp_list
        if len(email_list) == 0: 
            report_email_list = ['NOBODY'] 
        else:
            report_email_list = email_list
        if dryrun:
            report_body = 'The following message was NOT sent to %s\n\nSubject: %s\nMessage:\n%s\n' %(','.join(report_email_list),subject,body)
        else:
            try:
                send_custom_email(email_list, subject, body)
            except Exception:
                    app.logger.exception("Email %s could not be sent" %subject)            
            report_body = 'The following message was sent to %s\n\nSubject: %s\nMessage:\n%s\n' %(','.join(report_email_list),subject,body)
        send_custom_email([current_user.email], '[Email report] ' + subject, report_body)
        return redirect(url_for('enteroadmin.email_users',species=species))
    return render_template('admin/email_users.html', form=form, species=species, help = app.config['WIKI_BASE']+'/features/email_users.html')

def send_custom_email(to, subject, body, **kwargs):

    sender= app.config['ENTERO_MAIL_SENDER']
    sender = current_user.email
    subject = app.config['ENTERO_MAIL_SUBJECT_PREFIX'] + ' ' + subject
    thr = Thread(target=multi_send_email, args=[sender,subject, body,to])

    thr.start()  
    
def multi_send_email(sender, subject, msg,to):
    for person  in to:
        if not sender.endswith('warwick.ac.uk'): sender = 'enterobase@warwick.ac.uk'
        sendmail(me=sender,  to=[person], subject=subject,txt=msg)
    
    
#I am not sure about the postgres host????
    
def get_mlst_emails(subject, body, dryrun=True):
    Esau = '137.205.123.28'
    emails = [] 
    connection,cursor = ConnectToPostgres(Esau)
    if ExecPGQuery(cursor,'''select DISTINCT firstname, name, email from user_flat''', []):
        finished = cursor.fetchall()
        for firstname, name, email in finished:
            if email is not None:
                emails.append(email)
    return emails
    
    
def ConnectToPostgres(host='137.205.123.28'):
    try:
        #What is the mlst database?
        conn = psycopg2.connect('dbname=mlst user=mlst host=%s password=*python# port=6543' % (host))
        cursor = conn.cursor()
        return conn,cursor
    except:
        return None, None    
    
def ExecPGQuery(cursor,ex_val,paramlist,parm=None):
    try:
        if parm is None:
            cursor.execute(ex_val,paramlist)
        else:
            cursor.execute(ex_val,paramlist,parm)
        return True
    except:
        return False    

@adminbp.route('/local_enterobase_client/<local_enterobase_client_name>', methods=['GET', 'POST'])
@admin_required
def local_enterobase_client_dashboard(local_enterobase_client_name):
    """ H.F. 20/12/20
    View for the Central Enterobase administrator to approve/revoke Local Enterobase clients.

    Args:
        local_enterobase_client_name (str): Name of Local Enterobase client
    """
    local_enterobase_client = LocalEnterobaseClient.query.filter_by(client_name=local_enterobase_client_name).first()
    is_request = False # this variable describes which table the Local Enterobase is in (i.e. request table or regular table)
    if bool(local_enterobase_client):
        lead_admin = local_enterobase_client.admin
    else:
        local_enterobase_client = LocalEnterobaseClientRequest.query.filter_by(client_name=local_enterobase_client_name).first()
        if bool(local_enterobase_client):
            lead_admin = local_enterobase_client.admin
            is_request = True
        else:
            flash("Local Enterobase client \"{}\" does not exist.".format(local_enterobase_client_name))
            return redirect(url_for("main.index"))

    if is_request:
        local_enterobase_client_form = forms.ApproveRegisterLocalEnterobaseClientForm(
            client_name=local_enterobase_client.client_name,
            client_uri=local_enterobase_client.client_uri,
            description=local_enterobase_client.description,
            username=local_enterobase_client.admin.username,
            current_status=local_enterobase_client.current_status,
            test_status=local_enterobase_client.test_status,
            average_test_upload_time=local_enterobase_client.average_test_file_upload_time,
            average_test_assembly_time=local_enterobase_client.average_test_file_assembly_time
            )
    else:
        local_enterobase_client_form = forms.ApproveRegisterLocalEnterobaseClientForm(
            client_name=local_enterobase_client.client_name,
            client_uri=local_enterobase_client.client_uri,
            description=local_enterobase_client.description,
            username=local_enterobase_client.admin.username,
            current_status=local_enterobase_client.current_status)

    lead_admin_form = RegistrationForm(
        email=lead_admin.email,
        username=lead_admin.username,
        firstname= lead_admin.firstname,
        lastname=lead_admin.lastname,
        department = lead_admin.department,
        institution = lead_admin.institution,
        city = lead_admin.city,
        country=lead_admin.country)

    if local_enterobase_client_form.validate_on_submit():
        if local_enterobase_client_form.approve.data:
            if not is_request:
                local_enterobase_client.current_status = "Approved"
                db.session.commit()
                flash("Local Enterobase client \"{}\" has been approved.".format(local_enterobase_client_name))
                return redirect(url_for("enteroadmin.local_enterobase_client_dashboard", local_enterobase_client_name=local_enterobase_client_name))

            elif is_request:
                new_lec = LocalEnterobaseClient(
                    client_name=local_enterobase_client.client_name,
                    client_uri=local_enterobase_client.client_uri,
                    description=local_enterobase_client.description,
                    current_status="Approved",
                    admin=lead_admin,
                    admin_id=lead_admin.id
                )
                db.session.add(new_lec)
                new_lec.generate_client_credentials()
                db.session.delete(local_enterobase_client)
                db.session.commit()
                flash("Local Enterobase client \"{}\" has been approved.".format(local_enterobase_client_name))
                return redirect(url_for("enteroadmin.local_enterobase_client_dashboard", local_enterobase_client_name=local_enterobase_client_name))

            else:
                flash("Local Enterobase client \"{}\" does not exist.".format(local_enterobase_client_name))
                return redirect(url_for("main.index"))

        if local_enterobase_client_form.revoke.data:
            if not is_request:
                local_enterobase_client.current_status = "Revoked"
                db.session.commit()
                flash("Local Enterobase client \"{}\" has been temporarily revoked.".format(local_enterobase_client_name))
                return redirect(url_for("enteroadmin.local_enterobase_client_dashboard", local_enterobase_client_name=local_enterobase_client_name))

        if local_enterobase_client_form.reinstate.data:
            if not is_request:
                lec_to_reinstate = LocalEnterobaseClient.query.filter_by(id=local_enterobase_client.id).first()
                lec_to_reinstate.current_status = "Reinstated"
                db.session.commit()
                flash("Local Enterobase client \"{}\" has been reinstated.".format(local_enterobase_client_name))
                return redirect(url_for("enteroadmin.local_enterobase_client_dashboard", local_enterobase_client_name=local_enterobase_client_name))

        if local_enterobase_client_form.deny.data:
            if is_request:
                local_enterobase_client.current_status = "Denied"
                db.session.commit()
                flash("Local Enterobase client \"{}\" request has been temporarily denied.".format(local_enterobase_client_name))
                return redirect(url_for("enteroadmin.local_enterobase_client_dashboard", local_enterobase_client_name=local_enterobase_client_name))

    return render_template('admin/local_enterobase_client.html', local_enterobase_client_form=local_enterobase_client_form, lead_admin=lead_admin_form)

@adminbp.route('/resend_test_token_email/<local_enterobase_client_name>', methods=['GET'])
@admin_required
def resend_test_token_email(local_enterobase_client_name):
    """
    Resends the test token email

    Args:
        local_enterobase_client_name (str): name of the local enterobase client

    Returns:
        Response: redirect back to the referrer
    """
    local_enterobase_request = LocalEnterobaseClientRequest.query.filter_by(client_name=local_enterobase_client_name).first()
    send_email(local_enterobase_request.admin.email, 'We Have Received Your Local Enterobase Registration Request!', 'oauth/email/request_received_test_token', local_enterobase_client=local_enterobase_request, user=local_enterobase_request.admin)
    flash("Test token email has been resent.")
    return redirect(request.referrer)
        