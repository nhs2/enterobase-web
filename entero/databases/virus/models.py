from sqlalchemy import Column, String, MetaData
from sqlalchemy.ext.declarative import declarative_base
from entero.databases import generic_models as mod

metadata = MetaData()
Base = declarative_base(metadata=metadata)

TracesAssembly = mod.getTraceAssemblyTable(metadata)



### Raw data tables ###
class Assemblies(Base, mod.AssembliesRel):
    pass

class AssembliesArchive(Base, mod.AssembliesArchive):
    pass

class Traces(Base, mod.Traces):
    pass

class TracesArchive(Base, mod.TracesArchive):
    pass

class Schemes(Base,mod.Schemes):
    pass

class SchemesArchive(Base,mod.SchemesArchive):
    pass


class Strains(Base,mod.Strains): 
    species = Column("species",String(200))
    
class StrainsArchive(Base, mod.StrainsArchive): 
    species = Column("species",String(200))
    
    
class DataParam(Base,mod.DataParam): 
    pass

class AssemblyLookup(Base,mod.AssemblyLookup):
    pass