import base64

from entero.config import Config
from entero import  generate_database_conns
from HTMLParser import HTMLParser
from sys import stderr
from re import search, IGNORECASE

# WVN 12/3/18
# 401 is for did not authenticate or bad authentication
# (although usually described as the "not authorized" code).
# 403 is for not authorized when authentication was provided.
# 403 is also used for forbidden to *all* users regardless which
# is where my own confusion came from.
# Decided not to bother with change of variable names
# and having one for the 401 code.
ebHTTPNotAuth = 403
# httpNotOrBadAuthenticate=401
# httpNotAuthorized=403
jack_user_id = 158

# WVN 26/3/18 Kludge to get round the disappearance of the way the
# Config used to work.  Ideally I would access the config from the app
# object; but this would require using a fixture that would mess up
# the way parameterised tests based on database names work.
# generate_database_conns(POSTGRES_USER, POSTGRES_PASS, POSTGRES_SERVER)
fakeConfigActiveDBHash =  generate_database_conns("unknownUser", "unknownPass", "unknownServer")

publicDBList = ["senterica", "ecoli", "yersinia", "clostridium", "mcatarrhalis"]
# fullDBList = Config.ACTIVE_DATABASES.keys()
fullDBList = fakeConfigActiveDBHash.keys()
# notPublicDBList = ["miu"] # ignoring others for now
# combinedDBList = publicDBList + notPublicDBList
nonPublicDBList = [x for x in fullDBList if x not in publicDBList]
jackDBList = publicDBList + ["miu"]
nonJackDBList = [x for x in fullDBList if x not in jackDBList]
testbossDBList = fullDBList
nonTestbossDBList = []
              
def get_api_headers(api_key):
    return {
        'Authorization': 'Basic ' + base64.b64encode(
                (api_key+ ":").encode('utf-8')).decode('utf-8'),
            'Accept': 'application/json'
        }

def enHashInfoQueryResponseData(response):
    return {entry["name"]: entry for entry in response} 

# Ideally want to check that links accessible from a page do not give errors.
# This requires some simple parsing and is done via the standardised utility class 
# and function below.  (Originally got idea for checking the link for "Help" only; but
# it may be as easy or easier to look for all of the URL via the <a> tag and more useful.)
class aTagParser(HTMLParser):
    def __init__(self):
        self.hrefList = []
        # Not allowed in Python 2
        # super().__init__()
        # Not a new-style class so can't work
        # super(aTagParser, self).__init__()
        HTMLParser.__init__(self)

    def handle_starttag(self, tag, attrs):
        ## VTEMP
        #print >> stderr, "Start tag: ", tag
        if tag == "a":
            for attr in attrs:
                ## VTEMP
                # print >> stderr, "    attr: ", attr
                if attr[0] == "href":
                    self.hrefList.append(attr[1])

# WARNING - GETting the URLs from the returned list could have certain undesirable outcomes/ side-effects
# such as deleting data that one wants to keep.  (One such outcome was inadvertently logging
# out the test client which is why "logout" is filtered out by default.)
def getATagURLList(sourceURL, inHTML, filterMailto = True, filterHttp = True, filterLogout = True, filterJS = True, filterAdmin = True):
    parser = aTagParser()
    parser.feed(inHTML)
    urlList = [sourceURL + href if href[0] == "#" else href for href in parser.hrefList]
    # I'm not sure if the Flask client can make GET requests to external URLs
    # so filter them out
    # Also, GET requests to a "mailto:" make no sense at all
    # Filtering Javascript related tags is optional (but the default) since at some
    # point, the utility function might be used with the Selenium client
    # urlList = [url for url in urlList if search("^(mailto|https?)", url) is None]
    if filterMailto:
        urlList = [url for url in urlList if search("^mailto:", url, IGNORECASE) is None]
    if filterHttp:
        urlList = [url for url in urlList if search("^https?", url, IGNORECASE) is None]
    if filterLogout:
        urlList = [url for url in urlList if search("logout", url, IGNORECASE) is None]
    if filterJS:
        urlList = [url for url in urlList if search("^javascript:", url) is None]
    # This is intended to skip all admin pages by default now - both because of the potential
    # for inadvertently causing chaos and also because the admin-related stuff is not usually
    # loaded up when testing.
    if filterAdmin:
        urlList = [url for url in urlList if search("admin", url) is None]
    return urlList

# egHTML = '<p><a class=link href=#main>tag soup</p ></a>'
# print "hrefList: ", getATagURLList("http://www.pubmlst.org",  egHTML)
