/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
 */
$.fn.tabbedDialog = function (params) {
	this.tabs();
	this.dialog(params);
	var tabul = this.find('ul:first');
	this.parent().addClass('ui-tabs').prepend(tabul).draggable('option','handle',tabul); 
	this.siblings('.ui-dialog-titlebar').remove();
	tabul.addClass('ui-dialog-titlebar');
	//tabul.append($("<li style = 'float:right'><span id='multitab-close-icon' class='glyphicon glyphicon-plus-sign'></span></li>"));
	
};


$.fn.addTab= function(tabId, tabLabel, tabContentHtml) {
	 var header = "<li><a href='#" + tabId + "'>" + tabLabel + "</a> </li>";
	 //$("#multitab-close-icon").before(header);
	 var nav = this.parent().find(".ui-tabs-nav");
	 nav.append($(header));
	 this.children().append("<div id='" + tabId + "'><p>" + tabContentHtml + "</p></div>");
	 this.tabs("refresh");
};
firstBy = (function() {
	function makeCompareFunction(f, direction){
	  if(typeof(f)!="function"){
		var prop = f;
		f = function(v1,v2){return v1[prop] < v2[prop] ? -1 : (v1[prop] > v2[prop] ? 1 : 0);}
	  }
	  if(f.length === 1) {
		// f is a unary function mapping a single item to its sort score
		var uf = f;
		f = function(v1,v2) {return uf(v1) < uf(v2) ? -1 : (uf(v1) > uf(v2) ? 1 : 0);}
	  }
	  if(direction === -1)return function(v1,v2){return -f(v1,v2)};
	  return f;
	}
	/* mixin for the `thenBy` property */
	function extend(f, d) {
	  f=makeCompareFunction(f, d);
	  f.thenBy = tb;
	  return f;
	}

	/* adds a secondary compare function to the target function (`this` context)
	   which is applied in case the first one returns 0 (equal)
	   returns a new compare function, which has a `thenBy` method as well */
	function tb(y, d) {
		var x = this;
		y = makeCompareFunction(y, d);
		return extend(function(a, b) {
			return x(a,b) || y(a,b);
		});
	}
	return extend;
})();




FieldChooser.prototype = Object.create(null);
function FieldChooser(divName,fieldList){
    var self = this;
    this.fields = {}
    this.fieldList =  fieldList;
    this.numberOfFields = 0;
    this.rowID=1;
    var container = $("#"+divName);
    this.table = $("<table>");
    var addButton = $("<button>").html("Add").bind("click",function(e){
	self.addField();
    
    });
    container.append(this.table).append(addButton);  
}
FieldChooser.prototype.addField = function(initialLabel){
   var sel =$("<select>")
   var self = this;
   for (label in fieldList){     
	 sel.append($("<option>").text(label).val(fieldList[label][0]));   
   }
   var fieldID= "idfield"+this.rowID;
   var row = $("<tr>").attr("id",fieldID);
   
   this.table.append($("<tr>").append(sel))
   var removeButton = $("<button>").attr("info",fieldID)
			.html("-").bind("click",function(e){
			    var id = $(this).attr("info");
			   $("#"+id).remove();
			   delete self.fields[id];
			   self.numberOfFields--;
			    
			});
   row.append($("<td>").append(sel));
   row.append($("<td>").append(removeButton));
   this.table.append(row);
   this.fields[fieldID]=sel;
   this.numberOfFields++;
   if (initialLabel){
      sel.find(":selected").text(initialLabel);
      sel.val(this.fieldList[initialLabel][0]);
   
   }
   this.rowID++;
}

FieldChooser.prototype.getFields=function(){
    var retList =[]
    for (id in this.fields){
	var field  = this.fields[id].val();
	var label = this.fields[id].find(":selected").text();
	datatype = this.fieldList[label][1]
	retList.push({name:field,label:label,datatype:datatype});
    
    }
    return retList;
}



NestedDropDown.prototype = Object.create(null);
function NestedDropDown(container){
    
    
    
    var text = $('<input>');
    var menu = $('<ul>').css('position','absolute').css('display','block')
		    .addClass("ui-front").css("z-index",300).height(100).css('overflow-y','auto');
   
    menu.append($('<li>sdsdffdd</li><li>sdsdsd<ul><li>sdsdsd</li></ul></li>'));
     menu.append($('<li>sdsdffdd</li><li>sdsdsd<ul><li>sdsdsd</li></ul></li>'));
      menu.append($('<li>sdsdffdd</li><li>sdsdsd<ul><li>sdsdsd</li></ul></li>'));
      
    text.click(function(e){
	var offset =  text.offset();
	menu.css('top',offset.top).css('left',offset.left);
	$(".ui-dialog").removeClass("ui-front");
	menu.show();
	e.stopPropagation();
    });
    menu.click(function(e){
	menu.hide();
    
    });
    $(document).click(function(e){
	
	    menu.hide();
    })
     
     menu.hide();
     container.append(text);
    $(document.body).prepend(menu);
    
  
    menu.menu();
    
}





QueryDialog.prototype = Object.create(null);
/*
 *
 *
 *
 */
function QueryDialog(fields,callback){	
	this.row_to_group={};
	this.groups=0;
	this.fields = {};		 
	for(var label in fields) {
	    var dat = fields[label] 
		if (dat instanceof Array){
		    this.fields[label]=dat;
		}
		else{
		    for (lab in dat){
			this.fields[lab]=dat[lab]
		    }
		
		}
	}
	this.nested_fields=fields
	
	this.callback = callback;
	this.rows=1;
	this.operands = {};
	var self =  this;
	$(".ui-dialog").css('overflow',"visible");
	this.div = $("<div>");
	this.table = $("<table>").appendTo(this.div);
	this.addRow();
        this.div.dialog({
            autoOpen: true,
            modal: false,
            title: "Query Builder",
	    width: 'auto',
            height:'auto',
            buttons: {
                "Cancel": function () {
                   
                    $(this).dialog("close");
                },
		 "Submit Query": function () {
                    self.submit();
                    $(this).dialog("close");
                }
		
            },
	    
	    
            close: function () {
                $(this).dialog('destroy').remove();
            }
        });
	
}
QueryDialog.prototype.processCondition= function(field,op,val,type){
    
    if ((type === 'integer' || type === "double") && isNaN(val)){
	return null;
    }
	
    if ((op==='LIKE' || op === 'NOT LIKE' ) && !isNaN(val)){
	if (op ==='LIKE'){
	    op='=';
	}
	else{
	    op='<>';
	}
    }
	
	
    if (op ==='LIKE' || op === "NOT LIKE"){
       val=" '%"+val+"%'";
       op=" "+op+" ";
    }

	
	
    else{
	val="'"+val+"'";
    }
    
    var condition = field+op+val;
    
    return condition;
    
}
	
QueryDialog.prototype.submit= function(){
	    var text=""
	    var in_group = false;
	    var current_group = 0;
	    for (var n=1;n < this.rows;n++){
	        if (this.row_to_group[n] && !in_group){
				text+="(";
				in_group= true;
				current_group= this.row_to_group[n];
			}
			else if (this.row_to_group[n] != current_group && current_group !==0){
				text+="(";
				in_group = true;
				current_group=this.row_to_group[n];
			}
		
		var field = $("#"+this.name+"_field_"+n).val();
		var op = $("#"+this.name+"_operator_"+n).val()
		var val = $("#"+this.name+"_value__"+n).val();
		if (!val && val !==0){
		    return;
		}
		//get field type
		var label = $('#'+this.name+"_field_"+n).find('option:selected').text();
		var type = this.fields[label][1];
		
		var condition = this.processCondition(field,op,val,type)
		if (! condition){
		    return;		
		}
		text+=condition
		if (in_group && this.row_to_group[n+1] != current_group){
		    text+=")";
		    in_group = false;
		    current_group=0;
		}
		if (n!== this.rows-1){
		    text +=" "+this.operands[n]+" "
		}
	    
	    }    
	    this.processQuery(text);
};





QueryDialog.prototype.processQuery = function(text){
    return text
};


	
QueryDialog.prototype.addRow = function(fields,div){
	    var self = this;
	    var rowNum = this.rows
	    var row =$("<tr>").attr("id","row__"+this.rows);
	    var select = $("<select>").attr("id","field__"+this.rows).attr('class','jop');
	    for(var label in this.nested_fields) {
		var dat = this.nested_fields[label] 
		if (dat instanceof Array){
		select.append($("<option></option>")
                .attr("value",dat[0])
                .text(label));
		}
		else{
		var o = $('<optgroup>').attr("label",label);
		for (var lab in dat){
		    o.append($("<option></option>")
		.attr("value",dat[lab][0])
		.text(lab));
		
		}
		select.append(o);
		}
	    }
	    
	    
            var value =$("<input>").attr("type","text").attr("id","value__"+this.rows);
	    
	    var typeSelect= $("<select>").attr("id","operator__"+this.rows);
	    typeSelect.append($("<option>").attr("value","LIKE").text("contains"));
	    typeSelect.append($("<option>").attr("value","NOT LIKE").text("not contains"));
	    typeSelect.append($("<option>").attr("value","=").text("equals"));
	    typeSelect.append($("<option>").attr("value","<>").text("not equals"));
	    typeSelect.append($("<option>").attr("value",">").text(">"));
	    typeSelect.append($("<option>").attr("value","<").text("<"));
	    this.processRowItems(select,typeSelect,value);
	   
	    var andbut =$("<button>").html("AND").click(function(){
		self.operands[rowNum] = "AND"
		orbut.remove()
		$(this).attr("disabled","disabled");
		self.addRow();
	    
	    });
	    var orbut = $("<button>").html("OR").click(function(){
		self.operands[rowNum] = "OR"
		andbut.remove()
		$(this).attr("disabled","disabled");
		self.addRow();
	    });
	    var groupbut = $("<button>").data('num',rowNum)
	    .html("GRP").attr("id","grp-button-"+rowNum).click(function(){
		var grp_list=[]
		for (var i=1;i<=$(this).data('num');i++){
		    if (!self.row_to_group[i]){
			grp_list.push(i)
		    }
		}
		if (grp_list.length>1){
		    $('#left-bracket-'+grp_list[0]).html("<b>(</b>");
		    $('#right-bracket-'+grp_list[grp_list.length-1]).html("<b>)</b>");
		    self.groups++;
		    for (var i in grp_list){
			self.row_to_group[grp_list[i]] = self.groups;
			$("#grp-button-"+grp_list[i]).hide();
		    }
		    
		    
		}
		
		
	    });
	    row.append($("<td></td>").attr('id','left-bracket-'+rowNum)).append($("<td>").append(select))
			      .append($("<td>").append(typeSelect))
			      .append($("<td>").append(value)).append($("<td></td>").attr('id','right-bracket-'+rowNum))
			      
	    if ( rowNum>1 && !this.row_to_group[rowNum-1]){
		row.append(groupbut);
	    }	      
	    this.table.append(row);
	    row = $('<tr>').css('text-align','center');
	    row.append("<td>").append("<td>").append($('<td>').append(andbut).append(orbut));
	   
	    this.table.append(row);
	    this.rows++;
            
            select.trigger('change');
	    
};

QueryDialog.prototype.processRowItems= function(select,typeSelect,value){

};
	
StrainQueryDialog.prototype = Object.create(QueryDialog.prototype);
StrainQueryDialog.prototype.constructor = QueryDialog;
function StrainQueryDialog(fields,callback,database){
    this.database=database;
    this.checks = {}
    QueryDialog.call(this,fields,callback);
    this.div.dialog('option', 'title', 'Search Strains');
    this.mappings = {RJ:"study_accession",
			 RP:"secondary_study_accession",
			 AM:"sample_accession",
			 RS:"secondary_sample_accession",
			 RX:"experiment_accession"};
    var check = $("<input>");
    check.attr('type','checkbox');
    var self =this;
    var allbut  =$('<button>').html("Get All Strains").attr('class','query_button').click(function(e){
	self.processQuery('strains.id>=1')
	self.div.dialog("close");
    });
    var mystrainsbut  =$('<button>').html("Get My Strains").attr('class','query_button').click(function(e){
	self.processQuery('my_strains')
	self.div.dialog("close");
    });
    var latestbut  =$('<button>').html("Get Last 200 Entries").attr('class','querybutton').click(function(e){
	self.processQuery('default')
	self.div.dialog("close");
    });
        
    this.div.append($("<hr>"));
    this.div.append($('<label>Additional Options</label>')).append($('<br>'));
    this._addCheck("no_legacy_data","Ignore Legacy Data",false);
    this._addCheck("only_with_data","Only records with Experimental Data",false);
    this._addCheck("only_editable_data","Only Editable Records",false);
    var firstdiv = $("<div>");
    firstdiv.append($('<label>Predefined Search:</label>')).append('<br>')
    firstdiv.append(allbut).append(mystrainsbut).append(latestbut)
		    .append($("<hr>")).append($('<label>Custom Search:<label>'));
    this.div.prepend(firstdiv);
}
StrainQueryDialog.prototype.processQuery = function(text){
    if ($("#no_legacy_data").prop('checked') && text!=='default'){
    text="("+text+")";
	text+=" AND seq_platform <> '7GeneMLST'"
    }
    var only_with_data='false';
    if ($("#only_with_data").prop('checked')){
	only_with_data="true";
    }
    var only_editable_records='false';
    if ($("#only_editable_data").prop('checked')){
	only_editable_records='true';
    }  
    
    this.callback(text,only_with_data,only_editable_records);
	
};


StrainQueryDialog.prototype.addCheck=function(name,text,default_val){
    var check = $("<input>");
    check.attr('type','checkbox').attr("id",name)
    this.div.append(check).append($('<span>').html(text)).append("<br>");
}





StrainQueryDialog.prototype.processCondition = function(field,op,val,type){
    if (field=="accession"){
	var newField = this.mappings[val.substring(1,3)];
	if (newField){
	    field = newField;	
	}
    }
    return QueryDialog.prototype.processCondition.call(this,field,op,val,type);   
}

StrainQueryDialog.prototype.processRowItems= function(select,typeSelect,value){
    var self = this;
   /* value.autocomplete({source:[],
			minLength:3,
			open: function () {
			    $(".ui-dialog").removeClass("ui-front");
			}
    });
	*/
	
    select.bind('change',function(e){
	value.autocomplete("option","source",function(request,response){
	var to_send = {
	    text:request.term,
	    field:select.val(),
	    limit:15,
	    database:self.database
	}
	Enterobase.call_restful_json('/autocorrect','GET',to_send).done(function(data){
	    response(data);
	});

	});

    });
    
}



    
    
 /**
 * My namespace.
 * @namespace
 */

var Enterobase = {
	/**
	* Creates a div that can query a database allowing
	* the building of complex queries
	* @constructor
	* @param {object} fields - The fields used to construct the
	* query dialog They should be a dictionary of label:[name,datatype]
	* For nested fields the dictionary entry should be a 
	* label:nested dictionary of label:[name,datatype]. 
	*/
	QueryPane:function (fields,name){
	    this.name=name;
		
		//semi-permanant
		this.row_to_group={};
		this.field_to_type={};
		this.rows=1;
		this.groups=0;
		this.operands = {};
		this.field_values={};
		this.values={};
		this.operators={};
		
		this.fields = {};
		//read in the fields
		if (fields){
			for(var label in fields) {
				var dat = fields[label] 
				if (dat instanceof Array){
					this.fields[label]=dat;
					this.field_to_type[dat[0]]=dat[1];
				}
				else{
					for (lab in dat){
						this.fields[lab]=dat[lab]
						this.field_to_type[dat[lab][0]]=dat[lab][1]
					}			
				}
			}
		}
		this.nested_fields=fields
		
		
		var self =  this;
		$(".ui-dialog").css('overflow',"visible");
		this.div = $("<div>");
		this.table = $("<table>").appendTo(this.div).attr("align","center").css("padding-top","10px");
		this.setHeader();
		if (fields){
			this._addRow();
		}
		var clearbutton = $("<button>").html("Clear").bind("click",function(e){
			
			self.clear(true);
			
		
		}).css({
		"float":"right",
		"margin-right":"10px"
		}
		
		);
		
		this.div.append($("<br>")).append(clearbutton);
	},
	
	
	ExperimentQueryPane:function (fields,name,database){
		Enterobase.QueryPane.call(this,null,name);
		this.subFields = {};
		this.database = database;
		this.isSubfield={};
		for (var index in fields){
			
			var field = fields[index];
			if (field['not_searchable']){
			    continue;
			}
			var label = field['label'];
			var name = field['name']
			var datatype= field['datatype'];
			//add subtype
			var  has_alleles= fields[index]["group_name"]=="Locus";
			if (has_alleles){
				label = "Allele";
				name= "Allele";
				datatype ="text";
			}
					
			this.fields[label]=[name,datatype];
			this.field_to_type[name]=datatype;
			
			
		}
		this.nested_fields=this.fields;
		this._addRow();
		
	},
	
	/**
	* Creates a div that can query a strain database allowing
	* the building of complex queries
	* @overides
	* @constructor
	* @param {object} fields - The fields used to construct the
	* query dialog They should be a dictionary of label:[name,datatype]. For nested fields the dictionary entry should be a 
	* label:nested dictionary of label:[name,datatype]
	* @param {string} database - The name of the database e.g. senterica (required for the auto correct)
	* @param {string} name - The name of the query pane
	*/
	StrainQueryPane:function(fields,database,name){
		this.database=database;
		Enterobase.QueryPane.call(this,fields,name);
		this.mappings = {RJ:"study_accession",
				 RP:"secondary_study_accession",
				 AM:"sample_accession",
				 RS:"secondary_sample_accession",
				 RX:"experiment_accession"};
		
	},
	

	/**Determines whether a date is in the format dd/mm/yyyy or mm/yyyy or yyyy
	* @param {string} dateText  - The date text
	* @returns {array} a list of of day,month,year or false if it cannot be parsed 
	*/
	getDateAsList:function(dateText){
		//No date is still valid
		if (dateText===""){
			return ["","","",""]
		}
		var year,month,day = "";
		parts = dateText.split("/");
		
		if (parts.length === 1){
			year = parts[0];				
		}
		if (parts.length === 2){
			year = parts[1];
			month=parts[0];
			
		}
		if (parts.length === 3){
			year = parts[2];
			month = parts[1];
			day = parts[0];
		}
		
		
		return [year,month,day,""];
	},
	
	
    IconMenuBar:function(div,imgFolder,helpPage){
		this.div = $('#'+div);
		this.imgFolder=imgFolder;
		this.helpPage = helpPage+"#";
    },
	
    /**Creates a Scheme Query Dialog
	* @param {list}  schemes -  A list of scheme objects in the following format
	* {name:<name> label:<label>,allele_list:[<allele_1>,<allele_>""]} if the allelem list is absent only STs can be searched
	* @param {function} callback - 
	* @param {string} - The help url
	*/
    SchemeQueryDialog:function (schemes,initial_data,callback,help_url){
	this.help_url = help_url;
	this.schemes=schemes;
	this.callback=callback;
	this.index=0;
	var self =  this;
	this.div = $("<div>");
	var st = null;
	var alleles=null;
	
	
	this.select = $("<select>");
	if (initial_data['st']){
		st = initial_data['st'];
	}
	if (initial_data['alleles']){
	    alleles= initial_data['alleles'];
	}
	
	
	
	for (var index in this.schemes){
	    //wotk out initial index
	    if (this.schemes[index]['name']===initial_data['scheme']){
		this.index=index;
	    }
	    this.select.append($("<option>").text(this.schemes[index]['label']).attr("value",index));
	}
	this.select.on("change",function(e){
	    var scheme = self.schemes[$(this).val()];
	   self.index=index;
	    self.displayScheme(scheme);    
	});
	this.div.append($("<label>Scheme:</label>")).append(this.select);
	
	

	this.div.append($("<br>"));
	this.div.append($("<br>"));
	this.div.append($("<hr>"));
	this.div.append($("<br>"));
	this.form = $('<form>');
	this.div.append(this.form);
	this.table = $("<table>");
	
	
	  
	this.displayScheme(this.schemes[this.index],st,alleles);
	this.select.val(this.index);
	this.div.dialog({
	    autoOpen: true,
	    modal: false,
	    title: "MLST Query",
	    width: 'auto',
	    height:'auto',
	    buttons: {
		"Cancel": function () {
		   
		    $(this).dialog("close");
		},
		 "Submit": function () {
		    if (self.submit()){
			$(this).dialog("close");
		    }
		}
		
	    },
	    
	    
	    close: function () {
		$(this).dialog('destroy').remove();
	    }
	});
	 $('#match-number-spinner').spinner({
		min:0,
		}
	    );
         $(".locus-input-cls").on("focus",function(e){
	    $("#st-allele-radio").trigger("click");
	});
	
	
	if (help_url){
	this.div.parent().find('span.ui-dialog-title').append($("<a>Help</a>").css({"cursor":"pointer","float":"right","display":"inline"}).click(function(e){
				window.open(self.help_url,'newwindow','width=900, height=500');		
	}));
	}
	$("#st-locus-radio").trigger("click");
	    
	
    },
	
	  TextReader:function(file,delimiter,callback){
		var reader = new FileReader();
		reader.onload = function(progressEvent){
			var return_data=[];
			try{
				var data =this.result;
				var lines =  data.split(/\r\n|\r|\n/g);
				var header = lines[0].split(delimiter);
				var header_index={}
				for (var i=0;i<header.length;i++){
					header_index[i]=header[i];

				}

				for (var i=1;i<lines.length;i++){
					var map = {};
					if (!lines[i]){
					    continue;
					}
					 var arr = lines[i].split(delimiter);
					 for (var col in arr){
						map[header_index[col]]=arr[col];
					 }
					 return_data.push(map);
				}

			}catch(error){
				callback("Error",error.message)

			}
			callback("OK",return_data,header_index);
		};
		reader.readAsText(file);
	},

	
	FileBrowser:function(call_back,filter,folders,multiple){
	    if (! filter){
			filter=".txt";
		}
        var wrapper = $('<div/>').css({height:0,width:0,'overflow':'hidden'});
        this.fileInput = $("<input>").attr("type","file").css("display","none");
		if (folders){
			this.fileInput.attr("webkitdirectory","");
		}
		if (multiple){
			this.fileInput.attr("multiple","");
		}
        this.fileInput.attr("accept",filter);
        this.fileInput.wrap(wrapper);
        $('<body>').append(this.fileInput);
        
        this.fileInput.change(function(event){  
            call_back(event.target.files);

        });
    
	},
    
    
    
    
    
    //*********URLs for api *****************
    /**
    *@var {string} jbrowse_url- The base url of the jbrowse server
    */
    jbrowse_url:"http://137.205.123.127/entero_jbrowse",	
    /**
    /**
    *@var {string} wiki_url - The base url of the wikipage
    */
    wiki_url:"http://enterobase.readthedocs.io/en/latest/",
    /**
    *@var {string} strain_autosuggest_url - The url for autosuggesting strain metadata values
    */
    strain_autocorrect_url:"/autocorrect",
    /**@var {string} main_query_url - The url to query both strain and experimental data
    */
    main_query_url:"/get_data_for_experiment",	
    /**@var {string} save_preference_url - The url used to store user preference data 
    */
    save_preference_url:"/save_user_preferences",
    
    nserv_url:"/api/v1.0/call_nserv",
    load_preference_url:"/load_user_preferences",
    validation_url:"/api/v1.0/uploadfields",
    put_url:"/api/v1.0/strains",
    metadata_url:"/api/v1.0/strains",
    assembly_url:"/api/v1.0/assemble",
    cgmlst_url:"/api/v1.0/cgmlst",
    mlst_url:"/api/v1.0/mlst",
    stapi_url:"/api/v1.0/stapi",
    deleteURL:"/auth/delete_strains",
    downloadURL:function downloadURL(url) {
		//var hiddenIFrameID = 'hiddenDownloader' + count++;
		var iframe = document.createElement('iframe');
		//iframe.id = hiddenIFrameID;
		iframe.style.display = 'none';
		document.body.appendChild(iframe);
		iframe.src = url;
    },
    
         
 
    //call recieving data in json format
    call_restful_json:function (uri, method,sendData,callback,username,password){
        console.log(uri);
        var request = {
            url: uri,
            type: method,     
            dataType: "json",  //type of data recieved
            //contentType:'text/html',//application/json;charset=UTF-8', //type of data sent        
            data:sendData,      
	    timeout:900000,
	    cache:false,
            beforeSend: function (xhr) {
               //console.log(xhr.responseText);
                if (password){
                    xhr.setRequestHeader("Authorization", 
                    "Basic " + btoa(username + ":" +password));
                }
            },
            error: function(jqXHR,text,body) {
                console.log("ajax error" + jqXHR.responseText);       
                callback(jqXHR.responseText);
            } 
        };
        var a = $.ajax(request);   
        return a;
    },
    
     /*
     Calls the api with coventional data (not jsonified) and receives a standard response
     uri - The address of the api call
     method - 'PUT','GET' etc.
     sendData -  the data to send dictionary 
     callback - The function to call if there is an error the function will receive a message with
		information on the errror
     username,password - not required - but if present will be used for authentication
    */     
     call_restful:function (uri, method,sendData,callback,username,password){
        console.log(uri);
        var request = {
            url: uri,
            type: method,         
            data:sendData,
            beforeSend: function (xhr) {
               //console.log(xhr.responseText);
                if (password){
                    xhr.setRequestHeader("Authorization", 
                    "Basic " + btoa(username + ":" +password));
                }
            },
            error: function(jqXHR,text,body){    
                console.log("ajax error" + jqXHR.responseText);
                callback(jqXHR.responseText);
            }   
        };
        var a = $.ajax(request);
        return a;
    },
        
    call_restful_send_json:function (uri, method,sendData,callback,username,password){
        console.log(uri);
        var request = {
            url: uri,
            type: method,         
            data:JSON.stringify(sendData),
	    contentType:"application/json; charset=UTF-8",
            beforeSend: function (xhr) {
               //console.log(xhr.responseText);
                if (password){
                    xhr.setRequestHeader("Authorization", 
                    "Basic " + btoa(username + ":" +password));
                }
            },
            error: function(jqXHR,text,body){    
                console.log("ajax error" + jqXHR.responseText);
                callback(jqXHR.responseText);
            } 
        };
        var a = $.ajax(request);
        return a;
    },
    
    
    
    call_restful_crossdomain:function (uri, method,sendData,username,password){
        console.log(JSON.stringify(sendData));
        if (!sendData){
            sendData="";
        }
        var request = {
            url: uri,
            type: method,
          
            //type:"jsonp",
            //contentType: 'application/json;charset=UTF-8', //needed for json
            accepts: "application/json",
            cache: false,
            dataType: "json",
            crossOrigin: true,
            
            data:JSON.stringify(sendData),
            
          
               
           beforeSend: function (xhr) {
                if (password){
                    xhr.setRequestHeader("Authorization", 
                    "Basic " + btoa(username + ":" +password));
                }
            },
            
            error: function(jqXHR,text) {
                var p = jqXHR.error();
                console.log("ajax error " + jqXHR);
            }    
        };
    return $.ajax(request);
    },
            
    endsWith:function(str,suffix_array){
	for (index in suffix_array){
	    var suffix = suffix_array[index]
	    if (str.substring(str.length - suffix.length, str.length ) === suffix){
	    return true;
	    }
	}
	return false;
    
    
    },
    
    makeCustomFileInput:function(id,call_back){
        var   element = $("#"+id);
        var wrapper = $('<div/>').css({height:0,width:0,'overflow':'hidden'});
        var fileInput = $("<input>").attr("type","file").css("display","none");
        fileInput.attr("accept",".txt");
        fileInput.wrap(wrapper);
        $('<body>').append(fileInput);
        //fileInput.insertAfter(element);
        element.click(function(){
            fileInput.val("");
            fileInput.click();
            
        }).show();
        fileInput.change(function(event){  
            call_back(event.target.files[0]);

        });
        return fileInput;
    },
  
    
   
    
    
    
    
    /*save file dailog allowing user to input name
    in chrome the user does not get the option and file names are 
    very long and cryptic
    text- the text to save
    */
    showSaveDialog:function(text,downloadable){
        //button for the file save
        var save = $("<a download><button>Save</button></a>").click(function(){
            var name=$("#filename").val();
            if(!name){
            return;
            }
	    
	    if(downloadable==false){
		var d = new Date();
		//name=name+"_problems_"+d.getTime();
		name=name+"_problems";
	    }
	    
            //windows - don't actuallu use the file download anchor
            if(window.navigator.msSaveOrOpenBlob) {
                var data = new Blob([text], {type: 'text/plain'});   
                window.navigator.msSaveBlob(data,name);
            }
            //others
            else{
                var data = new Blob([text], {type: 'text/plain'});
               
                this.textFile = window.URL.createObjectURL(data);
                //set file name and contents before click event propogates
                $(this).attr("download",name);
                $(this).attr("href",this.textFile);
    
            }
            $(".ui-dialog-content").dialog().dialog("close");
    
        });       
        //the actual dailog box    
        $("<div id ='savedailog'></div>")
        .html("File Name: <input type='text' id ='filename'>")
        .dialog({
            autoOpen: true,
            modal: true,
            title: "Save File",
            buttons: {
                "Cancel": function () {
                   
                    $(this).dialog("close");
                }
            },
            close: function () {
                $(this).dialog('destroy').remove();
            }
        }).append(save);
    
    },


    showSaveDialog2:function(text1,text2){
	//text1 is the downloadable data
	//test2 is the not downloadable data
	var d = new Date();
	//d.getTime();
	
        //button for the file save
        var save = $("<a download><button>Save</button></a>").click(async function(){
            var name1=$("#filename").val();
	    //var name2=name1+"_problems_"+d.getTime();
	    var name2=name1+"_problems";
	    
            if(!name1){
		return;
            }          

	    $(".ui-dialog-content").dialog().dialog("close");
	    download(name1,text1);
	    await sleepSave(2000);
	    download(name2,text2);	    
	    
	    
	    function download(filename, text) {
		var element = document.createElement('a');
		element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
		element.setAttribute('download', filename);
		element.setAttribute('id','downloadTwoFile');
		
		element.style.display = 'none';
		document.body.appendChild(element);
    
		element.click();
		//$( "#downloadTwoFile" ).trigger( "click" );
		
		document.body.removeChild(element);
	    }   
	

    
        });       
        //the actual dailog box    
        $("<div id ='savedailog'></div>")
        .html("File Name: <input type='text' id ='filename'>")
        .dialog({
            autoOpen: true,
            modal: true,
            title: "Save File",
            buttons: {
                "Cancel": function () {
                   
                    $(this).dialog("close");
                }
            },
            close: function () {
                $(this).dialog('destroy').remove();
            }
        }).append(save);
    
    },
 
     
    makeCustomFileSave:function(id_download,call_back){
        
        this.textFile = null; 
        var download =$("#"+id_download);
        
        download.click(function(e){
            //get the file name
         
            
                var name=prompt("Enter File name");
                if(!name){
                    return;
                }
                download.attr("download",name);
                //windows
                if(window.navigator.msSaveOrOpenBlob) {
                    var data = new Blob([call_back()], {type: 'text/plain'});   
                    window.navigator.msSaveBlob(data);
                }
                //others
                else{
                    var data = new Blob([call_back()], {type: 'text/plain'});
                    if (this.textFile !== null) {
                        window.URL.revokeObjectURL(this.textFile);
                    }        
                    this.textFile = window.URL.createObjectURL(data);   
                    download.attr("href",this.textFile);
                }
                  
                   
                   
                
            
            });       
       
       
    },
    //a modal box 
    modalInput:function (question,title) {
        var defer = $.Deferred();
        $("<div></div>")
            .html(question+"<input type='text' id ='answer'>")
            .dialog({
                autoOpen: true,
                modal: true,
                title: title,
                buttons: {
                    "Submit": function () {
                        defer.resolve($("#answer").val());
                        $(this).dialog("close");
                    },
                    "Cancel": function () {
                        defer.resolve("");
                        $(this).dialog("close");
                    }
                },
                close: function () {
                    $(this).dialog('destroy').remove();
                }
            });
            return defer.promise();
    },
    
      /**
     * Displays a modal popup for OK or Cancel
     * @param {string} title - The text to be displyed
     * @param {string } text - The text to be dispalyed
     * @param {function} callback - The function called when the user makes a choice, should accept 
     * a single boolean value true or false
     * @param {Array} button_texts - An array of two strings with the button text (default is ['OK','Cancel'])
     */   
    oKorCancelDialog:function (title,text,callback,button_text) {
        if (!button_text){
	    button_text= ['OK','Cancel'];
	}
        $("<div></div>")
            .html(text)
            .dialog({
                autoOpen: true,
                modal: true,
                title: title,
                buttons: [
                   {
		   click: function () {
			    callback(true);
			    $(this).dialog("close");
			},
			text:button_text[0]
		    },
		    {
                    click: function () {
			    callback(false);
			    $(this).dialog("close");
			},
			text:button_text[1]
		    }
		],
                close: function () {
                    $(this).dialog('destroy').remove();
                }
            });
            
    },
    
     /**
     * Displays a modal popup which disables the web page when a lengthy operation is in progress
     * @param {string} message - The text to be displyed
     * @param {function } cancelFunction - A function, which should clean up any loose ends if the user 
	 * cancels the process
     * @param {string} img - The path to the waiting image to be displayed. If no string is provided, the
	 * dialog will show a progress bar
     */   
    ModalWaitingDialog:function (message,cancelFunction,img){
	
	
	var self = this;
	this.div = $("<div>");
	
	this.messageDiv=$("<div>").html(message);
	this.progressBar=null;
	if (img==='default'){
	    img = "/static/img/upload/queue.gif";
	}
	
	this.loadingImage = img;
	this.cancelFunction = cancelFunction;
	this.div.dialog({
	    autoOpen: true,
	    modal: true,
	    title: "Waiting",
	    width: 'auto',
	    height:'auto',
	    buttons:{
		"Cancel": function () {
		    if (self.cancelFunction){
			self.cancelFunction();
			
		    }
			self.close();
		    
		}
	    },
	    	    
	    close: function () {
		$(this).dialog('destroy').remove();
	    }
	});
	this.div.append(this.messageDiv);
	if (img){
	    this.loadingImage = $("<img align = 'middle' src = '"+img+"'><img>");
	    this.div.append($("<p style = 'text-align:center'></p>").append(this.loadingImage));
	}
	else{
	    this.progressBar = $("<div>").progressbar({value:0});
	    this.div.append(this.progressBar);
	}
	
	    
	
    },
    
    decodeBarcode:function(barcode){
	var code = barcode.split("_")[1];
	var prefix = code.substring(0,2);
	var suffix = code.substring(6,code.length);
	var charstring = prefix+suffix;
	var int_string =0;
	for (var idx=0;idx<charstring.length;idx++){
		int_string+= Math.pow(26,idx) * (charstring.charCodeAt(idx)-65)
	}
	int_string = int_string*10000
	return int_string + parseInt(barcode.substring(6,10));
    },
    
    encodeBarcode:function(input_number,db_code){
	input_number= parseInt(input_number);
	var int_val = 10000;
	var number = parseInt(input_number / int_val);
	var final_char = '';
	while (number > 0){
		final_char = final_char  + String.fromCharCode(number % 26 + 65);
		number = parseInt(number / 26)
	}
	padding = 'AAAA';
	pad_len= 4- final_char.length;
	final_char+= padding.substring(0,pad_len);
	var num=  (input_number % int_val)+"";
	var num_length = 4-num.length;
	padding ="0000";
	num = padding.substring(0,num_length)+num;
	
	
	return db_code + '_' + final_char.substring(0,2) + num + final_char.substring(2,final_char.length) ;
    
    },
    
    LocusSuggest:function(element_id,database,scheme,callback){
	this.database =database;
	this.scheme=scheme;
	var self= this
	this.autocomplete = $("#"+element_id).autocomplete({
	    minLength:2,
	    source:function(request,response){
		var to_send= {
		    text:request.term,
		    scheme:self.scheme,
		    database:self.database
		
		}
		Enterobase.call_restful_json('/autocomplete_locus','GET',to_send).done(function(data){
		    response(data);
	});

		
	    
	    },
	    select:function(event,ui){
		    if (callback){
			 callback(ui.item.value,ui.item.label);
			}
	    }
	    
	
	})
    
    },
    
      /**
     * Creates a suggest input which returns the user id as  value  and  first name second name (user name)
     * as label. It queries both first and second name 
     * @param {string} element_id - The id of text input used for the suggest
     * @param {function} callback - called when the user is selected with the value and label
     */   
    UserNameSuggest:function(element_id,callback){
	
	var self= this
	this.autocomplete = $("#"+element_id).autocomplete({
	    minLength:2,
	    source:function(request,response){
		Enterobase.call_restful_json('/autocomplete_user_name?text='+request.term,'GET',"").done(function(data){
		    response(data);
	});

	    },
	    select:function(event,ui){
		    if (callback){
			self.autocomplete.val(ui.item.label);
			event.preventDefault();
			 callback(ui.item.value,ui.item.label);
		    }
	    }
	    
	
	})
    
    },
    
    
    
    /**
     * Displays a popup box containing a message
     * @param {string} text - The text to be displyed (can contain html mark up)
     * @param {string} title - The title displayed in the dailog box - if none supplied,
     * default is Warning
     * @param {boolean} modal - If true then the popup will be modal i.e. the user
     * will need to close the dialog before proceeding. Default is false
     */
    modalAlert:function (text,title,modal,callback) {
        if (!title){
	    title="Warning";
	}
        var alert= $("<div></div>")
            .html(text)
            .dialog({
                autoOpen: true,
                modal: modal,
                title: title,
                maxHeight:500,
                buttons: {
                   
                    "OK": function () {
                        
                        $(this).dialog("close");
			if (callback){
			    callback();
			}
                    }
                },
                close: function () {
                    $(this).dialog('destroy').remove();
                }
            });
			
			return alert;
			
        },
	
    modalAlert2:function (text,title,modal,weight,cancleButtonFlag,callback) {
	var alert;
	
	if (!title){
	    title="Warning";
	}
	
	if(cancleButtonFlag){
	    var alert= $("<div></div>")
	    .html(text)
	    .dialog({
		autoOpen: true,
		modal: modal,
		title: title,
		width: weight,
		height:'auto',
		maxHeight:500,
		weight:weight,
		buttons: {
		    "Cancle": function () {
			$(this).dialog('destroy').remove();
		    },
		    "OK": function () {			
			$(this).dialog("close");
			if (callback){
			    callback();
			}
		    }

		},
		close: function () {
		    $(this).dialog('destroy').remove();
		}
	    });	
	}else{
	    var alert= $("<div></div>")
	    .html(text)
	    .dialog({
		autoOpen: true,
		modal: modal,
		title: title,
		width: weight,
		height:'auto',
		maxHeight:500,
		weight:weight,
		buttons: {
		    "OK": function () {			
			$(this).dialog("close");
			if (callback){
			    callback();
			}
		    }

		},
		close: function () {
		    $(this).dialog('destroy').remove();
		}
	    });	
	}

			
	return alert;
			
    },
    
    getDateAsString:function(date,difference){
		 var otherdate = new Date(date);
		 otherdate.setDate(date.getDate() +difference);
		 var dd = otherdate.getDate();
		var mm = otherdate.getMonth()+1; //January is 0!
		var yyyy = otherdate.getFullYear();
		if(dd<10){dd='0'+dd;} 
		if(mm<10){mm='0'+mm;}
		return yyyy+"-"+mm+"-"+dd;
	
	},
    //parses the given string and returns a list containing
    //year,month,day and time
    //if any of the above are not present an empty string will be present
    parseDate:function(dateString){
        var date =new Date(dateString);

	var year = date.getFullYear()
        var month = date.getMonth()+1
        var day  = date.getDate()
	var hour = date.getHours();
	var min =date.getMinutes();
	var rowID = this.getRowId(r);
	list=[]
	
        if (!isNaN(year)){
	    list.push(year);
	    this.setValueAt(r,colIndex,year,null,false);
	}
	else{
	    list.push("");
	    year ="";
	}
	if (!isNaN(month) && !isNaN(day)){
	    if (month===1 && day ===1){
		list.push("")
	    }
	    else{
		list.push(month);
		list.push(day);
	    }
	}
	else{
	    list.push("");
	}
	    
	if (!isNaN(hour) && !isNaN(min)){
	    if (hour ===0 && min ===0){
		list.push("");
	    }
	    else{
		hour = hour.toString();
		min = min.toString();
		if (hour.length ===1){
		    hour="0"+hour
		} 
		if (min.length ===1){
		    min="0"+min
		}	    
		list.push(hour+":"+min);
	    }
	}
	else{
	    list.push("");
	}			
	
	return list;
    
    
    
    }
    
    

    
};
    
Enterobase.IconMenuBar.prototype = Object.create(null);
Enterobase.IconMenuBar.prototype.addIcon =  
    function (name,imgName,tooltip,func){
	var self = this;
	var src = this.imgFolder+"/"+imgName;
	var icon = $('<img>').css('opacity','.7').attr('title',tooltip)
	.hover(
	    function() {$(this).fadeTo('fast',1);},
	    function() {$(this).fadeTo('fast',.7);}
	)
	.attr("src",src)
	.css({'cursor':'pointer','padd-left':'2px','padding-right':'2px'})
	.click(function(e){
	    func();
	})
	.data("name",name);
	
	    var menu = 
	    [
		{
		    name: 'Help',
		    title: 'Help',
	
		    fun: function () {
			window.open(self.helpPage+name,'newwindow','width=900, height=500');
		    }
		}
	    ];
	icon.contextMenu(menu,{triggerOn:'contextmenu'});
	    
	
	this.div.append(icon);
	
	    
};
    
   

	
Enterobase.SchemeQueryDialog.prototype = Object.create(null);

Enterobase.SchemeQueryDialog.prototype.displayScheme= function(scheme,st,alleles){
	this.form.empty();
	this.table.empty();
	this.radioAllele = $("<input type='radio' name='st_allele' value='allele'>").attr("id","st-allele-radio").on("click",function(e){
		$(".locus-input-cls").css("background-color","white");
		$("#st-input").css("background-color","lightgray");
	});
	
	if (scheme['allele_list']){
	    this.form.append(this.radioAllele).append($('<label>Search On Allele</label>'))
	    var labelRow = $("<tr>");
	    var inputRow= $("<tr>");
	    for (index in scheme['allele_list']){
		var locus= scheme['allele_list'][index];
		var all_val = "";
		if (alleles){
			all_val= alleles[index]
			if (isNaN(all_val)){
			    all_val="";			
			}
		}
		labelRow.append('<td>'+locus+'</td>');
		inputRow.append($('<td>').append($('<input>').attr({'id':'locus-input-'+locus,"class":"locus-input-cls"}).width(50).val(all_val)));
	    }
	    this.table.append(labelRow).append(inputRow);
	    this.form.append(this.table);
	    this.form.append($("<br>"));
	    this.form.append($("<hr>"));
	    this.form.append($("<br>"));
	}
	
	this.radioST = $("<input type='radio' name='st_allele' value='st'>").attr("id","st-locus-radio").on("click",function(e){
	    $(".locus-input-cls").css("background-color","lightgray");
	    $("#st-input").css("background-color","white");
	
	});
	this.form.append(this.radioST).append($('<label>Search On ST</label>')).append($('<br>'));
	var st_val = st?st:""
	this.form.append('<label>ST:</label>').append($('<input>').attr('id',"st-input")
													    .width(50)
													    .val(st_val)
													    .on("focus",function(e){
														    $("#st-locus-radio").trigger("click");
													     })
											);
	this.form.append($('<label>Max Number MisMatches:</label>').css('margin-left','20px'))
	var spin =  $('<input>').attr('id','match-number-spinner').attr("type","text").attr("value",0).width(40)
	                                     
	this.form.append(spin); 
	$(".locus-input-cls").on("focus",function(e){
	    $("#st-allele-radio").trigger("click");
	});
	$("#st-locus-radio").trigger("click");
   
   
}
Enterobase.SchemeQueryDialog.prototype.submit= function(){
    var ret_object = {scheme:this.schemes[this.index]['name']}
    if ((this.radioAllele).is(":checked")){
	 var good =false;
	 var vals =[];
	 count = 0 
	 for (i in this.schemes[this.index]['allele_list']){
	    var locus= this.schemes[this.index]['allele_list'][i];
	    var allele = $('#locus-input-'+locus).val()
	    if (isNaN(allele)){
		allele="";
	    }
	    if (count <= 100){
		vals.push(allele)
		count += 1 
	    }
	}
	ret_object['data']= "alleles:"+vals.join(",");
	this.callback(ret_object);
	
    }
    else{
	var st_val = $("#st-input").val();
	if (isNaN(st_val)){
	    return false;
	}
	var diff = $("#match-number-spinner").val();
	ret_object['data']="st:"+st_val+":"+diff;
	this.callback(ret_object);
	
    }
    return true;
    

}
Enterobase.FileBrowser.prototype.getFiles = function(){
			this.fileInput.val("");
			this.fileInput.click();		
}
	
	
var getFileBlob = function (url, cb) {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", url);
		xhr.responseType = "blob";
		xhr.addEventListener('load', function() {
			cb(xhr.response);
		});
		xhr.send();
};

var blobToFile = function (blob, name) {
		blob.lastModifiedDate = new Date();
		blob.name = name;
		return blob;
};

var getFileObject = function(filePathOrUrl, cb) {
	   getFileBlob(filePathOrUrl, function (blob) {
		  cb(blobToFile(blob, 'test.jpg'));
	   });
};

/**********/
Enterobase.QueryPane.prototype = Object.create(null);
/**
* Processes the current data in the form amd returns 
* a string containing the query
* @returns {string} The text version of the query
*/

Enterobase.QueryPane.prototype.getQuery= function(){
		var text=""
		var in_group = false;
		var current_group = 0;
		for (var n=1;n < this.rows;n++){
			if (this.row_to_group[n] && !in_group){
			text+="(";
			in_group= true;
			current_group= this.row_to_group[n];
		}
		else if (this.row_to_group[n] != current_group && current_group !==0){
			text+="(";
			in_group = true;
			current_group=this.row_to_group[n];
		}
		
		var field = this.field_values[n];
		if (!field){
			return "";
		}
		var op = this.operators[n];
		var val = this.values[n];
		if (!val && val !==0){
			return;
		}
		//get field type
		
		var type = this.field_to_type[field];
		condition = this.processCondition(field,op,val,type,n)
		if (! condition){
			return;		
		}
		text+=condition
		if (in_group && this.row_to_group[n+1] != current_group){
			text+=")";
			in_group = false;
			current_group=0;
		}
		if (n!== this.rows-1){
			text +=" "+this.operands[n]+" "
		}
		
		}
		console.log(text);	
		return this.processQuery(text);
		
};
/**
* This method is to be overidden in subclasses which require
* additional processing of the query text
* @param {string} text- The query text to be altered
* @returns {string} The modified version of the query
*/
Enterobase.QueryPane.prototype.processQuery = function(text){
    return text
};

/**
* Gets the <div> housing the GUI
* @returns {jquery object} The div object
*/
Enterobase.QueryPane.prototype.getPane = function(){
	return this.div
};

	
Enterobase.QueryPane.prototype._addRow = function(fields,div){
	var self = this;
	var rowNum = this.rows
	var row =$("<tr>").attr("id",this.name+"_row_"+this.rows);
	var select = $("<select>").attr("id",this.name+"_field_"+this.rows).data("row",rowNum)
	select.append($("<option></option>")
			.attr("value","")
			.text(""));
		
	for(var label in this.nested_fields) {
	
	
		
		var dat = this.nested_fields[label] 
		if (dat instanceof Array){
			select.append($("<option></option>")
			.attr("value",dat[0])
			.text(label));
		}
		else{
			var o = $('<optgroup>').attr("label",label);
			for (var lab in dat){
				o.append($("<option></option>")
				.attr("value",dat[lab][0])
				.text(lab));
			}
			select.append(o);
		}
	}
	    
	select.bind("change",function(e){
		var row = $(this).data("row");
		self.field_values[row]= $(this).val();
	
	
	});   
	var value =$("<input>").attr("type","text").data("row",this.rows).attr("id",this.name+"_value_"+this.rows);
	value.bind("click",function(e){
		var row = $(this).data("row");
		var op = $("#"+self.name+"_operator_"+row).val()
		if (op === "IN"){
			self.textListPopup(row);
		
		}
	
	}).blur(function(e){
		var row = $(this).data("row");
		var op = $("#"+self.name+"_operator_"+row).val()
		if (op === "IN"){
			return;		
		}
		self.values[row] = $(this).val();
		
	
	
	});
	
	var typeSelect= $("<select>").attr("id",this.name+"_operator_"+this.rows).data("row",this.rows);
	typeSelect.append($("<option>").attr("value","ILIKE").text("contains"));
	typeSelect.append($("<option>").attr("value","IN").text("in"));
	typeSelect.append($("<option>").attr("value","NOT LIKE").text("not contains"));
	typeSelect.append($("<option>").attr("value","=").text("equals"));
	typeSelect.append($("<option>").attr("value","<>").text("not equals"));
	typeSelect.append($("<option>").attr("value",">").text(">"));
	typeSelect.append($("<option>").attr("value","<").text("<"));
	self.operators[this.rows]="LIKE";
	typeSelect.bind("change",function(e){
		var row = $(this).data("row");
		self.operators[row]=$(this).val();
	
	});
	   
	var andbut =$("<button>").html("AND").attr("id",this.name+"_andbut_"+rowNum).data("row",rowNum).click(function(){
		var row = $(this).data("row")
		self.operands[row] = "AND"
		$("#"+self.name+"_orbut_"+row).hide()
		$(this).attr("disabled","disabled");
		self._addRow();
	});
	var orbut =$("<button>").html("OR").attr("id",this.name+"_orbut_"+rowNum).data("row",rowNum).click(function(){
		var row = $(this).data("row")
		self.operands[row] = "OR"
		$("#"+self.name+"_andbut_"+row).hide()
		$(this).attr("disabled","disabled");
		self._addRow();
	});
	
	var groupbut = $("<button>").data('num',rowNum)
	.html("GRP").attr("id",this.name+"_grp-button_"+rowNum).click(function(){
	var grp_list=[]
	for (var i=1;i<=$(this).data('num');i++){
		if (!self.row_to_group[i]){
		grp_list.push(i)
		}
	}
	if (grp_list.length>1){
		$('#'+self.name+'_left-bracket_'+grp_list[0]).html("<b>(</b>");
		$('#'+self.name+'_right-bracket_'+grp_list[grp_list.length-1]).html("<b>)</b>");
		self.groups++;
		for (var i in grp_list){
		self.row_to_group[grp_list[i]] = self.groups;
		$("#"+self.name+"_grp-button_"+grp_list[i]).hide();
		}
		
		
	}
	
	
	});
	row.append($("<td></td>").attr('id',this.name+'_left-bracket_'+rowNum)).append($("<td>").append(select))
			  .append($("<td>").append(typeSelect))
			  .append($("<td>").append(value)).append($("<td></td>").attr('id',this.name+'_right-bracket_'+rowNum))
			  
	if ( rowNum>1 && !this.row_to_group[rowNum-1]){
		row.append(groupbut);
	}
	if (rowNum>1){
		var r=rowNum-1;
		$("#"+this.name+"_rmvbut_"+r).hide();
	}
	if (rowNum !==1){
		var removeBut = $("<button>RMV</button>").data("row",rowNum).attr("id",this.name+"_rmvbut_"+rowNum).click(function(e){
			var rownum = $(this).data("row");
			$("#"+self.name+"_row_"+rownum).remove();
			$("#"+self.name+"_andor_"+rownum).remove();
			var group = self.row_to_group[rownum];
			if (group){
				for (var n=1;n<=rowNum;n++){
					if (self.row_to_group[n]===group){
						$('#'+self.name+'_left-bracket_'+n).html("");
						$("#"+self.name+"_grp-button_"+n).show()
						delete self.row_to_group[n];
						
					}
				
				}
				self.groups--;
			}
			rownum--;
			$("#"+self.name+"_andbut_"+rownum).removeAttr("disabled").show();
			$("#"+self.name+"_orbut_"+rownum).removeAttr("disabled").show();
			$("#"+self.name+"_rmvbut_"+rownum).show();
			self.rows--;
			
		});
		row.append(removeBut);
	}
	
	
	
	this.table.append(row);
	row = $('<tr>').css('text-align','center').attr("id",this.name+"_andor_"+rowNum);
	row.append("<td>").append("<td>").append($('<td>').append(andbut).append(orbut));
	
	this.table.append(row);
	
	this.processRowItems(select,typeSelect,value,rowNum);
	select.val("");
	this.rows++;
		
	//select.trigger('change');
	    
};

Enterobase.QueryPane.prototype.clear= function(addFirstRow){
	this.row_to_group={};
	this.rows=1;
	this.groups=0;
	this.operands = {};
	this.field_values={};
	this.values={};
	this.operators={};
	this.table.empty();
	this.setHeader();
	if (addFirstRow){
		this._addRow();
	}

}

Enterobase.QueryPane.prototype.save =function(){
	return {
		row_to_group:this.row_to_group,
		rows:this.rows,
		groups:this.groups,
		operands:this.operands,
		field_values:this.field_values,
		values:this.values,
		operators:this.operators
	}

}





Enterobase.QueryPane.prototype.load =function(object){
	
	
	for (var n=1;n<object.rows;n++){
		this._addRow();
		this.getElement("field",n).val(object.field_values[n]);
		this.getElement("operator",n).val(object.operators[n]);
		this.getElement("value",n).val(object.values[n]);
		if (n !== object.rows-1){
			if (object.operands[n]==="AND"){
				this.getElement("andbut",n).attr("disabled","disabled");
				this.getElement("orbut",n).hide();
			}
			else{
				this.getElement("orbut",n).attr("disabled","disabled");
				this.getElement("andbut",n).hide();
			
			}
			this.getElement("_rmbut_",n).hide();
		}
		
	}
	var breakOuter =false;
	for (var group=1;group<=object.groups;group++){
		var found=false;
		for (var row =1;row<object.rows;row++){
			if (object.row_to_group[row] == group){
				this.getElement("grp-button",row).hide();
				if (!found){
					this.getElement("left-bracket",row).html("<b>(</b>");
					found=true;
				}
				else{
					if (object.row_to_group[row+1]!==group){
						this.getElement("right-bracket",row).html("<b>)</b>");
						breakOuter=true;				
					}
				}
			}
			if (breakOuter){
				break;			
			}						
		}
	}
	
	this.row_to_group = object.row_to_group;
	this.rows = object.rows;
	this.groups =object.groups;
	this.operands = object.operands;
	this.field_values = object.field_values;
	this.values =object.values;
	this.operators =object.operators;
		
}	
	



Enterobase.QueryPane.prototype.getElement=function(type,n){
	var el = $("#"+this.name+"_"+type+"_"+n);
	return el;
}

Enterobase.QueryPane.prototype.setHeader= function(){
	var th = $("<tr>").css({
		"padding-top":"3px",
		"padding-botton":"3px",
		"font-weight":"bold",
		"text-align":"left"
	}).appendTo(this.table);
	th.append($("<td></td><td>Field</td><td>Operator</td><td>Value</td>"));

}




Enterobase.QueryPane.prototype.textListPopup=function(row){
	var self = this;
	var div = $("<div>");
	var textbox = $("<textarea></textarea>").width(200).height(200);
	if (self.values[row]){
		textbox.val(self.values[row]);
	}
    var comma = $("<input type='radio' name='list-delimiter' value=','>");
	var whitespace = $("<input type='radio' name='list-delimiter' value='w'>").attr("checked","checked");
	
	div.append(textbox);
	div.append($("<br><label>Delimiter:</label><br>"));
	div.append(comma).append($("<label>Comma</label>")).append(whitespace).append($("<label>WhiteSpace</label>"))
	div.dialog({
		autoOpen: true,
		modal: false,
		title: "Please Enter A list of Values",
		width: 'auto',
		height:'auto',
		buttons: {
		"Cancel": function () {   
			$(this).dialog("close");
		},
		 "Submit": function () {
		    var list = textbox.val();
			
			if (whitespace.is(":checked")){
			
				list =textbox.val().trim().replace(/\s+/g,",");
			}
			self.values[row]=list;
			$("#"+self.name+"_value_"+row).val(list.substring(0,5)+"....");
			$(this).dialog("close");
			
		}
		
		},
		
		
		close: function () {
		$(this).dialog('destroy').remove();
		}
	});
}
	 





/**
* Processes the condition and returns SQL text e.g. field LIKE '%sds%'
* @param {string} field- The name of the field
* @param {string} op- The operand e.g. LIKE,=,> etc
* @param {string} val- The query value
* @param {string} type- The query value type e.g. double,integer,text
* @returns {string} The SQL text
*/

Enterobase.QueryPane.prototype.processCondition= function(field,op,val,type,rowNumber){
    
    if ((type === 'integer' || type === "double") && isNaN(val) && op !== "IN"){
	return null;
    }
	if (type === "date"){
		
		if (op == "LIKE" || op == "NOT LIKE"){
			op="=";	
		}
		if (op === "="){
			var date_splits = ["year","month","day"]
			var date_format = val.split("-");
			var text ="("
			for (index in date_format){
				text=text+"EXTRACT ("+date_splits[index]+" FROM "+field+")="+date_format[index]
				if (parseInt(index) !==date_format.length-1){
				text=text+" AND "
				}
				
			}
			text=text+")";
			return text;
		}
		var date  = new Date(val);
		val = $.datepicker.formatDate("yy-mm-dd", date);
		if (val == "NaN-NaN-Nan"){
			return null;
		}		
		
	}
    if ((op==='LIKE' || op === 'NOT LIKE' ) && (type==='integer' || type ==='double') ){
		if (op ==='LIKE'){
			op='=';
	}
	else{
	    op='<>';
	}
    }
	
    if (op ==='LIKE' || op === "NOT LIKE"){
       val=" '%"+val+"%'";
       op=" "+op+" ";
    }
	else if (op === 'IN'){
		op = " IN ";
		val = val.replace(/,/g,"','");
		val = "('"+val+"')"
	
	}
    else{
	val="'"+val+"'";
    }
    
    var condition = field+op+val;
    
    return condition;
    
}





/**
* This method is to be overidden if extra items need to be added
* to the controls for selecting fields,operands and values
* e.g. adding extra operands, cheking legitimate query values 
* @param {jquery object} fieldSelect - The select used to choose the field
* @param {jquery object} operandSelect The select used to choose the operand
* @param {jquery object} valueText - The text input used to select the value of the query
*/
Enterobase.QueryPane.prototype.processRowItems= function(select,typeSelect,value){
	
};
	

Enterobase.StrainQueryPane.prototype = Object.create(Enterobase.QueryPane.prototype);
Enterobase.StrainQueryPane.prototype.constructor = Enterobase.QueryPane;



/**
* Ensures acccession will query any accession number not just run accession
* Also ensures barcode is strains.barcode to stop amniguity
* @overide
*/
Enterobase.StrainQueryPane.prototype.processCondition = function(field,op,val,type){
	//change accession to correct type 
    if (field==="accession"){
		var newField = this.mappings[val.substring(1,3)];
		if (newField){
			field = newField;	
		}
	}
	if (field==="barcode"){
		field="strains.barcode";
	}
	if (field === "created"){
		field = "strains.created";
	}
	
	//call super method
    return Enterobase.QueryPane.prototype.processCondition.call(this,field,op,val,type);   
}

/**
* Adds autocorrect feature to the values input text
* @overide
*/
Enterobase.StrainQueryPane.prototype.processRowItems= function(select,typeSelect,value){
    var self = this;
    /*value.autocomplete({source:[],
			minLength:3,
			open: function () {
			    $(".ui-dialog").removeClass("ui-front");
			}
    });
	*/

    select.bind('change',function(e){
		var type = self.field_to_type[$(this).val()];
		if (type !== 'date' && type !== 'integer'){
			value.autocomplete({
				source:function(request,response){
					var to_send = {
						text:request.term,
						field:select.val(),
						limit:15,
						database:self.database
					}
					Enterobase.call_restful_json(Enterobase.strain_autocorrect_url,'GET',to_send).done(function(data){
						response(data);
					});
				},
				minLength:3,
				open: function () {
					$(".ui-dialog").removeClass("ui-front");
				}
			});
		}
 
	});
}
Enterobase.StrainQueryPane.prototype.load =function(object){
	//call base method
	Enterobase.QueryPane.prototype.load.call(this,object);
	//trigger the value inputs for autoselect
	for (var n=1;n<object.rows;n++){
		this.getElement("field",n).trigger("change")
	}
}



Enterobase.ExperimentQueryPane.prototype = Object.create(Enterobase.QueryPane.prototype);
Enterobase.ExperimentQueryPane.prototype.constructor = Enterobase.QueryPane;




/**
* Adds the details (sub field capability to the row)
* @overide 
*/

Enterobase.ExperimentQueryPane.prototype.processRowItems= function(select,typeSelect,value,rowNumber){
	//create the subfield select
	var id  = this.name+"_subfield_"+rowNumber;
	var subFieldSelect=$("<input>").attr("id",id).attr("disabled","disabled").attr("size","10");
	
	var td = $("<td>").append(subFieldSelect);
	
	
	var self = this;
	select.parent().after(td);
	
	
	//parent is changed
	select.bind("change",function(e){
	    delete self.isSubfield[rowNumber];
	    var is_locus= $(this).val() == "Allele"
	    subFieldSelect.val("")
	    if (!is_locus){
		subFieldSelect.append($("<option></option>")
				.attr("value","none")
				.text("none"));
		subFieldSelect.attr("disabled","disabled");
		self.field_values[rowNumber]=$(this).val();
	    }
	    else{
		    self.isSubfield[rowNumber]=$(this).val();
		    var first =true;
		    
		    new Enterobase.LocusSuggest(id,self.database,self.name,function(item,value){
			self.field_values[rowNumber]=item;	    
		    });
		    subFieldSelect.removeAttr("disabled");
	    }						
    });	
};

Enterobase.ExperimentQueryPane.prototype.setHeader= function(){
	var th = $("<tr>").css({
		"padding-top":"3px",
		"padding-botton":"3px",
		"font-weight":"bold",
		"text-align":"left"
	}).appendTo(this.table);
	th.append($("<td></td><td>Data Type</td><td>Detail</td><td>Operator</td><td>Value</td>"));

}



Enterobase.ExperimentQueryPane.prototype.save =function(){
	//just add one field to the object
	var object =  Enterobase.QueryPane.prototype.save.call(this);
	object.isSubfield=this.isSubfield;
	return object;
}
Enterobase.ExperimentQueryPane.prototype.load=function(object){
	Enterobase.QueryPane.prototype.load.call(this,object);
	for (var n=1;n<object.rows;n++){
		var parent = object.isSubfield[n]
		if (parent){
		    var subvalue  = this.field_values[n];
			this.getElement("field",n).val(parent).trigger("change");
			this.getElement("subfield",n).val(subvalue)
		}
		else{
			this.getElement("field",n).val(object.field_values[n]).trigger("change");	
		}
	
	}
	this.isSubfield=object.isSubfield;
}





/**
* Changes the field to sub field if necessary
* @overide 
*/

Enterobase.ExperimentQueryPane.prototype.processCondition= function(field,op,val,type,rowNumber){
	var subField= $("#"+this.name+"_subfield_"+rowNumber).val();
	if (subField && subField !== "none"){
		field=subField;
	}
	field = '"'+field+'"';
	//call base method with updated value
	return Enterobase.QueryPane.prototype.processCondition.call(this,field,op,val,type,rowNumber);

}



Enterobase.ModalWaitingDialog.prototype = Object.create(null);
Enterobase.ModalWaitingDialog.prototype.constructor = Enterobase.ModalWaitingDialog;

/**
* Updates the progress bar
* @param {integer} percentComplete - the current value of the progress bar
*/
Enterobase.ModalWaitingDialog.prototype.setStatus=function(percentComplete){
    this.progressBar.progressbar("value",percentComplete)
};
/**
* Updates the message in the dialog
* @param {String} msg - The new message to be dispalyed (can contain html)
*/

Enterobase.ModalWaitingDialog.prototype.setMessage=function(msg){
    this.messageDiv.html(msg)
};

/**
* Closes the dialog
*/
Enterobase.ModalWaitingDialog.prototype.close=function(){
    if (this.div.hasClass("ui-dialog-content")){
	this.div.dialog("close");
    }
};


Enterobase.LocusSuggest.prototype = Object.create(null);
Enterobase.LocusSuggest.prototype.constructor = Enterobase.LocusSuggest;

Enterobase.LocusSuggest.prototype.setScheme = function(scheme){
    this.scheme=scheme;
}

Enterobase.UserNameSuggest.prototype = Object.create(null);
Enterobase.UserNameSuggest.prototype.constructor = Enterobase.UserNameSuggest;



