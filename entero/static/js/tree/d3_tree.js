D3Tree.prototype = Object.create(null);
D3Tree.prototype.constructor= D3Tree;

//****static functions******
var cols = {"Oryza_sativa":"red"};

var D3Tree_selected_tips ={};



function D3Tree_moveToFront() {
    this.parentNode.appendChild(this);
}
function D3Tree_selectSubnodes(d){
 
  d3.select(d.linkNode).classed('link--active',true);//D3Tree_color.domain().indexOf(d.name) >= 0 ? D3Tree_color(d.name) : d.parent ? d.parent.color : null;
  if (d.children){
    d.children.forEach(D3Tree_selectSubnodes);
  }
  else{
    D3Tree_selected_tips[d.name]=true;
  }

}

function D3Tree_deselectSubnodes(){
    d3.selectAll(".link--active").classed("link--active",false);
    D3Tree_selected_tips={};
    
}






function D3Tree(nwkTree,elementID){
    var self = this;
    this.root = this.parseNewick(nwkTree)
    this.outerRadius = 600;
    this.innerRadius =430;
    this.color = d3.scale.category10()
    .domain(["Bacteria", "Eukaryota", "Archaea"]);
    this.zoom = d3.behavior.zoom()
    this.cluster = null;
    this.links = null;
    this.nodes = null;
    this.linkExtensions = null;
    this.link = null;
    this.svg = d3.select("#"+elementID).append("svg")
    .attr("width", 1200)
    .attr("height", 1200);
   
       
    this._background_rect = this.svg.append('rect')
    .attr("pointer-events","all")
    .style('fill', 'none')
    .style('stroke', 'lightgray')
    .style('stroke-width', '2px')
    .attr("width", 1200)
    .attr("height", 1200)
    .call(this.zoom.on('zoom', function(){
        self.chart.attr('transform', "translate(" + d3.event.translate + ") scale(" + d3.event.scale + ")");     
      }));
    this.chart = this.svg.append("g");
    this.chart.attr("transform", "translate(" + 600 + "," + 600 + ")");
    this.zoom.translate([600,600]);
    

}



D3Tree.prototype.branchType = function(checked) {
    var self = this; 
    d3.transition().duration(750).each(function() {
      self.linkExtension.transition().attr("d", function(d) { return self.step(d.target.x, checked ? d.target.radius : d.target.y, d.target.x, self.innerRadius); });
      self.link.transition().attr("d", function(d) { return self.step(d.source.x, checked ? d.source.radius : d.source.y, d.target.x, checked ? d.target.radius : d.target.y) });
    });
  }

D3Tree.prototype.selectSubnodes= function(d){
  var self = this;
  d.linkNode.style= 'stroke:red';//D3Tree_color.domain().indexOf(d.name) >= 0 ? D3Tree_color(d.name) : d.parent ? d.parent.color : null;
  if (d.children) d.children.forEach(self.selectSubnodes);

}

D3Tree.prototype.tagLinks=function(link){
  link.tagged=true;
  if (link.target.children){
    for (var i in d.children){
        this.tagNodes(d.children[i]);
    }
  }

}

D3Tree.prototype.switchBranches= function(link){
  node = link.target;
  var temp = node.branchset[0];
  node.branchset[0]=node.branchset[1];
  node.children[0]=node.children[1];
  node.branchset[1]=temp;
  node.children[1]=temp;
  this.tagLinks(link);
  this.updateTree();

}

D3Tree.prototype.updateTree = function(){
  var self = this;
  this.chart.selectAll("path").filter(function(d) { 
            return d.target.tagged
            })
      .style("stroke", function(d) { return "purple" });
      //.attr("d", function(d) { return step(d.source.x, d.source.y, d.target.x, d.target.y) })
    
  }

D3Tree.prototype.draw = function(){
  this.cluster= d3.layout.cluster()
  .size([360, this.innerRadius])
  .children(function(d) { return d.branchset; })
  .value(function(d) { return 1; })
  .sort(function(a, b) { return (a.value - b.value) || d3.ascending(a.length, b.length); })
  .separation(function(a, b) { return 1; });
  
  this.nodes = this.cluster.nodes(this.root);
  this.links = this.cluster.links(this.nodes);
      
      
  var self = this;
  this.setRadius(this.root, this.root.length = 0, this.innerRadius / this.maxLength(this.root));
  this.setColor(this.root);

  this.linkExtension = this.chart.append("g")
      .attr("class", "link-extensions")
    .selectAll("path")
      .data(this.links.filter(function(d) { return !d.target.children; }))
    .enter().append("path")
      .each(function(d) { d.target.linkExtensionNode = this; })
      .attr("d", function(d) { return self.step(d.target.x, d.target.y, d.target.x, self.innerRadius); })
      .on("click", function(d) { 
      alert(d.name); 
      });

  this.link = this.chart.append("g")
      .attr("class", "links")
    .selectAll("path")
      .data(this.links)
    .enter().append("path")
      .each(function(d) { d.target.linkNode = this; })
      .attr("d", function(d) { return self.step(d.source.x, d.source.y, d.target.x, d.target.y) })
      .style("stroke", function(d) { return d.target.color; })
      .style("stroke-width","2px")
      .on("click", function(d) { 
            self.switchBranches(d);
           
      });

  this.chart.append("g")
      .attr("class", "labels")
    .selectAll("text")
      .data(this.nodes.filter(function(d) { return !d.children; }))
    .enter().append("text")
      .attr("dy", ".31em")
      .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + (self.innerRadius + 8) + ",0)" + (d.x < 180 ? "" : "rotate(180)"); })
      .style("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
      .text(function(d) { return d.name.replace(/_/g, " "); })
      .on("mouseover", function(d,i){
          self.labelMouseover(this,d,true);
      
      })
      .on("mouseout", function(d,i){
          self.labelMouseover(this,d,false);
      
      });
      
  this.chart.append("g").
  selectAll('circle')
    .data(this.nodes.filter(function(d) { return !d.children; }))
    .enter().append("circle")
    .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + (self.innerRadius + 4) + ",0)" + (d.x < 180 ? "" : "rotate(180)"); })     
    .attr("r",4)
    .style("fill", function(d) {
       col = "blue";
      if (cols[d.name]){
        col = cols[d.name];
      }
    return col;});
    
   
    
      
}

D3Tree.prototype.labelMouseover=function(selection,d,active) {
      d3.select(selection).classed("label--active", active);
      d3.select(d.linkExtensionNode).classed("link-extension--active", active).each(function(d){
          this.parentNode.appendChild(this);
      });
      do{
        d3.select(d.linkNode).classed("link--active", active).each(function(d){
          this.parentNode.appendChild(this);
        });
      }while (d = d.parent);
};


D3Tree.prototype.step = function(startAngle, startRadius, endAngle, endRadius){
  var c0 = Math.cos(startAngle = (startAngle - 90) / 180 * Math.PI),
      s0 = Math.sin(startAngle),
      c1 = Math.cos(endAngle = (endAngle - 90) / 180 * Math.PI),
      s1 = Math.sin(endAngle);
  return "M" + startRadius * c0 + "," + startRadius * s0
      + (endAngle === startAngle ? "" : "A" + startRadius + "," + startRadius + " 0 0 " + (endAngle > startAngle ? 1 : 0) + " " + startRadius * c1 + "," + startRadius * s1)
      + "L" + endRadius * c1 + "," + endRadius * s1;
}

D3Tree.prototype.setColor= function(d){
  d.color = this.color.domain().indexOf(d.name) >= 0 ? this.color(d.name) : d.parent ? d.parent.color : null;
  if (d.children) {
    for (var i in d.children){
        this.setColor(d.children[i])
    }
  }

}


D3Tree.prototype.labelMouseovered = function( active) {
    return function(d) {
      d3.select(this).classed("label--active", active);
      d3.select(d.linkExtensionNode).classed("link-extension--active", active).each(D3Tree_moveToFront);
      do d3.select(d.linkNode).classed("link--active", active).each(D3Tree_moveToFront); while (d = d.parent);
    };
}

D3Tree.prototype.parseNewick =  function (a){
    for(var e=[],r={},s=a.split(/\s*(;|\(|\)|,|:)\s*/),t=0;t<s.length;t++){
        var n=s[t];
        switch(n){
            case"(":var c={};r.branchset=[c],e.push(r),r=c;break;
            case",":var c={};e[e.length-1].branchset.push(c),r=c;break;
            case")":r=e.pop();break;case":":break;
            default:var h=s[t-1];")"==h||"("==h||","==h?r.name=n:":"==h&&(r.length=parseFloat(n))
        }
    }
    return r;
}


 


// Compute the maximum cumulative length of any node in the tree.
D3Tree.prototype.maxLength = function(d) {
  return d.length + (d.children ? d3.max(d.children, maxLength) : 0);
}

// Set the radius of each node by recursively summing and scaling the distance from the root.
D3Tree.prototype.setRadius=function(d, y0, k) {
  d.radius = (y0 += d.length) * k;
  if (d.children) d.children.forEach(function(d) { setRadius(d, y0, k); });
}



