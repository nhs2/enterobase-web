Name	Read Files	Source Niche	Source Type	Source Details	Collection Year	Collection Month	Collection Day	Collection Time	Continent	Country	Region	District	City	Post Code	Latitude	Longitude	Lab Contact	Species	Serological Group	Serotype	EcoR Cluster	Pathogen/Non pathogen	Simple Patho	Antibiotic Resistance	Comment	Simple Disease	Disease
Bug1	"bug1_R1.fq.gz,bug1_R2.fq.gz"								Europe	United Kingdom							Dr Smith										
Bug2	"bug2_R1.fq.gz,bug2_R2.fq.gz"								Europe	United Kingdom							Dr Smith										
Bug3	"bug3_R1.fq.gz,bug3_R2.fq.gz"								Europe	United Kingdom							Dr Smith										
